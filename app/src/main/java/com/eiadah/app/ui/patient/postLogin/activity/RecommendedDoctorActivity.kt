package com.eiadah.app.ui.patient.postLogin.activity

import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityRecommendedDoctorBinding
import com.eiadah.app.models.dataModel.DocListData
import com.eiadah.app.models.responseModel.DocListResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.adapter.DoctorAdapter
import com.eiadah.app.ui.patient.postLogin.view_modals.DocListViewModel
import com.eiadah.app.ui.patient.postLogin.view_modals.RecomendDocListViewModel
import kotlinx.android.synthetic.main.fragment_doctor_list.*

class RecommendedDoctorActivity : BaseActivity(), OnItemClickListener, Observer<RestObservable> {
    lateinit var docList:ArrayList<DocListData>

    var specialty:String=""
    lateinit var binding : ActivityRecommendedDoctorBinding
    private val viewModel: RecomendDocListViewModel
            by lazy {  ViewModelProviders.of(this).get(RecomendDocListViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding  = DataBindingUtil.setContentView(this, R.layout.activity_recommended_doctor)
        binding.clickHandler = this

        specialty=intent.getStringExtra("specialty")
        viewModel.RecommenddoctorList(specialty)
        viewModel.getRecodocList().observe(this,this)
    }

    override fun onItemClick(position: Int) {
        val intent= Intent(this,DoctorDetailActivity::class.java)
        intent.putExtra("id",docList.get(position).userId.toString())
        startActivity(intent)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun onFilter() {
        activitySwitcher(DoctorSearchFilterActivity::class.java, false)
    }


    @Suppress("UNCHECKED_CAST")
    override fun onChanged(it: RestObservable?) {

        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is DocListResponse) {

                    val docListResponse : DocListResponse = it.data
                    docList = (docListResponse.getUser()?.getDoctor() as ArrayList<DocListData>?)!!
                    val cityData = DocListData()
//                            cityData.id = "0"
//                            cityData.name = "Choose city"
                    docList.add(cityData)

                    val doctorAdapter = DoctorAdapter(this, this,docList,docList)
                    binding.doctorAdapter = doctorAdapter
                }
            }
            it.status == Status.ERROR -> progressDialog.dismiss()
            it.status == Status.LOADING -> progressDialog.show()
        }

    }
 }
