package com.eiadah.app.ui.doctor.postLogin.activity

import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.RadioGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityPatientProfileBinding
import com.eiadah.app.models.dataModel.CityData
import com.eiadah.app.models.dataModel.CountryData
import com.eiadah.app.models.dataModel.SpecialistData
import com.eiadah.app.models.dataModel.StateData
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.models.responseModel.*
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.viewModals.AddPracticeViewModel
import com.eiadah.app.ui.doctor.preLogin.activity.RegisterClinicActivity
import com.eiadah.app.ui.doctor.preLogin.viewModels.EditDoctorProfileViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.pieboat.shredpreference.SharePreferHelper

class PatientProfileActivity : BaseActivity(), Observer<RestObservable> {


    lateinit var binding:ActivityPatientProfileBinding
    private var cityList : ArrayList<CityData> = ArrayList()
    private var specialistList : ArrayList<SpecialistData> = ArrayList()
    lateinit var sharePreferHelper: SharePreferHelper
    private var strCity = ""
    private var strSpecialty = ""
    lateinit var request: SaveDoctorProfileRequest
    private var strCountry = ""
    private var strState = ""
    private var strName = ""
    private var strMobile = ""
    private var strEmail = ""


    private var strGender = GlobalVariables.Gender.YES
    private val viewModel: AddPracticeViewModel
            by lazy {  ViewModelProviders.of(this).get(AddPracticeViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_patient_profile)

        binding.clickHandler=this

        request = SaveDoctorProfileRequest()
        sharePreferHelper= SharePreferHelper(this)
        request.own_practice = strGender

//        loadCityData(sharePreferHelper.getString(GlobalVariables.SharePref.State_ID)!!)
        loadSpecialist()
    }


    fun onCheckedChanged(id : Int) {
        when(id) {
            R.id.radioButtonMale -> {
                strGender = GlobalVariables.Gender.YES

            }

            R.id.radioButtonFemale -> {
                strGender = GlobalVariables.Gender.NO

            }
        }
        request.gender = strGender
    }

    fun doneOncllick() {
        strName = binding.edtPtName.text.toString()
        strMobile = binding.edtPtMobile.text.toString()
        strEmail = binding.edtPtEmail.text.toString()



        if (isValid()) {
            activitySwitcher(PatientListActivity::class.java, false)
            request.name = strName
            request.mobile=strMobile
            request.email = strEmail
            viewModel.saveDoctorPracticeProfile(request)
            viewModel.getDoctorPracticeProfileData().observe(this, this)
        }
    }


    private fun loadCityData(stateId : String) {
        viewModel.loadCityList(stateId)
        viewModel.getCityList().observe(this, this)
    }

    private fun loadSpecialist() {
        viewModel.loadSpecialist()
        viewModel.getSpecialist().observe(this, this)
    }


    private fun setCityAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, cityList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.cityAdapter = adapter
    }

    private fun setSpecialistAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, specialistList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.specialityAdapter = adapter
    }


    fun onItemSelectedCity(adapterView: AdapterView<*>, view : View, i : Int, l : Long) {
        strCity = if (i!=0) {
            cityList[i].name!!
        } else {
            ""
        }

        request.city = strCity
    }

    fun onItemSelectedSpeciality(i : Int) {
        strSpecialty = if (i!=0) {
            specialistList[i].name!!
        } else {
            ""
        }

        request.specilaity = strSpecialty
    }


    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is CitiesResponse) {
                    val citiesResponse : CitiesResponse = it.data
                    if (citiesResponse.status!!) {
                        cityList = (citiesResponse.cityList as ArrayList<CityData>?)!!
                        val cityData = CityData()
                        cityData.id = "0"
                        cityData.name = "Choose city"
                        cityList.add(0, cityData)
                        setCityAdapter()
                    }

                } else if (it.data is GetSpecialistResponse) {
                    val getSpecialistResponse : GetSpecialistResponse = it.data
                    if (getSpecialistResponse.status!!) {
                        specialistList = getSpecialistResponse.data as ArrayList<SpecialistData>

                        val specialistData = SpecialistData()
                        specialistData.id = 0
                        specialistData.name = "Choose Speciality"
                        specialistData.date = ""
                        specialistList.add(0, specialistData)
                        setSpecialistAdapter()
                    }

                }    else if (it.data is AddPracticeResponse) {
                    val add_practice : AddPracticeResponse = it.data

                    if (add_practice.status!!) {
                        sharePreferHelper.saveString(GlobalVariables.SharePref.OWN_PRACTICE, add_practice.getUser()?.getOwnPractice()!!)
//                        SharedStorage.setDoctorData(this, add_practice.user!!)
                        activitySwitcher(PatientListActivity::class.java, false)
                        finish()
                    }
                }
            }
            it.status == Status.ERROR -> {
                it.error!!.fillInStackTrace()
                progressDialog.dismiss()
            }
            it.status == Status.LOADING -> progressDialog.show()
        }
    }

    private fun isValid() : Boolean {
        var check = false

        if (strName.isEmpty() &&  strSpecialty.isEmpty() && strGender.isEmpty() ) {
            showToast(R.string.please_fill_all_fields)
        } else if (strName.isEmpty()) {
            showToast(R.string.empty_name)
        } else if (strSpecialty.isEmpty()) {
            showToast(R.string.please_choose_specialty)
        } else if (strGender.isEmpty()) {
            showToast(R.string.please_choose_gender)
        } else {
            check = true
        }

        return check
    }


}
