package com.eiadah.app.ui.patient.postLogin.activity

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.eiadah.app.Listener.CheckupChangeFragmentListener
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.ui.patient.postLogin.fragments.CheckupStep1Fragment
import com.eiadah.app.ui.patient.postLogin.fragments.CheckupStep7Fragment


class StartCheckupActivity : BaseActivity() , CheckupChangeFragmentListener {

    lateinit var iv_back:ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start_checkup)
        iv_back=findViewById(R.id.iv_back)
        switchContent(CheckupStep1Fragment.newInstance(), CheckupStep1Fragment.TAG)

      iv_back.setOnClickListener(View.OnClickListener {

          finish()

        })
    }

    override fun onBackPressed() {
        val existingFragment = supportFragmentManager.findFragmentById(R.id.startCheckupContainer)
        if (supportFragmentManager.backStackEntryCount==1) {
            finish()
        } else {
            if (!(existingFragment!!.tag.equals(CheckupStep7Fragment.TAG))) {
                super.onBackPressed()
            } else {
                finish()
            }
        }
    }

    private fun switchContent(fragment: Fragment?, tag: String?) {
        val fragmentManager = supportFragmentManager
        //fragmentManager.popBackStack()
        if (fragment != null) {
            val transaction = fragmentManager
                .beginTransaction()
            transaction.add(R.id.startCheckupContainer, fragment, tag)
            //if (fragment !is CheckupStep1Fragment)
            transaction.addToBackStack(tag)

            transaction.setCustomAnimations(R.anim.enter, R.anim.exit)
            transaction.commit()
        }
    }

    override fun changeFragment(fragment: Fragment?, TAG: String?) {
        switchContent(fragment, TAG)
    }


}
