package com.eiadah.app.ui.doctor.postLogin.activity

import android.content.Intent
import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.appcompat.app.AlertDialog
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityAccountBinding
import com.eiadah.app.ui.patient.preLogin.activity.SplashActivity
import com.eiadah.app.utils.SharedStorage

class AccountActivity : BaseActivity() {

    lateinit var binding:ActivityAccountBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_account)

        binding.clickHandler=this
    }

    fun changePasswordtOncllick() {
        activitySwitcher(ChangePasswordActivity::class.java, false)
    }

    override fun onResume() {
        super.onResume()
        binding.data = SharedStorage.getDoctorData(this)

    }


    fun logout(){

            if (!isFinishing) {
                val builder = AlertDialog.Builder(this)
                builder.setIcon(R.mipmap.ic_launcher)
                builder.setTitle(getString(R.string.app_name))
                builder.setMessage(getString(R.string.exit_confirm))

                builder.setPositiveButton(R.string.yes) { _, _ ->
                    SharedStorage.clear(this)

                    val intent = Intent (this,SplashActivity::class.java)
                    startActivity(intent)
//                    activitySwitcher(SplashActivity::class.java, true)
                    finish()
                }

                builder.setNegativeButton(R.string.no) { dialog, _ ->
                    //Reset to previous seletion menu in navigation
                    dialog.dismiss()
//                    drawerLayout!!.openDrawer(GravityCompat.START)
                }
                builder.setCancelable(false)
                val dialog = builder.create()
                //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dialog.setOnShowListener {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                        .setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
                    dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                        .setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
                }
                dialog.show()
            }
        }

    fun backOncllick() {
        finish()
    }




}
