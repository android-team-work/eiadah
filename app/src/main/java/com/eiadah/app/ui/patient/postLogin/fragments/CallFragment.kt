package com.eiadah.app.ui.patient.postLogin.fragments


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager

import com.eiadah.app.R
import com.eiadah.app.core.BaseFragment
import com.eiadah.app.databinding.FragmentCallBinding
import com.eiadah.app.models.dataModel.ChatListData
import com.eiadah.app.models.dataModel.ReportListData
import com.eiadah.app.models.responseModel.ChatListResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.ReportListViewModel
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.ChatListViewModel
import com.myapplication.adapter.ChatListAdapter

/**
 * A simple [Fragment] subclass.
 */
class CallFragment : BaseFragment(), Observer<RestObservable> {

    lateinit var binding:FragmentCallBinding
    companion object {
        fun newInstance() = CallFragment()
        val TAG : String = CallFragment::class.java.simpleName
    }
    lateinit var mListener : HomeFragment.HomeFragmentListener
    lateinit var chatListData:ArrayList<ChatListData>
    lateinit var adapter: ChatListAdapter
    private val viewModel: ChatListViewModel
            by lazy {  ViewModelProviders.of(this).get(ChatListViewModel::class.java) }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
       binding=DataBindingUtil.inflate(inflater,R.layout.fragment_call, container, false)
        binding.clickhandler=this

        viewModel.getChatList().observe(this,this)
        viewModel.loadChatList()
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is HomeFragment.HomeFragmentListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement HomeFragmentListener")
        }
    }

    fun onMenuClick() {
        mListener.handleDrawer()
    }

    override fun onChanged(it: RestObservable?) {

        when {
            it!!.status == Status.SUCCESS -> {
               progressDialog?.dismiss()
                if (it.data is ChatListResponse) {

                    val reportListResponse : ChatListResponse = it.data
                    chatListData = (reportListResponse.getData() as ArrayList<ChatListData>?)!!
                    val reportData = ChatListData()
                    chatListData.add(reportData)

                    if(chatListData.size==0){
                        binding.textRec.visibility=View.VISIBLE
                        binding.chatRecyc.visibility=View.GONE

                    }
                    else{
                        binding.textRec.visibility=View.GONE
                        binding.chatRecyc.visibility=View.VISIBLE
                        adapter= ChatListAdapter(activity!!,chatListData)
                        binding.chatRecyc.setLayoutManager(LinearLayoutManager(activity))
                        binding.chatRecyc.setAdapter(adapter)
                    }

                }
            }
            it.status == Status.ERROR -> progressDialog?.dismiss()
            it.status == Status.LOADING -> progressDialog?.show()
        }
    }
}
