package com.myapplication.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.models.dataModel.AppontmentListData
import com.eiadah.app.models.dataModel.BankListData
import com.eiadah.app.ui.doctor.postLogin.activity.AppointmentDetailActivity
import com.eiadah.app.ui.doctor.postLogin.activity.PatientDetailActivity
import com.eiadah.app.utils.GlobalVariables
import java.util.ArrayList

class BankAdapter(private val mContext: Context, val onItemClickListener: OnItemClickListener, var bankListData: ArrayList<BankListData>) : RecyclerView.Adapter<BankAdapter.AppointmentViewHolder>() {
    override fun onBindViewHolder(p0: AppointmentViewHolder, p1: Int) {

        p0.tv_name.setText(bankListData.get(p1).name)
        p0.tv_acc_name.setText(bankListData.get(p1).accountNumber)
        p0.tv_ifsc.setText(bankListData.get(p1).ifscCode)
        p0.tv_bankname.setText(bankListData.get(p1).bankName)

        p0.iv_delete.setOnClickListener(View.OnClickListener {
           onItemClickListener.onItemClick(p1)

        })



        if (bankListData.get(p1).name.isNullOrEmpty()){
            p0.linear_lay.visibility=View.GONE
        }
        else{
            p0.linear_lay.visibility=View.VISIBLE

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppointmentViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.bank_account_items, parent, false)
        return AppointmentViewHolder(v)
    }

    override fun getItemCount(): Int {
        return bankListData.size
    }

    inner class AppointmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var tv_name:TextView
        var tv_acc_name:TextView
        var tv_ifsc:TextView
        var tv_bankname:TextView
        var linear_lay:RelativeLayout
        var iv_delete:ImageView

        init {

            tv_name=itemView.findViewById(R.id.tv_name)
            tv_acc_name=itemView.findViewById(R.id.tv_acc_name)
            tv_ifsc=itemView.findViewById(R.id.tv_ifsc)
            tv_bankname=itemView.findViewById(R.id.tv_bankname)
            linear_lay=itemView.findViewById(R.id.relativee_lay)
            iv_delete=itemView.findViewById(R.id.iv_delete)

        }
        fun onItemClick() {
            onItemClickListener.onItemClick(adapterPosition)
        }
    }
}