package com.eiadah.app.ui.patient.postLogin.view_modals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.SaveClinicRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RecomendDocListViewModel : ObservableViewModel() {

    var recodoctorListResponse : MutableLiveData<RestObservable> = MutableLiveData()

    fun getRecodocList() : LiveData<RestObservable> {
        return recodoctorListResponse
    }

    fun RecommenddoctorList(speciality:String) {
        getService().getRecomendDocList(getHeader(),speciality)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { recodoctorListResponse.value = RestObservable.loading() }
            .subscribe(
                {recodoctorListResponse.value = RestObservable.success(it)},
                {recodoctorListResponse.value = RestObservable.error (it)}
            )
    }
}