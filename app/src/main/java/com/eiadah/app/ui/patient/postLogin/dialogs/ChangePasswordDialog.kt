package com.eiadah.app.ui.patient.postLogin.dialogs

import android.app.Activity
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.Window
import com.eiadah.app.R
import com.eiadah.app.core.BaseDialog
import com.eiadah.app.databinding.DialogChangePasswordBinding
import com.eiadah.app.models.responseModel.BasicResponse
import com.eiadah.app.restAPI.RestCallback
import com.eiadah.app.restAPI.RestProcess
import com.eiadah.app.utils.GlobalVariables
import retrofit2.Call
import retrofit2.Response

class ChangePasswordDialog (activity: Activity) : BaseDialog(activity), RestCallback {


    lateinit var strOld : String
    lateinit var strNew : String
    lateinit var strConfirm : String
    lateinit var binding: DialogChangePasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_change_password, null, false)

        setContentView(binding.root)

        var window : Window = window
        window.setGravity(Gravity.CENTER)
        //window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window.setBackgroundDrawableResource(android.R.color.transparent)
        window.attributes.windowAnimations = R.style.DialogAnimation

        binding.clickHandler = this

    }

    fun onSubmit() {
        strOld = binding.edtOldPassword.text.toString()
        strNew = binding.edtNewPassword.text.toString()
        strConfirm = binding.edtConfirmPassword.text.toString()

        if (isValid()) {
            changePasswordApi()
        }
    }

    fun onCancel() {

    }

    private fun isValid() : Boolean {
        var check = false
        if (strOld.isEmpty() && strNew.isEmpty() && strConfirm.isEmpty()) {
            showToast(R.string.please_fill_all_fields)
        } else if (strOld.isEmpty()) {
            showToast(R.string.empty_old_password)
        } else if (strNew.isEmpty()) {
            showToast(R.string.empty_new_password)
        } else if (strNew.length<8) {
            showToast(R.string.invalid_password_length)
        } else if (strConfirm.isEmpty()) {
            showToast(R.string.empty_confirm_password)
        } else if (strNew != strConfirm) {
            showToast(R.string.password_not_matched)
        } else {
            check = true
        }

        return check
    }

    private fun changePasswordApi() {
        val changePasswordApi : Call<BasicResponse> = getService().changePassword(getHeader(), strOld, strNew, strConfirm)
        changePasswordApi.enqueue(RestProcess(context, this, GlobalVariables.SERVICE_MODE.CHANGE_PASSWORD, true))
    }

    override fun onResponse(call: Call<*>, response: Response<*>, serviceMode: Int) {
        showToast(R.string.password_change_successfully)
        dismiss()
    }

    override fun onFailure(call: Call<*>, t: Throwable, serviceMode: Int) {

    }
}