package com.eiadah.app.ui.patient.postLogin.view_modals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MyAppointmentViewModel : ObservableViewModel() {

    private var myAppointmentListResponse : MutableLiveData<RestObservable> = MutableLiveData()

     fun getMyAppointmentList() : LiveData<RestObservable> {
        return myAppointmentListResponse
    }

    fun loadMyAppointmentList() {
        getService().getMyAppontmentList(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { myAppointmentListResponse.value = RestObservable.loading() }
            .subscribe(
                {myAppointmentListResponse.value = RestObservable.success(it)},
                {myAppointmentListResponse.value = RestObservable.error (it)}
            )
    }



}