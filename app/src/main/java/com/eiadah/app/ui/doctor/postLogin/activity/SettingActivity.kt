package com.eiadah.app.ui.doctor.postLogin.activity

import android.content.Intent
import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivitySettingBinding
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.net.Uri



class SettingActivity : BaseActivity() {

    lateinit var binding:ActivitySettingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_setting)
        binding.clickHandler=this
    }

    fun accountOncllick() {
        activitySwitcher(AccountActivity::class.java, false)
    }
    fun notificationOncllick() {
        activitySwitcher(NotificationActivity::class.java, false)
    }
    fun eiadahOncllick() {
        activitySwitcher(WalletActivity::class.java, false)
    }
    fun backOncllick() {
            finish()
    }
    fun share() {
        val share = Intent(Intent.ACTION_SEND)

        // If you want to share a png image only, you can do:
        // setType("image/png"); OR for jpeg: setType("image/jpeg");
        share.type = "text/*"
//        share.putExtra(Intent.EXTRA_STREAM, uri)
        share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.eiadah.app&hl=en")
        startActivity(Intent.createChooser(share, "Share Image!"))
    }

    fun app_rate(){
        val appPackageName = packageName // getPackageName() from Context or Activity  object
        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$appPackageName")
                )
            )
        } catch (anfe: android.content.ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                )
            )
        }

    }
}
