package com.eiadah.app.ui.doctor.preLogin.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DoctorLoginViewModel : ObservableViewModel() {

    var loginResponse : MutableLiveData<RestObservable> = MutableLiveData()
    var profileResponse : MutableLiveData<RestObservable> = MutableLiveData()
    var saveTokenResponse : MutableLiveData<RestObservable> = MutableLiveData()

    fun getLoginResponse() : LiveData<RestObservable> {
        return loginResponse
    }



    fun getProfileResponse() : LiveData<RestObservable> {
        return profileResponse
    }
    fun getSaveToken() : LiveData<RestObservable> {
        return saveTokenResponse
    }

    fun loginApi(email : String, password : String) {
        getService().doctorLogin(email, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { loginResponse.value = RestObservable.loading() }
            .subscribe(
                {loginResponse.value = RestObservable.success(it)},
                {loginResponse.value = RestObservable.error (it)}
            )
    }

    fun profileApi() {
        getService().getDoctorProfile(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { profileResponse.value = RestObservable.loading() }
            .subscribe(
                {profileResponse.value = RestObservable.success(it)},
                {profileResponse.value = RestObservable.error (it)}
            )
    }
    fun saveToekn(registration_id : String, type : String) {
        getService().SaveToken(getHeader(),registration_id, type)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { saveTokenResponse.value = RestObservable.loading() }
            .subscribe(
                {saveTokenResponse.value = RestObservable.success(it)},
                {saveTokenResponse.value = RestObservable.error (it)}
            )
    }

}