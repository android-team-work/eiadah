package com.eiadah.app.ui.patient.postLogin.fragments

import android.app.Activity
import android.content.Intent
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.eiadah.app.Listener.UpdateProfileData

import com.eiadah.app.R
import com.eiadah.app.core.BaseFragment
import com.eiadah.app.databinding.FragmentMedicalProfileBinding
import com.eiadah.app.models.dataModel.AddAllergyData
import com.eiadah.app.models.responseModel.AddAllergy_Response
import com.eiadah.app.models.responseModel.ProfileResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.preLogin.activity.AddEductionActivity
import com.eiadah.app.ui.patient.postLogin.activity.AddIncidentActivity
import com.eiadah.app.ui.patient.postLogin.activity.AddMedicActivity
import com.eiadah.app.ui.patient.postLogin.activity.AddSurgeryActivity
import com.eiadah.app.ui.patient.postLogin.activity.Add_AllergyActivity
import com.eiadah.app.ui.patient.postLogin.dialogs.*
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.ProfileViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.pieboat.shredpreference.SharePreferHelper

class MedicalProfileFragment : BaseFragment(), UpdateProfileData{


    override fun updateProfileData(params: HashMap<String, String>) {
        viewModel.saveUserProfileData(params)
        onResponse()
    }

    override fun updateProfileData(data: ProfileViewModel) {
        binding.data = data
    }

    companion object {
        fun newInstance() = MedicalProfileFragment()
        val TAG : String = MedicalProfileFragment::class.java.simpleName
    }


    private val ALLERGY = 1
    private val INCIDENT = 2
    private val SURGERY = 3
    private val MEDICINE = 4
    lateinit var sharePreferHelper: SharePreferHelper

     var strAllergy:String=""
    private val viewModel: ProfileViewModel
            by lazy {  ViewModelProviders.of(this).get(ProfileViewModel::class.java) }

    private lateinit var binding : FragmentMedicalProfileBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_medical_profile, container, false)
        binding.clickHandler = this

        viewModel.setUserData(SharedStorage.getUserData(context!!))

        binding.data = viewModel
        binding.lifecycleOwner = this


        sharePreferHelper=SharePreferHelper(activity!!)



        return binding.root
    }

    fun onBloodGroupClick() {
        val bloodGroupDialog : AddBloodGroupDialog? = activity?.let { AddBloodGroupDialog(it, this) }
        bloodGroupDialog?.show()
    }

    fun onHeightClick() {
        val heightDialog : AddHeightDialog? = activity?.let { AddHeightDialog(it, this) }
        heightDialog?.show()
    }

    fun onWeightClick() {
        val weightDialog : AddWeightDialog? = activity?.let { AddWeightDialog(it, this) }
        weightDialog?.show()
    }
    fun onIncidentClick() {
        activitySwitcher(AddIncidentActivity::class.java, INCIDENT)

    }
    fun onSurgryClick() {
        activitySwitcher(AddSurgeryActivity::class.java, SURGERY)

    }
    fun onMedicClick() {
        activitySwitcher(AddMedicActivity::class.java, MEDICINE)

    }
    fun onAllergyClick() {
        activitySwitcher(Add_AllergyActivity::class.java, ALLERGY)
//        alergyDialog  = activity.let { AddAlergyDialog(it!!, this) }
//        alergyDialog?.show()
    }

    private fun onResponse() {
        viewModel.getProfileResponse().observe(this, Observer {
            Log.e(PersonalProfileFragment.TAG, "eerg")
            when {
                it!!.status == Status.SUCCESS -> {
                    progressDialog.dismiss()
                    if (it.data is ProfileResponse) {
                        val profileResponse: ProfileResponse = it.data as ProfileResponse
                        if (profileResponse.status!!) {
                            SharedStorage.setUserData(context!!, profileResponse.user!!)
                            viewModel.setUserData(profileResponse.user!!)
                            binding.data = viewModel

                            binding.tvAllergy.setText(profileResponse.user?.allergies)
                            binding.tvMdi.setText(profileResponse.user?.medications)
                            binding.tvSurgry.setText(profileResponse.user?.surgeries)
                            binding.tvIncident.setText(profileResponse.user?.inguries)

                            Log.e("allergies",profileResponse.user?.allergies)
                        }

                    }

                }
                it.status == Status.ERROR -> progressDialog.dismiss()
                it.status == Status.LOADING -> progressDialog.show()
            }
        })
    }

 override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == ALLERGY && resultCode == Activity.RESULT_OK && data != null) {
            strAllergy = data.getStringExtra(GlobalVariables.PARAMS.DoctorProfile.Allergy_NAME)

            binding.tvAllergy.text = strAllergy


//        } else if (requestCode == REGISTRATION && resultCode == Activity.RESULT_OK && data != null) {
//            strRegNumber = data.getStringExtra(GlobalVariables.PARAMS.DoctorProfile.REG_NUMBER)
//            strRegCouncil = data.getStringExtra(GlobalVariables.PARAMS.DoctorProfile.REG_COUNCIL)
//            strRegYear = data.getStringExtra(GlobalVariables.PARAMS.DoctorProfile.REG_YEAR)
//            binding.edtRegistrationDetails.text = strRegNumber
//            request.reg_number = strRegNumber
//            request.reg_council = strRegCouncil
//            request.reg_year = strRegYear
//        }
        }
    }
    override fun onResume() {
        super.onResume()

    }
}
