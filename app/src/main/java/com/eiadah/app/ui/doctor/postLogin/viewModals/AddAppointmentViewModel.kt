package com.eiadah.app.ui.doctor.postLogin.viewModals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.models.requestModels.AddPatientRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.models.responseModel.AddPatientResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AddAppointmentViewModel : ObservableViewModel() {

    private var addAppointmentResponse : MutableLiveData<RestObservable> = MutableLiveData()


    fun getAddAppointment() : LiveData<RestObservable> {
        return addAppointmentResponse
    }



    fun AddAppointment(request: AddAppointmenttRequest) {
        getService().AddAppointment(getHeader(), request.user_id,
           request.name, request.mobile, request.email,request.date,request.time,request.category,request.note)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addAppointmentResponse.value = RestObservable.loading() }
            .subscribe(
                {addAppointmentResponse.value = RestObservable.success(it)},
                {addAppointmentResponse.value = RestObservable.error(it)}
            )
    }
}