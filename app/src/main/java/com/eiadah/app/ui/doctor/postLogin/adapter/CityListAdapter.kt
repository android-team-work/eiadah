package com.myapplication.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.TextView
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.models.dataModel.CityData

class CityListAdapter(private val mContext: Context, val onItemClickListener: OnItemClickListener, var cityList : ArrayList<CityData>) : RecyclerView.Adapter<CityListAdapter.ViewHolder>() {
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        p0.text.setText(cityList.get(p1).name)

        p0.radioButton.setOnClickListener(View.OnClickListener {

            onItemClickListener.onItemClick(p1)
        })

        if (cityList.get(p1).name.isNullOrEmpty()){
            p0.linear_lay.visibility=View.GONE
        }
        else{
            p0.linear_lay.visibility=View.VISIBLE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.city_list_item_layout, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return cityList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var text:TextView
        var radioButton:RadioButton
        var linear_lay:LinearLayout

        init {
            text=itemView.findViewById(R.id.text)
            radioButton=itemView.findViewById(R.id.radioButton)
            linear_lay=itemView.findViewById(R.id.linear_lay)
        }
        fun onItemClick() {
            onItemClickListener.onItemClick(adapterPosition)
        }
    }


}