package com.eiadah.app.ui.doctor.postLogin.fragments


import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.eiadah.app.R
import com.eiadah.app.databinding.FragmentExperienceFeedbackBinding

/**
 * A simple [Fragment] subclass.
 */
class ExperienceFeedbackFragment : Fragment() {
    lateinit var binding:FragmentExperienceFeedbackBinding

    companion object {
        fun newInstance() = ExperienceFeedbackFragment()
        val TAG: String = ExperienceFeedbackFragment::class.java.simpleName
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding=DataBindingUtil.inflate(inflater,R.layout.fragment_experience_feedback, container, false)
        return  binding.root
    }


}
