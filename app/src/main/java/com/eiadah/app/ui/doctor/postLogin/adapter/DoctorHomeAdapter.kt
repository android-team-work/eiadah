package com.eiadah.app.ui.doctor.postLogin.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.eiadah.app.ui.doctor.postLogin.fragments.AppFragment
import com.eiadah.app.ui.doctor.postLogin.fragments.SummaryFragment

class DoctorHomeAdapter(context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val mContext: Context = context

    override fun getItem(position: Int): Fragment? {
        when (TABS[position]) {
            APPS -> return AppFragment.newInstance()
            SUMMARY -> return SummaryFragment.newInstance()
        }
        return null
    }

    override fun getCount(): Int {
        return TABS.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (TABS[position]) {
            APPS -> return "APPS"
            SUMMARY -> return "SUMMARY"
        }
        return null
    }

    companion object {
        private const val APPS = 0
        private const val SUMMARY = 1

        private val TABS = intArrayOf(APPS, SUMMARY)
    }
}