package com.eiadah.app.ui.patient.postLogin.fragments


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eiadah.app.Listener.CheckupChangeFragmentListener
import com.eiadah.app.Listener.OnItemClickListener

import com.eiadah.app.R
import com.eiadah.app.core.BaseFragment
import com.eiadah.app.databinding.FragmentCheckupStep4Binding
import com.eiadah.app.models.dataModel.BodyPartData
import com.eiadah.app.models.dataModel.SymptomData
import com.eiadah.app.models.requestModels.SaveCheckupDetailsRequest
import com.eiadah.app.ui.patient.postLogin.adapter.BodyPartAdapter
import com.eiadah.app.ui.patient.postLogin.dialogs.ChooseSymptomsDialog
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.CheckupStep4ViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.pieboat.shredpreference.SharePreferHelper


/**
 * A simple [Fragment] subclass.
 *
 */
class CheckupStep4Fragment : BaseFragment(), OnItemClickListener, ChooseSymptomsDialog.SymptomResult {



    lateinit var mListener : CheckupChangeFragmentListener
    lateinit var binding : FragmentCheckupStep4Binding
    lateinit var bodyPartList: List<BodyPartData>
    lateinit var saveCheckupDetailsRequest : SaveCheckupDetailsRequest
    lateinit var sharePreferHelper: SharePreferHelper
    companion object {
        fun newInstance() = CheckupStep4Fragment()
        val TAG : String = CheckupStep4Fragment::class.java.simpleName
    }

    private val viewModel: CheckupStep4ViewModel
            by lazy {  ViewModelProviders.of(this).get(CheckupStep4ViewModel::class.java) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_checkup_step4, container, false)
        val adapter = context?.let { BodyPartAdapter(it, this) }
        binding.bodyPartAdapter = adapter
        binding.clickHandler = this

        saveCheckupDetailsRequest = SharedStorage.getCheckupDetail(context!!)

        sharePreferHelper=SharePreferHelper(activity!!)
        loadData()


        return binding.root
    }

    private fun loadData() {
        progressDialog.show()
        viewModel.loadSymptomList()
        viewModel.getSymptomsList().observe(this, Observer {
            progressDialog.dismiss()
            if (it?.status!!) {
                bodyPartList = it.bodyPartList!!
                binding.bodyPartAdapter?.updateList(bodyPartList)
            }
        })

        viewModel.getError().observe(this, Observer {
            progressDialog.dismiss()
        })
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is CheckupChangeFragmentListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement CheckupChangeFragmentListener")
        }
    }

    fun onSaveClick() {
        mListener.changeFragment(CheckupStep5Fragment.newInstance(), CheckupStep5Fragment.TAG)
        sharePreferHelper.saveString(GlobalVariables.SharePref.SYMPTOMS_NAME,binding.txtSelectedSymptoms.text.toString())

    }

    override fun onItemClick(position: Int) {
        val chooseSymptomsDialog =
            context?.let { bodyPartList[position].symptoms?.let { it1 -> ChooseSymptomsDialog(it, bodyPartList[position].symptomsTypeName!!, it1, position, this) } }
        chooseSymptomsDialog?.show()
    }

    override fun onChooseSymptomResult(parentPosition: Int, list: List<SymptomData>) {
        bodyPartList[parentPosition].symptoms = list
        viewModel.getSelectedSymptoms(bodyPartList).observe(this, Observer {
            binding.selectedSymptoms = it
            saveCheckupDetailsRequest.your_symptoms = it!!
            SharedStorage.setCheckupDetail(context!!, saveCheckupDetailsRequest)
        })

        viewModel.getSelectedSymptomsIds().observe(this, Observer {
            SharedStorage.setSelectedSymptomsIds(context!!, it!!)
        })
    }
}
