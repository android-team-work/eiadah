package com.eiadah.app.ui.patient.postLogin.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import android.content.Context
import androidx.fragment.app.FragmentManager
import com.eiadah.app.R
import com.eiadah.app.ui.patient.postLogin.fragments.MedicalProfileFragment
import com.eiadah.app.ui.patient.postLogin.fragments.PersonalProfileFragment


class ProfileSectionAdapter(context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val mContext: Context = context

    override fun getItem(position: Int): Fragment? {
        when (TABS[position]) {
            PERSONAL -> return PersonalProfileFragment.newInstance()
            MEDICAL -> return MedicalProfileFragment.newInstance()
        }
        return null
    }

    override fun getCount(): Int {
        return TABS.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (TABS[position]) {
            PERSONAL -> return mContext.resources.getString(R.string.personal)
            MEDICAL -> return mContext.resources.getString(R.string.medical)
        }
        return null
    }

    companion object {
        private const val PERSONAL = 0
        private const val MEDICAL = 1

        private val TABS = intArrayOf(PERSONAL, MEDICAL)
    }
}