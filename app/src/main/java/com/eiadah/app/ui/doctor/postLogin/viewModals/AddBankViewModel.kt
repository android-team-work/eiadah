package com.eiadah.app.ui.doctor.postLogin.viewModals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.models.requestModels.AddBanktRequest
import com.eiadah.app.models.requestModels.AddPatientRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.models.responseModel.AddPatientResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AddBankViewModel : ObservableViewModel() {

    private var addBankResponse : MutableLiveData<RestObservable> = MutableLiveData()


    fun getAddBank() : LiveData<RestObservable> {
        return addBankResponse
    }



    fun AddBank(request: AddBanktRequest) {
        getService().addBank(getHeader(), request.name, request.bank_name, request.account_number,request.ifsc_code)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addBankResponse.value = RestObservable.loading() }
            .subscribe(
                {addBankResponse.value = RestObservable.success(it)},
                {addBankResponse.value = RestObservable.error(it)}
            )
    }
}