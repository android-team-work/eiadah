package com.eiadah.app.ui.doctor.preLogin.activity

import android.app.Activity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.view.View
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityAddOpeningClosingTimeBinding
import com.eiadah.app.models.dataModel.ClinicTimeData
import com.eiadah.app.ui.doctor.preLogin.viewModels.AddOpenCloseTimeingViewModel
import com.eiadah.app.utils.GlobalVariables
import com.google.gson.Gson
import java.io.Serializable
import com.google.gson.reflect.TypeToken
import com.pieboat.shredpreference.SharePreferHelper


class AddOpeningClosingTimeActivity : BaseActivity(), AddOpenCloseTimePickerDialog.TimeChanged {


    lateinit var binding : ActivityAddOpeningClosingTimeBinding
    var isWeekdays = false
    lateinit var sharePreferHelper:SharePreferHelper
    var list : ArrayList<ClinicTimeData> = getDefaultTimeList()

    val viewModel : AddOpenCloseTimeingViewModel
            by lazy { ViewModelProviders.of(this).get(AddOpenCloseTimeingViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_opening_closing_time)
        binding.clickHandler = this
        binding.data = viewModel

        sharePreferHelper= SharePreferHelper(this)

    }

    fun onSaveClick() {

        val intent = Intent()
        val type = object : TypeToken<List<ClinicTimeData>>() {}.type
        val json = Gson().toJson(list, type)
            intent.putExtra("list",json)
//            intent.putExtra("list", Gson().toJson(list))
            setResult(Activity.RESULT_OK, intent)
            finish()

        sharePreferHelper.saveString(GlobalVariables.SharePref.CLINIC_TIME, json)


//        if (isValid()) {
//            val intent = Intent()
//            intent.putExtra("list", Gson().toJson(list))
//            setResult(Activity.RESULT_OK, intent)
//            finish()
//        }
    }

    fun onAddMonTiming() {
        val timePickerDialog = AddOpenCloseTimePickerDialog(this, "monday", this)
        timePickerDialog.show()
    }

    fun onAddTueTiming() {
        val timePickerDialog = AddOpenCloseTimePickerDialog(this, "tuseday", this)
        timePickerDialog.show()
    }

    fun onAddWedTiming() {
        val timePickerDialog = AddOpenCloseTimePickerDialog(this, "wednesday", this)
        timePickerDialog.show()
    }

    fun onAddThuTiming() {
        val timePickerDialog = AddOpenCloseTimePickerDialog(this, "thursday", this)
        timePickerDialog.show()
    }

    fun onAddFriTiming() {
        val timePickerDialog = AddOpenCloseTimePickerDialog(this, "friday", this)
        timePickerDialog.show()
    }

    fun onAddSatTiming() {
        val timePickerDialog = AddOpenCloseTimePickerDialog(this, "satrday", this)
        timePickerDialog.show()
    }

    fun onAddSunTiming() {
        val timePickerDialog = AddOpenCloseTimePickerDialog(this, "sunday", this)
        timePickerDialog.show()
    }

    fun onAddMonFriTiming() {
        val timePickerDialog = AddOpenCloseTimePickerDialog(this, "monfri", this)
        timePickerDialog.show()
    }

    fun onCheckedChangedWeekdays(isChecked: Boolean) {
        isWeekdays = isChecked
        if (isChecked) {
            binding.lnrWeekdaysSame.visibility = View.VISIBLE
            binding.lnrWeekdays.visibility = View.GONE
        } else {
            binding.lnrWeekdaysSame.visibility = View.GONE
            binding.lnrWeekdays.visibility = View.VISIBLE
        }
    }

    fun onCheckedChangedMonFri(isChecked : Boolean) {
        if (!isChecked) {
            list[0].close = "1"
//            list[0].isFill = true

            list[1].close = "1"
//            list[1].isFill = true

            list[2].close = "1"
//            list[2].isFill = true

            list[3].close = "1"
//            list[3].isFill = true

            list[4].close = "1"
//            list[4].isFill = true
        } else {
            list[0].close = "0"
//            list[0].isFill = !(list[0].openHours!!.isEmpty() && list[0].closeHours!!.isEmpty())

            list[1].close = "0"
//            list[1].isFill = !(list[1].openHours!!.isEmpty() && list[1].closeHours!!.isEmpty())

            list[2].close = "0"
//            list[2].isFill = !(list[2].openHours!!.isEmpty() && list[2].closeHours!!.isEmpty())

            list[3].close = "0"
//            list[3].isFill = !(list[3].openHours!!.isEmpty() && list[3].closeHours!!.isEmpty())

            list[4].close = "0"
//            list[4].isFill = !(list[4].openHours!!.isEmpty() && list[4].closeHours!!.isEmpty())
        }
        viewModel.setMonFriTime(list[0])
        binding.data = viewModel
    }

    fun onCheckedChangedMon(isChecked : Boolean) {
        if (!isChecked) {
            list[0].close = "1"
//            list[0].isFill = true
        } else {
            list[0].close = "0"
//            list[0].isFill = !(list[0].openHours!!.isEmpty() && list[0].closeHours!!.isEmpty())
        }
        viewModel.setMonFriTime(list[0])
        binding.data = viewModel
    }

    fun onCheckedChangedTue(isChecked : Boolean) {
        if (!isChecked) {
            list[1].close = "1"
//            list[1].isFill = true
        } else {
            list[1].close = "0"
//            list[1].isFill = !(list[1].openHours!!.isEmpty() && list[1].closeHours!!.isEmpty())
        }
        viewModel.setTueTime(list[1])
        binding.data = viewModel
    }

    fun onCheckedChangedWed(isChecked : Boolean) {
        if (!isChecked) {
            list[2].close = "1"
//            list[2].isFill = true
        } else {
            list[2].close = "0"
//            list[2].isFill = !(list[2].openHours!!.isEmpty() && list[2].closeHours!!.isEmpty())
        }
        viewModel.setWedTime(list[2])
        binding.data = viewModel
    }

    fun onCheckedChangedThu(isChecked : Boolean) {
        if (!isChecked) {
            list[3].close = "1"
//            list[3].isFill = true
        } else {
            list[3].close = "0"
//            list[3].isFill = !(list[3].openHours!!.isEmpty() && list[3].closeHours!!.isEmpty())
        }
        viewModel.setThuTime(list[3])
        binding.data = viewModel
    }

    fun onCheckedChangedFri(isChecked : Boolean) {
        if (!isChecked) {
            list[4].close = "1"
//            list[4].isFill = true
        } else {
            list[4].close = "0"
//            list[4].isFill = !(list[4].openHours!!.isEmpty() && list[4].closeHours!!.isEmpty())
        }
        viewModel.setFriTime(list[4])
        binding.data = viewModel
    }

    fun onCheckedChangedSat(isChecked : Boolean) {
        if (!isChecked) {
            list[5].close = "1"
//            list[5].isFill = true
        } else {
            list[5].close = "0"
//            list[5].isFill = !(list[5].openHours!!.isEmpty() && list[5].closeHours!!.isEmpty())
        }
        viewModel.setSatTime(list[5])
        binding.data = viewModel
    }

    fun onCheckedChangedSun(isChecked : Boolean) {
        if (!isChecked) {
            list[6].close = "1"

            ;//            list[6].isFill = true
        } else {
            list[6].close = "0"
//            list[6].isFill = !(list[6].openHours!!.isEmpty() && list[6].closeHours!!.isEmpty())
        }
        viewModel.setSunTime(list[6])
        binding.data = viewModel
    }

    override fun onTimeChanged(day: String, startTime: String, endTime: String) {
        when (day) {
            "monday" -> {
                list[0] = ClinicTimeData("monday", startTime, endTime,"0")
                viewModel.setMonTime(list[0])
            }
            "tuseday" -> {
                list[1] = ClinicTimeData("tuseday", startTime, endTime,"0")
                viewModel.setTueTime(list[1])
            }
            "wednesday" -> {
                list[2] = ClinicTimeData("wednesday", startTime, endTime,"0")
                viewModel.setWedTime(list[2])
            }
            "thursday" -> {
                list[3] = ClinicTimeData("thursday", startTime, endTime,"0")
                viewModel.setThuTime(list[3])
            }
            "friday" -> {
                list[4] = ClinicTimeData("friday", startTime, endTime,"0")
                viewModel.setFriTime(list[4])
            }
            "satrday" -> {
                list[5] = ClinicTimeData("satrday", startTime, endTime,"0")
                viewModel.setSatTime(list[5])
            }
            "sunday" -> {
                list[6] = ClinicTimeData("sunday", startTime, endTime,"0")
                viewModel.setSunTime(list[6])
            }
            "monfri" -> {
                list[0] = ClinicTimeData("monday", startTime, endTime,"0")
                list[1] = ClinicTimeData("tuseday", startTime, endTime,"0")
                list[2] = ClinicTimeData("wednesday", startTime, endTime,"0")
                list[3] = ClinicTimeData("thursday", startTime, endTime,"0")
                list[4] = ClinicTimeData("friday", startTime, endTime,"0")
                viewModel.setMonFriTime(list[0])
            }
        }

        binding.data = viewModel
    }

    private fun getDefaultTimeList() : ArrayList<ClinicTimeData> {
        val list : ArrayList<ClinicTimeData> = ArrayList()
//
//        list.add(ClinicTimeData("monday", "", "", "0"))
//        list.add(ClinicTimeData("tuseday", "", "", "0"))
//        list.add(ClinicTimeData("wednesday", "", "", "0"))
//        list.add(ClinicTimeData("thursday", "", "", "0"))
//        list.add(ClinicTimeData("friday", "", "", "0"))
//        list.add(ClinicTimeData("satrday", "", "", "0"))
//        list.add(ClinicTimeData("sunday", "", "", "0"))

list.add(ClinicTimeData("monday", "00:00", "00:00", "0"))
        list.add(ClinicTimeData("tuseday", "00:00", "00:00", "0"))
        list.add(ClinicTimeData("wednesday", "00:00", "00:00", "0"))
        list.add(ClinicTimeData("thursday", "00:00", "00:00", "0"))
        list.add(ClinicTimeData("friday", "00:00", "00:00", "0"))
        list.add(ClinicTimeData("satrday", "00:00", "00:00", "0"))
        list.add(ClinicTimeData("sunday", "00:00", "00:00", "0"))


        return list
    }

//    private fun isValid() : Boolean {
//        var check = true
//
//        for (i in 0..6) {
//            if (!list[i].isFill) {
//                check = false
//                break
//            }
//        }
//        return check
//    }
}
