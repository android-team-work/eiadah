package com.eiadah.app.ui.doctor.postLogin.activity

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.content.ContextCompat
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.RadioGroup
import com.bumptech.glide.Glide
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityEditDoctorProfileBinding
import com.eiadah.app.models.dataModel.CityData
import com.eiadah.app.models.dataModel.CountryData
import com.eiadah.app.models.dataModel.SpecialistData
import com.eiadah.app.models.dataModel.StateData
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.models.responseModel.*
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.RestProcess.Companion.progressDialog
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.preLogin.activity.RegisterClinicActivity
import com.eiadah.app.ui.doctor.preLogin.viewModels.CreateDoctorProfileViewModel
import com.eiadah.app.ui.doctor.preLogin.viewModels.EditDoctorProfileViewModel
import com.eiadah.app.ui.patient.postLogin.fragments.PersonalProfileFragment
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.esafirm.imagepicker.model.Image
import kotlinx.android.synthetic.main.activity_edit_doctor_profile.*

class EditDoctorProfileActivity : BaseActivity(), Observer<RestObservable> {


    lateinit var binding : ActivityEditDoctorProfileBinding
    private var strCountry = ""
    private var strState = ""
    private var strCity = ""
    private var strSpecialty = ""
    private var strName = ""
    private var strExperience = ""

    private var countryList : ArrayList<CountryData> = ArrayList()
    private var stateList : ArrayList<StateData> = ArrayList()
    private var cityList : ArrayList<CityData> = ArrayList()
    private var specialistList : ArrayList<SpecialistData> = ArrayList()
    lateinit var request: SaveDoctorProfileRequest
    private var strGender = GlobalVariables.Gender.MALE

    var idPath = ""
    var docPath = ""
    var isIdImage = true


    private val viewModel: EditDoctorProfileViewModel
            by lazy {  ViewModelProviders.of(this).get(EditDoctorProfileViewModel::class.java) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_doctor_profile)
        binding.clickHandler = this

        request = SaveDoctorProfileRequest()
        request.gender = strGender

        loadCountryData()
        loadSpecialist()

    }
    fun onRegistrationDocClick() {
        isIdImage = false
        openImagePicker()
    }

    fun onPhotoIdClick() {
        isIdImage = true
        openImagePicker()

    } fun onSaveClick() {
        strName = binding.edtFullName.text.toString()
        strExperience = binding.edtExperience.text.toString()

        request.name = strName
        request.experience = strExperience
        viewModel.saveDoctorProfile(request)
        viewModel.getDoctorProfileData().observe(this, this)
    }


     fun openImagePicker() {
        ImagePicker.create(this)
            .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
            .folderMode(true) // folder mode (false by default)
            .toolbarFolderTitle("Folder") // folder selection title
            .toolbarImageTitle("Tap to select") // image selection title
            .toolbarArrowColor(Color.BLACK) // Toolbar 'up' arrow color
            .single() // single mode
            .showCamera(true) // show camera or not (true by default)
            .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
            .enableLog(false) // disabling log
            .start() // start image picker activity with request code
    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
//            // Get a list of picked images
//            val images : List<Image> = ImagePicker.getImages(data)
//            // or get a single image only
//            val image : Image = ImagePicker.getFirstImageOrNull(data)
//
//            if (isIdImage) {
//                idPath = image.path
//                Glide.with(this).load(docPath).into(binding.ivUser)
//
////                binding.photoIdPath.text = idPath
//            } else {
//                docPath = image.path
////                binding.iv_register_proof.text = docPath
//                Glide.with(this).load(idPath).into(iv_register_proof)
//
//            }
//
//            //binding.data?.image?.value = image.path
//        }
//        super.onActivityResult(requestCode, resultCode, data)
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            val images : List<Image> = ImagePicker.getImages(data)
            // or get a single image only
            val image : Image = ImagePicker.getFirstImageOrNull(data)

            Log.e(PersonalProfileFragment.TAG, image.path)
            Glide.with(this).load(image.path).into(binding.ivUser)
//            viewModel.setImage(image.path)
//            updateImage(image.path)
            //binding.data?.image?.value = image.path
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onResume() {
        super.onResume()
        binding.data = SharedStorage.getDoctorData(this)
    }

    fun onCheckedChanged(radioGroup: RadioGroup, id : Int) {
        when(id) {
            R.id.radioButtonMale -> {
               // viewModel.setGender(GlobalVariables.Gender.MALE)
                binding.radioButtonMale.setTextColor(ContextCompat.getColorStateList(this, R.color.white))
                binding.radioButtonFemale.setTextColor(ContextCompat.getColorStateList(this, R.color.black))
            }

            R.id.radioButtonFemale -> {
                //viewModel.setGender(GlobalVariables.Gender.FEMALE)
                binding.radioButtonMale.setTextColor(ContextCompat.getColorStateList(this, R.color.black))
                binding.radioButtonFemale.setTextColor(ContextCompat.getColorStateList(this, R.color.white))
            }
        }
    }

    fun backOncllick() {
        finish()
    }

    private fun loadCountryData() {
        viewModel.loadCountryList()
        viewModel.getCountryList().observe(this, this)
    }

    private fun loadStateData(countryId : String) {
        viewModel.loadStateList(countryId)
        viewModel.getStateList().observe(this, this)
    }

    private fun loadCityData(stateId : String) {
        viewModel.loadCityList(stateId)
        viewModel.getCityList().observe(this, this)
    }

    private fun loadSpecialist() {
        viewModel.loadSpecialist()
        viewModel.getSpecialist().observe(this, this)
    }

    private fun setCountryAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, countryList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.countryAdapter = adapter
    }

    private fun setStateAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, stateList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.stateAdapter = adapter
    }

    private fun setCityAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, cityList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.cityAdapter = adapter
    }

    private fun setSpecialistAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, specialistList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.specialityAdapter = adapter
    }

    fun onItemSelectedCountry(adapterView: AdapterView<*>, view : View, i : Int, l : Long) {
        if (i!=0) {
            strCountry = countryList[i].name.toString()
            loadStateData(countryList[i].id.toString())
        } else {
            strCountry = ""
        }
        request.country = strCountry

    }

    fun onItemSelectedState(adapterView: AdapterView<*>, view : View, i : Int, l : Long) {
        if (i!=0) {
            strState = stateList[i].name.toString()
            loadCityData(stateList[i].id.toString())
        } else {
            strState = ""
        }
        request.state = strState

    }

    fun onItemSelectedCity(adapterView: AdapterView<*>, view : View, i : Int, l : Long) {
        strCity = if (i!=0) {
            cityList[i].name!!
        } else {
            ""
        }

        request.city = strCity
    }

    fun onItemSelectedSpeciality(i : Int) {
        strSpecialty = if (i!=0) {
            specialistList[i].name!!
        } else {
            ""
        }

        request.specilaity = strSpecialty
    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is CountryResponse) {
                    val countryResponse : CountryResponse = it.data
                    if (countryResponse.status!!) {
                        countryList = (countryResponse.countryList as ArrayList<CountryData>?)!!
                        val countryData = CountryData()
                        countryData.id = 0
                        countryData.name = "Choose country"
                        countryList.add(0, countryData)
                        setCountryAdapter()
                    }

                } else if (it.data is StatesResponse) {
                    val statesResponse : StatesResponse = it.data
                    if (statesResponse.status!!) {
                        stateList = (statesResponse.stateList as ArrayList<StateData>?)!!
                        val stateData = StateData()
                        stateData.id = 0
                        stateData.countryId =0
                        stateData.name = "Choose state"
                        stateList.add(0, stateData)
                        setStateAdapter()
                    }

                } else if (it.data is CitiesResponse) {
                    val citiesResponse : CitiesResponse = it.data
                    if (citiesResponse.status!!) {
                        cityList = (citiesResponse.cityList as ArrayList<CityData>?)!!
                        val cityData = CityData()
                        cityData.id = "0"
                        cityData.name = "Choose city"
                        cityList.add(0, cityData)
                        setCityAdapter()
                    }

                } else if (it.data is GetSpecialistResponse) {
                    val getSpecialistResponse : GetSpecialistResponse = it.data
                    if (getSpecialistResponse.status!!) {
                        specialistList = getSpecialistResponse.data as ArrayList<SpecialistData>

                        val specialistData = SpecialistData()
                        specialistData.id = 0
                        specialistData.name = "Choose Speciality"
                        specialistData.date = ""
                        specialistList.add(0, specialistData)
                        setSpecialistAdapter()
                    }

                } else if (it.data is DoctorProfileResponse) {
                    val doctorProfileResponse : DoctorProfileResponse = it.data

                    if (doctorProfileResponse.status!!) {
                        SharedStorage.setDoctorData(this, doctorProfileResponse.data!!)
                        activitySwitcher(HomeActivity::class.java, true)
                    }
                }
            }
            it.status == Status.ERROR -> {
                it.error!!.fillInStackTrace()
                progressDialog.dismiss()
            }
            it.status == Status.LOADING -> progressDialog.show()
        }
    }

}

