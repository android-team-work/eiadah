package com.eiadah.app.ui.doctor.postLogin.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.eiadah.app.R


/**
 * A simple [Fragment] subclass.
 *
 */
class SummaryFragment : Fragment() {

    companion object {
        fun newInstance() = SummaryFragment()
        val TAG: String = SummaryFragment::class.java.simpleName
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_summary, container, false)
    }


}
