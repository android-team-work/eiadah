package com.eiadah.app.ui.doctor.postLogin.viewModals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddPrescriptionRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DeletePatientViewModel : ObservableViewModel() {


    private var deltePatientResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var addPriscriptionResponse : MutableLiveData<RestObservable> = MutableLiveData()

    fun getDeletePatient() : LiveData<RestObservable> {
        return deltePatientResponse
    }

    fun getAddPrescription() : LiveData<RestObservable> {
        return addPriscriptionResponse
    }

    fun deletepatient(id : String) {
        getService().getDeletePatient(getHeader(), id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { deltePatientResponse.value = RestObservable.loading() }
            .subscribe(
                {deltePatientResponse.value = RestObservable.success(it)},
                {deltePatientResponse.value = RestObservable.error (it)}
            )
    }

    fun AddAddPrescription(request: AddPrescriptionRequest) {
        getService().AddPrescription(getHeader(), request.user_id,request.name)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addPriscriptionResponse.value = RestObservable.loading() }
            .subscribe(
                {addPriscriptionResponse.value = RestObservable.success(it)},
                {addPriscriptionResponse.value = RestObservable.error(it)}
            )
    }

}