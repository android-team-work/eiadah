package com.eiadah.app.ui.doctor.preLogin.activity

import androidx.databinding.DataBindingUtil
import android.os.Bundle
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityCreateDoctorProfileStep2Binding

class CreateDoctorProfileStep2Activity : BaseActivity() {

    lateinit var binding : ActivityCreateDoctorProfileStep2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_doctor_profile_step2)
        binding.clickHandler = this
    }

    fun onSaveClick() {
        activitySwitcher(AddOpeningClosingTimeActivity::class.java, true)
    }

    fun onEductionClick() {
        activitySwitcher(AddEductionActivity::class.java, false)
    }

    fun onRegistrationClick() {
        activitySwitcher(AddRegistrationDetailActivity::class.java, false)
    }
}
