package com.eiadah.app.ui.patient.postLogin.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityCustomerSupportBinding
import com.eiadah.app.models.requestModels.AddPatientRequest
import com.eiadah.app.models.responseModel.ReportResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.RestProcess.Companion.progressDialog
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.activity.HomeActivity
import com.eiadah.app.ui.doctor.postLogin.viewModals.ReportViewModel
import com.eiadah.app.utils.AppUtils

class CustomerSupportActivity : BaseActivity(), Observer<RestObservable> {


    lateinit var binding:ActivityCustomerSupportBinding
    lateinit var request: AddPatientRequest
    lateinit var strText:String

    private val viewModel: ReportViewModel
            by lazy {  ViewModelProviders.of(this).get(ReportViewModel::class.java) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil.setContentView(this,R.layout.activity_customer_support)
        binding.clickHandler=this

        request=AddPatientRequest()
    }

    fun onBackClick(){
        finish()
    }

    fun onSaveClick(){

        strText=binding.textReport.text.toString()
        if (isValid()) {
            request.text=strText

            viewModel.AddReport(request)
            viewModel.getAddReport().observe(this,this)

        }

    }

    private fun isValid() : Boolean {
        var check = false

        if (strText.isEmpty()) {
            AppUtils.showToast(this, "Please enter text")
        } else {
            check = true
        }

        return check
    }
    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is ReportResponse) {
                    val reportResponse : ReportResponse = it.data

                    if (reportResponse.status!!) {
                        finish()
                    }
                }
            }
            it.status == Status.ERROR -> {
                it.error!!.fillInStackTrace()
                progressDialog?.dismiss()
            }
            it.status == Status.LOADING -> progressDialog?.show()
        }
    }
}
