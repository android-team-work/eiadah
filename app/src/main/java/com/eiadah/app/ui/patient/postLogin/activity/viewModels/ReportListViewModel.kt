package com.eiadah.app.ui.patient.postLogin.activity.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ReportListViewModel : ObservableViewModel() {

    private var reportListResponse : MutableLiveData<RestObservable> = MutableLiveData()


    fun getReportlist() : LiveData<RestObservable> {
        return reportListResponse
    }


    fun reportList() {
        getService().getaReportList(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { reportListResponse.value = RestObservable.loading() }
            .subscribe(
                {reportListResponse.value = RestObservable.success(it)},
                {reportListResponse.value = RestObservable.error(it)}
            )
    }



}