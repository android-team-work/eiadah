package com.eiadah.app.ui.patient.postLogin.dialogs

import android.app.DatePickerDialog
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import android.widget.DatePicker
import com.eiadah.app.Listener.UpdateProfileData
import com.eiadah.app.core.BaseDialog
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.ProfileViewModel
import com.eiadah.app.utils.GlobalVariables
import java.util.*
import java.text.SimpleDateFormat


class DatePickerDialog(private val context: FragmentActivity, private val outputDateFormat: String, private val updateData: UpdateProfileData
    ) : BaseDialog(context) , DatePickerDialog.OnDateSetListener, LifecycleOwner {
    override fun getLifecycle(): Lifecycle {
        return lifecycleRegistry
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleRegistry = LifecycleRegistry(this)
    }

    lateinit var myCalendar : Calendar
    private lateinit var lifecycleRegistry: LifecycleRegistry

    private val profileViewModel : ProfileViewModel
            by lazy {
                ViewModelProviders.of(context).get(ProfileViewModel::class.java)
            }

    override fun show() {
        myCalendar = Calendar.getInstance()
        DatePickerDialog(
            context, this, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
            myCalendar.get(Calendar.DAY_OF_MONTH)
        ).show()
    }


    override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        myCalendar.set(Calendar.YEAR, year)
        myCalendar.set(Calendar.MONTH, monthOfYear)
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        val sdf = SimpleDateFormat(outputDateFormat, Locale.US)

        updateDOB(sdf.format(myCalendar.time))
    }

    private fun updateDOB(date : String) {
        var params : HashMap<String, String> = HashMap()
        params[GlobalVariables.PARAMS.PROFILE.DOB] = date
        updateData.updateProfileData(params)
        dismiss()
    }
}