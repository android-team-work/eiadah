package com.eiadah.app.ui.doctor.postLogin.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityWalletBinding
import com.eiadah.app.models.dataModel.BankListData
import com.eiadah.app.models.dataModel.PatientListData
import com.eiadah.app.models.requestModels.WithdrawRequest
import com.eiadah.app.models.responseModel.*
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.viewModals.CreditDataViewModel
import com.eiadah.app.ui.doctor.postLogin.viewModals.PatientListViewModel
import com.eiadah.app.utils.GlobalVariables
import com.myapplication.adapter.BankAdapter
import com.myapplication.adapter.PatientListAdapter
import com.pieboat.shredpreference.SharePreferHelper
import kotlinx.android.synthetic.main.activity_patient_list.*

class WalletActivity : BaseActivity(), Observer<RestObservable>, OnItemClickListener {

    lateinit var binding:ActivityWalletBinding
    lateinit var adpter:BankAdapter
    lateinit var bankList:ArrayList<BankListData>
    lateinit var request:WithdrawRequest
    lateinit var sharePreferHelper: SharePreferHelper
    var credit:String=""
    var id:String=""
    private val viewModel: CreditDataViewModel
            by lazy {  ViewModelProviders.of(this).get(CreditDataViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_wallet)
        binding.clickHandler=this

        request= WithdrawRequest()
        sharePreferHelper= SharePreferHelper(this)
        viewModel.getMyCredit().observe(this,this)
        viewModel.loaadMyCredit()

        viewModel.getBankList().observe(this,this)
        viewModel.loadBankList()
    }

    fun backOncllick() {
        finish()
    }

    fun addBank(){
        val intent=Intent(this,AddBankDetailsActivity::class.java)
        startActivity(intent)
    }
    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is MyCreditResponse) {
                    val myCreditResponse : MyCreditResponse = it.data

                    if (myCreditResponse.status!!) {

                        binding.tvCredit.setText(myCreditResponse.getUser()?.get(0)?.amount)

                        credit=myCreditResponse.getUser()?.get(0)?.amount!!
                        if(myCreditResponse.getUser()?.get(0)?.amount.equals("0")){
                            binding.btnWithdraw.visibility=View.GONE
                        }
                        else{
                            binding.btnWithdraw.visibility=View.VISIBLE
                        }
                    }
                }
               else  if (it.data is BankListResponse) {

                    val bankListResponse : BankListResponse = it.data
                    bankList = (bankListResponse.getUser() as ArrayList<BankListData>?)!!
                    val bankListData = BankListData()
                    bankList.add(bankListData)

                    adpter= BankAdapter(this,this,bankList)
                    binding.bankRecycle.setLayoutManager(LinearLayoutManager(this))
                    binding.bankRecycle.setAdapter(adpter)
                }

             else  if (it.data is DeleteBankResponse) {
                    val deleteBankResponse : DeleteBankResponse = it.data

                    if (deleteBankResponse.status!!) {

                      adpter.notifyDataSetChanged()
                        viewModel.getBankList().observe(this,this)
                        viewModel.loadBankList()
                    }
                }
            }
            it.status == Status.ERROR -> progressDialog.dismiss()
            it.status == Status.LOADING -> progressDialog.show()
        }
    }

    override fun onItemClick(position: Int) {
        viewModel.getDeleteBank().observe(this,this)
        viewModel.loadDeleteBank(bankList.get(position).id.toString())
        bankList.removeAt(position)
    }

    override fun onResume() {
        super.onResume()

        viewModel.getBankList().observe(this,this)
        viewModel.loadBankList()
    }

    fun onWithdrawClick(){

        request.amount=credit
        request.id=sharePreferHelper.getString(GlobalVariables.SharePref.DOCTOR_ID)!!
        viewModel.getwithdrawRequest().observe(this,this)
        viewModel.loadWithdrawRequest(request)
    }
}
