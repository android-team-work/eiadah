package com.eiadah.app.ui.patient.postLogin.adapter

import android.content.Context
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.databinding.LayoutBodyPartListItemBinding
import com.eiadah.app.models.dataModel.BodyPartData

class BodyPartAdapter(val context : Context, val onItemClickListener: OnItemClickListener) : RecyclerView.Adapter<BodyPartAdapter.BodyPartViewHolder>() {

    var list : List<BodyPartData> = ArrayList()

    fun updateList(list: List<BodyPartData>) {
        this.list = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): BodyPartViewHolder {
        val itemBinding : LayoutBodyPartListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_body_part_list_item, p0, false)

        return BodyPartViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: BodyPartViewHolder, p1: Int) {
        p0.itemBinding.bodaydata = list[p1]
    }

    inner class BodyPartViewHolder(val itemBinding: LayoutBodyPartListItemBinding) : RecyclerView.ViewHolder(itemBinding.root) {
        init {
            itemBinding.clickHandler = this
        }

        fun onItemClick() {
            onItemClickListener.onItemClick(adapterPosition)
        }
    }
}