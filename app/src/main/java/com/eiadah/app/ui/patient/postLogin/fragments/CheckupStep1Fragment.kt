package com.eiadah.app.ui.patient.postLogin.fragments


import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eiadah.app.Listener.CheckupChangeFragmentListener

import com.eiadah.app.R
import com.eiadah.app.core.BaseFragment
import com.eiadah.app.databinding.FragmentCheckupStep1Binding

/**
 * A simple [Fragment] subclass.
 *
 */
class CheckupStep1Fragment : BaseFragment() {

    private var mListener : CheckupChangeFragmentListener? = null
    lateinit var  binding : FragmentCheckupStep1Binding

    companion object {
        fun newInstance() = CheckupStep1Fragment()
        val TAG : String = CheckupStep1Fragment::class.java.simpleName
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_checkup_step1, container, false)
        binding.clickEvent = this
        return binding.root
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is CheckupChangeFragmentListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement CheckupChangeFragmentListener")
        }
    }

    fun onSaveClick() {
        if (binding.checkboxCheckupStep1.isChecked)
            mListener!!.changeFragment(CheckupStep2Fragment.newInstance(), CheckupStep2Fragment.TAG)
        else
            Log.e(TAG, "checkbox not get")
    }


}
