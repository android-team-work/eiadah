package com.eiadah.app.ui.patient.postLogin.activity

import android.app.Dialog
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.applandeo.materialcalendarview.CalendarView
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityPatientFormBinding
import com.eiadah.app.models.dataModel.*
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.models.responseModel.PaymentResponse
import com.eiadah.app.models.responseModel.PtAppointmentResponse
import com.eiadah.app.models.responseModel.TimeSlotResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.AddPtAppointmentViewModel
import com.mukesh.countrypicker.Country
import com.mukesh.countrypicker.CountryPicker
import com.mukesh.countrypicker.listeners.OnCountryPickerListener
import com.myapplication.adapter.TimeSlotAdapter
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import org.json.JSONObject
import java.io.Serializable
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class PatientFormActivity : BaseActivity(), Observer<RestObservable> ,Serializable, OnItemClickListener, PaymentResultListener{


    private val TAG = PatientFormActivity::class.java!!.getSimpleName()

    lateinit var binding : ActivityPatientFormBinding
    var doctor_id:String?=""
    var currancy_code:String?=""
    var fees:String?=""
    var transact_id:String?=""
    var appoint_id:String?=""
    var doctor_name:String?=""
    lateinit var request: AddAppointmenttRequest
    lateinit var adapter:TimeSlotAdapter
    lateinit var  timeslotList:ArrayList<TimeSlotData>
    private val viewModel: AddPtAppointmentViewModel
            by lazy {  ViewModelProviders.of(this).get(AddPtAppointmentViewModel::class.java) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_patient_form)
        binding.clickHandler = this

        request= AddAppointmenttRequest()
        doctor_id=intent.getStringExtra("doctor_id")
        fees=intent.getStringExtra("fees")
        doctor_name=intent.getStringExtra("doctor_name")
        currancy_code=intent.getStringExtra("currancy_code")

         binding.docName.setText("Dr."+" "+doctor_name)

        viewModel.gettimeslot().observe(this,this)
        viewModel.loadtimeslot()
    }

    fun onCheckedChanged(radioGroup: RadioGroup, id : Int) {
        when(id) {
            R.id.radioButtonMale -> {
                binding.radioButtonMale.setTextColor(this?.let { ContextCompat.getColorStateList(it, R.color.white) })
                binding.radioButtonFemale.setTextColor(this?.let { ContextCompat.getColorStateList(it, R.color.black) })
            }

            R.id.radioButtonFemale -> {
                binding.radioButtonMale.setTextColor(this?.let { ContextCompat.getColorStateList(it, R.color.black) })
             }
        }
    }


    fun btn_click(){
        val name = binding.edtPtName.text.toString()
        val number = binding.edtPtMobile.text.toString()
        val date = binding.tvPtDate.text.toString()
        val time = binding.tvPtTime.text.toString()
        val age = binding.edtPtAge.text.toString()
        val address = binding.edtPtAddress.text.toString()
        val email = binding.edtPtEmail.text.toString()
        val notes = binding.edtPtComm.text.toString()
        val dial_code = binding.edtMobileCode.text.toString()


        if (isValid(name,number, date, time,email,age,notes,dial_code)) {
//            dialog()
            request.doctor_id= doctor_id.toString()
            request.name = name
            request.mobile=dial_code+number
            request.email=email
            request.date=date
            request.time=time
            request.note=notes
            request.doctor_id=doctor_id.toString()!!

            viewModel.getAddPtAppointment().observe(this,this)
            viewModel.AddPtAppointment(request)
        }


    }

    fun dialog() {
        val dialog = Dialog(this,R.style.Theme_Dialog)
        dialog .requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog .setContentView(R.layout.payment_dialog)
        dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        val window = dialog.window
        window!!.setGravity(Gravity.CENTER)
        dialog.setCancelable(false)
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

        var tv_fees: TextView
        tv_fees=dialog.findViewById(R.id.tv_fees)
        tv_fees.setText("Appointment Fees"+" - "+currancy_code+fees)
        var btn_pay: Button
        btn_pay=dialog.findViewById(R.id.btn_pay)
        btn_pay.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            startPayment()
        })
        dialog .show()

    }

    private fun isValid(name : String,number : String, date : String, time : String, email : String, age : String, notes : String, dial_code : String) : Boolean {
        var check = false
        if (name.isEmpty() &&number.isEmpty() && date.isEmpty() && email.isEmpty()&& age.isEmpty()&&  notes.isEmpty()) {
            showToast(R.string.please_fill_all_fields)
        } else if (name.isEmpty()) {
            showToast("Please Enter Your Name")
        }  else if (number.isEmpty()) {
            showToast(R.string.empty_reg_number)
        } else if (date.isEmpty()) {
            showToast("Please Select Appointment Date")
        } else if (email.isEmpty()) {
            showToast("Please Enter your email id")
        }else if (age.isEmpty()) {
            showToast("Please Enter Age")
        }else if (notes.isEmpty()) {
            showToast("Please Enter Comments")
        }else if (notes.isEmpty()) {
            showToast("Please Enter Comments")
        }else if (dial_code.isEmpty()) {
            showToast("Please Enter Country_Code")
        } else {
            check = true
        }

        return check
    }
    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()

                 if (it.data is TimeSlotResponse) {
                    val timeSlotResponse : TimeSlotResponse = it.data
                    if (timeSlotResponse.status!!) {

                        timeslotList = (timeSlotResponse.list as ArrayList<TimeSlotData>?)!!
                        val cityData = TimeSlotData()
                        timeslotList.add(cityData)

                        if (timeslotList.size<=0){
                            binding.rcyPatientFormTimeSlot.visibility=View.GONE
                            binding.tvSlot.visibility=View.VISIBLE
                        }else{
                            binding.rcyPatientFormTimeSlot.visibility=View.VISIBLE
                            binding.tvSlot.visibility=View.GONE
                            adapter= TimeSlotAdapter(this,this, timeslotList)
                            binding.rcyPatientFormTimeSlot.setLayoutManager(GridLayoutManager(this, 3))
                            binding.rcyPatientFormTimeSlot.setAdapter(adapter)
                        }
                    }
                }
                 else   if (it.data is PtAppointmentResponse) {
                    val appointmentResponse : PtAppointmentResponse = it.data

                    if (appointmentResponse.status!!) {

                        appoint_id=appointmentResponse.getUser()?.id.toString()

                        dialog()
                    }
                }
                 else   if (it.data is PaymentResponse) {
                    val appointmentResponse : PaymentResponse = it.data

                    if (appointmentResponse.status!!) {
                        val intent=Intent(this,MyAppointmentActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                }
            }
            it.status == Status.ERROR -> {
                it.error!!.fillInStackTrace()
                progressDialog.dismiss()
            }
            it.status == Status.LOADING -> progressDialog.show()
        }
    }
    override fun onItemClick(position: Int) {
        binding.tvPtTime.setText(timeslotList.get(position).timeSlot)
    }


    fun Datedialog() {
        val dialog = Dialog(this,R.style.Theme_Dialog)
        dialog .requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog .setContentView(R.layout.dilaog_calender_layout)
        dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        val window = dialog.window
        window!!.setGravity(Gravity.CENTER)
        dialog.setCancelable(false)
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)


        var btn_cancel: Button
        btn_cancel=dialog.findViewById(R.id.btn_cancel)
        btn_cancel.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
        })
        var btn_date: Button
        btn_date=dialog.findViewById(R.id.btn_date)
        btn_date.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
        })

        val calendarView = dialog.findViewById<View>(R.id.calendarView) as CalendarView



        calendarView.setOnDayClickListener { eventDay ->

            val date_time=eventDay.calendar.time.toString()
            var date:Date?=null;
            val formatter =  SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
            val temp = date_time
            try {
                date = formatter.parse(temp);

            } catch (e: ParseException) {
                e.printStackTrace();
            }


            val dateNew= SimpleDateFormat("yyyy-MM-dd").format(date);

            viewModel.getTimeSlot().observe(this,this)
            viewModel.TimeSlot(doctor_id.toString(),dateNew)

            binding.tvPtDate.setText(dateNew)
            dialog.dismiss()

            Log.e("vvvvvvvvvv",dateNew);
        }
        dialog .show()
    }

    fun startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        val activity = this

        val co = Checkout()

        try {
            val options = JSONObject()
            options.put("name", binding.edtPtName.text.toString())
            options.put("description", "Appointment Fees")
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")
            options.put("currency", currancy_code)
            options.put("amount", fees+"00")

            val preFill = JSONObject()
            preFill.put("email", binding.edtPtEmail.text.toString())
            preFill.put("contact", binding.edtPtMobile.text.toString())
            options.put("prefill", preFill)

            co.open(activity, options)
        } catch (e: Exception) {
            Toast.makeText(activity, "Error in payment: " + e.message, Toast.LENGTH_SHORT)
                .show()
            e.printStackTrace()
        }
    }

    /**
     * The name of the function has to be
     * onPaymentSuccess
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    override fun onPaymentSuccess(razorpayPaymentID: String) {
        try {
            Toast.makeText(this, "Payment Successful: $razorpayPaymentID", Toast.LENGTH_SHORT).show()

            transact_id=razorpayPaymentID
            request.transaction_id=transact_id.toString()
            request.appointment_id=appoint_id.toString()

            viewModel.getPayment().observe(this,this)
            viewModel.AddPaymentResponse(request)
            val intent=Intent(this,MyAppointmentActivity::class.java)
            startActivity(intent)
            finish()
        } catch (e: Exception) {
            Log.e(TAG, "Exception in onPaymentSuccess", e)
        }

    }

    /**
     * The name of the function has to be
     * onPaymentError
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    override fun onPaymentError(code: Int, response: String?) {
        try {
            Toast.makeText(this, "Payment failed: $code $response", Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            Log.e(TAG, "Exception in onPaymentError", e)
        }

    }
     fun showcountrypickerdialog() {

          var builder = CountryPicker.Builder().with(this).listener(object : OnCountryPickerListener{

                 override fun onSelectCountry(country: Country) {
                     binding.edtMobileCode.setText(country.dialCode)
                 }
             })
         var picker = builder.build()
         picker.showDialog(this);

    }
}
