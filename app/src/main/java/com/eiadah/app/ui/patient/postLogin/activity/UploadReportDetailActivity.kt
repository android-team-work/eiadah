package com.eiadah.app.ui.patient.postLogin.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.eiadah.app.R
import com.eiadah.app.databinding.ActivityUploadReportDetailBinding

class UploadReportDetailActivity : AppCompatActivity() {
    lateinit var binding:ActivityUploadReportDetailBinding
    lateinit var report_id:String
    lateinit var image:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_upload_report_detail)
        binding.clickHandler=this

        image=intent.getStringExtra("image")
        report_id=intent.getStringExtra("report_id")

        Glide.with(this).load("https://www.eiadahapp.com/"+image).into(binding.ivReport)
    }

    override fun onBackPressed() {
        finish()
    }
}
