package com.eiadah.app.ui.patient.postLogin.activity.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddAllergytRequest
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AddMedicineViewModel : ObservableViewModel() {

    private var medicResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var AddmedicResponse : MutableLiveData<RestObservable> = MutableLiveData()
    var selectedMedic: MutableLiveData<String> =  MutableLiveData()

    fun getMedicList() : LiveData<RestObservable> {
        return medicResponse
    }

     fun getAddMedicList() : LiveData<RestObservable> {
        return AddmedicResponse
    }


    fun loadMedicList(){
        getService().getMediclist(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { medicResponse.value = RestObservable.loading() }
            .subscribe(
                {medicResponse.value = RestObservable.success(it)},
                {medicResponse.value = RestObservable.error (it)}
            )
    }
    fun loadAddMedicList(request: AddAllergytRequest){
        getService().AddMedic(getHeader(),request.title)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { AddmedicResponse.value = RestObservable.loading() }
            .subscribe(
                {AddmedicResponse.value = RestObservable.success(it)},
                {AddmedicResponse.value = RestObservable.error (it)}
            )
    }

    fun getSelectedMedic(value : String) : LiveData<String> {
        if (selectedMedic.value==null) {
            selectedMedic.value = "$value, "
        } else {
            if (!selectedMedic.value!!.contains(value)) {
                selectedMedic.value = selectedMedic.value + value + ", "
            }
        }

        return selectedMedic
    }
}