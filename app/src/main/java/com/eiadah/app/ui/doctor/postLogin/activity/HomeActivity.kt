package com.eiadah.app.ui.doctor.postLogin.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.eiadah.app.R
import androidx.databinding.DataBindingUtil
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityHome2Binding
import com.eiadah.app.utils.SharedStorage
import com.google.firebase.FirebaseApp


class HomeActivity : BaseActivity() {

    lateinit var binding : ActivityHome2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home2)
        binding.clickHandler = this
        Log.e("tokennnnnnnn", SharedStorage.getToken(this))

        FirebaseApp.initializeApp(this);
    }

    fun settingOncllick() {
        activitySwitcher(SettingActivity::class.java, false)
    }
    fun reportOncllick() {
        activitySwitcher(ReportIssueActivity::class.java, false)
    }
    fun notificationListOncllick() {
        activitySwitcher(NotificationListActivity::class.java, false)
    }
}
