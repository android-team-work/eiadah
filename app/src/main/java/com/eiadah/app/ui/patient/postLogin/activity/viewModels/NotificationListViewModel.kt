package com.eiadah.app.ui.patient.postLogin.activity.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class NotificationListViewModel : ObservableViewModel() {

    private var notificationListResponse : MutableLiveData<RestObservable> = MutableLiveData()


    fun getNotificationlist() : LiveData<RestObservable> {
        return notificationListResponse
    }


    fun loadNotificationList() {
        getService().getNotificationList(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { notificationListResponse.value = RestObservable.loading() }
            .subscribe(
                {notificationListResponse.value = RestObservable.success(it)},
                {notificationListResponse.value = RestObservable.error(it)}
            )
    }



}