package com.eiadah.app.ui.doctor.preLogin.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.SaveClinicRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RegisterClinicViewModel : ObservableViewModel() {

    var saveClinicResponse : MutableLiveData<RestObservable> = MutableLiveData()

    fun getClinicResponse() : LiveData<RestObservable> {
        return saveClinicResponse
    }

    fun saveClinicResponse(request: SaveClinicRequest) {
        getService().saveClinic(getHeader(), request.clinic_name,request.clinic_location,request.timing,request.time)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { saveClinicResponse.value = RestObservable.loading() }
            .subscribe(
                {saveClinicResponse.value = RestObservable.success(it)},
                {saveClinicResponse.value = RestObservable.error (it)}
            )
    }
}