package com.eiadah.app.ui.doctor.postLogin.viewModals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.SaveClinicRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AppointmentListViewModel : ObservableViewModel() {

    var ApptListResponse : MutableLiveData<RestObservable> = MutableLiveData()
    var ApptLisDatetResponse : MutableLiveData<RestObservable> = MutableLiveData()

    fun getAppointmentList() : LiveData<RestObservable> {
        return ApptListResponse
    }
    fun getAppointmentListDate() : LiveData<RestObservable> {
        return ApptLisDatetResponse
    }

    fun appointmenttList() {
        getService().getappointmentList(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { ApptListResponse.value = RestObservable.loading() }
            .subscribe(
                {ApptListResponse.value = RestObservable.success(it)},
                {ApptListResponse.value = RestObservable.error (it)}
            )
    }
    fun appointmenttListDate(date:String) {
        getService().getapptlistdate(getHeader(),date)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { ApptLisDatetResponse.value = RestObservable.loading() }
            .subscribe(
                {ApptLisDatetResponse.value = RestObservable.success(it)},
                {ApptLisDatetResponse.value = RestObservable.error (it)}
            )
    }
}