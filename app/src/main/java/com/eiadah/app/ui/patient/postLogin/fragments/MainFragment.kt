package com.eiadah.app.ui.patient.postLogin.fragments


import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.eiadah.app.R
import com.eiadah.app.databinding.FragmentMainBinding


/**
 * A simple [Fragment] subclass.
 *
 */
class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding : FragmentMainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        val view : View = binding.root

        binding.bottomNavigationHome.setOnNavigationItemSelectedListener { menuItem ->
            when(menuItem.itemId) {
                R.id.home -> {
                    switchContent(HomeFragment.newInstance(), HomeFragment.TAG)
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.menu_doctor -> {
                    switchContent(DoctorListFragment.newInstance(), DoctorListFragment.TAG)
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.report -> {
                    switchContent(CallFragment.newInstance(), CallFragment.TAG)
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.setting -> {
                    switchContent(RecordFragment.newInstance(), RecordFragment.TAG)
                    return@setOnNavigationItemSelectedListener true
                }

            }
            false
        }

        switchContent(HomeFragment.newInstance(), HomeFragment.TAG)

        return view
    }


    private fun switchContent(fragment: Fragment?, tag: String) {
        val fragmentManager = childFragmentManager
        fragmentManager.popBackStack()
        if (fragment != null) {
            val transaction = fragmentManager
                .beginTransaction()
            transaction.replace(R.id.homeChildContainer, fragment, tag)
            if (fragment !is HomeFragment)
                transaction.addToBackStack(tag)
            transaction.commit()
        }
    }

}
