package com.myapplication.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.models.dataModel.ClinicTiming
import com.eiadah.app.models.dataModel.TimeSlotData
import java.util.ArrayList

class TimeSlotAdapter(private val mContext: Context, val onItemClickListener: OnItemClickListener, var clinicTimingList: ArrayList<TimeSlotData>) : RecyclerView.Adapter<TimeSlotAdapter.AppointmentViewHolder>() {
    private var lastSelectedPosition = 0
    override fun onBindViewHolder(p0: AppointmentViewHolder, p1: Int) {

        p0.itemView.setOnClickListener(View.OnClickListener {
            onItemClickListener.onItemClick(p1)
            p0.radio_btn.isChecked()
        })
        p0.radio_btn.setChecked(lastSelectedPosition == p1)
        if (p0.radio_btn.isChecked()) {
            onItemClickListener.onItemClick(p1)
        }
        p0.tv_day.setText(clinicTimingList.get(p1).day)
        p0.tv_time.setText(clinicTimingList.get(p1).timeSlot)

        if (clinicTimingList.get(p1).timeSlot.isNullOrEmpty()){
            p0.card_view.visibility=View.GONE
        }

        else{
            p0.card_view.visibility=View.VISIBLE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppointmentViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.time_item_layout, parent, false)
        return AppointmentViewHolder(v)
    }

    override fun getItemCount(): Int {
        return clinicTimingList.size
    }

    inner class AppointmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


         var radio_btn:RadioButton
         var tv_day:TextView
         var tv_time:TextView
         var card_view:CardView

        init {

            radio_btn=itemView.findViewById(R.id.radio_btn)
            tv_day=itemView.findViewById(R.id.tv_day)
            tv_time=itemView.findViewById(R.id.tv_time)
            card_view=itemView.findViewById(R.id.card_view)


            radio_btn.setOnClickListener(View.OnClickListener {
                lastSelectedPosition = adapterPosition
                notifyDataSetChanged()
            })
        }
        fun onItemClick() {
            onItemClickListener.onItemClick(adapterPosition)
        }
    }
}