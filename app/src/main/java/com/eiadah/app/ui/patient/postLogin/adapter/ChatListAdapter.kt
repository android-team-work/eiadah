package com.myapplication.adapter

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.bumptech.glide.Glide
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.models.dataModel.ChatListData
import com.eiadah.app.models.dataModel.ReportListData
import com.eiadah.app.ui.patient.postLogin.activity.ChatActivity
import com.eiadah.app.ui.patient.postLogin.activity.ReportDetailsActivity
import com.pieboat.shredpreference.SharePreferHelper

class ChatListAdapter(private val mContext: Context, var chatListData:ArrayList<ChatListData>) : RecyclerView.Adapter<ChatListAdapter.ViewHolder>() {
    lateinit var sharePreferHelper: SharePreferHelper
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        sharePreferHelper= SharePreferHelper(mContext)
        p0.itemView.setOnClickListener(View.OnClickListener {
            val intent = Intent(mContext, ChatActivity::class.java)
            intent.putExtra("id",chatListData.get(p1).receiver.toString())
            intent.putExtra("name",chatListData.get(p1).name)
            mContext?.startActivity(intent)

        })

        p0.text_username.setText(chatListData.get(p1).name)
        Glide.with(mContext).load("https://www.eiadahapp.com/uploads/"+chatListData.get(p1).image).into(p0.iv_user)
        if(chatListData.get(p1).name.isNullOrEmpty()){
            p0.card_view.visibility=View.GONE
        }
        else{
            p0.card_view.visibility=View.VISIBLE

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.chat_list_item, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return chatListData.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

         var iv_user:ImageView
         var text_username:TextView
         var card_view:CardView
        init {

            iv_user=itemView.findViewById(R.id.iv_user)
            text_username=itemView.findViewById(R.id.text_username)
            card_view=itemView.findViewById(R.id.card_view)
        }

//        fun onItemClick() {
//            onItemClickListener.onItemClick(adapterPosition)
//        }
    }




}