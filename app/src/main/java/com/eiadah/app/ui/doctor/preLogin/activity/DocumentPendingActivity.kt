package com.eiadah.app.ui.doctor.preLogin.activity

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityDocumentPendingBinding
import com.eiadah.app.models.responseModel.DoctorProfileResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.activity.HomeActivity
import com.eiadah.app.ui.doctor.preLogin.viewModels.DocumentPendingViewModel
import com.eiadah.app.ui.patient.postLogin.fragments.PersonalProfileFragment
import com.eiadah.app.ui.patient.preLogin.activity.SplashActivity
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.esafirm.imagepicker.model.Image
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class DocumentPendingActivity : BaseActivity(), Observer<RestObservable> {


    lateinit var binding : ActivityDocumentPendingBinding

    var idPath = ""
    var docPath = ""
    var isIdImage = true

    val viewModel : DocumentPendingViewModel by lazy { ViewModelProviders.of(this).get(DocumentPendingViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_document_pending)
        binding.clickHandler = this

        viewModel.getProfileResponse().observe(this,this)
        viewModel.profileApi()
    }

    fun onSubmitClick() {
        Toast.makeText(this,"Please Wait for verification call", Toast.LENGTH_LONG).show()

        activitySwitcher(LoginDoctorActivity::class.java, true)
        finish()
        if (isValid()) {
            updateImage(idPath, docPath)
        }

    }

    fun onRegistrationDocClick() {
        isIdImage = false
        openImagePicker()
    }

    fun onPhotoIdClick() {
        isIdImage = true
        openImagePicker()
    }

    private fun openImagePicker() {
        ImagePicker.create(this)
            .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
            .folderMode(true) // folder mode (false by default)
            .toolbarFolderTitle("Folder") // folder selection title
            .toolbarImageTitle("Tap to select") // image selection title
            .toolbarArrowColor(Color.BLACK) // Toolbar 'up' arrow color
            .single() // single mode
            .showCamera(true) // show camera or not (true by default)
            .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
            .enableLog(false) // disabling log
            .start() // start image picker activity with request code
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            val images : List<Image> = ImagePicker.getImages(data)
            // or get a single image only
            val image : Image = ImagePicker.getFirstImageOrNull(data)

            if (isIdImage) {
                idPath = image.path
                binding.photoIdPath.text = idPath
            } else {
                docPath = image.path
                binding.registrationDocPath.text = docPath
            }

            //binding.data?.image?.value = image.path
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun updateImage(idProof: String, doc: String) {
        val file = File(idProof)
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val proof1 = MultipartBody.Part.createFormData("id_proof", file.name, requestFile)

        val file2 = File(doc)
        val requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file2)
        val proof2 = MultipartBody.Part.createFormData("registration_image", file.name, requestFile2)

        viewModel.saveDocument(proof1, proof2)
        viewModel.getDocumentResponse().observe(this, this)

    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is DoctorProfileResponse) {
                    val profileResponse : DoctorProfileResponse = it.data

                    if (profileResponse.status!!) {
                        SharedStorage.setSession(this, true)
                        SharedStorage.setUserType(this, GlobalVariables.User.DOCTOR)
                        SharedStorage.setDoctorData(this, profileResponse.data!!)
//                        Toast.makeText(this,"Please Wait for verification call", Toast.LENGTH_LONG).show()
//
//                        SharedStorage.clear(this)
//                        activitySwitcher(SplashActivity::class.java, true)
//                        finish()

                    }
                }

                else if (it.data is DoctorProfileResponse) {

                    val profileResponse : DoctorProfileResponse = it.data
                    if (profileResponse.status!!) {

                        SharedStorage.setSession(this, true)
                        SharedStorage.setUserType(this, GlobalVariables.User.DOCTOR)
                        SharedStorage.setDoctorData(this, profileResponse.data!!)
                        val doctorData = SharedStorage.getDoctorData(this)
//                        if (doctorData != null) {
//                             if (doctorData.doctor!!.verify!!.equals("0")) {
//
//                                Toast.makeText(this,"Please Wait for verification call",Toast.LENGTH_LONG).show()
//                                SharedStorage.clear(this)
//                                activitySwitcher(SplashActivity::class.java, true)
//                                finish()
//                            }
//                        }
                    }
                }

            }
            it.status == Status.ERROR -> progressDialog.dismiss()
            it.status == Status.LOADING -> progressDialog.show()
        }
    }

    private fun isValid(): Boolean {
        var check = false
        if (idPath.isEmpty() && docPath.isEmpty()) {
            showToast(getString(R.string.empty_both_proof))
        } else if (idPath.isEmpty()) {
            showToast("Please choose Id Proof image")
        } else if (docPath.isEmpty()) {
            showToast("Please choose Registration document image")
        } else {
            check = true
        }

        return check
    }
}
