package com.eiadah.app.ui.doctor.postLogin.activity

import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.widget.Toast
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityChangePasswordBinding
import com.eiadah.app.models.responseModel.BasicResponse
import com.eiadah.app.restAPI.RestCallback
import com.eiadah.app.restAPI.RestProcess
import com.eiadah.app.utils.GlobalVariables
import kotlinx.android.synthetic.main.activity_change_password.*
import retrofit2.Call
import retrofit2.Response

class ChangePasswordActivity : BaseActivity(), RestCallback {

    lateinit var binding:ActivityChangePasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_change_password)
        binding.clickHandler=this
    }
    fun backOncllick() {
        finish()
    }
    fun onSaveClick() {

        if (resetPasswordValidator(edt_old_pass.text.toString(),edt_new_pass.text.toString(),edt_confirm_pass.text.toString(),this )) {

            changePasswordApi();


        }
    }

    fun resetPasswordValidator(oldpass: String, pass: String, confirm_passord: String, context: Context): Boolean {
        if (oldpass.isEmpty()) {
            Toast.makeText(context, R.string.error_password, Toast.LENGTH_SHORT).show()
            return false

        } else if (pass.isEmpty()) {
            Toast.makeText(context, R.string.error_password, Toast.LENGTH_SHORT).show()
            return false

        } else if (confirm_passord.isEmpty()) {
            Toast.makeText(context, R.string.error_password, Toast.LENGTH_SHORT).show()
            return false

        } else if (pass != confirm_passord) {
            Toast.makeText(context, R.string.password_mismatch, Toast.LENGTH_SHORT).show()
            return false
        } else {
            return true
        }
    }


    private fun changePasswordApi() {
        val changePasswordApi : Call<BasicResponse> = getService().changePassword(getHeader(), edt_old_pass.text.toString(), edt_new_pass.text.toString(), edt_confirm_pass.text.toString())
        changePasswordApi.enqueue(RestProcess(this, this, GlobalVariables.SERVICE_MODE.CHANGE_PASSWORD, true))
    }

    override fun onResponse(call: Call<*>, response: Response<*>, serviceMode: Int) {
        showToast(R.string.password_change_successfully)
        activitySwitcher(HomeActivity::class.java, true)
        finish()
    }

    override fun onFailure(call: Call<*>, t: Throwable, serviceMode: Int) {

    }

}
