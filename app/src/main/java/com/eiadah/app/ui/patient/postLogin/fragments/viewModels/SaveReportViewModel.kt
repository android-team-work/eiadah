package com.eiadah.app.ui.patient.postLogin.fragments.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.models.requestModels.SaveReportRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SaveReportViewModel : ObservableViewModel() {

    private var saveReportResponse : MutableLiveData<RestObservable> = MutableLiveData()

    fun getSaveReport() : LiveData<RestObservable> {
        return saveReportResponse
    }

    fun saveReport(request: SaveReportRequest) {
        getService().saveReport(getHeader(), request.result111,request.symptoms,request.name,request.age,request.gender,request.weight)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { saveReportResponse.value = RestObservable.loading() }
            .subscribe(
                {saveReportResponse.value = RestObservable.success(it)},
                {saveReportResponse.value = RestObservable.error(it)}
            )
    }


}