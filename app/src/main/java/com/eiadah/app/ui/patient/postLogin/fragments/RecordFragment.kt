package com.eiadah.app.ui.patient.postLogin.fragments


import android.content.Context
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager

import com.eiadah.app.R
import com.eiadah.app.databinding.FragmentRecordBinding
import com.eiadah.app.models.dataModel.ReportListData
import com.eiadah.app.models.responseModel.ReportListResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.RestProcess
import com.eiadah.app.restAPI.RestProcess.Companion.progressDialog
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.activity.RecordsActivity
import com.eiadah.app.ui.patient.postLogin.activity.UploadedReportActivity
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.ReportListViewModel
import com.eiadah.app.ui.patient.postLogin.dialogs.ChooseFilePickerDialog
import com.myapplication.adapter.RecordAdapter

/**
 * A simple [Fragment] subclass.
 *
 */
class RecordFragment : Fragment() {

    lateinit var mListener : HomeFragment.HomeFragmentListener

    lateinit var adapter: RecordAdapter
    lateinit var reportListData:ArrayList<ReportListData>
    lateinit var binding : FragmentRecordBinding
    private val viewModel: ReportListViewModel
            by lazy {  ViewModelProviders.of(this).get(ReportListViewModel::class.java) }

    companion object {
        fun newInstance() = RecordFragment()
        val TAG : String = RecordFragment::class.java.simpleName
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_record, container, false)

        binding.clickhandler = this

//        viewModel.getReportlist().observe(this,this)
//        viewModel.reportList()

        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is HomeFragment.HomeFragmentListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement HomeFragmentListener")
        }
    }

    fun onMenuClick() {
        mListener.handleDrawer()
    }

    fun reportClick() {

        val intent=Intent(activity,RecordsActivity::class.java)
        startActivity(intent)
    }
        fun uplodedreportClick() {

        val intent=Intent(activity,UploadedReportActivity::class.java)
        startActivity(intent)
    }

//    fun onUploadClick() {
//        val chooseFileDialog : ChooseFilePickerDialog? = activity?.let { ChooseFilePickerDialog(it, this) }
//        chooseFileDialog?.show()
//    }
//
//    override fun onTakeCamera() {
//
//    }
//
//    override fun onChooseGallery() {
//
//    }
//
//    override fun onChooseFile() {
//
//    }
//    override fun onChanged(it: RestObservable?) {
//
//        when {
//            it!!.status == Status.SUCCESS -> {
//               progressDialog?.dismiss()
//                if (it.data is ReportListResponse) {
//
//                    val reportListResponse : ReportListResponse = it.data
//                    reportListData = (reportListResponse.getData() as ArrayList<ReportListData>?)!!
//                    val reportData = ReportListData()
//                    reportListData.add(reportData)
//
//                    if(reportListData.size==0){
//                        binding.textRec.visibility=View.VISIBLE
//                        binding.rcyRecords.visibility=View.GONE
//
//                    }
//                    else{
//                        binding.textRec.visibility=View.GONE
//                        binding.rcyRecords.visibility=View.VISIBLE
//                        adapter= RecordAdapter(activity!!,reportListData)
//                        binding.rcyRecords.setLayoutManager(LinearLayoutManager(activity))
//                        binding.rcyRecords.setAdapter(adapter)
//                    }
//
//                }
//            }
//            it.status == Status.ERROR -> progressDialog?.dismiss()
//            it.status == Status.LOADING -> progressDialog?.show()
//        }
//    }
}
