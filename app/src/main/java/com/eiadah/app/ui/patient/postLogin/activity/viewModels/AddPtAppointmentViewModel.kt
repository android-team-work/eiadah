package com.eiadah.app.ui.patient.postLogin.activity.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AddPtAppointmentViewModel : ObservableViewModel() {

    private var addPtAppointmentResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var timeSlotResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var PaymentResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var timeslot : MutableLiveData<RestObservable> = MutableLiveData()
    fun getTimeSlot() : LiveData<RestObservable> {
        return timeSlotResponse
    }

    fun getAddPtAppointment() : LiveData<RestObservable> {
        return addPtAppointmentResponse
    }

    fun getPayment() : LiveData<RestObservable> {
        return PaymentResponse
    }

    fun gettimeslot() : LiveData<RestObservable> {
        return timeslot
    }


    fun TimeSlot(id:String,date:String) {
        getService().getTimeSlot(getHeader(),id,date)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { timeSlotResponse.value = RestObservable.loading() }
            .subscribe(
                {timeSlotResponse.value = RestObservable.success(it)},
                {timeSlotResponse.value = RestObservable.error(it)}
            )
    }

    fun AddPtAppointment(request: AddAppointmenttRequest) {
        getService().AddPtAppointment(getHeader(), request.doctor_id,
           request.name, request.mobile, request.email,request.date,request.time,request.category,request.note)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addPtAppointmentResponse.value = RestObservable.loading() }
            .subscribe(
                {addPtAppointmentResponse.value = RestObservable.success(it)},
                {addPtAppointmentResponse.value = RestObservable.error(it)}
            )
    }

  fun AddPaymentResponse(request: AddAppointmenttRequest) {
        getService().paymentResponse(getHeader(), request.appointment_id, request.transaction_id,request.doctor_id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { PaymentResponse.value = RestObservable.loading() }
            .subscribe(
                {PaymentResponse.value = RestObservable.success(it)},
                {PaymentResponse.value = RestObservable.error(it)}
            )
    }
    fun loadtimeslot() {
        getService().getSpecialist(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { timeslot.value = RestObservable.loading() }
            .subscribe(
                {timeslot.value = RestObservable.success(it)},
                {timeslot.value = RestObservable.error(it)}
            )
    }

}