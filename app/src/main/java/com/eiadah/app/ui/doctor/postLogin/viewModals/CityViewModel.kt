package com.eiadah.app.ui.doctor.postLogin.viewModals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.models.requestModels.SaveTokentRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CityViewModel : ObservableViewModel() {


    private var cityResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var addcityResponse : MutableLiveData<RestObservable> = MutableLiveData()
    var selectedCities : MutableLiveData<String> =  MutableLiveData()


    fun getCityList() : LiveData<RestObservable> {
        return cityResponse
    }
    fun getAddCityList() : LiveData<RestObservable> {
        return addcityResponse
    }


    fun loadCityList(stateId : String) {
        getService().getCityList(getHeader(), stateId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { cityResponse.value = RestObservable.loading() }
            .subscribe(
                {cityResponse.value = RestObservable.success(it)},
                {cityResponse.value = RestObservable.error (it)}
            )
    }
    fun loadAddCityList(request: SaveTokentRequest) {
        getService().AddCitiez(getHeader(),request.city_name )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { cityResponse.value = RestObservable.loading() }
            .subscribe(
                {cityResponse.value = RestObservable.success(it)},
                {cityResponse.value = RestObservable.error (it)}
            )
    }

    fun getSelectedAllrgy(value : String) : LiveData<String> {
        if (selectedCities.value==null) {
            selectedCities.value = "$value, "
        } else {
            if (!selectedCities.value!!.contains(value)) {
                selectedCities.value = selectedCities.value + value + ", "
            }
        }

        return selectedCities
    }


}