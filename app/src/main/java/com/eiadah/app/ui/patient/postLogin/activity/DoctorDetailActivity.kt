package com.eiadah.app.ui.patient.postLogin.activity

import android.content.Intent
import android.net.Uri
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityDoctorDetailBinding
import com.eiadah.app.models.dataModel.CityData
import com.eiadah.app.models.dataModel.ClinicTimeData
import com.eiadah.app.models.dataModel.ClinicTiming
import com.eiadah.app.models.responseModel.DoctorProfileResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.view_modals.DocDetailsViewModel
import java.io.Serializable




class DoctorDetailActivity : BaseActivity(), Observer<RestObservable> {
    var id:String=""
    var currency_code:String=""
    lateinit var binding : ActivityDoctorDetailBinding
    private val viewModel: DocDetailsViewModel
            by lazy {  ViewModelProviders.of(this).get(DocDetailsViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding = DataBindingUtil.setContentView(this, R.layout.activity_doctor_detail)

        binding.clickHandler = this

        id=intent.getStringExtra("id")
        viewModel.getdocdetails().observe(this,this)
        viewModel.doctorDetails(id)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun onBookClick() {


        val intent=Intent(this,DoctorDetailActivity::class.java)
        startActivity(intent)
    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is DoctorProfileResponse) {
                    val doctorProfileResponse : DoctorProfileResponse = it.data

                    if (doctorProfileResponse.status!!){

                        binding.tvName.setText("Dr."+" "+doctorProfileResponse.data?.doctor?.name)
                        binding.tvAddress.setText(doctorProfileResponse.data?.doctor?.city)
                         binding.tvStu.setText(doctorProfileResponse.data?.doctor?.degree)
                         binding.tvSpecl.setText(doctorProfileResponse.data?.doctor?.specilaity)
                         binding.tvSpl.setText(doctorProfileResponse.data?.doctor?.sub_specilaity)
                         binding.tvFees.setText(doctorProfileResponse.data?.doctor?.fee)
                         binding.tvServices.setText(doctorProfileResponse.data?.doctor?.services)
                         binding.tvCurrency.setText(doctorProfileResponse.data?.doctor?.currancy_code)
                         binding.tvExp.setText(doctorProfileResponse.data?.doctor?.experience+" "+"years experience")


                        binding.contactLay.setOnClickListener(View.OnClickListener {

                            var intent = Intent(Intent.ACTION_DIAL)
                            intent.data = Uri.parse("tel:"+doctorProfileResponse.data?.doctor?.mobile);
                            startActivity(intent)
                        })
                        binding.tvBookDoc.setOnClickListener(View.OnClickListener {
                            val intent=Intent(this,PatientFormActivity::class.java)
                            val args = Bundle()
                            intent.putExtra("BUNDLE", args)
                            intent.putExtra("fees",doctorProfileResponse.data?.doctor?.fee.toString())
                            intent.putExtra("doctor_name",doctorProfileResponse.data?.doctor?.name.toString())
                            intent.putExtra("doctor_id",doctorProfileResponse.data?.doctor?.userId.toString())
                            intent.putExtra("currancy_code",doctorProfileResponse.data?.doctor?.currancy_code.toString())
                            startActivity(intent)

                        })
                    }
                }
            }
            it.status == Status.ERROR -> progressDialog.dismiss()
            it.status == Status.LOADING -> progressDialog.show()
        }

    }
}
