package com.eiadah.app.ui.doctor.postLogin.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityAddBankDetailsBinding
import com.eiadah.app.models.requestModels.AddBanktRequest
import com.eiadah.app.models.responseModel.AddAppointmentResponse
import com.eiadah.app.models.responseModel.AddBankDetailRespons
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.viewModals.AddAppointmentViewModel
import com.eiadah.app.ui.doctor.postLogin.viewModals.AddBankViewModel
import com.eiadah.app.utils.AppUtils.showToast

class AddBankDetailsActivity : BaseActivity(), Observer<RestObservable> {


    lateinit var binding:ActivityAddBankDetailsBinding
    var name:String=""
    var account_No:String=""
    var Ifsc:String=""
    var BankName:String=""
    lateinit var request:AddBanktRequest

    private val viewModel: AddBankViewModel
            by lazy {  ViewModelProviders.of(this).get(AddBankViewModel::class.java) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil. setContentView(this,R.layout.activity_add_bank_details)
        binding.clickHandler=this

        request=AddBanktRequest()
    }

    fun backOncllick() {
        finish()
    }

    private fun isValid(name : String,bank_name : String,acc_number : String, ifsc : String) : Boolean {
        var check = false

        if (name.isEmpty() && bank_name.isEmpty() &&acc_number.isEmpty() && ifsc.isEmpty()) {
            showToast(R.string.please_fill_all_fields)
        } else if (name.isEmpty()) {
            showToast("Please Enter Your Name")
        } else if (bank_name.isEmpty()) {
            showToast("Please Enter Your Bank Name")
        }  else if (acc_number.isEmpty()) {
            showToast("Please Enter Your Account Number")
        } else if (ifsc.isEmpty()) {
            showToast("Please Enter Your IFSC Code")
        } else {
            check = true
        }

        return check
    }

    fun onSubmitClick(){

        name=binding.edtAccName.text.toString()
        account_No=binding.edtAccNo.text.toString()
        Ifsc=binding.edtIfsc.text.toString()
        BankName=binding.edtBankName.text.toString()
        if (isValid(name,BankName, account_No, Ifsc)) {

            request.name = name
            request.bank_name=BankName
            request.account_number=account_No
            request.ifsc_code=Ifsc


            viewModel.AddBank(request)
            viewModel.getAddBank().observe(this, this)

        }

    }
    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is AddBankDetailRespons) {
                    val addBankDetailRespons : AddBankDetailRespons = it.data

                    if (addBankDetailRespons.status!!) {
                      finish()
                    }
                }
            }
            it.status == Status.ERROR -> {
                it.error!!.fillInStackTrace()
                progressDialog.dismiss()
            }
            it.status == Status.LOADING -> progressDialog.show()
        }
    }
}
