package com.eiadah.app.ui.patient.postLogin.fragments

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.content.SharedPreferences
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.eiadah.app.Listener.CheckupChangeFragmentListener

import com.eiadah.app.R
import com.eiadah.app.core.BaseFragment
import com.eiadah.app.databinding.FragmentCheckupStep6Binding
import com.eiadah.app.models.dataModel.CityData
import com.eiadah.app.models.dataModel.DiseaseData
import com.eiadah.app.models.dataModel.GetResultData
import com.eiadah.app.models.responseModel.CitiesResponse
import com.eiadah.app.models.responseModel.GetQuestionResponse
import com.eiadah.app.models.responseModel.GetResultResponse
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.CheckupStep6ViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.myapplication.adapter.CityListAdapter
import com.pieboat.shredpreference.SharePreferHelper
import kotlinx.android.synthetic.main.activity_reach2.*
import java.io.Serializable
import java.sql.ResultSetMetaData
import com.google.gson.Gson



class CheckupStep6Fragment : BaseFragment() {

    lateinit var binding : FragmentCheckupStep6Binding
    lateinit var mListener : CheckupChangeFragmentListener
    lateinit var diseaseList : List<DiseaseData>
    var finalQuestionList : ArrayList<List<String>> = ArrayList()
    var selectedQuestionList : ArrayList<String>? = null
    lateinit var id : String
    lateinit var questionResponse: GetQuestionResponse
     var resultList:ArrayList<GetResultData> = ArrayList()
    lateinit var sharePreferHelper: SharePreferHelper
    companion object {
        fun newInstance() = CheckupStep6Fragment()
        val TAG : String = CheckupStep6Fragment::class.java.simpleName
    }

    private val viewModel: CheckupStep6ViewModel
            by lazy {  ViewModelProviders.of(this).get(CheckupStep6ViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_checkup_step6, container, false)
        binding.clickHandler = this

        loadData()

        sharePreferHelper= SharePreferHelper(activity!!)

        Log.e("symptoms",sharePreferHelper.getString(GlobalVariables.SharePref.SYMPTOMS_NAME))
        return binding.root
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is CheckupChangeFragmentListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement CheckupChangeFragmentListener")
        }
    }

    private fun loadData() {
        id = SharedStorage.getSelectedSymptomsIds(context!!).split(", ")[0]
        progressDialog.show()
        viewModel.loadQuesuionList(id)
        viewModel.getQuestionList().observe(this, Observer {
            progressDialog.dismiss()
            questionResponse = it!!
            if (it?.status!!) {
                diseaseList = it?.diseaseList!!
                if (diseaseList.isNotEmpty()) {
                    binding.data = diseaseList[0].questions?.get(0)
                }

            }
        })
    }


    fun onYes() {
        if (diseaseList.isNotEmpty()) {
            if (selectedQuestionList==null) {
                selectedQuestionList = ArrayList()
            }
            e(TAG, "//"+diseaseList[0].questions?.get(0)?.count.toString())
            selectedQuestionList!!.add(diseaseList[0].questions?.get(0)?.count.toString())
            nextQuestion(true)
        } else {
            submitResult()
        }

    }

    fun onNo() {
        if (diseaseList.isNotEmpty()) {
            if (selectedQuestionList==null) {
                selectedQuestionList = ArrayList()
            }
            e(TAG, "Selected : "+selectedQuestionList.toString())
            selectedQuestionList?.let { finalQuestionList.add(it) }
            e(TAG, "final : $finalQuestionList")
            selectedQuestionList = null
            nextQuestion(false)
        } else {
            submitResult()
        }

    }

    private fun nextQuestion(check : Boolean) {
        viewModel.getQuestionData(check).observe(this, Observer {
            binding.data = it
        })

        viewModel.getEnd().observe(this, Observer {

            if (it!!) {
                e(TAG, finalQuestionList.toString())
                viewModel.setEnd(false)
                submitResult()
            }
        })
    }

    private fun submitResult() {
        viewModel.loadResult(SharedStorage.getCheckupId(context!!), id, questionResponse.listCount.toString(), finalQuestionList.toString())

        viewModel.getResultResponse().observe(this, Observer {
            when {
                it!!.status == Status.SUCCESS -> {
                    progressDialog.dismiss()

                    if (it.data is GetResultResponse) {
                        val getResultData : GetResultResponse = it.data
                        if (getResultData.status!!) {
                            resultList = (getResultData.result111 as ArrayList<GetResultData>?)!!
                            val cityData = GetResultData()
//                            cityData.id = "0"
//                            cityData.name = "Choose city"
                            resultList.add(cityData)
                            val fragment = CheckupStep7Fragment()
                            val fragmentManager = fragmentManager
                            val bundle = Bundle().apply {
                                putSerializable("ARRAYLIST", resultList)
                            }
//                            bundle.putSerializable("ARRAYLIST", resultList)
//
//                            bundle.putString("BUNDLE", bundle.toString())
                            fragment.arguments = bundle
                            val fragmentTransaction = fragmentManager!!.beginTransaction()
                            fragmentTransaction.replace(R.id.startCheckupContainer, fragment)
                            fragmentTransaction.commit()

                            val gson = Gson()
                            val json = gson.toJson(resultList)
                            sharePreferHelper.saveString(GlobalVariables.SharePref.RESULT_LIST, json)

                            Log.e("json",json)
                        }
                    }


//                    mListener.changeFragment(CheckupStep7Fragment.newInstance(), CheckupStep7Fragment.TAG)
                }
                it!!.status == Status.ERROR -> progressDialog.dismiss()
                it!!.status == Status.LOADING -> progressDialog.show()
            }
        })
    }
}
