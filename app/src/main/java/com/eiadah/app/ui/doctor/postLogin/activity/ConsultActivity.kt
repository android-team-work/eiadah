package com.eiadah.app.ui.doctor.postLogin.activity

import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityConsultBinding
import com.eiadah.app.ui.doctor.preLogin.activity.CreateDoctorProfileStep1Activity

class ConsultActivity : BaseActivity() {

    lateinit var binding:ActivityConsultBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil.setContentView(this,R.layout.activity_consult)
        binding.clickHandler=this
    }

    fun starttOncllick() {
        activitySwitcher(CreateDoctorProfileStep1Activity::class.java, false)
    }
    fun backOncllick() {
        finish()
    }

}
