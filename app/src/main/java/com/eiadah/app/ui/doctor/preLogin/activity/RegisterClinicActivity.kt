package com.eiadah.app.ui.doctor.preLogin.activity

import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityRegisterClinicBinding
import com.eiadah.app.models.dataModel.ClinicTimeData
import com.eiadah.app.models.requestModels.SaveClinicRequest
import com.eiadah.app.models.responseModel.DoctorProfileResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.preLogin.adapter.ClinicTimingAdapter
import com.eiadah.app.ui.doctor.preLogin.viewModels.RegisterClinicViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import java.io.Serializable
import java.util.*
import java.util.stream.Collectors.mapping
import kotlin.collections.ArrayList
import com.google.gson.GsonBuilder
import com.pieboat.shredpreference.SharePreferHelper


class RegisterClinicActivity : BaseActivity(), Observer<RestObservable> ,Serializable{


    lateinit var binding : ActivityRegisterClinicBinding
    var list : ArrayList<ClinicTimeData> = ArrayList()
    lateinit var timeAdapter : ClinicTimingAdapter
    lateinit var sharePreferHelper: SharePreferHelper
    private var strTime = GlobalVariables.Gender.TIME_1
    val viewModel : RegisterClinicViewModel
            by lazy { ViewModelProviders.of(this).get(RegisterClinicViewModel::class.java)  }
    lateinit  var request:SaveClinicRequest
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register_clinic)
        timeAdapter = ClinicTimingAdapter()
        binding.clickHandler = this
        binding.adapter = timeAdapter

        sharePreferHelper= SharePreferHelper(this)
        Log.e("tokennnnnnnn",SharedStorage.getToken(this))
         request = SaveClinicRequest()
    }

    fun onSaveClick() {

        if (isValid(binding.edtClinicName.text.toString(), binding.edtClinicAddress.text.toString(), list.size)) {

//            activitySwitcher(DocumentPendingActivity::class.java, true)

            request.clinic_name = binding.edtClinicName.text.toString()
            request.clinic_location = binding.edtClinicAddress.text.toString()
            request.timing = sharePreferHelper.getString(GlobalVariables.SharePref.CLINIC_TIME)!!
            viewModel.saveClinicResponse(request)
            viewModel.getClinicResponse().observe(this, this)
        }
    }

    fun onAddTiming() {
        activitySwitcher(AddOpeningClosingTimeActivity::class.java, 1)
    }

    fun onCheckedChanged(id : Int) {
        when(id) {
            R.id.radioButton -> {
                strTime = GlobalVariables.Gender.TIME_1
            }

            R.id.radioButton2 -> {
                strTime = GlobalVariables.Gender.TIME_2
            }
            R.id.radioButton3 -> {
                strTime = GlobalVariables.Gender.TIME_3
            }
        }
        request.time = strTime
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data!=null) {
            val gson = Gson()
            list = gson.fromJson(data.getStringExtra("list"), object : TypeToken<ArrayList<ClinicTimeData>>() {}.type)

            Log.e("timie_list",sharePreferHelper.getString(GlobalVariables.SharePref.CLINIC_TIME))
            timeAdapter.updateList(list)


            Log.e("show_list", list.size.toString())
        }
    }

    private fun isValid(name: String, location: String, size: Int): Boolean {
        var check = false
        if (name.isEmpty() && location.isEmpty() && size!=7) {
            showToast(R.string.please_fill_all_fields)
        } else if (name.isEmpty()) {
            showToast(R.string.empty_clinic_name)
        } else if (location.isEmpty()) {
            showToast(R.string.empty_clinic_address)
        } else if (size!=7) {
            showToast(R.string.empty_all_day_timing)
        } else {
            check = true
        }

        return check

    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is DoctorProfileResponse) {
                    val profileResponse : DoctorProfileResponse = it.data

                    if (profileResponse.status!!) {
                        SharedStorage.setSession(this, true)
                        SharedStorage.setUserType(this, GlobalVariables.User.DOCTOR)
                        SharedStorage.setDoctorData(this, profileResponse.data!!)
                        activitySwitcher(DocumentPendingActivity::class.java, true)
                    }
                }
            }
            it.status == Status.ERROR -> progressDialog.dismiss()
            it.status == Status.LOADING -> progressDialog.show()
        }
    }
}
