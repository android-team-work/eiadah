package com.myapplication.adapter

import android.content.Context
import android.content.Intent
import android.media.Image
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.models.dataModel.PatientListData
import com.eiadah.app.ui.doctor.postLogin.activity.AddPatientProfileActivity
import com.eiadah.app.ui.doctor.postLogin.activity.PatientDetailActivity
import com.eiadah.app.utils.GlobalVariables
import com.pieboat.shredpreference.SharePreferHelper

class PatientListAdapter(private val mContext: Context, val onItemClickListener: OnItemClickListener,var patientList:ArrayList<PatientListData>) : RecyclerView.Adapter<PatientListAdapter.ViewHolder>() {
    lateinit var sharePreferHelper: SharePreferHelper
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        sharePreferHelper= SharePreferHelper(mContext)
        p0.itemView.setOnClickListener(View.OnClickListener {
            val intent = Intent(mContext, PatientDetailActivity::class.java)
            intent.putExtra("id",  patientList.get(p1).id.toString())
            mContext?.startActivity(intent)

            sharePreferHelper.saveString(GlobalVariables.SharePref.PATIENT_ID,
                patientList.get(p1).id.toString()
            )
            Log.e("patient_id",sharePreferHelper.getString(GlobalVariables.SharePref.PATIENT_ID))
        })
        p0.optin_menu.setOnClickListener(View.OnClickListener {

            onItemClickListener.onItemClick(p1)
        })

        p0.tv_patient_name.setText(patientList.get(p1).name)

        if(patientList.get(p1).name.isNullOrEmpty()){
            p0.card_view.visibility=View.GONE
        }
        else{
            p0.card_view.visibility=View.VISIBLE

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.patient_list_itms, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return patientList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var optin_menu:ImageView
        lateinit var tv_patient_name:TextView
        lateinit var card_view:CardView
        init {

            optin_menu=itemView.findViewById(R.id.optin_menu)
            tv_patient_name=itemView.findViewById(R.id.tv_patient_name)
            card_view=itemView.findViewById(R.id.card_view)
        }

        fun onItemClick() {
            onItemClickListener.onItemClick(adapterPosition)
        }
    }




}