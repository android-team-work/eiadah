package com.eiadah.app.ui.patient.postLogin.activity.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import okhttp3.RequestBody

class SendMsgViewModel : ObservableViewModel() {

    private var sendMsgResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var getMsgResponse : MutableLiveData<RestObservable> = MutableLiveData()


    fun getSendMsg() : LiveData<RestObservable> {
        return sendMsgResponse
    }
    fun getMsg() : LiveData<RestObservable> {
        return getMsgResponse
    }


    fun loadSendMsg(sender: String, receiver: String, message: String, image: String) {
        getService().SendMsg(getHeader(),sender,receiver,message,image)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { sendMsgResponse.value = RestObservable.loading() }
            .subscribe(
                {sendMsgResponse.value = RestObservable.success(it)},
                {sendMsgResponse.value = RestObservable.error(it)}
            )
    }
//
//    fun loadSendMsg(sender: RequestBody, receiver: RequestBody, message: RequestBody, image: MultipartBody.Part) {
//        getService().SendMsg(getHeader(),sender,receiver,message,image)
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .doOnSubscribe { sendMsgResponse.value = RestObservable.loading() }
//            .subscribe(
//                {sendMsgResponse.value = RestObservable.success(it)},
//                {sendMsgResponse.value = RestObservable.error(it)}
//            )
//    }


     fun loadMsg(sender: String, receiver: String) {
        getService().getMsg(getHeader(),sender,receiver)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { getMsgResponse.value = RestObservable.loading() }
            .subscribe(
                {getMsgResponse.value = RestObservable.success(it)},
                {getMsgResponse.value = RestObservable.error(it)}
            )
    }



}