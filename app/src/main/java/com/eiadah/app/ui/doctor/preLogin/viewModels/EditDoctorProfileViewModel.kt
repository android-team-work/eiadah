package com.eiadah.app.ui.doctor.preLogin.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class EditDoctorProfileViewModel : ObservableViewModel() {

    private var countryResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var stateResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var cityResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var specialistResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var saveDoctorProfile : MutableLiveData<RestObservable> = MutableLiveData()

    fun getCountryList() : LiveData<RestObservable> {
        return countryResponse
    }

    fun getStateList() : LiveData<RestObservable> {
        return stateResponse
    }

    fun getCityList() : LiveData<RestObservable> {
        return cityResponse
    }

    fun getSpecialist() : LiveData<RestObservable> {
        return specialistResponse
    }

    fun getDoctorProfileData() : LiveData<RestObservable> {
        return saveDoctorProfile
    }

    fun loadCountryList() {
        getService().getCountryList(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { countryResponse.value = RestObservable.loading() }
            .subscribe(
                {countryResponse.value = RestObservable.success(it)},
                {countryResponse.value = RestObservable.error (it)}
            )
    }

    fun loadStateList(countryId : String) {
        getService().getStatesList(getHeader(), countryId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { stateResponse.value = RestObservable.loading() }
            .subscribe(
                {stateResponse.value = RestObservable.success(it)},
                {stateResponse.value = RestObservable.error (it)}
            )
    }

    fun loadCityList(stateId : String) {
        getService().getCityList(getHeader(), stateId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { cityResponse.value = RestObservable.loading() }
            .subscribe(
                {cityResponse.value = RestObservable.success(it)},
                {cityResponse.value = RestObservable.error (it)}
            )
    }

    fun loadSpecialist() {
        getService().getSpecialist(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { specialistResponse.value = RestObservable.loading() }
            .subscribe(
                {specialistResponse.value = RestObservable.success(it)},
                {specialistResponse.value = RestObservable.error(it)}
            )
    }

    fun saveDoctorProfile(request: SaveDoctorProfileRequest) {
        getService().saveDoctorProfile(getHeader(), request.name,
            request.country, request.state, request.city, request.specilaity, request.gender, request.degree,
            request.college, request.year, request.reg_number, request.reg_council, request.reg_year, request.experience,request.sub_specilaity,request.services,request.fee,request.currancy_code,request.symptoms)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { saveDoctorProfile.value = RestObservable.loading() }
            .subscribe(
                {saveDoctorProfile.value = RestObservable.success(it)},
                {saveDoctorProfile.value = RestObservable.error(it)}
            )
    }
}