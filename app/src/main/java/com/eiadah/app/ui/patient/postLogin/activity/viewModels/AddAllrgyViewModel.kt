package com.eiadah.app.ui.patient.postLogin.activity.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddAllergytRequest
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AddAllrgyViewModel : ObservableViewModel() {

    private var allergyResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var AddallergyResponse : MutableLiveData<RestObservable> = MutableLiveData()
    var selectedAllergy : MutableLiveData<String> =  MutableLiveData()


    fun getAllergyList() : LiveData<RestObservable> {
        return allergyResponse
    }
    fun getAddAllergy() : LiveData<RestObservable> {
        return AddallergyResponse
    }


    fun loadAllergyList(){
        getService().getAllergylist(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { allergyResponse.value = RestObservable.loading() }
            .subscribe(
                {allergyResponse.value = RestObservable.success(it)},
                {allergyResponse.value = RestObservable.error (it)}
            )
    }
    fun loadAddAllergy(request: AddAllergytRequest){
        getService().AddAllergy(getHeader(),request.title)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { allergyResponse.value = RestObservable.loading() }
            .subscribe(
                {allergyResponse.value = RestObservable.success(it)},
                {allergyResponse.value = RestObservable.error (it)}
            )
    }

    fun getSelectedAllrgy(value : String) : LiveData<String> {
        if (selectedAllergy.value==null) {
            selectedAllergy.value = "$value, "
        } else {
            if (!selectedAllergy.value!!.contains(value)) {
                selectedAllergy.value = selectedAllergy.value + value + ", "
            }
        }

        return selectedAllergy
    }

}