package com.eiadah.app.ui.patient.postLogin.fragments.viewModels


import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.databinding.Bindable
import android.util.Log
import com.eiadah.app.BR
import com.eiadah.app.models.dataModel.UserData
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody


class ProfileViewModel : ObservableViewModel() {


    private var userData : MutableLiveData<UserData> = MediatorLiveData()

    var getProfileResponse : MutableLiveData<RestObservable> = MutableLiveData()



    private var gender: String = ""

    @Bindable
    fun getName() : String? {
        return userData.value?.name
    }

    @Bindable
    fun getImage() : String? {
        return GlobalVariables.URL.IMAGE_BASE_URL+userData.value?.image
    }

    @Bindable
    fun getPhone() : String? {
        return userData.value?.phone
    }

    @Bindable
    fun getEmail() : String? {
        return userData.value?.emailId
    }

    @Bindable
    fun getDob() : String? {
        return userData.value?.dob
    }

    @Bindable
    fun getLocation() : String? {
        return userData.value?.location
    }

    @Bindable
    fun getBloodGroup() : String? {
        return userData.value?.bloodgroup
    }

    @Bindable
    fun getHeight() : String? {
        return userData.value?.height
    }

    @Bindable
    fun getWeight() : String? {
        return userData.value?.weight
    }

    @Bindable
    fun getUsername() : String? {
        return userData.value?.username
    }

    @Bindable
    fun getGender() : String? {
        return userData.value?.gender
    }

    fun setGender(gender : String) {
        this.gender = gender
        notifyPropertyChanged(BR.gender)
    }

    @Bindable
    fun isImageVisible() : Boolean {
        Log.e("Image", userData.value?.image.isNullOrEmpty().toString())
        return !userData.value?.image.isNullOrEmpty()
    }

    fun setImage(image : String) {
        this.userData.value?.image = image
        Log.e("image2", image)
        notifyPropertyChanged(BR.imageVisible)
    }

    fun getUserData() : LiveData<UserData> {
        return userData
    }

    fun setUserData(userData : UserData) {
        this.userData.value = userData
    }

    fun getProfileResponse() : LiveData<RestObservable> {
        return getProfileResponse
    }



    fun saveUserProfileData(params : HashMap<String, String>) {
        getService().updateProfile(getHeader(), params)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { getProfileResponse.value = RestObservable.loading() }
            .subscribe(
                {getProfileResponse.value = RestObservable.success(it)},
                {getProfileResponse.value = RestObservable.error (it)}
            )
    }

    fun saveUserProfileImageData(params : MultipartBody.Part) {
        getService().updateProfileImage(getHeader(), params)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { getProfileResponse.value = RestObservable.loading() }
            .subscribe (
                {getProfileResponse.value = RestObservable.success(it)},
                {getProfileResponse.value = RestObservable.error (it)}
            )
    }




}
