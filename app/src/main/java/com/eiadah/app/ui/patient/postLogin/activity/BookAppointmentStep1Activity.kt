package com.eiadah.app.ui.patient.postLogin.activity

import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityBookAppointmentStep1Binding
import com.eiadah.app.models.dataModel.CityData
import com.eiadah.app.models.dataModel.CountryData
import com.eiadah.app.models.dataModel.SpecialistData
import com.eiadah.app.models.dataModel.StateData
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.models.responseModel.*
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.preLogin.activity.RegisterClinicActivity
import com.eiadah.app.ui.doctor.preLogin.viewModels.CreateDoctorProfileViewModel
import com.eiadah.app.ui.patient.postLogin.view_modals.AppointmentViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import java.io.Serializable

class BookAppointmentStep1Activity : BaseActivity(), Observer<RestObservable> {

    private var strCountry = ""
    private var strState = ""
    private var strCity = ""
    private var strSpecialty = ""
    private var specialty = ""
    private var countryList : ArrayList<CountryData> = ArrayList()
    private var stateList : ArrayList<StateData> = ArrayList()
    private var cityList : ArrayList<CityData> = ArrayList()
    private var specialistList : ArrayList<SpecialistData> = ArrayList()
    lateinit var binding : ActivityBookAppointmentStep1Binding
    lateinit var request: SaveDoctorProfileRequest

    private val viewModel: AppointmentViewModel
            by lazy {  ViewModelProviders.of(this).get(AppointmentViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_book_appointment_step1)
        binding.clickHandler = this

        request = SaveDoctorProfileRequest()

        loadCountryData()
        loadSpecialist()
    }

    fun onContinue() {
        val intent= Intent(this,RecommendedDoctorActivity::class.java)
        intent.putExtra("specialty",specialty)
        startActivity(intent)
    }


    private fun loadCountryData() {
        viewModel.loadCountryList()
        viewModel.getCountryList().observe(this, this)
    }

    private fun loadStateData(countryId : String) {
        viewModel.loadStateList(countryId)
        viewModel.getStateList().observe(this, this)
    }

    private fun loadCityData(stateId : String) {
        viewModel.loadCityList(stateId)
        viewModel.getCityList().observe(this, this)
    }

    private fun loadSpecialist() {
        viewModel.loadSpecialist()
        viewModel.getSpecialist().observe(this, this)
    }

    private fun setCountryAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, countryList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.countryAdapter = adapter
    }

    private fun setStateAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, stateList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.stateAdapter = adapter
    }

    private fun setCityAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, cityList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.cityAdapter = adapter
    }

    private fun setSpecialistAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, specialistList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.specialityAdapter = adapter
    }

    fun onItemSelectedCountry(i : Int) {
        if (i!=0) {
            strCountry = countryList[i].name.toString()
            loadStateData(countryList[i].id.toString())
        } else {
            strCountry = ""
        }
        request.country = strCountry
    }

    fun onItemSelectedState(i : Int) {
        if (i!=0) {
            strState = stateList[i].name.toString()
            loadCityData(stateList[i].id.toString())
//            sharePreferHelper.saveString(GlobalVariables.SharePref.State_ID,stateList[i].id.toString())

            Log.e("State_id",stateList[i].id.toString())
        } else {
            strState = ""
        }
        request.state = strState
    }

    fun onItemSelectedCity(i : Int) {
        strCity = if (i!=0) {
            cityList[i].name!!
        } else {
            ""
        }

        request.city = strCity
    }

    fun onItemSelectedSpeciality(i : Int) {
        strSpecialty = if (i!=0) {
            specialistList[i].name!!
        } else {
            ""
        }
        specialty= specialistList[i].name!!

        Log.e("sssssssss0",specialty)
        request.specilaity = strSpecialty
    }
    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is CountryResponse) {
                    val countryResponse : CountryResponse = it.data
                    if (countryResponse.status!!) {
                        countryList = (countryResponse.countryList as ArrayList<CountryData>?)!!
                        val countryData = CountryData()
                        countryData.id = 0
                        countryData.name = "Choose country"
                        countryList.add(0, countryData)
                        setCountryAdapter()
                    }

                } else if (it.data is StatesResponse) {
                    val statesResponse : StatesResponse = it.data
                    if (statesResponse.status!!) {
                        stateList = (statesResponse.stateList as ArrayList<StateData>?)!!
                        val stateData = StateData()
                        stateData.id = 0
                        stateData.countryId =0
                        stateData.name = "Choose state"
                        stateList.add(0, stateData)
                        setStateAdapter()
                    }

                } else if (it.data is CitiesResponse) {
                    val citiesResponse : CitiesResponse = it.data
                    if (citiesResponse.status!!) {
                        cityList = (citiesResponse.cityList as ArrayList<CityData>?)!!
                        val cityData = CityData()
                        cityData.id = "0"
                        cityData.name = "Choose city"
                        cityList.add(0, cityData)
                        setCityAdapter()
                    }

                } else if (it.data is GetSpecialistResponse) {
                    val getSpecialistResponse : GetSpecialistResponse = it.data
                    if (getSpecialistResponse.status!!) {
                        specialistList = getSpecialistResponse.data as ArrayList<SpecialistData>

                        val specialistData = SpecialistData()
                        specialistData.id = 0
                        specialistData.name = "Choose Speciality"
                        specialistData.date = ""
                        specialistList.add(0, specialistData)
                        setSpecialistAdapter()
                    }

                }
            }
            it.status == Status.ERROR -> {
                it.error!!.fillInStackTrace()
                progressDialog.dismiss()
            }
            it.status == Status.LOADING -> progressDialog.show()
        }
    }

}
