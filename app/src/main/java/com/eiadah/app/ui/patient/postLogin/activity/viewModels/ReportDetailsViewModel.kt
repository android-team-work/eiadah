package com.eiadah.app.ui.patient.postLogin.activity.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ReportDetailsViewModel : ObservableViewModel() {

    private var reportDetailsResponse : MutableLiveData<RestObservable> = MutableLiveData()


    fun getreportDetails() : LiveData<RestObservable> {
        return reportDetailsResponse
    }


    fun reportDetails(id:String) {
        getService().reportDetail(getHeader(),id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { reportDetailsResponse.value = RestObservable.loading() }
            .subscribe(
                {reportDetailsResponse.value = RestObservable.success(it)},
                {reportDetailsResponse.value = RestObservable.error(it)}
            )
    }



}