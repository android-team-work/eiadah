package com.eiadah.app.ui.doctor.postLogin.viewModals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddPrescriptionRequest
import com.eiadah.app.models.requestModels.SaveClinicRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PatientListViewModel : ObservableViewModel() {

    var patientListResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var addPriscriptionResponse : MutableLiveData<RestObservable> = MutableLiveData()



    fun getpatientList() : LiveData<RestObservable> {
        return patientListResponse
    }

    fun getAddPrescription() : LiveData<RestObservable> {
        return addPriscriptionResponse
    }

    fun patientList() {
        getService().getpatientList(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { patientListResponse.value = RestObservable.loading() }
            .subscribe(
                {patientListResponse.value = RestObservable.success(it)},
                {patientListResponse.value = RestObservable.error (it)}
            )
    }

    fun AddAddPrescription(request: AddPrescriptionRequest) {
        getService().AddPrescription(getHeader(), request.user_id,request.name)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addPriscriptionResponse.value = RestObservable.loading() }
            .subscribe(
                {addPriscriptionResponse.value = RestObservable.success(it)},
                {addPriscriptionResponse.value = RestObservable.error(it)}
            )
    }
}