package com.eiadah.app.ui.doctor.postLogin.viewModals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddPatientRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.models.responseModel.AddPatientResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class EditPatientViewModel : ObservableViewModel() {


    private var cityResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var addPatientResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var patientDetailsResponse : MutableLiveData<RestObservable> = MutableLiveData()


    fun getCityList() : LiveData<RestObservable> {
        return cityResponse
    }


    fun getEditPatient() : LiveData<RestObservable> {
        return addPatientResponse
    }

    fun getPatientDetails() : LiveData<RestObservable> {
        return patientDetailsResponse
    }

    fun loadCityList(stateId : String) {
        getService().getCityList(getHeader(), stateId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { cityResponse.value = RestObservable.loading() }
            .subscribe(
                {cityResponse.value = RestObservable.success(it)},
                {cityResponse.value = RestObservable.error (it)}
            )
    }


    fun EditPatient(request: AddPatientRequest) {
        getService().EditPatientDetails(getHeader(), request.id,request.name,
           request.city, request.gender, request.dob,request.age,request.address,request.locality,request.pincode,request.mobile,request.email)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addPatientResponse.value = RestObservable.loading() }
            .subscribe(
                {addPatientResponse.value = RestObservable.success(it)},
                {addPatientResponse.value = RestObservable.error(it)}
            )
    }

    fun patientDetail(id : String) {
        getService().getpatientDetail(getHeader(), id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { patientDetailsResponse.value = RestObservable.loading() }
            .subscribe(
                {patientDetailsResponse.value = RestObservable.success(it)},
                {patientDetailsResponse.value = RestObservable.error (it)}
            )
    }


}