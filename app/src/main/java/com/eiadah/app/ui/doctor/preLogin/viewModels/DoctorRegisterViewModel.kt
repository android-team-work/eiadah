package com.eiadah.app.ui.doctor.preLogin.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DoctorRegisterViewModel : ObservableViewModel() {

    var registerResponse : MutableLiveData<RestObservable> = MutableLiveData()
    var profileResponse : MutableLiveData<RestObservable> = MutableLiveData()


    fun getRegisterResponse() : LiveData<RestObservable> {
        return registerResponse
    }

    fun loadReisterData(
        email : String,
        mobile : String,
        password : String,
        confirmPassword : String
    ) {
        getService().doctorRegister(email, mobile, password, confirmPassword)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { registerResponse.value = RestObservable.loading() }
            .subscribe(
                {registerResponse.value = RestObservable.success(it)},
                {registerResponse.value = RestObservable.error(it)}
            )
    }

    fun getProfileResponse() : LiveData<RestObservable> {
        return profileResponse
    }

    fun profileApi() {
        getService().getProfile(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { profileResponse.value = RestObservable.loading() }
            .subscribe(
                {profileResponse.value = RestObservable.success(it)},
                {profileResponse.value = RestObservable.error (it)}
            )
    }
}