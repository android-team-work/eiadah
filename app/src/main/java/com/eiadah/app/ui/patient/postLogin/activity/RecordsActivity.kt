package com.eiadah.app.ui.patient.postLogin.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityRecordsBinding
import com.eiadah.app.models.dataModel.PatientListData
import com.eiadah.app.models.dataModel.ReportListData
import com.eiadah.app.models.responseModel.PatientListResponse
import com.eiadah.app.models.responseModel.ReportListResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.RestProcess.Companion.progressDialog
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.viewModals.PatientListViewModel
import com.eiadah.app.ui.doctor.postLogin.viewModals.ReportViewModel
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.ReportListViewModel
import com.myapplication.adapter.PatientListAdapter
import com.myapplication.adapter.RecordAdapter
import kotlinx.android.synthetic.main.activity_patient_list.*

class RecordsActivity : BaseActivity(), Observer<RestObservable> {


    lateinit var binding:ActivityRecordsBinding

    lateinit var adapter: RecordAdapter
    lateinit var reportListData:ArrayList<ReportListData>

    private val viewModel: ReportListViewModel
            by lazy {  ViewModelProviders.of(this).get(ReportListViewModel::class.java) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_records)
        binding.clickHandler=this

        viewModel.getReportlist().observe(this,this)
        viewModel.reportList()
    }

    override fun onBackPressed(){
        finish()
    }


    override fun onChanged(it: RestObservable?) {

        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is ReportListResponse) {

                    val reportListResponse : ReportListResponse = it.data
                    reportListData = (reportListResponse.getData() as ArrayList<ReportListData>?)!!
                    val reportData = ReportListData()
                    reportListData.add(reportData)

                    if(reportListData.size==1){
                        binding.textRec.visibility=View.VISIBLE
                        binding.recordRecyclerview.visibility=View.GONE

                    }
                    else{
                        binding.textRec.visibility=View.GONE
                        binding.recordRecyclerview.visibility=View.VISIBLE
                        adapter= RecordAdapter(this,reportListData)
                        binding.recordRecyclerview.setLayoutManager(LinearLayoutManager(this))
                        binding.recordRecyclerview.setAdapter(adapter)
                    }

                }
            }
            it.status == Status.ERROR -> progressDialog?.dismiss()
            it.status == Status.LOADING -> progressDialog?.show()
        }
    }
}
