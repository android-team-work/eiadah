package com.eiadah.app.ui.patient.postLogin.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityMyAppointmentBinding
import com.eiadah.app.models.dataModel.MyAppointmentData
import com.eiadah.app.models.dataModel.NotificationListData
import com.eiadah.app.models.responseModel.MyAppointmentResponse
import com.eiadah.app.models.responseModel.NotiicationListResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.NotificationListViewModel
import com.eiadah.app.ui.patient.postLogin.view_modals.MyAppointmentViewModel
import com.myapplication.adapter.MyAppointmentAdapter
import com.myapplication.adapter.NotificationAdapter

class MyAppointmentActivity : BaseActivity(), Observer<RestObservable> {

    lateinit var binding:ActivityMyAppointmentBinding
    lateinit var adapter: MyAppointmentAdapter
    private var appointmentList : ArrayList<MyAppointmentData> = ArrayList()
    private val viewModel: MyAppointmentViewModel
            by lazy {  ViewModelProviders.of(this).get(MyAppointmentViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_my_appointment)
        binding.clickHandler=this

        viewModel.getMyAppointmentList().observe(this,this)
        viewModel.loadMyAppointmentList()
    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is MyAppointmentResponse) {
                    val myAppointmentResponse : MyAppointmentResponse = it.data
                    if (myAppointmentResponse.status!!) {
                        appointmentList = myAppointmentResponse.user as ArrayList<MyAppointmentData>
                        val notificationData=MyAppointmentData()

                        appointmentList.add(notificationData)
                        adapter=MyAppointmentAdapter(this,appointmentList)
                        binding.apptRecycle.setLayoutManager(LinearLayoutManager(this))
                        binding.apptRecycle.setAdapter(adapter)


                    }

                }

            }
            it.status == Status.ERROR -> progressDialog?.dismiss()
            it.status == Status.LOADING -> progressDialog?.show()
        }
    }

    override fun onBackPressed() {
        finish()
    }
}
