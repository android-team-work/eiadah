package com.eiadah.app.ui.patient.postLogin.fragments


import androidx.lifecycle.ViewModelProviders
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.drawerlayout.widget.DrawerLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.eiadah.app.R
import com.eiadah.app.databinding.FragmentNavigationDrawerBinding
import com.eiadah.app.models.NavMenu
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.ProfileViewModel
import com.eiadah.app.utils.SharedStorage


/**
 * NavigationDrawerFragment used in Admi Mode.
 * usage : used to navigate between screens within the App.
 * the listener is implement.
 * Dealer Navigation Drawer
 */
open class NavigationDrawerFragment : Fragment() {

    private var mNavMenuItems: NavMenu? = NavMenu.START_CHECKUP

    private lateinit var mListener: NavDrawerFgmtListener
    private lateinit var mDrawerLayout: DrawerLayout
    private lateinit var mNavigationView: View

    /*val isDrawerOpen: Boolean
        get() = mDrawerLayout != null && mDrawerLayout!!.isDrawerOpen(mNavigationView!!)*/

    private val viewModel: ProfileViewModel
            by lazy {  ViewModelProviders.of(this).get(ProfileViewModel::class.java) }

    fun isDrawerOpen(): Boolean {
        return mDrawerLayout != null && mDrawerLayout!!.isDrawerOpen(mNavigationView!!)
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var binding : FragmentNavigationDrawerBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_navigation_drawer,container , false)
        var myView : View  = binding.root
        binding.click = this

        viewModel.setUserData(SharedStorage.getUserData(context!!))
        binding.data = viewModel

        return myView
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is NavDrawerFgmtListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    fun onClick(v: View) {
        if (isDrawerOpen())
            closeDrawer()

        when (v.id) {
            R.id.navigation_header -> mListener!!.headerClicked()

            R.id.txtNavStartCheckup -> mNavMenuItems = NavMenu.START_CHECKUP

            R.id.txtNavPreviousReports -> mNavMenuItems = NavMenu.PREVIOUS_REPORTS

            R.id.txtNavMyAccount -> mNavMenuItems = NavMenu.MY_ACCOUNT

            R.id.txtNavNotifications -> mNavMenuItems = NavMenu.NOTIFICATIONS

            R.id.txtNavMyAppointment -> mNavMenuItems = NavMenu.MY_APPOINTMENT

//            R.id.txtNavHelp -> mNavMenuItems = NavMenu.HELP

            R.id.rl_logout -> mNavMenuItems = NavMenu.LOGOUT
        }


        if (mNavMenuItems != null)
            mListener.menuClicked(mNavMenuItems!!)

    }

    fun closeDrawer() {
        if (mDrawerLayout != null && isDrawerOpen()) {
            mDrawerLayout.closeDrawer(mNavigationView!!)
        }
    }

    fun openDrawer() {
        if (mDrawerLayout != null && !isDrawerOpen()) {
            mDrawerLayout.openDrawer(mNavigationView!!)
        }
    }

    fun setupDrawer(fragmentId: Int, drawerLayout: DrawerLayout) {
        mDrawerLayout = drawerLayout
        mNavigationView = activity!!.findViewById(fragmentId)
        mListener.menuClicked(NavMenu.START_CHECKUP)
    }

    fun enableDisableDrawer(isEnable: Boolean) {
        if (mDrawerLayout != null) {
            val lockMode = if (isEnable) DrawerLayout.LOCK_MODE_UNLOCKED else DrawerLayout.LOCK_MODE_LOCKED_CLOSED
            mDrawerLayout.setDrawerLockMode(lockMode)
        }
        //checkBtnStates();
    }

    fun setNavMenuItems(navMenuItems: NavMenu) {
        this.mNavMenuItems = navMenuItems
        //checkBtnStates();
    }

    private fun setBasicDetails() {

    }

    /*@Override
    public void onProfileUpdateReflect() {
        setBasicDetails();
    }*/

    interface NavDrawerFgmtListener {
        fun menuClicked(navMenuItems: NavMenu)

        fun headerClicked()
    }

    companion object {

        val TAG = "NavDrawerFgmt"

        fun newInstance(): NavigationDrawerFragment {
            val fragment = NavigationDrawerFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

}
