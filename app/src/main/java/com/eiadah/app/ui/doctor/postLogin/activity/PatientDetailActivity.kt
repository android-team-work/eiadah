package com.eiadah.app.ui.doctor.postLogin.activity

import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import androidx.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.PopupMenu
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityPatientDetailBinding
import com.eiadah.app.models.dataModel.Appointment
import com.eiadah.app.models.requestModels.AddPrescriptionRequest
import com.eiadah.app.models.responseModel.AddPrescriptionResponse
import com.eiadah.app.models.responseModel.DeletePatientResponse
import com.eiadah.app.models.responseModel.PatientProfileResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.viewModals.DeletePatientViewModel
import com.eiadah.app.ui.doctor.postLogin.viewModals.GetPatientDetailsViewModel
import com.eiadah.app.ui.patient.postLogin.activity.ChatActivity
import com.eiadah.app.utils.GlobalVariables
import com.myapplication.adapter.PtAppointmentAdapter
import com.pieboat.shredpreference.SharePreferHelper
import kotlinx.android.synthetic.main.activity_patient_detail.*
import java.util.*

class PatientDetailActivity : BaseActivity(), Observer<RestObservable> {

    lateinit var binding:ActivityPatientDetailBinding

    lateinit var adapter: PtAppointmentAdapter
    lateinit var appointment_list:ArrayList<Appointment>
    private val viewModel: DeletePatientViewModel
            by lazy {  ViewModelProviders.of(this).get(DeletePatientViewModel::class.java) }

   private val viewModel1: GetPatientDetailsViewModel
            by lazy {  ViewModelProviders.of(this).get(GetPatientDetailsViewModel::class.java) }

    lateinit var request: AddPrescriptionRequest
    var phone_num:String=""
    var email:String=""

    lateinit var sharePreferHelper: SharePreferHelper
    companion object{
        var patient_id:String=""
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_patient_detail)
        binding.clickHandler=this

        request=AddPrescriptionRequest()
        sharePreferHelper= SharePreferHelper(this)

        patient_id=intent.getStringExtra("id")
        viewModel1.patientDetail(sharePreferHelper.getString(GlobalVariables.SharePref.PATIENT_ID)!!)
        viewModel1.getPatientDetails().observe(this,this)

    }
    fun backOncllick() {
        finish()
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    fun opendialog() {

        val popup = PopupMenu(Objects.requireNonNull(this), iv_option)
        //Inflating the Popup using xml file
        popup.menuInflater.inflate(R.menu.menu_popup, popup.menu)

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.tv_view_profile ->{
                    val intent = Intent(this, AddPatientProfileActivity::class.java)
                    intent.putExtra("id",  patient_id)
                    startActivity(intent)
                }

                R.id.tv_edit_profile -> {
                    val intent = Intent(this, EditPtientDetailActivity::class.java)
                    intent.putExtra("id",  patient_id)
                    startActivity(intent)
                }

                R.id.tv_delete ->{

                    viewModel.deletepatient(sharePreferHelper.getString(GlobalVariables.SharePref.PATIENT_ID)!!)
                    viewModel.getDeletePatient().observe(this,this)
                }
            }
            true
        }
        popup.show() //showing popup menu

    }
    fun onplusClick() {
        val dialog = Dialog(this,R.style.Theme_Dialog)
        dialog .requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog .setContentView(R.layout.dialog_add_paitent_prescription_layout)
        dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        val window = dialog.window
        window!!.setGravity(Gravity.BOTTOM)
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

        var text_presc: TextView
        text_presc=dialog.findViewById(R.id.text_presc)
        text_presc.setOnClickListener(View.OnClickListener {
            dialog.dismiss()

            precription_dialog(patient_id)
        })
        var text_app: TextView
        text_app=dialog.findViewById(R.id.text_app)
        text_app.setOnClickListener(View.OnClickListener {
            dialog.dismiss()

            val intent = Intent(this, AddAppointmentActivity::class.java)
            intent.putExtra("id",patient_id)
            startActivity(intent)
        })
        var img_cross: ImageView
        img_cross=dialog.findViewById(R.id.img_cross)
        img_cross.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
        })
        dialog .show()

    }
    fun onTextClick() {
        activitySwitcher(AppointmentDetailActivity::class.java, false)
    }

    fun onphone_click(){
        var intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:"+phone_num);
        startActivity(intent)

    }
    fun onmail_click(){
//        val intent = Intent(this, ChatActivity::class.java)
//        intent.putExtra("id",  patient_id)
//        startActivity(intent)


        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + email))
            intent.putExtra(Intent.EXTRA_SUBJECT, "")
            intent.putExtra(Intent.EXTRA_TEXT, "")
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            //TODO smth
        }


    }
    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is PatientProfileResponse) {
                    val patientDetailsViewModel : PatientProfileResponse = it.data

//                    appointment_list= patientDetailsViewModel.AppointmentData().getAppointment() as ArrayList<Appointment>
                    if (patientDetailsViewModel.status!!) {

                        tv_pt_name.setText(patientDetailsViewModel.data?.name)
                        phone_num=patientDetailsViewModel.data?.phone!!
                        email=patientDetailsViewModel.data?.email!!

                        appointment_list = (patientDetailsViewModel.getData()?.appointmentData?.getAppointment() as ArrayList<Appointment>?)!!
                        val appointmentData = Appointment()
//
                        appointment_list.add(appointmentData)

                        adapter= PtAppointmentAdapter(this,appointment_list)
                        binding.ptAptRecyclerview.setLayoutManager(LinearLayoutManager(this))
                        binding.ptAptRecyclerview.setAdapter(adapter)

                    }
                }else if (it.data is DeletePatientResponse) {
                    val deletePatientResponse : DeletePatientResponse = it.data

                    if (deletePatientResponse.status!!) {
                        activitySwitcher(HomeActivity::class.java, true)
                    }
                }

                else   if (it.data is AddPrescriptionResponse) {
                    val addPrescriptionResponse : AddPrescriptionResponse = it.data

                    if (addPrescriptionResponse.status!!) {

                    }
                }
            }
            it.status == Status.ERROR -> {
                it.error!!.fillInStackTrace()
                progressDialog.dismiss()
            }
            it.status == Status.LOADING -> progressDialog.show()
        }
    }


    fun precription_dialog(id:String){
        val dialog = Dialog(this,R.style.Theme_Dialog)
        dialog .requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog .setContentView(R.layout.add_prescribe)
        dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        val window = dialog.window
        window!!.setGravity(Gravity.BOTTOM)
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

        var edtPrescribe: EditText
        edtPrescribe=dialog.findViewById(R.id.edtPrescribe)

        request.user_id=id
        request.name=edtPrescribe.text.toString()
        var btn_submit: Button
        btn_submit=dialog.findViewById(R.id.btn_submit)
        btn_submit.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            viewModel.AddAddPrescription(request)
            viewModel.getAddPrescription().observe(this,this)

        })
        var btn_cancel: Button
        btn_cancel=dialog.findViewById(R.id.btn_cancel)
        btn_cancel.setOnClickListener(View.OnClickListener {
            dialog.dismiss()

        })
        dialog .show()

    }
}
