package com.eiadah.app.ui.doctor.preLogin.activity

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.widget.ArrayAdapter
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityCreateDoctorProfileStep1Binding
import com.eiadah.app.models.responseModel.*
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.preLogin.viewModels.CreateDoctorProfileViewModel
import com.eiadah.app.utils.GlobalVariables
import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.AdapterView
import com.eiadah.app.models.dataModel.*
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.utils.SharedStorage
import com.mukesh.countrypicker.Country
import com.mukesh.countrypicker.CountryPicker
import com.mukesh.countrypicker.listeners.OnCountryPickerListener
import com.pieboat.shredpreference.SharePreferHelper
import com.mynameismidori.currencypicker.CurrencyPicker



class CreateDoctorProfileStep1Activity : BaseActivity(), Observer<RestObservable> {


    lateinit var binding : ActivityCreateDoctorProfileStep1Binding

    private var strCountry = ""
    private var strState = ""
    private var strCity = ""
    private var strSpecialty = ""
    private var strSpecialtyId = ""
    private var strName = ""
    private var strMobile = ""
    private var strGender = GlobalVariables.Gender.MALE
    private var strEduction = ""
    private var strRegistration = ""
    private var strExperience = ""
    private var strDegree = ""
    private var strCollege = ""
    private var strYear = ""
    private var strRegNumber = ""
    private var strRegCouncil = ""
    private var strRegYear = ""
    private var strMobile_Code = ""
    private var strFees_Code = ""

    lateinit var sharePreferHelper: SharePreferHelper
    private val viewModel: CreateDoctorProfileViewModel
            by lazy {  ViewModelProviders.of(this).get(CreateDoctorProfileViewModel::class.java) }

    private var countryList : ArrayList<CountryData> = ArrayList()
    private var stateList : ArrayList<StateData> = ArrayList()
    private var cityList : ArrayList<CityData> = ArrayList()
    private var specialistList : ArrayList<SpecialistData> = ArrayList()
    private var subspecialistList : ArrayList<SubSpecialityData> = ArrayList()
    private var servicesList : ArrayList<ServiceListData> = ArrayList()
    private var symptomsList : ArrayList<SymptomsListData> = ArrayList()

    private val EDUCATION = 1
    private val REGISTRATION = 2

    lateinit var request: SaveDoctorProfileRequest

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_doctor_profile_step1)
        binding.clickHandler = this

        request = SaveDoctorProfileRequest()
        request.gender = strGender

        loadCountryData()
        loadSpecialist()
        loadServicelist()
        loadSymptomslist()
        sharePreferHelper= SharePreferHelper(this)
    }

    override fun onResume() {
        super.onResume()
        hideKeyboard()
    }

    fun onSaveClick() {
        strName = binding.edtDoctorName.text.toString()
        strMobile = binding.edtDoctorMobile.text.toString()
        strEduction = binding.edtEducationQualification.text.toString()
        strRegistration = binding.edtRegistrationDetails.text.toString()
        strExperience = binding.edtYoursExperience.text.toString()
        strMobile_Code = binding.edtDoctorMobileCode.text.toString()
        strFees_Code = binding.edtDoctorfees.text.toString()

        if (isValid()) {
            request.name = strName
            request.mobile=strMobile_Code+strMobile
            request.experience = binding.edtYoursExperience.text.toString()
            request.fee=binding.edtFees.text.toString()
            request.currancy_code=strFees_Code
            viewModel.saveDoctorProfile(request)
            viewModel.getDoctorProfileData().observe(this, this)
            //activitySwitcher(CreateDoctorProfileStep2Activity::class.java, true)
        }
    }

    fun onEductionClick() {
        activitySwitcher(AddEductionActivity::class.java, EDUCATION)
    }

    fun onRegistrationClick() {
        activitySwitcher(AddRegistrationDetailActivity::class.java, REGISTRATION)
    }

    private fun loadCountryData() {
        viewModel.loadCountryList()
        viewModel.getCountryList().observe(this, this)
    }

    private fun loadStateData(countryId : String) {
        viewModel.loadStateList(countryId)
        viewModel.getStateList().observe(this, this)
    }

    private fun loadCityData(stateId : String) {
        viewModel.loadCityList(stateId)
        viewModel.getCityList().observe(this, this)
    }

    private fun loadSpecialist() {
        viewModel.loadSpecialist()
        viewModel.getSpecialist().observe(this, this)
    }
    private fun loadSubSpecialist(id:String) {
        viewModel.loadSubSpecialist(id)
        viewModel.getSubSpecialist().observe(this, this)
    }

    private fun loadServicelist() {
        viewModel.loadSrviceList()
        viewModel.getServiceList().observe(this, this)
    }

    private fun loadSymptomslist() {
        viewModel.loadSymptomsList()
        viewModel.getSymptomsList().observe(this, this)
    }

    private fun setCountryAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, countryList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.countryAdapter = adapter
    }

    private fun setStateAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, stateList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.stateAdapter = adapter
    }

    private fun setCityAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, cityList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.cityAdapter = adapter
    }

    private fun setSpecialistAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, specialistList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.specialityAdapter = adapter
    }
       private fun setSubSpecialistAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, subspecialistList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.subspecialityAdapter = adapter
    }
    private fun setServicelistAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, servicesList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.servicesAdapter = adapter
    }
    private fun setSymptomslistAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, symptomsList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.symptomsAdapter = adapter
    }

    fun onItemSelectedCountry(i : Int) {
        if (i!=0) {
            strCountry = countryList[i].name.toString()
            loadStateData(countryList[i].id.toString())
        } else {
            strCountry = ""
        }
        request.country = strCountry
    }

    fun onItemSelectedState(i : Int) {
        if (i!=0) {
            strState = stateList[i].name.toString()
            loadCityData(stateList[i].id.toString())
            sharePreferHelper.saveString(GlobalVariables.SharePref.State_ID,stateList[i].id.toString())

            Log.e("State_id",stateList[i].id.toString())
        } else {
            strState = ""
        }
        request.state = strState
    }

    fun onItemSelectedCity(i : Int) {
        strCity = if (i!=0) {
            cityList[i].name!!
        } else {
            ""
        }

        request.city = strCity
    }

    fun onItemSelectedSpeciality(i : Int) {
        strSpecialty = if (i!=0) {
            strSpecialtyId= specialistList[i].id.toString()
            loadSubSpecialist(specialistList[i].id.toString())
            specialistList[i].name!!


        } else {
            ""
        }

        request.specilaity = strSpecialty
        request.specilaity_id = strSpecialtyId
    }

    fun onItemSelectedSpeciality(adapterView: AdapterView<*>, view : View, i : Int, l : Long) {
        if (i!=0) {
            setSelectedubspecialityy(subspecialistList[i].title!!)
        }

    }

    fun onItemSelectedServiceList(adapterView: AdapterView<*>, view : View, i : Int, l : Long) {
        if (i!=0) {
            setSelectedServices(servicesList[i].title!!)
        }

    }
    fun onItemSelectedSymptomsList(adapterView: AdapterView<*>, view : View, i : Int, l : Long) {
        if (i!=0) {
            setSelectedSymptoms(symptomsList[i].name!!)
        }

    }

//    fun onItemSelectedSubpeciality(i : Int) {
//       if (i!=0) {
////           strSubSpecialty =
//            subspecialistList[i].title!!
////            specialistList[i].id!!
//        } else {
//            ""
//        }
//
//        request.sub_specilaity = strSubSpecialty
////        request.id = strSpecialtyId
//    }

//
//    fun onItemSelectedServiceList(i : Int) {
//        if (i!=0) {
//            strService = servicesList[i].title.toString()
//        } else {
//            strService = ""
//        }
//        request.services = strService
//    }
//   fun onItemSelectedSymptomsList(i : Int) {
//        if (i!=0) {
//            strSymptoms = symptomsList[i].name.toString()
//        } else {
//            strSymptoms = ""
//        }
//        request.symptoms = strSymptoms
//    }

    fun onCheckedChanged(id : Int) {
        when(id) {
            R.id.radioButtonMale -> {
                strGender = GlobalVariables.Gender.MALE
                /*binding.radioButtonMale.setTextColor(ContextCompat.getColorStateList(this, R.color.white))
                binding.radioButtonFemale.setTextColor(ContextCompat.getColorStateList(this, R.color.black))*/
            }

            R.id.radioButtonFemale -> {
                strGender = GlobalVariables.Gender.FEMALE
                /*binding.radioButtonMale.setTextColor(ContextCompat.getColorStateList(this, R.color.black))
                binding.radioButtonFemale.setTextColor(ContextCompat.getColorStateList(this, R.color.white))*/
            }
        }
        request.gender = strGender
    }

    private fun isValid() : Boolean {
        var check = false

        if (strName.isEmpty() && strCountry.isEmpty() && strState.isEmpty() && strCity.isEmpty() && strSpecialty.isEmpty()
            && strGender.isEmpty() && strEduction.isEmpty() && strRegistration.isEmpty() && strExperience.isEmpty()) {
            showToast(R.string.please_fill_all_fields)
        } else if (strName.isEmpty()) {
            showToast(R.string.empty_name)
        } else if (strCountry.isEmpty()) {
            showToast(R.string.please_choose_country)
        } else if (strState.isEmpty()) {
            showToast(R.string.please_choose_state)
        } else if (strCity.isEmpty()) {
            showToast(R.string.please_choose_city)
        } else if (strSpecialty.isEmpty()) {
            showToast(R.string.please_choose_specialty)
        } else if (strGender.isEmpty()) {
            showToast(R.string.please_choose_gender)
        } else if (strEduction.isEmpty()) {
            showToast(R.string.empty_eduction)
        } else if (strRegistration.isEmpty()) {
            showToast(R.string.empty_registration)
        } else if (strExperience.isEmpty()) {
            showToast(R.string.empty_experience)
        } else {
            check = true
        }

        return check
    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is CountryResponse) {
                    val countryResponse : CountryResponse = it.data
                    if (countryResponse.status!!) {
                        countryList = (countryResponse.countryList as ArrayList<CountryData>?)!!
                        val countryData = CountryData()
                        countryData.id = 0
                        countryData.name = "Choose country"
                        countryList.add(0, countryData)
                        setCountryAdapter()
                    }

                } else if (it.data is StatesResponse) {
                    val statesResponse : StatesResponse = it.data
                    if (statesResponse.status!!) {
                        stateList = (statesResponse.stateList as ArrayList<StateData>?)!!
                        val stateData = StateData()
                        stateData.id = 0
                        stateData.countryId =0
                        stateData.name = "Choose state"
                        stateList.add(0, stateData)
                        setStateAdapter()
                    }

                } else if (it.data is CitiesResponse) {
                    val citiesResponse : CitiesResponse = it.data
                    if (citiesResponse.status!!) {
                        cityList = (citiesResponse.cityList as ArrayList<CityData>?)!!
                        val cityData = CityData()
                        cityData.id = "0"
                        cityData.name = "Choose city"
                        cityList.add(0, cityData)
                        setCityAdapter()
                    }

                } else if (it.data is GetSpecialistResponse) {
                    val getSpecialistResponse : GetSpecialistResponse = it.data
                    if (getSpecialistResponse.status!!) {
                        specialistList = getSpecialistResponse.data as ArrayList<SpecialistData>

                        val specialistData = SpecialistData()
                        specialistData.id = 0
                        specialistData.name = "Choose Speciality"
                        specialistData.date = ""
                        specialistList.add(0, specialistData)
                        setSpecialistAdapter()
                    }

                }else if (it.data is SubSpecilityResponse) {
                    val getSpecialistResponse : SubSpecilityResponse = it.data
                    if (getSpecialistResponse.status!!) {
                        subspecialistList = getSpecialistResponse.data as ArrayList<SubSpecialityData>

                        val specialistData = SubSpecialityData()
                        specialistData.id = 0
                        specialistData.title = "Choose Speciality"
//                        specialistData.date = ""
                        subspecialistList.add(0, specialistData)
                        setSubSpecialistAdapter()
                    }

                }else if (it.data is ServicDataResponse) {
                    val serviceListData : ServicDataResponse = it.data
                    if (serviceListData.status!!) {
                        servicesList = serviceListData.data as ArrayList<ServiceListData>

                        val serviceData = ServiceListData()
                        serviceData.id = 0
                        serviceData.title = "Choose Services"
//                        specialistData.date = ""
                        servicesList.add(0, serviceData)
                        setServicelistAdapter()
                    }

                } else if (it.data is SymptomsListResponse) {
                    val symptomsResponse : SymptomsListResponse = it.data
                    if (symptomsResponse.status!!) {
                        symptomsList = symptomsResponse.data as ArrayList<SymptomsListData>

                        val symptomsListData = SymptomsListData()
                        symptomsListData.id = 0
                        symptomsListData.name = "Choose Symptoms"
//                        specialistData.date = ""
                        symptomsList.add(0, symptomsListData)
                        setSymptomslistAdapter()
                    }

                } else if (it.data is DoctorProfileResponse) {
                    val doctorProfileResponse : DoctorProfileResponse = it.data

                    if (doctorProfileResponse.status!!) {
                        SharedStorage.setDoctorData(this, doctorProfileResponse.data!!)
                        activitySwitcher(RegisterClinicActivity::class.java, true)
                    }
                }
            }
            it.status == Status.ERROR -> {
                it.error!!.fillInStackTrace()
                progressDialog.dismiss()
            }
            it.status == Status.LOADING -> progressDialog.show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == EDUCATION && resultCode == Activity.RESULT_OK && data != null) {
            strDegree = data.getStringExtra(GlobalVariables.PARAMS.DoctorProfile.DEGREE)
            strCollege = data.getStringExtra(GlobalVariables.PARAMS.DoctorProfile.COLLEGE)
            strYear = data.getStringExtra(GlobalVariables.PARAMS.DoctorProfile.YEAR)

            binding.edtEducationQualification.text = strDegree

            request.degree = strDegree
            request.college = strCollege
            request.year = strYear

        } else if (requestCode == REGISTRATION && resultCode == Activity.RESULT_OK && data != null) {
            strRegNumber = data.getStringExtra(GlobalVariables.PARAMS.DoctorProfile.REG_NUMBER)
            strRegCouncil = data.getStringExtra(GlobalVariables.PARAMS.DoctorProfile.REG_COUNCIL)
            strRegYear = data.getStringExtra(GlobalVariables.PARAMS.DoctorProfile.REG_YEAR)
            binding.edtRegistrationDetails.text = strRegNumber
            request.reg_number = strRegNumber
            request.reg_council = strRegCouncil
            request.reg_year = strRegYear
        }
    }


    private fun setSelectedubspecialityy(value : String) {
        viewModel.getSelectedubspeciality(value).observe(this, Observer {
            binding.selectedSubSpeciality = it
            request.sub_specilaity = it!!
//            SharedStorage.setCheckupDetail(this, saveCheckupDetailsRequest)
        })
    }

    private fun setSelectedServices(value : String) {
        viewModel.getSelectedServices(value).observe(this, Observer {
            binding.selectedServices = it
            request.services = it!!
//            SharedStorage.setCheckupDetail(this, saveCheckupDetailsRequest)
        })
    }
    private fun setSelectedSymptoms(value : String) {
        viewModel.getSelectedSymptoms(value).observe(this, Observer {
            binding.selectedSymptoms = it
            request.symptoms = it!!
//            SharedStorage.setCheckupDetail(this, saveCheckupDetailsRequest)
        })
    }

    fun showcountrypickerdialog() {

        var builder = CountryPicker.Builder().with(this).listener(object :
            OnCountryPickerListener {

            override fun onSelectCountry(country: Country) {
                binding.edtDoctorMobileCode.setText(country.dialCode)
            }
        })
        var picker = builder.build()
        picker.showDialog(this);

    }
    fun showCurrencypickerdialog() {

        val picker = CurrencyPicker.newInstance("Select Currency")  // dialog title
        picker.setListener { name, code, symbol, flagDrawableResID ->
            binding.edtDoctorfees.setText(code)
            picker.dismiss()

            // Implement your code here
        }
        picker.show(supportFragmentManager, "CURRENCY_PICKER")
//        val picker = CurrencyPicker.newInstance("Select Currency")  // dialog title
//        picker.setListener { name, code, symbol, flagDrawableResID ->
//
//            @Override
//            public void onSelectCurrency(String name, String code, String symbol, int flagDrawableResID) {
//                // Implement your code here
//            }
//            binding.edtDoctorfees.setText(code)
//            // Implement your code here
//        }
//        picker.show(supportFragmentManager, "CURRENCY_PICKER")
    }
 }
