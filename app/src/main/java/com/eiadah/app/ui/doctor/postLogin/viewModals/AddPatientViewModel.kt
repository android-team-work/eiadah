package com.eiadah.app.ui.doctor.postLogin.viewModals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddPatientRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.models.responseModel.AddPatientResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AddPatientViewModel : ObservableViewModel() {


    private var cityResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var addPatientResponse : MutableLiveData<RestObservable> = MutableLiveData()


    fun getCityList() : LiveData<RestObservable> {
        return cityResponse
    }


    fun getAddPatient() : LiveData<RestObservable> {
        return addPatientResponse
    }

    fun loadCityList(stateId : String) {
        getService().getCityList(getHeader(), stateId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { cityResponse.value = RestObservable.loading() }
            .subscribe(
                {cityResponse.value = RestObservable.success(it)},
                {cityResponse.value = RestObservable.error (it)}
            )
    }


    fun AddPatient(request: AddPatientRequest) {
        getService().AddPatient(getHeader(), request.name,
           request.city, request.gender, request.dob,request.age,request.address,request.locality,request.pincode,request.mobile,request.email)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addPatientResponse.value = RestObservable.loading() }
            .subscribe(
                {addPatientResponse.value = RestObservable.success(it)},
                {addPatientResponse.value = RestObservable.error(it)}
            )
    }
}