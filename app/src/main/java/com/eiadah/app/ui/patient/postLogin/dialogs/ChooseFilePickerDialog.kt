package com.eiadah.app.ui.patient.postLogin.dialogs

import android.app.Activity
import android.app.Dialog
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import com.eiadah.app.R
import com.eiadah.app.databinding.LayoutChooseFileOptionBinding
import android.view.LayoutInflater



class ChooseFilePickerDialog(activity: Activity, onChooseFilePickerDialogListener: OnChooseFilePickerDialogListener) : Dialog(activity) {

    val activity = activity
    private val onChooseFilePickerDialogListener = onChooseFilePickerDialogListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding : LayoutChooseFileOptionBinding = DataBindingUtil.inflate(inflater, R.layout.layout_choose_file_option, null, false)

        setContentView(binding.root)

        var window : Window = window
        window.setGravity(Gravity.BOTTOM)
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window.setBackgroundDrawableResource(android.R.color.transparent)
        window.attributes.windowAnimations = R.style.DialogAnimation

        binding.clickhandler = this
    }

    fun onTakeCamera() {
        dismiss()
        onChooseFilePickerDialogListener.onTakeCamera()
    }

    fun onChooseGallery() {
        dismiss()
        onChooseFilePickerDialogListener.onChooseGallery()
    }

    fun onChooseFile() {
        dismiss()
        onChooseFilePickerDialogListener.onChooseFile()
    }

    interface OnChooseFilePickerDialogListener {
        fun onTakeCamera()
        fun onChooseGallery()
        fun onChooseFile()
    }
}