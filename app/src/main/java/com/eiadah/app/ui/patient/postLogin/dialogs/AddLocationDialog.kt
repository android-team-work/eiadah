package com.eiadah.app.ui.patient.postLogin.dialogs

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import android.view.Gravity
import android.view.LayoutInflater
import android.view.Window
import com.eiadah.app.Listener.UpdateProfileData
import com.eiadah.app.R
import com.eiadah.app.core.BaseDialog
import com.eiadah.app.databinding.DialogAddLocationBinding
import com.eiadah.app.databinding.DialogAddWeightBinding
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.ProfileViewModel
import com.eiadah.app.utils.GlobalVariables

class AddLocationDialog  (activity: FragmentActivity, private val updateData: UpdateProfileData) : BaseDialog(activity),
    LifecycleOwner {

    private val profileViewModel: ProfileViewModel
            by lazy {
                ViewModelProviders.of(activity).get(ProfileViewModel::class.java)
            }

    lateinit var binding: DialogAddLocationBinding
    lateinit var lifecycleRegistry: LifecycleRegistry

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_location, null, false)

        setContentView(binding.root)

        val window = window
        window?.setGravity(Gravity.CENTER)
        //window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        window?.attributes?.windowAnimations = R.style.DialogAnimation

        lifecycleRegistry = LifecycleRegistry(this)
        binding.clickHandler = this
        binding.data = profileViewModel
        binding.lifecycleOwner = this

    }

    override fun getLifecycle(): Lifecycle {
        return lifecycleRegistry
    }

    fun onSubmit() {
        if (binding.edtloc.text.toString().isNullOrEmpty()) {
            showToast("Please Enter Location")
        } else {
            updatelocation(binding.edtloc.text.toString())
        }
    }

    fun onCancel() {
        dismiss()
    }

    private fun updatelocation(location : String) {
        var params : HashMap<String, String> = HashMap()
        params[GlobalVariables.PARAMS.PROFILE.LOCATION] = location
        updateData.updateProfileData(params)
        dismiss()
    }

}