package com.eiadah.app.ui.patient.postLogin.activity

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityAddAllergyBinding
import com.eiadah.app.models.dataModel.AddAllergyData
import com.eiadah.app.models.requestModels.AddAllergytRequest
import com.eiadah.app.models.responseModel.AddAllergy_Response
import com.eiadah.app.models.responseModel.AddAlrgyResponse
import com.eiadah.app.models.responseModel.AddSurgeryResponse
import com.eiadah.app.models.responseModel.ProfileResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.RestProcess.Companion.progressDialog
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.AddAllrgyViewModel
import com.eiadah.app.ui.patient.postLogin.view_modals.AppointmentViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage

class Add_AllergyActivity : BaseActivity(), Observer<RestObservable> {


    private var alrgyList : ArrayList<AddAllergyData> = ArrayList()
    private var strAllergy = ""
    lateinit var  request:AddAllergytRequest
    lateinit var binding:ActivityAddAllergyBinding
    private val viewModel: AddAllrgyViewModel
            by lazy {  ViewModelProviders.of(this).get(AddAllrgyViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_add__allergy)
        binding.clickHandler=this

        request= AddAllergytRequest()
        loadAllergyData()
    }

    private fun loadAllergyData() {
        viewModel.loadAllergyList()
        viewModel.getAllergyList().observe(this, this)
    }

    private fun setAllergyAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, alrgyList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.alergyAdapter = adapter
    }


    fun onItemSelectedAlergy(adapterView: AdapterView<*>, view : View, i : Int, l : Long) {
        if (i!=0) {
            setSelectedAllergy(alrgyList[i].title!!)
        }

    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                 if (it.data is AddAllergy_Response) {
                    val addallergyResponse : AddAllergy_Response = it.data
                    if (addallergyResponse.status!!) {
                        alrgyList = addallergyResponse.data as ArrayList<AddAllergyData>

                        val addAllergyData = AddAllergyData()
                        addAllergyData.title = "0"
                        addAllergyData.title = "Choose Allergy"
                        alrgyList.add(0, addAllergyData)
                        setAllergyAdapter()
                    }

                }
                else if (it.data is AddAlrgyResponse) {
                    val addallergyResponse : AddAlrgyResponse = it.data
                    if (addallergyResponse.status!!) {

                        val returnIntent = Intent()
                        returnIntent.putExtra(GlobalVariables.PARAMS.DoctorProfile.Allergy_NAME, strAllergy)
                        setResult(Activity.RESULT_OK, returnIntent)
                        finish()
                    }

                }

            }
            it.status == Status.ERROR -> progressDialog?.dismiss()
            it.status == Status.LOADING -> progressDialog?.show()
        }
    }

    override fun onBackPressed(){
        finish()
    }

    fun onSaveClick() {
        viewModel.getAddAllergy().observe(this,this)
        viewModel.loadAddAllergy(request)
    }


    private fun setSelectedAllergy(value : String) {
        viewModel.getSelectedAllrgy(value).observe(this, Observer {
            binding.selectedAllergy = it
            request.title = it!!
        })
    }

}
