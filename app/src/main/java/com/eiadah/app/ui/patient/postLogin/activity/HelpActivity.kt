package com.eiadah.app.ui.patient.postLogin.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.eiadah.app.R
import com.eiadah.app.databinding.ActivityHelpBinding

class HelpActivity : AppCompatActivity() {

    lateinit var binding:ActivityHelpBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_help)
        binding.clickHandler=this
    }

    override fun onBackPressed(){
        finish()
    }
}
