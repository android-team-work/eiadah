package com.myapplication.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.models.dataModel.AppontmentListData
import com.eiadah.app.models.dataModel.MyAppointmentData
import com.eiadah.app.ui.doctor.postLogin.activity.AppointmentDetailActivity
import com.eiadah.app.ui.doctor.postLogin.activity.PatientDetailActivity
import com.eiadah.app.ui.patient.postLogin.activity.ChatActivity
import com.eiadah.app.utils.GlobalVariables
import java.util.ArrayList

class MyAppointmentAdapter(private val mContext: Context, var appointmentList: ArrayList<MyAppointmentData>) : RecyclerView.Adapter<MyAppointmentAdapter.AppointmentViewHolder>() {
    override fun onBindViewHolder(p0: AppointmentViewHolder, p1: Int) {
        p0.iv_phone.setOnClickListener(View.OnClickListener {
            var intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:"+appointmentList.get(p1).drMobile);
            mContext.startActivity(intent)
        })
        p0.iv_msg.setOnClickListener(View.OnClickListener {
            var intent = Intent(mContext,ChatActivity::class.java)
            intent.putExtra("id",appointmentList.get(p1).doctor_id)
            intent.putExtra("name",appointmentList.get(p1).drName)
            mContext.startActivity(intent)
        })

        p0.tv_time.setText(appointmentList.get(p1).date)
        p0.tv_msg.setText(appointmentList.get(p1).note)
        p0.username.setText(appointmentList.get(p1).drName)

        if (appointmentList.get(p1).name.isNullOrEmpty()){
            p0.relative_lay.visibility=View.GONE
        }
        else{
            p0.relative_lay.visibility=View.VISIBLE

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppointmentViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.my_appointment_items, parent, false)
        return AppointmentViewHolder(v)
    }

    override fun getItemCount(): Int {
        return appointmentList.size
    }

    inner class AppointmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tv_time:TextView
        var tv_msg:TextView
        var username:TextView
        var relative_lay:RelativeLayout
        var contact_lay:LinearLayout
        var iv_phone:ImageView
        var iv_msg:ImageView
        init {

            tv_time=itemView.findViewById(R.id.tv_time)
            tv_msg=itemView.findViewById(R.id.tv_msg)
            username=itemView.findViewById(R.id.username)
            relative_lay=itemView.findViewById(R.id.relative_lay)
            contact_lay=itemView.findViewById(R.id.contact_lay)
            iv_phone=itemView.findViewById(R.id.iv_phone)
            iv_msg=itemView.findViewById(R.id.iv_msg)
        }
//        fun onItemClick() {
//            onItemClickListener.onItemClick(adapterPosition)
//        }
    }
}