package com.eiadah.app.ui.patient.postLogin.fragments.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.responseModel.GetSymptomsResponse
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import android.util.Log
import com.eiadah.app.models.dataModel.BodyPartData


class CheckupStep4ViewModel : ObservableViewModel() {

    var response : MutableLiveData<GetSymptomsResponse> = MutableLiveData()

    private var selectedSymptoms : MutableLiveData<String> =  MutableLiveData()
    private var selectedSymptomsIds : MutableLiveData<String> =  MutableLiveData()
    private var errorInApi : MutableLiveData<Throwable> = MutableLiveData()


    fun getSymptomsList() : LiveData<GetSymptomsResponse> {
        return response
    }

    fun getError() : LiveData<Throwable> {
        return errorInApi
    }

    fun loadSymptomList() {
        getCheckupService().getSymptomsListResponse()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::setResponse, this::onError)

    }

    private fun setResponse(getSymptomsResponse: GetSymptomsResponse) {
        Log.e("model", "model")
        this.response.value = getSymptomsResponse
    }

    private fun onError(throwable: Throwable) {
        errorInApi.value = throwable
        throwable.fillInStackTrace()
    }


    fun getSelectedSymptoms(bodyPartList : List<BodyPartData>) : LiveData<String> {
        var result = ""
        var id = ""
        for (bodyPart in bodyPartList) {
            if (bodyPart.symptoms?.isNotEmpty()!!) {
                for (symptom in bodyPart.symptoms!!) {
                    if (symptom.isChecked) {
                        id = id + symptom.id + ", "
                        result = result + symptom.name + ", "
                    }
                }
            }

        }

        selectedSymptoms.value = result
        selectedSymptomsIds.value = id

        return selectedSymptoms
    }

    fun getSelectedSymptomsIds() : LiveData<String> {
        return selectedSymptomsIds
    }


}