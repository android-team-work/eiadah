package com.eiadah.app.ui.doctor.postLogin.activity

import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityReviewDoctorProfileBinding
import com.eiadah.app.utils.SharedStorage

class DoctorProfileActivity : BaseActivity() {

    lateinit var binding : ActivityReviewDoctorProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_review_doctor_profile)
        binding.clickHandler = this

    }

    override fun onResume() {
        super.onResume()
        binding.data = SharedStorage.getDoctorData(this)

    }

    fun onbackClick(){
        finish()
    }
    fun onEditClick() {
        activitySwitcher(EditDoctorProfileActivity::class.java, false)
    }
}
