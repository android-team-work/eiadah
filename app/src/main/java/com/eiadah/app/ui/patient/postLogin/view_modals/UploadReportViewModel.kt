package com.eiadah.app.ui.patient.postLogin.view_modals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import okhttp3.RequestBody

class UploadReportViewModel: ObservableViewModel() {

    var uploadReportResponse : MutableLiveData<RestObservable> = MutableLiveData()

    fun getUploadReport() : LiveData<RestObservable> {
        return uploadReportResponse
    }

    fun saveUploadReport(title:RequestBody, proof2: MultipartBody.Part) {
        getService().uploadReport(getHeader(), title, proof2)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { uploadReportResponse.value = RestObservable.loading() }
            .subscribe({uploadReportResponse.value = RestObservable.success(it)}, {uploadReportResponse.value = RestObservable.error(it)})
    }
}