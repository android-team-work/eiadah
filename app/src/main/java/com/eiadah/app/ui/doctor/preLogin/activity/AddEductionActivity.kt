package com.eiadah.app.ui.doctor.preLogin.activity

import androidx.databinding.DataBindingUtil
import android.os.Bundle
import com.eiadah.app.R
import com.eiadah.app.databinding.ActivityAddEductionBinding
import android.app.Activity
import android.content.Intent
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.utils.GlobalVariables


class AddEductionActivity : BaseActivity() {

    lateinit var binding : ActivityAddEductionBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_eduction)
        binding.clickHandler = this

    }

    fun onSaveClick() {
        val strDegree = binding.edtDegree.text.toString()
        val strCollege = binding.edtCollegeUniversity.text.toString()
        val strYear = binding.edtYear.text.toString()

        if (isValid(strDegree, strCollege, strYear)) {
            val returnIntent = Intent()
            returnIntent.putExtra(GlobalVariables.PARAMS.DoctorProfile.DEGREE, strDegree)
            returnIntent.putExtra(GlobalVariables.PARAMS.DoctorProfile.COLLEGE, strCollege)
            returnIntent.putExtra(GlobalVariables.PARAMS.DoctorProfile.YEAR, strYear)
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
        }

    }

    private fun isValid(degree : String, college : String, year : String) : Boolean {
        var check = false

        if (degree.isEmpty() && college.isEmpty() && year.isEmpty()) {
            showToast(R.string.please_fill_all_fields)
        } else if (degree.isEmpty()) {
            showToast(R.string.empty_degree)
        } else if (college.isEmpty()) {
            showToast(R.string.empty_college)
        } else if (year.isEmpty()) {
            showToast(R.string.empty_year)
        } else {
            check = true
        }

        return check
    }
}
