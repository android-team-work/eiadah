package com.eiadah.app.ui.patient.postLogin.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.eiadah.app.R
import android.webkit.WebView
import android.webkit.WebViewClient




class WebActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)

        val view = findViewById(R.id.webView) as WebView
        view.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return false
            }
        }
        view.settings.javaScriptEnabled = true
        view.loadUrl("https://www.eiadahapp.com/")

    }
}
