package com.eiadah.app.ui.patient.postLogin.fragments.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.responseModel.CitiesResponse
import com.eiadah.app.models.responseModel.CountryResponse
import com.eiadah.app.models.responseModel.StatesResponse
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CheckupStep2ViewModel : ObservableViewModel() {

    var countryResponse : MutableLiveData<CountryResponse> = MutableLiveData()
    var stateResponse : MutableLiveData<StatesResponse> = MutableLiveData()
    var cityResponse : MutableLiveData<CitiesResponse> = MutableLiveData()
    private var gender : String = GlobalVariables.Gender.MALE
    private var countryName : String = ""
    private var stateName : String = ""
    private var cityName : String = ""

    fun setGender(value : String) {
        this.gender = value
    }

    fun getGender() : String {
        return gender
    }

    fun setCountryName(value: String) {
        this.countryName = value
    }

    fun getCountryName() : String {
        return countryName
    }

    fun setStateName(value: String) {
        this.stateName = value
    }

    fun getStateName() : String {
        return stateName
    }

    fun setCityName(value: String) {
        this.cityName = value
    }

    fun getCityName() : String {
        return cityName
    }

    fun getCountryList() : LiveData<CountryResponse> {
        return countryResponse
    }

    fun getStateList() : LiveData<StatesResponse> {
        return stateResponse
    }

    fun getCityList() : LiveData<CitiesResponse> {
        return cityResponse
    }

    fun loadCountryList() {
        getService().getCountryList(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::setCountyResponse, this::onError)
    }

    fun loadStateList(countryId : String) {
        getService().getStatesList(getHeader(), countryId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::setStateResponse, this::onError)
    }

    fun loadCityList(stateId : String) {
        getService().getCityList(getHeader(), stateId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::setCityResponse, this::onError)
    }

    private fun setCountyResponse(countryResponse: CountryResponse) {
        this.countryResponse.value = countryResponse
    }

    private fun setStateResponse(statesResponse: StatesResponse) {
        this.stateResponse.value = statesResponse
    }

    private fun setCityResponse(citiesResponse: CitiesResponse) {
        this.cityResponse.value = citiesResponse
    }

    private fun onError(throwable: Throwable) {
        throwable.fillInStackTrace()
    }
}