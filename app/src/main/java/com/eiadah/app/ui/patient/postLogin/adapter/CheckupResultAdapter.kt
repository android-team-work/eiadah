package com.eiadah.app.ui.patient.postLogin.adapter

import android.content.Context
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import com.eiadah.app.R
import com.eiadah.app.databinding.LayoutCheckupResultListItemBinding
import com.eiadah.app.models.dataModel.GetResultData

class CheckupResultAdapter(context: Context?, var resultList:ArrayList<GetResultData> ) : RecyclerView.Adapter<CheckupResultAdapter.CheckupResultViewHolder>() {

    private val context = context

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CheckupResultViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate( R.layout.layout_checkup_result_list_item, viewGroup, false)
        return CheckupResultViewHolder(v)
    }

    override fun onBindViewHolder(checkupResultViewHolder: CheckupResultViewHolder, i: Int) {
        checkupResultViewHolder.tv_disease.setText(resultList.get(i).name)
        checkupResultViewHolder.tv_total.setText(resultList.get(i).value+"%")
//        checkupResultViewHolder.progress100!!.setProgress(Integer.parseInt(resultList.get(i).value!!))

//        Log.e("Valueeee", Integer.parseInt(resultList.get(i).value!!).toString())

        if (resultList.get(i).value.isNullOrEmpty()){
            checkupResultViewHolder.tv_disease.visibility=View.GONE
            checkupResultViewHolder.tv_total.visibility=View.GONE
            checkupResultViewHolder.progress100.visibility=View.GONE
        }
        else{
            checkupResultViewHolder.tv_disease.visibility=View.VISIBLE
            checkupResultViewHolder.tv_total.visibility=View.VISIBLE
            checkupResultViewHolder.progress100.visibility=View.VISIBLE
        }
    }

    override fun getItemCount(): Int {
        return resultList.size
    }

    inner class CheckupResultViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var tv_disease:TextView
        lateinit var tv_total:TextView
        lateinit var progress100:ProgressBar
         init {

            tv_disease=itemView.findViewById(R.id.tv_disease)
            tv_total=itemView.findViewById(R.id.tv_total)
            progress100=itemView.findViewById(R.id.progress100)
        }
    }


}
