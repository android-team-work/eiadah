package com.eiadah.app.ui.doctor.postLogin.activity

import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityProfileIssueBinding
import com.eiadah.app.models.dataModel.CityData
import com.eiadah.app.models.requestModels.AddPatientRequest
import com.eiadah.app.models.responseModel.AddPatientResponse
import com.eiadah.app.models.responseModel.CitiesResponse
import com.eiadah.app.models.responseModel.ReportResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.RestProcess.Companion.progressDialog
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.viewModals.AddPatientViewModel
import com.eiadah.app.ui.doctor.postLogin.viewModals.ReportViewModel
import com.eiadah.app.utils.AppUtils.showToast
import com.eiadah.app.utils.SharedStorage

class ProfileIssueActivity : BaseActivity(), Observer<RestObservable> {

    lateinit var binding:ActivityProfileIssueBinding
    lateinit var strText:String
    lateinit var request: AddPatientRequest

    private val viewModel: ReportViewModel
            by lazy {  ViewModelProviders.of(this).get(ReportViewModel::class.java) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_profile_issue)
        binding.clickHandler=this
        request = AddPatientRequest()
    }

    fun backOncllick() {
        finish()
    }

    fun onSaveClick(){

        strText=binding.textReport.text.toString()
        if (isValid()) {
            request.dob=strText

            viewModel.AddReport(request)
            viewModel.getAddReport().observe(this,this)

        }

    }

    private fun isValid() : Boolean {
        var check = false

        if (strText.isEmpty()) {
            showToast(this,"Please enter text")
        } else {
            check = true
        }

        return check
    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
               if (it.data is ReportResponse) {
                    val reportResponse : ReportResponse = it.data

                    if (reportResponse.status!!) {
                        activitySwitcher(HomeActivity::class.java, true)
                    }
                }
            }
            it.status == Status.ERROR -> {
                it.error!!.fillInStackTrace()
                progressDialog.dismiss()
            }
            it.status == Status.LOADING -> progressDialog.show()
        }
    }
}
