package com.eiadah.app.ui.doctor.postLogin.activity

import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityPatientBinding
import com.eiadah.app.utils.GlobalVariables
import com.pieboat.shredpreference.SharePreferHelper

class PatientActivity : BaseActivity() {

    lateinit var binding:ActivityPatientBinding
    lateinit var sharePreferHelper: SharePreferHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil.setContentView(this,R.layout.activity_patient)
        binding.clickHandler=this
        sharePreferHelper= SharePreferHelper(this)
    }

    fun startOncllick() {
        if (sharePreferHelper.getString(GlobalVariables.SharePref.OWN_PRACTICE).isNullOrEmpty()){
            activitySwitcher(PatientProfileActivity::class.java, false)
            finish()
        }
        else{
            activitySwitcher(PatientListActivity::class.java, false)
            finish()
        }

    }
    fun backOncllick() {
        finish()
    }
}
