package com.eiadah.app.ui.patient.preLogin.activity

import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Patterns
import android.view.Gravity
import android.view.LayoutInflater
import android.view.Window
import com.eiadah.app.R
import com.eiadah.app.core.BaseDialog
import com.eiadah.app.databinding.DialogForgotPasswordBinding
import com.eiadah.app.models.responseModel.BasicResponse
import com.eiadah.app.restAPI.RestCallback
import com.eiadah.app.restAPI.RestProcess
import com.eiadah.app.utils.GlobalVariables
import retrofit2.Call
import retrofit2.Response

class ForgotPasswordDialog(context: Context) : BaseDialog(context), RestCallback {


    lateinit var binding : DialogForgotPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_forgot_password, null, false)

        setContentView(binding.root)

        var window : Window = window
        window.setGravity(Gravity.CENTER)
        //window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window.setBackgroundDrawableResource(android.R.color.transparent)
        window.attributes.windowAnimations = R.style.DialogAnimation

        binding.clickHandler = this


    }

    fun onCancel() {
        dismiss()
    }

    fun onSubmit() {
        if (isValid()) {
            forgotPasswordApi()
        }
    }

    private fun isValid() : Boolean {
        var check= false

        if (binding.edtEmail.text.toString().isNullOrEmpty()) {
            showToast(R.string.empty_email)
        } else if (!Patterns.EMAIL_ADDRESS.matcher(binding.edtEmail.text.toString()).matches()) {
            showToast(R.string.invalid_email)
        } else {
            check = true
        }

        return check
    }

    private fun forgotPasswordApi() {
        val forgotPasswordResponse : Call<BasicResponse> = getService().forgotPassword(binding.edtEmail.text.toString())
        forgotPasswordResponse.enqueue(RestProcess(context, this, GlobalVariables.SERVICE_MODE.FORGOT_PASSWORD, true))
    }

    override fun onResponse(call: Call<*>, response: Response<*>, serviceMode: Int) {
        val basicResponse : BasicResponse = response.body() as BasicResponse
        showToast(R.string.password_send_in_your_email)
        dismiss()
    }

    override fun onFailure(call: Call<*>, t: Throwable, serviceMode: Int) {

    }
 }