package com.eiadah.app.ui.patient.postLogin.fragments



import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatDelegate
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter

import com.eiadah.app.R
import com.eiadah.app.Listener.CheckupChangeFragmentListener
import com.eiadah.app.core.BaseFragment
import com.eiadah.app.databinding.FragmentCheckupStep5Binding
import com.eiadah.app.models.dataModel.CountryData
import com.eiadah.app.models.requestModels.SaveCheckupDetailsRequest
import com.eiadah.app.models.responseModel.SaveCheckupResponse
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.CheckupStep5ViewModel
import com.eiadah.app.utils.SharedStorage


/**
 * A simple [Fragment] subclass.
 *
 */
class CheckupStep5Fragment : BaseFragment() {


    lateinit var binding : FragmentCheckupStep5Binding
    private var countryList : ArrayList<CountryData> = ArrayList()
    lateinit var saveCheckupDetailsRequest : SaveCheckupDetailsRequest
    lateinit var mListener : CheckupChangeFragmentListener

    private lateinit var viewModel: CheckupStep5ViewModel
            /*by lazy {  ViewModelProviders.of(this).get(CheckupStep5ViewModel::class.java) }*/

    companion object {
        fun newInstance() = CheckupStep5Fragment()
        val TAG : String = CheckupStep5Fragment::class.java.simpleName
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_checkup_step5, container, false)
        binding.clickHandler = this



        saveCheckupDetailsRequest = SharedStorage.getCheckupDetail(context!!)


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CheckupStep5ViewModel::class.java)
        loadData()
    }

    fun onSaveClick() {
        if (saveCheckupDetailsRequest.Recently_Visited_Locations.isNotEmpty()) {
            progressDialog.show()
            viewModel.saveCheckupDetail(SharedStorage.getCheckupDetail(context!!))
            viewModel.getSaveCheckupResponse().observe(this, Observer {
                if (it!!.status == Status.SUCCESS) {
                    val saveCheckupResponse : SaveCheckupResponse = it.data as SaveCheckupResponse
                    Log.e(TAG, "ID : "+saveCheckupResponse.data!!.firstName)
                    SharedStorage.setCheckupId(context!!, saveCheckupResponse.data!!.id.toString())
                    progressDialog.dismiss()
                    mListener.changeFragment(CheckupStep6Fragment.newInstance(), CheckupStep6Fragment.TAG)
                } else if (it!!.status == Status.ERROR) {
                    progressDialog.dismiss()
                }
            })

        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is CheckupChangeFragmentListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement CheckupChangeFragmentListener")
        }
    }

    private fun loadData() {
        viewModel.loadCountryList()
        viewModel.getCountryList().observe(this, Observer {
            progressDialog.dismiss()
            if (it?.status!!) {
                countryList = (it.countryList as ArrayList<CountryData>?)!!
                var countryData = CountryData()
                countryData.id = 0
                countryData.name = "Choose country"
                countryList.add(0, countryData)
                setCountryAdapter()
            }
        })
    }

    private fun setCountryAdapter() {
        val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, countryList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.countryAdapter = adapter
    }

    fun onItemSelectedCountry(adapterView: AdapterView<*>, view : View, i : Int, l : Long) {
        if (i!=0) {
            setVisitedLocation(countryList[i].name!!)
        }

    }

    private fun setVisitedLocation(value : String) {
        viewModel.getVisitedLocation(value).observe(this, Observer {
            binding.visitedLocation = it
            saveCheckupDetailsRequest.Recently_Visited_Locations = it!!
            SharedStorage.setCheckupDetail(context!!, saveCheckupDetailsRequest)
        })
    }

}
