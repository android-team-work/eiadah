package com.eiadah.app.ui.doctor.postLogin.activity

import android.app.Dialog
import android.graphics.Color
import android.os.Build
import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityCalenderBinding
import com.eiadah.app.models.dataModel.AppontmentListData
import com.eiadah.app.models.dataModel.PatientListData
import com.eiadah.app.models.responseModel.AddAppointmentListResponse
import com.eiadah.app.models.responseModel.PatientListResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.viewModals.AppointmentListViewModel
import com.eiadah.app.ui.doctor.postLogin.viewModals.PatientListViewModel
import com.eiadah.app.utils.GlobalVariables
import com.myapplication.adapter.AppointmentAdapter
import com.myapplication.adapter.PatientListAdapter
import com.pieboat.shredpreference.SharePreferHelper
import devs.mulham.horizontalcalendar.HorizontalCalendar
import devs.mulham.horizontalcalendar.model.CalendarEvent
import devs.mulham.horizontalcalendar.utils.CalendarEventsPredicate
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener
import kotlinx.android.synthetic.main.activity_patient_list.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*

class CalenderActivity : BaseActivity(), OnItemClickListener, Observer<RestObservable> {

    lateinit var binding : ActivityCalenderBinding
    lateinit var adapter : AppointmentAdapter
    lateinit var appointmentList:ArrayList<AppontmentListData>
    lateinit var sharePreferHelper: SharePreferHelper
    lateinit var horizontalCalendar: HorizontalCalendar
    var selectedDateStr:String=""
    private val viewModel: AppointmentListViewModel
            by lazy {  ViewModelProviders.of(this).get(AppointmentListViewModel::class.java) }


    companion object{
        var is_from: Boolean? = false
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_calender)
        binding.clickHandler=this

        sharePreferHelper= SharePreferHelper(this)
//        setupCalenderView()
//        viewModel.appointmenttList()
//        viewModel.getAppointmentList().observe(this,this)

        binding.tvDocName.setText("Dr. "+sharePreferHelper.getString(GlobalVariables.SharePref.DOCTOR_NAME))

        val startDate = Calendar.getInstance()
        startDate.add(Calendar.MONTH, -2)

        /* end after 2 months from now */
        val endDate = Calendar.getInstance()
        endDate.add(Calendar.MONTH, 2)

        // Default Date set to Today.
        val defaultSelectedDate = Calendar.getInstance()

        horizontalCalendar = HorizontalCalendar.Builder(this, R.id.calendarView)
            .range(startDate, endDate)
            .datesNumberOnScreen(5)
            .configure()
            .formatTopText("MMM")
            .formatMiddleText("dd")
            .formatBottomText("EEE")
            .showTopText(true)
            .showBottomText(true)
            .textColor(Color.LTGRAY, Color.WHITE)
            .colorTextMiddle(Color.LTGRAY, Color.parseColor("#8AC13D"))
            .end()
            .defaultSelectedDate(defaultSelectedDate)
            .build()

        Log.i("Default Date", DateFormat.format("yyyy-MM-dd", defaultSelectedDate).toString())
        selectedDateStr=DateFormat.format("yyyy-MM-dd", defaultSelectedDate).toString()
        cal_click()
        horizontalCalendar!!.setCalendarListener(object : HorizontalCalendarListener() {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onDateSelected(date: Calendar, position: Int) {
                 selectedDateStr = DateFormat.format("yyyy-MM-dd", date).toString()
//                Toast.makeText(this@CalenderActivity, "$selectedDateStr selected!", Toast.LENGTH_SHORT)
//                    .show()
                Log.i("onDateSelected", "$selectedDateStr - Position = $position")

                cal_click()
            }
        })


    }
    fun backOncllick() {
        finish()
    }
    fun addOncllick() {
        activitySwitcher(AddAppointmentActivity::class.java, false)
        is_from=true

    }



    override fun onItemClick(position: Int) {
    val dialog = Dialog(this,R.style.Theme_Dialog)
    dialog .requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog .setContentView(R.layout.patient_app_options)
    dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
    val window = dialog.window
    window!!.setGravity(Gravity.BOTTOM)
    window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
    dialog .show()

    }
    fun cal_click(){
        viewModel.getAppointmentListDate().observe(this, this)
        viewModel.appointmenttListDate(selectedDateStr)
    }
    override fun onChanged(it: RestObservable?) {

        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                    if (it.data is AddAppointmentListResponse) {

                    val patientListResponse : AddAppointmentListResponse = it.data
                    appointmentList = (patientListResponse.getData() as ArrayList<AppontmentListData>?)!!
                    val cityData = AppontmentListData()
                    appointmentList.add(cityData)

                    if(appointmentList.size>1){
                        binding.tv.visibility=View.GONE
                        binding.calRecyclerview.visibility=View.VISIBLE

                        adapter = AppointmentAdapter(this,this,appointmentList)
                        binding.calRecyclerview.setLayoutManager(LinearLayoutManager(this))
                        binding.calRecyclerview.setAdapter(adapter)
                    }
                    else{
                        binding.tv.visibility=View.VISIBLE
                        binding.calRecyclerview.visibility=View.GONE
                    }

                }
            }
            it.status == Status.ERROR -> progressDialog.dismiss()
            it.status == Status.LOADING -> progressDialog.show()
        }
    }



    override fun onResume() {
        super.onResume()

        cal_click()
    }
}
