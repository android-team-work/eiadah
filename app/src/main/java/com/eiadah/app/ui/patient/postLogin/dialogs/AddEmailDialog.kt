package com.eiadah.app.ui.patient.postLogin.dialogs

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import android.util.Patterns
import android.view.Gravity
import android.view.LayoutInflater
import android.view.Window
import com.eiadah.app.Listener.UpdateProfileData
import com.eiadah.app.R
import com.eiadah.app.core.BaseDialog
import com.eiadah.app.databinding.DialogAddEmailBinding
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.ProfileViewModel
import com.eiadah.app.utils.GlobalVariables

class AddEmailDialog (activity: FragmentActivity, private val updateData: UpdateProfileData) : BaseDialog(activity),
    LifecycleOwner {

    private val profileViewModel : ProfileViewModel
            by lazy {
                ViewModelProviders.of(activity).get(ProfileViewModel::class.java)
            }

    lateinit var binding : DialogAddEmailBinding
    lateinit var lifecycleRegistry: LifecycleRegistry

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_email, null, false)

        setContentView(binding.root)

        var window = window
        window?.setGravity(Gravity.CENTER)
        //window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        window?.attributes?.windowAnimations = R.style.DialogAnimation

        lifecycleRegistry = LifecycleRegistry(this)
        binding.clickHandler = this
        binding.data = profileViewModel
        binding.lifecycleOwner = this

    }

    override fun getLifecycle(): Lifecycle {
        return lifecycleRegistry
    }

    fun onSubmit() {
        if (isValid())
            updateEmail(binding.edtEmail.text.toString())
    }

    fun onCancel() {
        dismiss()
    }

    private fun isValid() : Boolean {
        var check= false

        if (binding.edtEmail.text.toString().isNullOrEmpty()) {
            showToast(R.string.empty_email)
        } else if (!Patterns.EMAIL_ADDRESS.matcher(binding.edtEmail.text.toString()).matches()) {
            showToast(R.string.invalid_email)
        } else {
            check = true
        }

        return check
    }

    private fun updateEmail(email : String) {
        var params : HashMap<String, String> = HashMap()
        params[GlobalVariables.PARAMS.PROFILE.EMAIL] = email
        updateData.updateProfileData(params)
        dismiss()
    }


}

