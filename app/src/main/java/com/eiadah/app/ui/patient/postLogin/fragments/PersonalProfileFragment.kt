package com.eiadah.app.ui.patient.postLogin.fragments

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eiadah.app.Listener.UpdateProfileData

import com.eiadah.app.databinding.PersonalProfileFragmentBinding
import com.eiadah.app.ui.patient.postLogin.dialogs.*
import com.esafirm.imagepicker.features.ReturnMode
import com.esafirm.imagepicker.features.ImagePicker
import android.graphics.Color
import android.util.Log
import com.eiadah.app.utils.SharedStorage
import com.esafirm.imagepicker.model.Image
import java.io.File
import okhttp3.RequestBody
import okhttp3.MediaType
import okhttp3.MultipartBody
import com.eiadah.app.core.BaseFragment
import com.eiadah.app.models.responseModel.ProfileResponse
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.ProfileViewModel


class PersonalProfileFragment : BaseFragment(), UpdateProfileData{



    companion object {
        fun newInstance() = PersonalProfileFragment()
        val TAG : String = PersonalProfileFragment::class.java.simpleName
    }

    private val viewModel: ProfileViewModel
            by lazy {  ViewModelProviders.of(this).get(ProfileViewModel::class.java) }

    lateinit var binding : PersonalProfileFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, com.eiadah.app.R.layout.personal_profile_fragment, container, false)
        binding.clickEvent = this
        viewModel.setUserData(SharedStorage.getUserData(context!!))
        binding.data = viewModel
        binding.lifecycleOwner = this



        return binding.root
    }

    fun onSaveClick() {

    }

    fun onAddName() {
        val addNameDialog : AddNameDialog? = activity?.let { AddNameDialog(it, this) }
        addNameDialog?.show()
    }

    fun onAddImage() {
        openImagePicker()
    }

    fun onAddContact() {
        val addContactDialog : ContactNumberDialog? = activity?.let { ContactNumberDialog(it, this) }
        addContactDialog?.show()
    }

    fun onEmailClick() {
        val addEmailDialog : AddEmailDialog? = activity?.let { AddEmailDialog(it, this) }
        addEmailDialog?.show()
    }

    fun onGenderClick() {
        val chooseGenderDialog : ChooseGenderDialog? = activity?.let { ChooseGenderDialog(it, this) }
        chooseGenderDialog?.show()
    }
    fun onLocClick() {
        val addLocationDialog : AddLocationDialog? = activity?.let { AddLocationDialog(it, this) }
        addLocationDialog?.show()
    }

    fun onDobClick() {
        val datePickerDialog : DatePickerDialog? = activity?.let { DatePickerDialog(it, "dd/MM/yyyy", this) }
        datePickerDialog?.show()
    }

    fun onChangePasswordClick() {
        val changePasswordDialog : ChangePasswordDialog? = activity?.let { ChangePasswordDialog(it) }
        changePasswordDialog?.show()

    }

    override fun updateProfileData(data: ProfileViewModel) {
        binding.data = data
    }

    private fun openImagePicker() {
        ImagePicker.create(this)
            .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
            .folderMode(true) // folder mode (false by default)
            .toolbarFolderTitle("Folder") // folder selection title
            .toolbarImageTitle("Tap to select") // image selection title
            .toolbarArrowColor(Color.BLACK) // Toolbar 'up' arrow color
            .single() // single mode
            .showCamera(true) // show camera or not (true by default)
            .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
            .enableLog(false) // disabling log
            .start() // start image picker activity with request code
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            val images : List<Image> = ImagePicker.getImages(data)
            // or get a single image only
            val image : Image = ImagePicker.getFirstImageOrNull(data)

            Log.e(TAG, image.path)
            viewModel.setImage(image.path)
            updateImage(image.path)
            //binding.data?.image?.value = image.path
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun updateImage(image: String) {
        var file = File(image)
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val body = MultipartBody.Part.createFormData("image", file.name, requestFile)
        viewModel.saveUserProfileImageData(body)

    }

    private fun onResponse() {
        viewModel.getProfileResponse().observe(this, Observer {
            Log.e(TAG, "eerg")
            when {
                it!!.status == Status.SUCCESS -> {
                    progressDialog.dismiss()
                    val profileResponse : ProfileResponse = it.data as ProfileResponse
                    if (profileResponse.status!!) {
                        SharedStorage.setUserData(context!!, profileResponse.user!!)
                        viewModel.setUserData(profileResponse.user!!)
                        binding.data = viewModel
                    }
                }
                it!!.status == Status.ERROR -> progressDialog.dismiss()
                it!!.status == Status.LOADING -> progressDialog.show()
            }
        })
    }

    override fun updateProfileData(params: HashMap<String, String>) {
        viewModel.saveUserProfileData(params)
        onResponse()
    }
}
