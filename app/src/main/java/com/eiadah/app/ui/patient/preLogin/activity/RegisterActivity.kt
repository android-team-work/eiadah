package com.eiadah.app.ui.patient.preLogin.activity

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityRegisterBinding
import com.eiadah.app.models.responseModel.ProfileResponse
import com.eiadah.app.models.responseModel.RegisterResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.activity.HomeActivity
import com.eiadah.app.ui.patient.preLogin.viewModels.RegisterViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage

class RegisterActivity : BaseActivity(), Observer<RestObservable> {


    lateinit var binding : ActivityRegisterBinding

    private val viewModel: RegisterViewModel
            by lazy {  ViewModelProviders.of(this).get(RegisterViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        binding.clickEvent = this
    }

    override fun onBackPressed() {
        activitySwitcher(SplashActivity::class.java, true)
    }

    fun onRegisterClick() {
        hideKeyboard()
        if (isValid())
            registerApi()
    }

    fun onLoginClick() {
        hideKeyboard()
        activitySwitcher(LoginActivity::class.java, true)
    }

    private fun isValid() : Boolean {
        var check = false

        val strUsername : String = binding.edtRegisterUsername.text.toString()
        val strName : String = binding.edtRegisterName.text.toString()
        val strEmail : String = binding.edtRegisterEmail.text.toString()
        val strPassword : String = binding.edtRegisterPassword.text.toString()
        val strConfirmPassword : String = binding.edtRegisterConfirmPassword.text.toString()

        if (strUsername.isEmpty() && strName.isEmpty() && strEmail.isEmpty() && strPassword.isEmpty() && strConfirmPassword.isEmpty()) {
            showToast(R.string.please_fill_all_fields)
        } else if (strUsername.isEmpty()) {
            showToast(R.string.empty_username)
        } else if (strName.isEmpty()) {
            showToast(R.string.empty_name)
        } else if (strEmail.isEmpty()) {
            showToast(R.string.empty_email)
        } else if (!Patterns.EMAIL_ADDRESS.matcher(strEmail).matches()) {
            showToast(R.string.invalid_email)
        } else if (strPassword.isEmpty()) {
            showToast(R.string.empty_password)
        } else if (strPassword.length<6) {
            showToast(R.string.invalid_password_length)
        } else if (strConfirmPassword.isEmpty()) {
            showToast(R.string.empty_confirm_password)
        } else if (strPassword != strConfirmPassword) {
            showToast(R.string.password_not_matched)
        } else {
            check = true
        }
        return check
    }

    private fun registerApi() {

        val username = binding.edtRegisterUsername.text.toString()
        val email = binding.edtRegisterEmail.text.toString()
        val password1 = binding.edtRegisterPassword.text.toString()
        val password2 = binding.edtRegisterConfirmPassword.text.toString()
        val name = binding.edtRegisterName.text.toString()

        viewModel.registerApi(username, name, email, password1, password2)
        viewModel.getRegisterResponse().observe(this, this)

    }

    private fun getProfileApi() {
        viewModel.profileApi()
        viewModel.getProfileResponse().observe(this, this)
    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is RegisterResponse) {

                    val registerResponse : RegisterResponse = it.data
                    if (registerResponse.token.isNotEmpty()) {
                        SharedStorage.setToken(this, registerResponse.token)
                        activitySwitcher(LoginActivity::class.java, true)
                        Toast.makeText(this,"Please Verify Your Email id",Toast.LENGTH_SHORT).show()
                        getProfileApi()

                    }

                } else if (it.data is ProfileResponse) {
                    val profileResponse : ProfileResponse = it.data

                    if (profileResponse.status!!) {
                        SharedStorage.setSession(this, true)
                        SharedStorage.setUserType(this, GlobalVariables.User.PATIENT)
                        SharedStorage.setUserData(this, profileResponse.user!!)
                        activitySwitcher(LoginActivity::class.java, true)
                    }
                }
            }
            it.status == Status.ERROR -> progressDialog.dismiss()
            it.status == Status.LOADING -> progressDialog.show()
        }
    }
}


