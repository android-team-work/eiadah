package com.eiadah.app.ui.patient.postLogin.fragments

import android.Manifest
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.core.BaseFragment
import com.eiadah.app.databinding.FragmentDoctorListBinding
import com.eiadah.app.models.dataModel.DocListData
import com.eiadah.app.models.responseModel.DocListResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.activity.DoctorDetailActivity
import com.eiadah.app.ui.patient.postLogin.activity.DoctorSearchFilterActivity
import com.eiadah.app.ui.patient.postLogin.adapter.DoctorAdapter
import com.eiadah.app.ui.patient.postLogin.view_modals.DocListViewModel
import kotlinx.android.synthetic.main.fragment_doctor_list.*

class DoctorListFragment : BaseFragment(), OnItemClickListener, Observer<RestObservable> {

    private var mListener : HomeFragment.HomeFragmentListener? = null
    lateinit var docList:ArrayList<DocListData>
    var adapter:DoctorAdapter?=null
    companion object {
        fun newInstance() = DoctorListFragment()
        val TAG : String = DoctorListFragment::class.java.simpleName
    }

    private val viewModel: DocListViewModel
            by lazy {  ViewModelProviders.of(this).get(DocListViewModel::class.java) }

//    private lateinit var viewModel: DoctorListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding : FragmentDoctorListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_doctor_list, container, false)
//        val doctorAdapter = context?.let { DoctorAdapter(it, this) }
//        binding.doctorAdapter = doctorAdapter
        binding.clickHandler = this
        ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.CALL_PHONE), 1)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel
        viewModel.doctorList()
        viewModel.getdocList().observe(this,this)

        edt_search.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (adapter != null) {
                    adapter?.filter(s.toString())

                }
            }
        })

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is HomeFragment.HomeFragmentListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement SubCategoryFgmtListener")
        }
    }

     fun onClick() {
        mListener!!.handleDrawer()
    }

    fun onFilter() {
        activitySwitcher(DoctorSearchFilterActivity::class.java, false)
    }

    override fun onItemClick(position: Int) {
        val intent=Intent(activity,DoctorDetailActivity::class.java)
        intent.putExtra("id",docList.get(position).userId.toString())
        startActivity(intent)

    }

    @Suppress("UNCHECKED_CAST")
    override fun onChanged(it: RestObservable?) {

        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is DocListResponse) {

                    val docListResponse : DocListResponse = it.data
                    docList = (docListResponse.getUser()?.getDoctor() as ArrayList<DocListData>?)!!
                    val cityData = DocListData()
//                            cityData.id = "0"
//                            cityData.name = "Choose city"
                    docList.add(cityData)

                    adapter= DoctorAdapter(activity!!,this,docList,docList)
                    rcySearchDoctor.setLayoutManager(LinearLayoutManager(activity!!))
                    rcySearchDoctor.setAdapter(adapter)
                }
            }
            it.status == Status.ERROR -> progressDialog.dismiss()
            it.status == Status.LOADING -> progressDialog.show()
        }

    }
}
