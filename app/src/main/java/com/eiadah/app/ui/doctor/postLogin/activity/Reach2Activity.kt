package com.eiadah.app.ui.doctor.postLogin.activity

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityReach2Binding
import com.eiadah.app.models.dataModel.CityData
import com.eiadah.app.models.requestModels.SaveTokentRequest
import com.eiadah.app.models.responseModel.AddCitiesNameResponse
import com.eiadah.app.models.responseModel.CitiesResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.viewModals.CityViewModel
import com.eiadah.app.utils.GlobalVariables
import com.myapplication.adapter.CityListAdapter
import com.pieboat.shredpreference.SharePreferHelper
import kotlinx.android.synthetic.main.activity_reach2.*

class Reach2Activity : BaseActivity(), Observer<RestObservable>, OnItemClickListener {
    lateinit var binding:ActivityReach2Binding
    lateinit var adapter: CityListAdapter
    lateinit var sharePreferHelper: SharePreferHelper
    lateinit var request:SaveTokentRequest
    private var cityList : ArrayList<CityData> = ArrayList()

    private val viewModel: CityViewModel
            by lazy {  ViewModelProviders.of(this).get(CityViewModel::class.java) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_reach2)

        binding.clickHandler=this
        sharePreferHelper= SharePreferHelper(this)

        loadCityData(sharePreferHelper.getString(GlobalVariables.SharePref.State_ID)!!);

        request=SaveTokentRequest()
    }

    fun backClick(){
        finish()
    }
    private fun loadCityData(stateId : String) {
        viewModel.loadCityList(stateId)
        viewModel.getCityList().observe(this, this)
    }
    override fun onChanged(it: RestObservable?) {
            when {
                it!!.status == Status.SUCCESS -> {
                    progressDialog.dismiss()
                    if (it.data is CitiesResponse) {
                        val citiesResponse : CitiesResponse = it.data
                        if (citiesResponse.status!!) {
                            cityList = (citiesResponse.cityList as ArrayList<CityData>?)!!
                            val cityData = CityData()
                            cityList.add(cityData)
                            adapter=CityListAdapter(this,this,cityList)
                            city_recyclerview.setLayoutManager(LinearLayoutManager(this))
                            city_recyclerview.setAdapter(adapter)
                        }

                    }
                    else if (it.data is AddCitiesNameResponse) {
                        val citiesResponse : AddCitiesNameResponse = it.data
                        if (citiesResponse.status!!) {
                           finish()
                        }

                    }
                }
                it.status == Status.ERROR -> {
                    it.error!!.fillInStackTrace()
                    progressDialog.dismiss()
                }
                it.status == Status.LOADING -> progressDialog.show()
            }
        }

    private fun setSelectedAllergy(value : String) {
        viewModel.getSelectedAllrgy(value).observe(this, Observer {
            request.city_name = it!!
        })
    }
    override fun onItemClick(position: Int) {
        setSelectedAllergy(cityList[position].name!!)
    }

    fun onSaveClick(){
        viewModel.getAddCityList().observe(this,this)
        viewModel.loadAddCityList(request)
    }
}
