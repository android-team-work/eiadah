package com.eiadah.app.ui.doctor.postLogin.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityChatListBinding
import com.eiadah.app.models.dataModel.ChatListData
import com.eiadah.app.models.responseModel.ChatListResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.fragments.HomeFragment
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.ChatListViewModel
import com.myapplication.adapter.ChatListAdapter

class ChatListActivity : BaseActivity(), Observer<RestObservable> {

    lateinit var binding:ActivityChatListBinding
    lateinit var mListener : HomeFragment.HomeFragmentListener
    lateinit var chatListData:ArrayList<ChatListData>
    lateinit var adapter: ChatListAdapter
    private val viewModel: ChatListViewModel
            by lazy {  ViewModelProviders.of(this).get(ChatListViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil. setContentView(this,R.layout.activity_chat_list)
        binding.clickhandler=this

        viewModel.getChatList().observe(this,this)
        viewModel.loadChatList()

    }

    override fun onBackPressed() {
        finish()
    }

    override fun onChanged(it: RestObservable?) {

        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog?.dismiss()
                if (it.data is ChatListResponse) {

                    val reportListResponse : ChatListResponse = it.data
                    chatListData = (reportListResponse.getData() as ArrayList<ChatListData>?)!!
                    val reportData = ChatListData()
                    chatListData.add(reportData)

                    if(chatListData.size==0){
                        binding.textRec.visibility= View.VISIBLE
                        binding.chatRecyc.visibility= View.GONE

                    }
                    else{
                        binding.textRec.visibility= View.GONE
                        binding.chatRecyc.visibility= View.VISIBLE
                        adapter= ChatListAdapter(this,chatListData)
                        binding.chatRecyc.setLayoutManager(LinearLayoutManager(this))
                        binding.chatRecyc.setAdapter(adapter)
                    }

                }
            }
            it.status == Status.ERROR -> progressDialog?.dismiss()
            it.status == Status.LOADING -> progressDialog?.show()
        }
    }

}
