package com.eiadah.app.ui.patient.postLogin.view_modals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ProfileViewModel : ObservableViewModel() {

    var profileResponse : MutableLiveData<RestObservable> = MutableLiveData()

    fun getProfileResponse() : LiveData<RestObservable> {
        return profileResponse
    }


    fun profileApi() {
        getService().getProfile(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { profileResponse.value = RestObservable.loading() }
            .subscribe(
                {profileResponse.value = RestObservable.success(it)},
                {profileResponse.value = RestObservable.error (it)}
            )
    }

}