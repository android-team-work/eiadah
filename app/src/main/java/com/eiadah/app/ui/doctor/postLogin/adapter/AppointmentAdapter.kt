package com.myapplication.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.models.dataModel.AppontmentListData
import com.eiadah.app.ui.doctor.postLogin.activity.AppointmentDetailActivity
import com.eiadah.app.ui.doctor.postLogin.activity.PatientDetailActivity
import com.eiadah.app.utils.GlobalVariables
import java.util.ArrayList

class AppointmentAdapter(private val mContext: Context,val onItemClickListener: OnItemClickListener,var appointmentList: ArrayList<AppontmentListData>) : RecyclerView.Adapter<AppointmentAdapter.AppointmentViewHolder>() {
    override fun onBindViewHolder(p0: AppointmentViewHolder, p1: Int) {
        p0.optin_menu.setOnClickListener(View.OnClickListener {

            onItemClickListener.onItemClick(p1)
        })

        p0.tv_time.setText(appointmentList.get(p1).time)
        p0.tv_appointment.setText(appointmentList.get(p1).name)
        p0.tv_notes.setText(appointmentList.get(p1).note)
        p0.tv_cancel.setText(appointmentList.get(p1).status)


        p0.itemView.setOnClickListener(View.OnClickListener {
            val intent = Intent(mContext, AppointmentDetailActivity::class.java)
            intent.putExtra("id",appointmentList.get(p1).id.toString())
            mContext?.startActivity(intent)

        })


//        if (appointmentList.get(p1).status.equals("cancel")){
//            p0.optin_menu.visibility=View.INVISIBLE
//        }
//        else{
//            p0.optin_menu.visibility=View.VISIBLE
//        }
//        Log.e("iddddddd", appointmentList.get(p1).id.toString())

        if (appointmentList.get(p1).name.isNullOrEmpty()){
            p0.relative_lay.visibility=View.GONE
        }
        else{
            p0.relative_lay.visibility=View.VISIBLE

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppointmentViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.appointment_items, parent, false)
        return AppointmentViewHolder(v)
    }

    override fun getItemCount(): Int {
        return appointmentList.size
    }

    inner class AppointmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var optin_menu:ImageView
        var tv_time:TextView
        var tv_appointment:TextView
        var tv_notes:TextView
        var tv_cancel:TextView
        var relative_lay:RelativeLayout
        init {

            optin_menu=itemView.findViewById(R.id.optin_menu)
            tv_time=itemView.findViewById(R.id.tv_time)
            tv_appointment=itemView.findViewById(R.id.tv_appointment)
            tv_notes=itemView.findViewById(R.id.tv_notes)
            tv_cancel=itemView.findViewById(R.id.tv_cancel)
            relative_lay=itemView.findViewById(R.id.relative_lay)
        }
        fun onItemClick() {
            onItemClickListener.onItemClick(adapterPosition)
        }
    }
}