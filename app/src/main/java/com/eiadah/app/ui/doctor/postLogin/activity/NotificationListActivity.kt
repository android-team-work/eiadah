package com.eiadah.app.ui.doctor.postLogin.activity

import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityNotificationListBinding
import com.eiadah.app.models.dataModel.NotificationListData
import com.eiadah.app.models.responseModel.NotiicationListResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.NotificationListViewModel
import com.myapplication.adapter.NotificationAdapter
import kotlinx.android.synthetic.main.activity_notification_list.*

class NotificationListActivity : BaseActivity(), Observer<RestObservable> {

    lateinit var binding:ActivityNotificationListBinding
    lateinit var adapter:NotificationAdapter
    private var notificationList : ArrayList<NotificationListData> = ArrayList()

    private val viewModel: NotificationListViewModel
            by lazy {  ViewModelProviders.of(this).get(NotificationListViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_notification_list)




        binding.clickHandler=this

        viewModel.getNotificationlist().observe(this,this)
        viewModel.loadNotificationList()
    }


    fun backOncllick() {
        finish()
    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is NotiicationListResponse) {
                    val notiicationListResponse : NotiicationListResponse = it.data
                    if (notiicationListResponse.status!!) {
                        notificationList = notiicationListResponse.data as ArrayList<NotificationListData>
                        val notificationData=NotificationListData()

                        notificationList.add(notificationData)
                        adapter=NotificationAdapter(this,notificationList)
                        recyclerView_list.setLayoutManager(LinearLayoutManager(this))
                        recyclerView_list.setAdapter(adapter)
                        binding.adapter=adapter


                    }

                }

            }
            it.status == Status.ERROR -> progressDialog?.dismiss()
            it.status == Status.LOADING -> progressDialog?.show()
        }
    }

}
