package com.myapplication.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.eiadah.app.R
import com.eiadah.app.models.dataModel.Appointment
import java.util.ArrayList

class PtAppointmentAdapter(private val mContext: Context, var appointmentList: ArrayList<Appointment>) : RecyclerView.Adapter<PtAppointmentAdapter.AppointmentViewHolder>() {
    override fun onBindViewHolder(p0: AppointmentViewHolder, p1: Int) {


        p0.tv_apt_name.setText(appointmentList.get(p1).name)
        p0.tv_cancel.setText(appointmentList.get(p1).status)



        if (appointmentList.get(p1).name.isNullOrEmpty()){
            p0.card_view.visibility=View.GONE
        }
        else{
            p0.card_view.visibility=View.VISIBLE

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppointmentViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.pt_appointment_item, parent, false)
        return AppointmentViewHolder(v)
    }

    override fun getItemCount(): Int {
        return appointmentList.size
    }

    inner class AppointmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var tv_cancel:TextView
        var tv_apt_name:TextView
        var card_view:CardView
        init {


            tv_cancel=itemView.findViewById(R.id.tv_cancel)
            tv_apt_name=itemView.findViewById(R.id.tv_apt_name)
            card_view=itemView.findViewById(R.id.card_view)
        }

    }
}