package com.eiadah.app.ui.patient.postLogin.fragments


import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eiadah.app.R
import com.eiadah.app.core.BaseFragment
import com.eiadah.app.databinding.FragmentHomeBinding
import com.eiadah.app.ui.patient.postLogin.activity.BookAppointmentStep1Activity
import com.eiadah.app.ui.patient.postLogin.activity.CustomerSupportActivity
import com.eiadah.app.ui.patient.postLogin.activity.RecordsActivity
import com.eiadah.app.ui.patient.postLogin.activity.StartCheckupActivity

/**
 * A simple [Fragment] subclass.
 *
 */
open class HomeFragment : BaseFragment() {
    public fun onClick(v: View) {
        mListener!!.handleDrawer()
    }

    private var mListener : HomeFragmentListener? = null


    companion object {
        fun newInstance(): HomeFragment {
            val fragment = HomeFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }

        val TAG : String = HomeFragment::class.java.simpleName
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var binding : FragmentHomeBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_home,container , false)
        var myView : View  = binding.root
        binding.click = this

        return myView
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is HomeFragmentListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement SubCategoryFgmtListener")
        }
    }

    fun onStartCheckupClick() {
        activitySwitcher(StartCheckupActivity::class.java, false)
    }

    fun onOnlineAppointment() {
        activitySwitcher(BookAppointmentStep1Activity::class.java, false)
    }
    fun onSupportAppointment() {
        activitySwitcher(CustomerSupportActivity::class.java, false)
    }
    fun onReportAppointment() {
        activitySwitcher(RecordsActivity::class.java, false)
    }

    interface HomeFragmentListener {

        fun handleDrawer()

    }
}
