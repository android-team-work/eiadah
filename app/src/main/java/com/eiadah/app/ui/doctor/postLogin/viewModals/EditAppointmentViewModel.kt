package com.eiadah.app.ui.doctor.postLogin.viewModals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.models.requestModels.AddPatientRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.models.responseModel.AddPatientResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class EditAppointmentViewModel : ObservableViewModel() {

    private var editAppointmentResponse : MutableLiveData<RestObservable> = MutableLiveData()


    fun getEditAppointment() : LiveData<RestObservable> {
        return editAppointmentResponse
    }



    fun EditAppointment(request: AddAppointmenttRequest) {
        getService().EditAppointment(getHeader(), request.id,
           request.name, request.mobile, request.email,request.date,request.time,request.category,request.note)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { editAppointmentResponse.value = RestObservable.loading() }
            .subscribe(
                {editAppointmentResponse.value = RestObservable.success(it)},
                {editAppointmentResponse.value = RestObservable.error(it)}
            )
    }
}