package com.eiadah.app.ui.doctor.postLogin.activity

import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityCollectFeedbackBinding

class CollectFeedbackActivity : BaseActivity() {

    lateinit var binding:ActivityCollectFeedbackBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_collect_feedback)
        binding.clickHandler=this
    }
//    fun optionOncllick() {
//        activitySwitcher(CollectFeedbackActivity::class.java, false)
//    }
    fun backOncllick() {
        finish()
    }


}
