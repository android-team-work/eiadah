package com.eiadah.app.ui.patient.postLogin.activity

import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import androidx.viewpager.widget.ViewPager
import com.eiadah.app.databinding.ActivityProfileBinding
import com.eiadah.app.ui.patient.postLogin.adapter.ProfileSectionAdapter
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.ProfileViewModel
import com.eiadah.app.utils.SharedStorage


class ProfileActivity : BaseActivity() {

    private val viewModel: ProfileViewModel
            by lazy {  ViewModelProviders.of(this).get(ProfileViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityProfileBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile)
        binding.clickEvent = this
        binding.manager = supportFragmentManager

        viewModel.setUserData(SharedStorage.getUserData(this))
        binding.data = viewModel

        setViewPagerAdapter(binding.pager)
        setViewPagerTabs(binding.tabLayoutProfile, binding.pager)
    }


    private fun setViewPagerAdapter(view: ViewPager) {
        val adapter = ProfileSectionAdapter(view.context, supportFragmentManager)
        view.adapter = adapter
    }


    private fun setViewPagerTabs(view: TabLayout, pagerView: ViewPager) {
        view.setupWithViewPager(pagerView)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}
