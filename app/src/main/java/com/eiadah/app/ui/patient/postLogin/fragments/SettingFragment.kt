package com.eiadah.app.ui.patient.postLogin.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager

import com.eiadah.app.R
import com.eiadah.app.databinding.FragmentSettingBinding
import com.google.android.material.tabs.TabLayout
import java.util.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class SettingFragment : Fragment() {

    lateinit var binding:FragmentSettingBinding
    lateinit var tabLayout: TabLayout
    lateinit var viewPager: ViewPager

    companion object {
        fun newInstance() = SettingFragment()
        val TAG : String = SettingFragment::class.java.simpleName
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        binding=DataBindingUtil.inflate(inflater,R.layout.fragment_setting, container, false)
        setupViewPager(binding.viewpager)
        binding.tabs.setupWithViewPager(binding.viewpager)

        return  binding.root
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(childFragmentManager)
        adapter.addFragment(PersonalFragment(), "Personal")
        adapter.addFragment(MedicalFragment(), "Medical")

        viewPager.adapter = adapter
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }
}
