package com.eiadah.app.ui.doctor.preLogin.activity

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityRegisterDoctorBinding
import com.eiadah.app.models.responseModel.DoctorProfileResponse
import com.eiadah.app.models.responseModel.ProfileResponse
import com.eiadah.app.models.responseModel.RegisterResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.preLogin.viewModels.DoctorRegisterViewModel
import com.eiadah.app.ui.patient.postLogin.activity.HomeActivity
import com.eiadah.app.ui.patient.preLogin.activity.SplashActivity
import com.eiadah.app.ui.patient.preLogin.viewModels.RegisterViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.mukesh.countrypicker.Country
import com.mukesh.countrypicker.CountryPicker
import com.mukesh.countrypicker.listeners.OnCountryPickerListener

class RegisterDoctorActivity : BaseActivity(), Observer<RestObservable> {


    lateinit var binding: ActivityRegisterDoctorBinding

    private val viewModel: DoctorRegisterViewModel
            by lazy {  ViewModelProviders.of(this).get(DoctorRegisterViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register_doctor)
        binding.clickHandler = this
    }

    override fun onBackPressed() {
        activitySwitcher(SplashActivity::class.java, true)
    }

    fun onRegisterClick() {
        val email = binding.edtRegisterEmail.text.toString()
        val mobile = binding.edtRegisterMobile.text.toString()
        val password = binding.edtRegisterPassword.text.toString()
        val dial_code = binding.edtDoctorMobileCode.text.toString()
        val confirmPassword = binding.edtRegisterConfirmPassword.text.toString()
        val mobile_number=dial_code+mobile
        if (isValid(email, mobile, password, confirmPassword)) {
            viewModel.loadReisterData(email, mobile_number, password, confirmPassword)
            registerResponse()
        }
    }

    fun onLoginClick() {
        activitySwitcher(LoginDoctorActivity::class.java, true)
    }

    private fun isValid(email : String, phone : String, password : String, confirmPassword : String) : Boolean {
        var check = false

        if (email.isEmpty() && phone.isEmpty() && password.isEmpty() && confirmPassword.isEmpty()) {
            showToast(R.string.please_fill_all_fields)
        } else if (email.isEmpty()) {
            showToast(R.string.empty_email)
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            showToast(R.string.invalid_email)
        } else if (phone.isEmpty()) {
            showToast(R.string.empty_contact_number)
        } else if (password.isEmpty()) {
            showToast(R.string.empty_password)
        } else if (password.length<6) {
            showToast(R.string.invalid_password_length)
        } else if (confirmPassword.isEmpty()) {
            showToast(R.string.empty_confirm_password)
        } else if (password != confirmPassword) {
            showToast(R.string.password_not_matched)
        } else {
            check = true
        }

        return check
    }

    private fun registerResponse() {
        viewModel.getRegisterResponse().observe(this, this)
    }

    private fun getProfileApi() {
        viewModel.profileApi()
        viewModel.getProfileResponse().observe(this, this)
    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is RegisterResponse) {

                    val registerResponse : RegisterResponse = it.data
                    if (registerResponse.token.isNotEmpty()) {
                        SharedStorage.setToken(this, registerResponse.token)
                        activitySwitcher(LoginDoctorActivity::class.java, true)
                        Toast.makeText(this,"Please Verify Your Email id",Toast.LENGTH_SHORT).show()
                        getProfileApi()
                    }


                } else if (it.data is DoctorProfileResponse) {
                    val profileResponse : DoctorProfileResponse = it.data

                    if (profileResponse.status!!) {
                        SharedStorage.setSession(this, true)
                        SharedStorage.setUserType(this, GlobalVariables.User.DOCTOR)
                        SharedStorage.setDoctorData(this, profileResponse.data!!)
                        activitySwitcher(LoginDoctorActivity::class.java, true)
                    }
                }
            }
            it.status == Status.ERROR -> progressDialog.dismiss()
            it.status == Status.LOADING -> progressDialog.show()
        }
    }

    fun showcountrypickerdialog() {

        var builder = CountryPicker.Builder().with(this).listener(object :
            OnCountryPickerListener {

            override fun onSelectCountry(country: Country) {
                binding.edtDoctorMobileCode.setText(country.dialCode)
            }
        })
        var picker = builder.build()
        picker.showDialog(this);

    }
}
