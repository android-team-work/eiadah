package com.eiadah.app.ui.patient.postLogin.activity

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityAddAllergyBinding
import com.eiadah.app.databinding.ActivityAddSurgeryBinding
import com.eiadah.app.models.dataModel.AddAllergyData
import com.eiadah.app.models.requestModels.AddAllergytRequest
import com.eiadah.app.models.responseModel.AddAllergy_Response
import com.eiadah.app.models.responseModel.AddSurgeryResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.RestProcess
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.AddAllrgyViewModel
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.AddSurgeryViewModel
import com.eiadah.app.utils.GlobalVariables

class AddSurgeryActivity : BaseActivity(), Observer<RestObservable> {


    lateinit var binding:ActivityAddSurgeryBinding

    private var surgeryList : ArrayList<AddAllergyData> = ArrayList()
    private var strSurgery :String=""
    lateinit var  request: AddAllergytRequest

    private val viewModel: AddSurgeryViewModel
            by lazy {  ViewModelProviders.of(this).get(AddSurgeryViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_add_surgery)
        binding.clickHandler=this

        request=AddAllergytRequest()
        loadSurgeryData()
    }


    private fun loadSurgeryData() {
        viewModel.loadSurgeryList()
        viewModel.getSurgeryList().observe(this, this)
    }

    private fun setSurgeryAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, surgeryList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.surgeryAdapter = adapter
    }

    fun onItemSelectedSurgery(adapterView: AdapterView<*>, view : View, i : Int, l : Long) {
        if (i != 0) {
            setSelectedSurgery(surgeryList[i].title!!)
        }
    }
//    fun onItemSelectedSurgery(i : Int) {
//        if (i!=0) {
//            strSurgery = surgeryList[i].title.toString()
//        } else {
//            strSurgery = ""
//        }
//        request.title = strSurgery
//    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
               progressDialog.dismiss()
                if (it.data is AddAllergy_Response) {
                    val addallergyResponse : AddAllergy_Response = it.data
                    if (addallergyResponse.status!!) {
                        surgeryList = addallergyResponse.data as ArrayList<AddAllergyData>

                        val addAllergyData = AddAllergyData()
                        addAllergyData.title = "0"
                        addAllergyData.title = "Choose Surgery"
                        surgeryList.add(0, addAllergyData)

                        setSurgeryAdapter()
                    }

                }
                else if (it.data is AddSurgeryResponse) {
                    val addSurgeryResponse : AddSurgeryResponse = it.data
                    if (addSurgeryResponse.status!!) {

                        finish()
                    }

                }

            }
            it.status == Status.ERROR -> RestProcess.progressDialog?.dismiss()
            it.status == Status.LOADING -> RestProcess.progressDialog?.show()
        }
    }

    override fun onBackPressed(){
        finish()
    }

    fun onSaveClick() {

        viewModel.getAddSurgery().observe(this,this)
        viewModel.loadAddSurgery(request)


    }

    private fun setSelectedSurgery(value : String) {
        viewModel.getSelectedSurgery(value).observe(this, Observer {
            binding.selectedSurgery = it
            request.title = it!!
//            SharedStorage.setCheckupDetail(this, saveCheckupDetailsRequest)
        })
    }

}
