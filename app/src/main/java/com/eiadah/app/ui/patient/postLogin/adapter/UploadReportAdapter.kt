package com.myapplication.adapter

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.models.dataModel.ReportListData
import com.eiadah.app.models.dataModel.UploadReportListData
import com.eiadah.app.ui.patient.postLogin.activity.UploadReportDetailActivity
import com.pieboat.shredpreference.SharePreferHelper

class UploadReportAdapter(private val mContext: Context, var reportList:ArrayList<UploadReportListData>) : RecyclerView.Adapter<UploadReportAdapter.ViewHolder>() {
    lateinit var sharePreferHelper: SharePreferHelper
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        sharePreferHelper= SharePreferHelper(mContext)
        p0.itemView.setOnClickListener(View.OnClickListener {
            val intent = Intent(mContext, UploadReportDetailActivity::class.java)
            intent.putExtra("report_id",reportList.get(p1).id)
            intent.putExtra("image",reportList.get(p1).file)
            mContext?.startActivity(intent)
        })

        p0.tv_count.visibility=View.GONE
        p0.tv_record.setText(reportList.get(p1).title)

        if(reportList.get(p1).title.isNullOrEmpty()){

            p0.liner_lay.visibility=View.GONE
        }
        else{

            p0.liner_lay.visibility=View.VISIBLE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_record_list_item, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return reportList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tv_record:TextView
        var tv_count:TextView
        var liner_lay:LinearLayout
        init {

            tv_record=itemView.findViewById(R.id.tv_record)
            tv_count=itemView.findViewById(R.id.tv_count)
            liner_lay=itemView.findViewById(R.id.liner_lay)
        }

//        fun onItemClick() {
//            onItemClickListener.onItemClick(adapterPosition)
//        }
    }




}