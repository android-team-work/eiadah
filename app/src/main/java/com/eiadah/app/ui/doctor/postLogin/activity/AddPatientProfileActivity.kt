package com.eiadah.app.ui.doctor.postLogin.activity

import android.content.Intent
import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityAddPatientProfileBinding
import com.eiadah.app.databinding.ActivityPatientProfileBinding
import com.eiadah.app.models.responseModel.DeletePatientResponse
import com.eiadah.app.models.responseModel.PatientProfileResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.dialogs.AddPrescriptionDialog
import com.eiadah.app.ui.doctor.postLogin.viewModals.GetPatientDetailsViewModel
import com.eiadah.app.ui.patient.postLogin.dialogs.AddEmailDialog
import com.eiadah.app.utils.GlobalVariables
import com.pieboat.shredpreference.SharePreferHelper
import kotlinx.android.synthetic.main.activity_patient_detail.*

class AddPatientProfileActivity : BaseActivity(), Observer<RestObservable> {

    lateinit var binding:ActivityAddPatientProfileBinding
    lateinit var sharePreferHelper: SharePreferHelper
     var id:String=""
    private val viewModel1: GetPatientDetailsViewModel
            by lazy {  ViewModelProviders.of(this).get(GetPatientDetailsViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_add_patient_profile)
        binding.clickHandler=this;

        sharePreferHelper= SharePreferHelper(this)

        id=intent.getStringExtra("id")
        viewModel1.patientDetail(id)
        viewModel1.getPatientDetails().observe(this,this)
    }

    fun edittOncllick() {
        var intent = Intent(this,EditPtientDetailActivity::class.java)
        intent.putExtra("id",id)
        startActivity(intent)
    }
    fun backOncllick() {
        finish()
    }

    fun onplusClick() {
        val addDialog : AddPrescriptionDialog? = this?.let { AddPrescriptionDialog(it,this) }
        addDialog?.show()
    }


    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is PatientProfileResponse) {
                    val patientDetailsViewModel : PatientProfileResponse = it.data

                    if (patientDetailsViewModel.status!!) {

                        binding.edtName.setText(patientDetailsViewModel.data?.name)
                        binding.tvPhone.setText(patientDetailsViewModel.data?.phone)
                        binding.tvEmail.setText(patientDetailsViewModel.data?.email)
                        binding.tvGender.setText(patientDetailsViewModel.data?.gender)
                        binding.tvAge.setText(patientDetailsViewModel.data?.age+" Years")

                    }
                }else if (it.data is DeletePatientResponse) {
                    val deletePatientResponse : DeletePatientResponse = it.data

                    if (deletePatientResponse.status!!) {
                        activitySwitcher(HomeActivity::class.java, true)
                    }
                }
            }
            it.status == Status.ERROR -> {
                it.error!!.fillInStackTrace()
                progressDialog.dismiss()
            }
            it.status == Status.LOADING -> progressDialog.show()
        }
    }
}
