package com.eiadah.app.ui.patient.postLogin.dialogs

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import android.view.Gravity
import android.view.LayoutInflater
import android.view.Window
import com.eiadah.app.Listener.UpdateProfileData
import com.eiadah.app.R
import com.eiadah.app.core.BaseDialog
import com.eiadah.app.databinding.DialogAddNameBinding
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.ProfileViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage

class AddNameDialog (activity: FragmentActivity, updateData: UpdateProfileData) : BaseDialog(activity), LifecycleOwner {


    override fun getLifecycle(): Lifecycle {
        return lifecycleRegistry
    }

    private val profileViewModel : ProfileViewModel
            by lazy {
                ViewModelProviders.of(activity).get(ProfileViewModel::class.java)
            }

    lateinit var binding : DialogAddNameBinding
    lateinit var lifecycleRegistry: LifecycleRegistry

    private val updateData = updateData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_name, null, false)

        setContentView(binding.root)

        val window = window
        window?.setGravity(Gravity.CENTER)
        //window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        window?.attributes?.windowAnimations = R.style.DialogAnimation

        lifecycleRegistry = LifecycleRegistry(this)
        binding.clickHandler = this
        profileViewModel.setUserData(SharedStorage.getUserData(context))
        binding.data = profileViewModel
        binding.lifecycleOwner = this
    }

    fun onSubmit() {
        if (binding.edtName.text.toString().isNullOrEmpty()) {
            showToast(R.string.empty_name)
        } else {
            updateName(binding.edtName.text.toString())
        }

    }

    fun onCancel() {
        dismiss()
    }

    private fun updateName(name : String) {
        var params : HashMap<String, String> = HashMap()
        params[GlobalVariables.PARAMS.PROFILE.NAME] = name
        updateData.updateProfileData(params)
        dismiss()
    }

}