package com.eiadah.app.ui.doctor.preLogin.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody

class DocumentPendingViewModel: ObservableViewModel() {

    var documentResponse : MutableLiveData<RestObservable> = MutableLiveData()
    var profileResponse : MutableLiveData<RestObservable> = MutableLiveData()

    fun getDocumentResponse() : LiveData<RestObservable> {
        return documentResponse
    }

    fun getProfileResponse() : LiveData<RestObservable> {
        return profileResponse
    }

    fun saveDocument(proof1: MultipartBody.Part, proof2: MultipartBody.Part) {
        getService().saveDocument(getHeader(), proof1, proof2)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { documentResponse.value = RestObservable.loading() }
            .subscribe({documentResponse.value = RestObservable.success(it)}, {documentResponse.value = RestObservable.error(it)})
    }

    fun profileApi() {
        getService().getDoctorProfile(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { profileResponse.value = RestObservable.loading() }
            .subscribe(
                {profileResponse.value = RestObservable.success(it)},
                {profileResponse.value = RestObservable.error (it)}
            )
    }
}