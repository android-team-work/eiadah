package com.eiadah.app.ui.patient.postLogin.activity.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class UploadReportListViewModel : ObservableViewModel() {

    private var uploadreportListResponse : MutableLiveData<RestObservable> = MutableLiveData()


    fun getUploadReportlist() : LiveData<RestObservable> {
        return uploadreportListResponse
    }


    fun UploadreportList() {
        getService().getUploadReportList(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { uploadreportListResponse.value = RestObservable.loading() }
            .subscribe(
                {uploadreportListResponse.value = RestObservable.success(it)},
                {uploadreportListResponse.value = RestObservable.error(it)}
            )
    }



}