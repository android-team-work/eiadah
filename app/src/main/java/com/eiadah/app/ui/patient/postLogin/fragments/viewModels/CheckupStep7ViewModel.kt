package com.eiadah.app.ui.patient.postLogin.fragments.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import com.eiadah.app.restAPI.RestObservable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CheckupStep7ViewModel : ViewModel() {

    private var getResultResponse : MutableLiveData<RestObservable> = MutableLiveData()

    fun getResultResponse() : LiveData<RestObservable> {
        return getResultResponse
    }

//    fun loadResult(checkupId : String, symptomId : String, listCount : String, ansList : String) {
//        getCheckupService().getCheckupResult(checkupId, symptomId, listCount, ansList)
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .doOnSubscribe { getResultResponse.value = RestObservable.loading() }
//            .subscribe({ getResultResponse.value = RestObservable.success(it) },
//                { getResultResponse.value = RestObservable.error(it)})
//    }
}
