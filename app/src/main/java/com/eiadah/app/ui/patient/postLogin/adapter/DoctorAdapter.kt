package com.eiadah.app.ui.patient.postLogin.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.databinding.LayoutDoctorListItemBinding
import com.eiadah.app.models.dataModel.DocListData

class DoctorAdapter(context: Context, onItemClickListener: OnItemClickListener,var docList:ArrayList<DocListData>,var tempList:ArrayList<DocListData>) : RecyclerView.Adapter<DoctorAdapter.DoctorViewHolder>() {

    val context = context
    val onItemClickListener = onItemClickListener

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): DoctorViewHolder {
        val itemBinding : LayoutDoctorListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(context),
            R.layout.layout_doctor_list_item, p0, false)
        return DoctorViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return docList.size
    }

    override fun onBindViewHolder(p0: DoctorViewHolder, p1: Int) {

        p0.tv_doc_name.setText("Dr."+" "+docList.get(p1).name)
        p0.tv_spl.setText(docList.get(p1).specilaity)
        p0.tv_exp.setText(docList.get(p1).experience+" yrs of exp.")
        p0.tv_address.setText(docList.get(p1).state)
        p0.tv_clinic_name.setText("Clinic Name : "+" "+docList.get(p1).clinicName)
        p0.tv_fees.setText(docList.get(p1).fee)
        p0.currancy_code.setText(docList.get(p1).currancy_code)

        p0.contact_lay.setOnClickListener(View.OnClickListener {

            var intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:"+docList.get(p1).mobile);
            context.startActivity(intent)
        })


        if (docList.get(p1).name.isNullOrEmpty()){
            p0.doc_lay.visibility=View.GONE
        }

        else{
            p0.doc_lay.visibility=View.VISIBLE
        }
    }

    inner class DoctorViewHolder(itemBinding: LayoutDoctorListItemBinding) : RecyclerView.ViewHolder(itemBinding.root) {

         var tv_doc_name:TextView
         var tv_spl:TextView
         var tv_exp:TextView
         var tv_address:TextView
         var tv_fees:TextView
         var tv_clinic_name:TextView
         var currancy_code:TextView
         var contact_lay:LinearLayout
         var doc_lay:LinearLayout
        init {
            itemBinding.clickHandler = this
            tv_doc_name=itemBinding.tvDocName
            tv_spl=itemBinding.tvSpl
            tv_exp=itemBinding.tvExp
            tv_address=itemBinding.tvAddress
            tv_clinic_name=itemBinding.tvClinicName
            contact_lay=itemBinding.contactLay
            doc_lay=itemBinding.docLay
            tv_fees=itemBinding.tvFees
            currancy_code=itemBinding.currancyCode
        }

        fun onItemClick() {
            onItemClickListener.onItemClick(adapterPosition)
        }

    }


    fun filter(charText: String) {
        var charText = charText
        charText = charText.toLowerCase()
        val nList = ArrayList<DocListData>()
        if (charText.length == 0) {
            nList.addAll(tempList)
        } else {
            for (wp in tempList) {
                val value = wp.name
                if (value != null) {
                    if (value.toLowerCase().contains(charText.toLowerCase()))
                    //contains for less accurate result nd matches for accurate result
                    {
                        nList.add(wp)
                    }
                }
            }
        }
        docList = nList

        notifyDataSetChanged()

    }
}

