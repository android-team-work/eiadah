package com.myapplication.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.bumptech.glide.Glide
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.models.dataModel.ChatListData
import com.eiadah.app.models.dataModel.ReportListData
import com.eiadah.app.models.dataModel.SendMsgData
import com.eiadah.app.ui.patient.postLogin.activity.ChatActivity
import com.eiadah.app.ui.patient.postLogin.activity.ReportDetailsActivity
import com.eiadah.app.utils.GlobalVariables
import com.pieboat.shredpreference.SharePreferHelper

class ChatAdapter(private val mContext: Context, var chatData:ArrayList<SendMsgData>) : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {
    lateinit var sharePreferHelper: SharePreferHelper
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        sharePreferHelper= SharePreferHelper(mContext)


        if (sharePreferHelper.getString(GlobalVariables.SharePref.USER_ID)!!.equals(chatData.get(p1).sender.toString())){
            p0.tv_msg_sender.setText(chatData.get(p1).message)
            Glide.with(mContext).load("/www.eiadahapp.com/uploads/"+sharePreferHelper.getString(GlobalVariables.SharePref.USER_Image)).into(p0.iv_user_sender)
            p0.left_lay.visibility=View.GONE
            p0.right_lay.visibility=View.VISIBLE

            Log.e("imageee","/www.eiadahapp.com/uploads/"+sharePreferHelper.getString(GlobalVariables.SharePref.USER_Image))
        }
        else{
            p0.tv_msg_reciver.setText(chatData.get(p1).message)
            p0.left_lay.visibility=View.VISIBLE
            p0.right_lay.visibility=View.GONE
        }

        Log.e("patient_id",sharePreferHelper.getString(GlobalVariables.SharePref.USER_ID)!!)
        Log.e("patient_idd",chatData.get(p1).sender.toString())
        if(chatData.get(p1).message.isNullOrEmpty()){
            p0.left_lay.visibility=View.GONE
            p0.right_lay.visibility=View.GONE
        }
//        else{
//            p0.left_lay.visibility=View.VISIBLE
//            p0.right_lay.visibility=View.VISIBLE
//        }
   }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.chat_item_layout, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return chatData.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

         var tv_msg_reciver:TextView
         var tv_msg_sender:TextView
         var left_lay:RelativeLayout
         var right_lay:RelativeLayout
         var iv_user_sender:ImageView
        init {

             tv_msg_reciver=itemView.findViewById(R.id.tv_msg_reciver)
             tv_msg_sender=itemView.findViewById(R.id.tv_msg_sender)
            left_lay=itemView.findViewById(R.id.left_lay)
            right_lay=itemView.findViewById(R.id.right_lay)
            iv_user_sender=itemView.findViewById(R.id.iv_user_sender)

        }

//        fun onItemClick() {
//            onItemClickListener.onItemClick(adapterPosition)
//        }
    }




}