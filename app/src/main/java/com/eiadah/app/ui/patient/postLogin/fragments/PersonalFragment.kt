package com.eiadah.app.ui.patient.postLogin.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil

import com.eiadah.app.R
import com.eiadah.app.databinding.FragmentPersonalBinding

/**
 * A simple [Fragment] subclass.
 */
class PersonalFragment : Fragment() {

    lateinit var binding:FragmentPersonalBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding=DataBindingUtil.inflate(inflater,R.layout.fragment_personal, container, false)

       return binding.root
    }


}
