package com.eiadah.app.ui.patient.postLogin.activity.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddAllergytRequest
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AddSurgeryViewModel : ObservableViewModel() {

    private var surgeryResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var AddsurgeryResponse : MutableLiveData<RestObservable> = MutableLiveData()
    var selectedSurgery : MutableLiveData<String> =  MutableLiveData()


    fun getSurgeryList() : LiveData<RestObservable> {
        return surgeryResponse
    }
    fun getAddSurgery() : LiveData<RestObservable> {
        return AddsurgeryResponse
    }


    fun loadSurgeryList(){
        getService().getSurgerylist(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { surgeryResponse.value = RestObservable.loading() }
            .subscribe(
                {surgeryResponse.value = RestObservable.success(it)},
                {surgeryResponse.value = RestObservable.error (it)}
            )
    }
    fun loadAddSurgery(request: AddAllergytRequest){
        getService().AddSurgery(getHeader(),request.title)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { AddsurgeryResponse.value = RestObservable.loading() }
            .subscribe(
                {AddsurgeryResponse.value = RestObservable.success(it)},
                {AddsurgeryResponse.value = RestObservable.error (it)}
            )
    }

    fun getSelectedSurgery(value : String) : LiveData<String> {
        if (selectedSurgery.value==null) {
            selectedSurgery.value = "$value, "
        } else {
            if (!selectedSurgery.value!!.contains(value)) {
                selectedSurgery.value = selectedSurgery.value + value + ", "
            }
        }

        return selectedSurgery
    }
}