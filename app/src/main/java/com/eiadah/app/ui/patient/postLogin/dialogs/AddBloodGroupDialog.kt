package com.eiadah.app.ui.patient.postLogin.dialogs

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import android.view.Gravity
import android.view.LayoutInflater
import android.view.Window
import android.widget.Toast
import com.eiadah.app.Listener.UpdateProfileData
import com.eiadah.app.R
import com.eiadah.app.core.BaseDialog
import com.eiadah.app.databinding.DialogAddBloodGroupBinding
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.ProfileViewModel
import com.eiadah.app.utils.GlobalVariables

class AddBloodGroupDialog (activity: FragmentActivity, private val updateData: UpdateProfileData) : BaseDialog(activity),
    LifecycleOwner {

    private val profileViewModel : ProfileViewModel
            by lazy {
                ViewModelProviders.of(activity).get(ProfileViewModel::class.java)
            }

    lateinit var binding : DialogAddBloodGroupBinding
    lateinit var lifecycleRegistry: LifecycleRegistry

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_blood_group, null, false)

        setContentView(binding.root)

        var window = window
        window?.setGravity(Gravity.CENTER)
        //window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        window?.attributes?.windowAnimations = R.style.DialogAnimation

        lifecycleRegistry = LifecycleRegistry(this)
        binding.clickHandler = this
        binding.data = profileViewModel
        binding.lifecycleOwner = this

    }

    override fun getLifecycle(): Lifecycle {
        return lifecycleRegistry
    }

    fun onSubmit() {
        if (binding.spinnerBloodGroup.selectedItemPosition==0) {
            showToast(R.string.empty_blood_group)
        } else {
            updateBloodGroup(context.resources.getStringArray(R.array.blood_group_list)[binding.spinnerBloodGroup.selectedItemPosition])
        }

    }

    fun onCancel() {
        dismiss()
    }

    private fun updateBloodGroup(blood : String) {
        var params : HashMap<String, String> = HashMap()
        params[GlobalVariables.PARAMS.PROFILE.BLOOD_GROUP] = blood
        updateData.updateProfileData(params)
        dismiss()
    }

}