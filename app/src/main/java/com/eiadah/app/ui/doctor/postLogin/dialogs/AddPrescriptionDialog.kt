package com.eiadah.app.ui.doctor.postLogin.dialogs

import android.app.Dialog
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.core.content.ContextCompat.startActivity
import android.util.Patterns
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import com.eiadah.app.Listener.UpdateProfileData
import com.eiadah.app.R
import com.eiadah.app.core.BaseDialog
import com.eiadah.app.databinding.DialogAddEmailBinding
import com.eiadah.app.databinding.DialogAddPaitentPrescriptionLayoutBinding
import com.eiadah.app.ui.doctor.postLogin.activity.AddAppointmentActivity
import com.eiadah.app.ui.doctor.postLogin.activity.AddPatientActivity
import com.eiadah.app.ui.doctor.postLogin.activity.ChangePasswordActivity
import com.eiadah.app.ui.doctor.postLogin.activity.PatientDetailActivity
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.ProfileViewModel
import com.eiadah.app.utils.GlobalVariables

class AddPrescriptionDialog (activity: FragmentActivity, context: Context) : BaseDialog(activity),
    LifecycleOwner {

    private val profileViewModel : ProfileViewModel
            by lazy {
                ViewModelProviders.of(activity).get(ProfileViewModel::class.java)
            }

    lateinit var binding : DialogAddPaitentPrescriptionLayoutBinding
    lateinit var lifecycleRegistry: LifecycleRegistry

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_paitent_prescription_layout, null, false)

        setContentView(binding.root)

        var window = window
        window?.setGravity(Gravity.BOTTOM)
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        window?.attributes?.windowAnimations = R.style.DialogAnimation

        lifecycleRegistry = LifecycleRegistry(this)
        binding.clickHandler = this
//        binding.data = profileViewModel
        binding.lifecycleOwner = this

    }

    override fun getLifecycle(): Lifecycle {
        return lifecycleRegistry
    }

    fun onCancel() {
        dismiss()
    }


    fun addApp(){
        val intent = Intent(context, AddAppointmentActivity::class.java)
        intent.putExtra("id",PatientDetailActivity.patient_id)
         context.startActivity(intent)

    }


}

