package com.eiadah.app.ui.patient.postLogin.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityAddMedicBinding
import com.eiadah.app.models.dataModel.AddAllergyData
import com.eiadah.app.models.requestModels.AddAllergytRequest
import com.eiadah.app.models.responseModel.AddAllergy_Response
import com.eiadah.app.models.responseModel.AddMedicResponse
import com.eiadah.app.models.responseModel.AddSurgeryResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.RestProcess
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.AddMedicineViewModel

class AddMedicActivity : BaseActivity(), Observer<RestObservable> {

    lateinit var binding:ActivityAddMedicBinding
    private var medicList : ArrayList<AddAllergyData> = ArrayList()
    private var strMedic = ""
    lateinit var   request: AddAllergytRequest
    private val viewModel: AddMedicineViewModel
            by lazy {  ViewModelProviders.of(this).get(AddMedicineViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_add_medic)
        binding.clickHandler=this

        request= AddAllergytRequest()
        loadMedicData()
    }



    private fun loadMedicData() {
        viewModel.loadMedicList()
        viewModel.getMedicList().observe(this, this)
    }

    private fun setMdicAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, medicList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.medicAdapter = adapter
    }


//    fun onItemSelectedAlergy(i : Int) {
//        if (i!=0) {
//            strMedic = medicList[i].title.toString()
//        } else {
//            strMedic = ""
//        }
//        request.title = strMedic
//    }


    fun onItemSelectedmedic(adapterView: AdapterView<*>, view : View, i : Int, l : Long) {
        if (i != 0) {
            setSelectedAllergy(medicList[i].title!!)
        }
    }

        override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is AddAllergy_Response) {
                    val addallergyResponse : AddAllergy_Response = it.data
                    if (addallergyResponse.status!!) {
                        medicList = addallergyResponse.data as ArrayList<AddAllergyData>

                        val addAllergyData = AddAllergyData()
                        addAllergyData.title = "0"
                        addAllergyData.title = "Choose Mediciation"
                        medicList.add(0, addAllergyData)

                        setMdicAdapter()
                    }

                }
                else if (it.data is AddMedicResponse) {
                    val addMedicResponse : AddMedicResponse = it.data
                    if (addMedicResponse.status!!) {
                       finish()
                    }

                }

            }
            it.status == Status.ERROR -> RestProcess.progressDialog?.dismiss()
            it.status == Status.LOADING -> RestProcess.progressDialog?.show()
        }
    }

    override fun onBackPressed(){
        finish()
    }

    fun onSaveClick() {

        viewModel.getAddMedicList().observe(this,this)
        viewModel.loadAddMedicList(request)


    }
    private fun setSelectedAllergy(value : String) {
        viewModel.getSelectedMedic(value).observe(this, Observer {
            binding.selectedMedic = it
            request.title = it!!
//            SharedStorage.setCheckupDetail(this, saveCheckupDetailsRequest)
        })
    }
}
