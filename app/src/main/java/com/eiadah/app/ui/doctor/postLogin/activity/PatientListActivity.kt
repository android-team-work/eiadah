package com.eiadah.app.ui.doctor.postLogin.activity

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityPatientListBinding
import com.eiadah.app.models.dataModel.CityData
import com.eiadah.app.models.dataModel.PatientListData
import com.eiadah.app.models.requestModels.AddPrescriptionRequest
import com.eiadah.app.models.responseModel.*
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.viewModals.PatientListViewModel
import com.myapplication.adapter.PatientListAdapter
import kotlinx.android.synthetic.main.activity_patient_list.*
import kotlinx.android.synthetic.main.activity_reach2.*

class PatientListActivity : BaseActivity(), OnItemClickListener, Observer<RestObservable> {



    lateinit var binding:ActivityPatientListBinding
    lateinit var adapter:PatientListAdapter
    lateinit var patientList:ArrayList<PatientListData>
    lateinit var request: AddPrescriptionRequest
     var id:String=""
    private val viewModel: PatientListViewModel
            by lazy {  ViewModelProviders.of(this).get(PatientListViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_patient_list)
        binding.clickHandler=this


        request=AddPrescriptionRequest()
        viewModel.patientList()
        viewModel.getpatientList().observe(this, this)


    }

    fun addOncllick() {
        activitySwitcher(AddPatientActivity::class.java, false)
    }
    fun backOncllick() {
        finish()
    }
    override fun onItemClick(position: Int) {

            val dialog = Dialog(this,R.style.Theme_Dialog)
            dialog .requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog .setContentView(R.layout.patient_app_options)
            dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
            val window = dialog.window
            window!!.setGravity(Gravity.BOTTOM)
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            var tv_call: TextView
            tv_call=dialog.findViewById(R.id.tv_call)
            tv_call.setOnClickListener(View.OnClickListener {
                    dialog.dismiss()

                var intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:"+patientList.get(position).mobileNumber);
                startActivity(intent)
            })
           var tv_presc: TextView
            tv_presc=dialog.findViewById(R.id.tv_presc)
            tv_presc.setOnClickListener(View.OnClickListener {
                        dialog.dismiss()

              precription_dialog(patientList.get(position).id.toString())
            })
        var tv_edit: TextView
             tv_edit=dialog.findViewById(R.id.tv_edit)
             tv_edit.setOnClickListener(View.OnClickListener {
                    dialog.dismiss()

                var intent = Intent(this,EditPtientDetailActivity::class.java)
                 intent.putExtra("id",patientList.get(position).id.toString())
                startActivity(intent)
            })
            dialog .show()


        id=patientList.get(position).id.toString()
    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is PatientListResponse) {

                    val patientListResponse : PatientListResponse = it.data
                    patientList = (patientListResponse.getData() as ArrayList<PatientListData>?)!!
                    val cityData = PatientListData()
//                            cityData.id = "0"
//                            cityData.name = "Choose city"
                    patientList.add(cityData)

                    adapter= PatientListAdapter(this,this,patientList)
                    patient_recycle.setLayoutManager(LinearLayoutManager(this))
                    patient_recycle.setAdapter(adapter)
                }
                else   if (it.data is AddPrescriptionResponse) {
                    val addPrescriptionResponse : AddPrescriptionResponse = it.data

                    if (addPrescriptionResponse.status!!) {

                    }
                }
            }
            it.status == Status.ERROR -> progressDialog.dismiss()
            it.status == Status.LOADING -> progressDialog.show()
        }
    }


    fun precription_dialog(id:String){
        val dialog = Dialog(this,R.style.Theme_Dialog)
        dialog .requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog .setContentView(R.layout.add_prescribe)
        dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        val window = dialog.window
        window!!.setGravity(Gravity.BOTTOM)
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

        var edtPrescribe: EditText
        edtPrescribe=dialog.findViewById(R.id.edtPrescribe)

        request.user_id=id
        request.name=edtPrescribe.text.toString()
        var btn_submit: Button
        btn_submit=dialog.findViewById(R.id.btn_submit)
        btn_submit.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            viewModel.AddAddPrescription(request)
            viewModel.getAddPrescription().observe(this,this)

        })
        var btn_cancel: Button
        btn_cancel=dialog.findViewById(R.id.btn_cancel)
        btn_cancel.setOnClickListener(View.OnClickListener {
            dialog.dismiss()

        })
        dialog .show()

    }
}
