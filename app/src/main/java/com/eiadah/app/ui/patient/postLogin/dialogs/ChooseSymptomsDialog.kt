package com.eiadah.app.ui.patient.postLogin.dialogs

import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.Window
import com.eiadah.app.R
import com.eiadah.app.core.BaseDialog
import com.eiadah.app.databinding.DialogChooseSymptomsBinding
import com.eiadah.app.models.dataModel.SymptomData
import com.eiadah.app.ui.patient.postLogin.adapter.SymptomAdapter

class ChooseSymptomsDialog(context: Context, val title : String, val list: List<SymptomData>, private val parentPosition: Int, private val symptomResult: SymptomResult) : BaseDialog(context), SymptomAdapter.SymptomChecked {
    lateinit var binding : DialogChooseSymptomsBinding

    init {

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_choose_symptoms, null, false)

        setContentView(binding.root)

        var window : Window = window
        window.setGravity(Gravity.CENTER)
        //window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window.setBackgroundDrawableResource(android.R.color.transparent)
        window.attributes.windowAnimations = R.style.DialogAnimation

        binding.clickHandler = this
        binding.title = title

        val adapter = SymptomAdapter(context, this)
        binding.adapter = adapter
        binding.adapter!!.updateList(list)

    }

    fun onCancel() {

    }

    fun onSubmit() {
        dismiss()
    }

    override fun onSymptomChecked(position: Int, checked: Boolean) {
        list[position].isChecked  = checked
        symptomResult.onChooseSymptomResult(parentPosition, list)
    }

    interface SymptomResult {
        fun onChooseSymptomResult(parentPosition: Int, list: List<SymptomData>)
    }
}