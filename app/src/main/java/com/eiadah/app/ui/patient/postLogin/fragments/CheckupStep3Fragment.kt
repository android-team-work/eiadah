package com.eiadah.app.ui.patient.postLogin.fragments


import android.app.Dialog
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.RelativeLayout
import com.eiadah.app.Listener.CheckupChangeFragmentListener

import com.eiadah.app.R
import com.eiadah.app.core.BaseFragment
import com.eiadah.app.databinding.FragmentCheckupStep3Binding
import com.eiadah.app.models.requestModels.SaveCheckupDetailsRequest
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.CheckupStep3ViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.pieboat.shredpreference.SharePreferHelper
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException

/**
 * A simple [Fragment] subclass.
 *
 */
class CheckupStep3Fragment : BaseFragment() {

    lateinit var mListener : CheckupChangeFragmentListener
    lateinit var binding : FragmentCheckupStep3Binding
    lateinit var saveCheckupDetailsRequest: SaveCheckupDetailsRequest
    var heightFeet = ""
    var heightInches = ""
    lateinit var sharePreferHelper: SharePreferHelper

    private val viewModel: CheckupStep3ViewModel
            by lazy {  ViewModelProviders.of(this).get(CheckupStep3ViewModel::class.java) }

    companion object {
        fun newInstance() = CheckupStep3Fragment()

        val TAG : String = CheckupStep3Fragment::class.java.simpleName
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_checkup_step3, container, false)
        binding.clickHandler = this

        saveCheckupDetailsRequest = SharedStorage.getCheckupDetail(context!!)
        sharePreferHelper= SharePreferHelper(activity!!)
        return binding.root
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is CheckupChangeFragmentListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement CheckupChangeFragmentListener")
        }
    }

    fun onCalculateBMI() {
        hideKeyboard()
        if (isValidBMI()) {
            val strFeet : String = heightFeet.split("'")[0]
            val strInches : String = heightInches.split('"')[0]
            val feet : Float = java.lang.Float.parseFloat(strFeet)
            val inches : Float = java.lang.Float.parseFloat(strInches)
            val height : Float = viewModel.getCm(viewModel.getInches(feet, inches))/100

            val weight : Float = java.lang.Float.parseFloat(binding.edtCheckupStep3Weight.text.toString())
            viewModel.setBmi(weight, height)
            binding.data = viewModel
        }
    }

    fun onSaveClick() {
        if (isValid()) {
            SharedStorage.setCheckupDetail(context!!, saveCheckupDetailsRequest)
            sharePreferHelper.saveString(GlobalVariables.SharePref.WIGHT,binding.edtCheckupStep3Weight.text.toString())
            mListener.changeFragment(CheckupStep4Fragment.newInstance(), CheckupStep4Fragment.TAG)
        }

    }

    fun onItemSelectedFeet(adapterView: AdapterView<*>, view : View, i : Int, l : Long) {
        heightFeet = if (i!=0) {
            context!!.resources.getStringArray(R.array.height_feet_list)[i]
        } else {
            ""
        }
    }

    fun onItemSelectedInches(adapterView: AdapterView<*> , view : View , i : Int , l : Long) {
        heightInches = if (i!=0) {
            context!!.resources.getStringArray(R.array.height_inch_list)[i]
        } else {
            ""
        }
    }

    private fun isValidBMI() : Boolean {
        var check = false
        if (binding.edtCheckupStep3Weight.text.toString().isEmpty()
            && heightFeet.isEmpty()
            && heightInches.isEmpty()) {
            showToast(R.string.please_fill_all_fields)
        } else if (binding.edtCheckupStep3Weight.text.toString().isEmpty()) {
            showToast(R.string.empty_weight)
        } else if (heightFeet.isEmpty()) {
            showToast(R.string.empty_height_feet)
        } else if (heightInches.isEmpty()) {
            showToast(R.string.empty_height_inches)
        } else {
            check = true
        }

        return check
    }

    private fun isValid() : Boolean {
        var check = false

        if (binding.txtCheckupStep3BodyMass.text.toString().isEmpty()) {
            showToast(R.string.empty_bmi)
        } else {
            check = true
            saveCheckupDetailsRequest.weight = binding.edtCheckupStep3Weight.text.toString()
            saveCheckupDetailsRequest.height_feet = "$heightFeet $heightInches"
            saveCheckupDetailsRequest.body_mass_index = binding.txtCheckupStep3BodyMass.text.toString()
            saveCheckupDetailsRequest.results = binding.txtCheckupStep3Result.text.toString()
        }

        return check
    }
    fun dialogImage(){

        val dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.view_bmi_dialog)

        dialog.show()

    }


}
