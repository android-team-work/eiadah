package com.eiadah.app.ui.patient.postLogin.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityUploadedReportBinding
import com.eiadah.app.models.dataModel.ReportListData
import com.eiadah.app.models.dataModel.UploadReportListData
import com.eiadah.app.models.responseModel.ReportListResponse
import com.eiadah.app.models.responseModel.UploadReportListResponse
import com.eiadah.app.models.responseModel.UploadReportResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.RestProcess
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.ReportListViewModel
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.UploadReportListViewModel
import com.myapplication.adapter.RecordAdapter
import com.myapplication.adapter.UploadReportAdapter

class UploadedReportActivity : BaseActivity(), Observer<RestObservable> {

    lateinit var binding:ActivityUploadedReportBinding
    lateinit var adapter: UploadReportAdapter
    lateinit var reportlist:ArrayList<UploadReportListData>

    private val viewModel: UploadReportListViewModel
            by lazy {  ViewModelProviders.of(this).get(UploadReportListViewModel::class.java) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil.setContentView(this,R.layout.activity_uploaded_report)
        binding.clickHandler=this

        viewModel.UploadreportList()
        viewModel.getUploadReportlist().observe(this,this)
    }

   override fun onBackPressed(){
        finish()
    }
    
    fun onUploaadClick(){
        
        val intent=Intent(this,UploadReportActivity::class.java)
        startActivity(intent)
    }

    override fun onChanged(it: RestObservable?) {

        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is UploadReportListResponse) {

                    val reportListResponse : UploadReportListResponse = it.data
                    reportlist = (reportListResponse.getData() as ArrayList<UploadReportListData>?)!!
                    val reportData = UploadReportListData()
                    reportlist.add(reportData)

                    if(reportlist.size==1){
                        binding.textRec.visibility= View.VISIBLE
                        binding.uplodedRecordRecyclerview.visibility= View.GONE

                    }
                    else{
                        binding.textRec.visibility= View.GONE
                        binding.uplodedRecordRecyclerview.visibility= View.VISIBLE
                        adapter= UploadReportAdapter(this,reportlist)
                        binding.uplodedRecordRecyclerview.setLayoutManager(LinearLayoutManager(this))
                        binding.uplodedRecordRecyclerview.setAdapter(adapter)
                    }

                }
            }
            it.status == Status.ERROR -> RestProcess.progressDialog?.dismiss()
            it.status == Status.LOADING -> RestProcess.progressDialog?.show()
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.UploadreportList()
        viewModel.getUploadReportlist().observe(this,this)

    }
}
