package com.myapplication.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.eiadah.app.R
import com.eiadah.app.models.dataModel.NotificationListData

class NotificationAdapter(private val mContext: Context,var notificationList : ArrayList<NotificationListData>) : RecyclerView.Adapter<NotificationAdapter.AppointmentViewHolder>() {
    override fun onBindViewHolder(p0: AppointmentViewHolder, p1: Int) {

        p0.username.setText(notificationList.get(p1).name)
        p0.tv_msg.setText(notificationList.get(p1).meesage)

        if (notificationList.get(p1).name.isNullOrEmpty()){

            p0.card_view.visibility=View.GONE
        }
        else{
            p0.card_view.visibility=View.VISIBLE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppointmentViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.notification_item_layout, parent, false)
        return AppointmentViewHolder(v)
    }

    override fun getItemCount(): Int {
        return notificationList.size
    }

    inner class AppointmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

         var username:TextView
         var tv_msg:TextView
         var tv_time:TextView
         var card_view:CardView

        init {

            username=itemView.findViewById(R.id.username)
            tv_msg=itemView.findViewById(R.id.tv_msg)
            tv_time=itemView.findViewById(R.id.tv_time)
            card_view=itemView.findViewById(R.id.card_view)

        }
    }
}