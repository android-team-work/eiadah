package com.eiadah.app.ui.doctor.preLogin.adapter

import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eiadah.app.R
import com.eiadah.app.databinding.ClinicTimingListItemBinding
import com.eiadah.app.models.dataModel.ClinicTimeData

class ClinicTimingAdapter : RecyclerView.Adapter<ClinicTimingAdapter.ClinicTimingViewHolder>() {

    lateinit var list: ArrayList<ClinicTimeData>

    init {
        list = ArrayList()
    }

    fun updateList(list: ArrayList<ClinicTimeData>) {
        this.list = list
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ClinicTimingViewHolder {
        val itemBinding : ClinicTimingListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(p0.context), R.layout.clinic_timing_list_item, p0, false)
        return ClinicTimingViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: ClinicTimingViewHolder, p1: Int) {

        p0.itemBinding.data = list[p1]
    }

    inner class ClinicTimingViewHolder(val itemBinding : ClinicTimingListItemBinding) : RecyclerView.ViewHolder(itemBinding.root) {
        init {

        }
    }
}