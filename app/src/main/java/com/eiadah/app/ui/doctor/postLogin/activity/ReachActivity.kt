package com.eiadah.app.ui.doctor.postLogin.activity

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityReachBinding
import com.eiadah.app.models.dataModel.CountryData
import com.eiadah.app.restAPI.RestObservable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ReachActivity : BaseActivity() {
    lateinit var binding:ActivityReachBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil.setContentView(this,R.layout.activity_reach)
        binding.clickHandler=this


    }


    fun startedtOncllick() {
        activitySwitcher(Reach2Activity::class.java, false)
    }
    fun backOncllick() {

        finish()
    }

}
