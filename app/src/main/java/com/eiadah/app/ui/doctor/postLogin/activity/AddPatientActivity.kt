package com.eiadah.app.ui.doctor.postLogin.activity

import android.app.DatePickerDialog
import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.RadioGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityAddPatientBinding
import com.eiadah.app.models.dataModel.CityData
import com.eiadah.app.models.dataModel.CountryData
import com.eiadah.app.models.dataModel.SpecialistData
import com.eiadah.app.models.dataModel.StateData
import com.eiadah.app.models.requestModels.AddPatientRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.models.responseModel.*
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.viewModals.AddPatientViewModel
import com.eiadah.app.ui.doctor.preLogin.viewModels.EditDoctorProfileViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.mukesh.countrypicker.Country
import com.mukesh.countrypicker.CountryPicker
import com.mukesh.countrypicker.listeners.OnCountryPickerListener
import com.pieboat.shredpreference.SharePreferHelper
import kotlinx.android.synthetic.main.activity_add_appointment.*
import java.util.*
import kotlin.collections.ArrayList

class AddPatientActivity : BaseActivity(), Observer<RestObservable> {


    lateinit var binding:ActivityAddPatientBinding

    private var strCity = ""
    private var strName = ""
    private var strMobile = ""
    private var strDob = ""
    private var strAge = ""
    private var strAddress = ""
    private var strlocatily = ""
    private var strEmail = ""
    private var strPincode = ""
    private var strdialCode = ""
    private var cityList : ArrayList<CityData> = ArrayList()
    lateinit var request: AddPatientRequest
    private var strGender = GlobalVariables.Gender.MALE
    lateinit var sharePreferHelper: SharePreferHelper
    lateinit var datePickerDialog: DatePickerDialog

    private val viewModel: AddPatientViewModel
            by lazy {  ViewModelProviders.of(this).get(AddPatientViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_add_patient)
        binding.clickHandler=this
        request = AddPatientRequest()
        request.gender = strGender

        sharePreferHelper= SharePreferHelper(this)

    }
    fun crossOncllick() {
       finish()
    }


    fun onSaveClick(){

        strAddress=binding.edtStreetAddress.text.toString()
        strCity=binding.edtStreetAddress.text.toString()
        strName=binding.edtName.text.toString()
        strMobile=binding.edtPtHone.text.toString()
        strDob=binding.edtDob.text.toString()
        strAge=binding.edtAge.text.toString()
        strlocatily=binding.edtLocality.text.toString()
        strPincode=binding.edtPincode.text.toString()
        strEmail=binding.edtEmail.text.toString()
        strdialCode=binding.edtDoctorMobileCode.text.toString()

        if (isValid()) {
            request.name = strName
            request.mobile=strdialCode+strMobile
            request.email=strEmail
            request.locality=strlocatily
            request.address=strAddress
            request.pincode=strPincode
            request.age=strAge
            request.dob=strDob


            viewModel.AddPatient(request)
            viewModel.getAddPatient().observe(this, this)
//            activitySwitcher(HomeActivity::class.java, true)
        }

    }


    private fun loadCityData(stateId : String) {
        viewModel.loadCityList(stateId)
        viewModel.getCityList().observe(this, this)
    }

    private fun setCityAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, cityList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.cityAdapter = adapter
    }

    fun onItemSelectedCity(adapterView: AdapterView<*>, view : View, i : Int, l : Long) {
        strCity = if (i!=0) {
            cityList[i].name!!
        } else {
            ""
        }

        request.city = strCity
    }
    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
               if (it.data is CitiesResponse) {
                    val citiesResponse : CitiesResponse = it.data
                    if (citiesResponse.status!!) {
                        cityList = (citiesResponse.cityList as ArrayList<CityData>?)!!
                        val cityData = CityData()
                        cityData.id = "0"
                        cityData.name = "Choose city"
                        cityList.add(0, cityData)
                        setCityAdapter()
                    }

                }
               else if (it.data is AddPatientResponse) {
                    val addPatientResponse : AddPatientResponse = it.data

                    if (addPatientResponse.status!!) {
                        sharePreferHelper.saveString(GlobalVariables.SharePref.PATIENT_ID, addPatientResponse.getUser()?.getId().toString())
                      SharedStorage.setPatientData(this, addPatientResponse.getUser()!!)
                        activitySwitcher(HomeActivity::class.java, true)
                    }
                }
            }
            it.status == Status.ERROR -> {
                it.error!!.fillInStackTrace()
                progressDialog.dismiss()
            }
            it.status == Status.LOADING -> progressDialog.show()
        }
    }


    fun onCheckedChanged(radioGroup: RadioGroup, id : Int) {
        when(id) {
            R.id.radioButtonMale -> {
                // viewModel.setGender(GlobalVariables.Gender.MALE)
                binding.radioButtonMale.setTextColor(ContextCompat.getColorStateList(this, R.color.white))
                binding.radioButtonFemale.setTextColor(ContextCompat.getColorStateList(this, R.color.black))
            }

            R.id.radioButtonFemale -> {
                //viewModel.setGender(GlobalVariables.Gender.FEMALE)
                binding.radioButtonMale.setTextColor(ContextCompat.getColorStateList(this, R.color.black))
                binding.radioButtonFemale.setTextColor(ContextCompat.getColorStateList(this, R.color.white))
            }
        }
    }

    fun openCalender(){
        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR) // current year
        val mMonth = c.get(Calendar.MONTH) // current month
        val mDay = c.get(Calendar.DAY_OF_MONTH) // current day
        // date picker dialog   MM/dd/yyyy
        datePickerDialog = DatePickerDialog(this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // set day of month , month and year value in the edit text
                binding.edtDob.setText(year.toString() + "-" + (monthOfYear + 1).toString() + "-" + dayOfMonth) }, mYear, mMonth, mDay)
//                   tv_app_date.setText((monthOfYear + 1).toString() + "/" + dayOfMonth + "/" + year) }, mYear, mMonth, mDay)
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis() - 1000
        datePickerDialog.show()
    }

    private fun isValid() : Boolean {
        var check = false

        if (strName.isEmpty() && strAddress.isEmpty() && strAge.isEmpty() && strDob.isEmpty()
            && strGender.isEmpty() && strMobile.isEmpty() && strPincode.isEmpty() && strlocatily.isEmpty()) {
            showToast(R.string.please_fill_all_fields)
        } else if (strName.isEmpty()) {
            showToast(R.string.empty_name)
        } else if (strMobile.isEmpty()) {
            showToast("Please Enter Mobile Number")
        }  else if (strDob.isEmpty()) {
            showToast("Please Enter Dob")
        } else if (strGender.isEmpty()) {
            showToast(R.string.please_choose_gender)
        } else if (strAge.isEmpty()) {
            showToast("Please Enter Age ")
        }  else if (strAddress.isEmpty()) {
            showToast("Please Enter Address ")
        } else if (strlocatily.isEmpty()) {
            showToast("Please Enter Locality")
        } else if (strPincode.isEmpty()) {
            showToast("Please Enter pincode")
        } else {
            check = true
        }

        return check
    }
    fun showcountrypickerdialog() {

        var builder = CountryPicker.Builder().with(this).listener(object :
            OnCountryPickerListener {

            override fun onSelectCountry(country: Country) {
                binding.edtDoctorMobileCode.setText(country.dialCode)
            }
        })
        var picker = builder.build()
        picker.showDialog(this);

    }
}
