package com.eiadah.app.ui.doctor.postLogin.activity

import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityCreateProfileBinding

class CreateProfileActivity : BaseActivity() {
    lateinit var binding:ActivityCreateProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil.setContentView(this,R.layout.activity_create_profile)
        binding.clickHandler=this
    }

    fun continueOncllick() {
        activitySwitcher(CreateProfile2Activity::class.java, false)
    }
    fun helpOncllick() {
        activitySwitcher(ProfileIssueActivity::class.java, false)
    }
    fun backOncllick() {
        finish()
    }

}
