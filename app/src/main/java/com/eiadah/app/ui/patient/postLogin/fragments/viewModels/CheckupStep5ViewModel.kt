package com.eiadah.app.ui.patient.postLogin.fragments.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.SaveCheckupDetailsRequest
import com.eiadah.app.models.responseModel.CountryResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CheckupStep5ViewModel : ObservableViewModel() {

    var countryResponse : MutableLiveData<CountryResponse> = MutableLiveData()

    var visitedLocation : MutableLiveData<String> =  MutableLiveData()

    var saveCheckupResponse : MutableLiveData<RestObservable> = MutableLiveData()

    fun getCountryList() : LiveData<CountryResponse> {
        return countryResponse
    }

    fun loadCountryList() {
        getService().getCountryList(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::setCountyResponse, this::onError)
    }

    private fun setCountyResponse(countryResponse: CountryResponse) {
        this.countryResponse.value = countryResponse
    }

    private fun onError(throwable: Throwable) {
        throwable.fillInStackTrace()
    }

    fun getVisitedLocation(value : String) : LiveData<String> {
        if (visitedLocation.value==null) {
            visitedLocation.value = "$value, "
        } else {
            if (!visitedLocation.value!!.contains(value)) {
                visitedLocation.value = visitedLocation.value + value + ", "
            }
        }

        return visitedLocation
    }

    fun getSaveCheckupResponse() : LiveData<RestObservable> {
        return saveCheckupResponse
    }

    fun saveCheckupDetail(saveCheckupDetailsRequest: SaveCheckupDetailsRequest) {
        getCheckupService().saveCheckupResponse(saveCheckupDetailsRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { saveCheckupResponse.value = RestObservable.loading() }
            .subscribe({ saveCheckupResponse.value = RestObservable.success(it) },
                { saveCheckupResponse.value = RestObservable.error(it)})
    }



}