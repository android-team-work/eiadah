package com.eiadah.app.ui.patient.postLogin.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityAddIncidentBinding
import com.eiadah.app.models.dataModel.AddAllergyData
import com.eiadah.app.models.requestModels.AddAllergytRequest
import com.eiadah.app.models.responseModel.AddAllergy_Response
import com.eiadah.app.models.responseModel.AddIncidentResponse
import com.eiadah.app.models.responseModel.AddSurgeryResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.RestProcess
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.AddAllrgyViewModel
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.AddIncidentViewModel

class AddIncidentActivity : BaseActivity(), Observer<RestObservable> {

    private var alrgyList : ArrayList<AddAllergyData> = ArrayList()
    lateinit var binding:ActivityAddIncidentBinding
    var strIncident:String=""
    private val viewModel: AddIncidentViewModel
            by lazy {  ViewModelProviders.of(this).get(AddIncidentViewModel::class.java) }


    lateinit var  request: AddAllergytRequest
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         binding= DataBindingUtil.setContentView(this,R.layout.activity_add_incident)

        binding.clickHandler=this
        request= AddAllergytRequest()
        loadAllergyData()
    }
    private fun loadAllergyData() {
        viewModel.loadIncidentList()
        viewModel.getIncidentList().observe(this, this)
    }

    private fun setAllergyAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, alrgyList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.incidentAdapter = adapter
    }


    fun onItemSelectedAlergy(adapterView: AdapterView<*>, view : View, i : Int, l : Long) {
        if (i!=0) {
            setSelectedIncident(alrgyList[i].title!!)
        }
//        if (i!=0) {
//            strIncident = alrgyList[i].title.toString()
//        } else {
//            strIncident = ""
//        }
//        request.title = strIncident
    }

    override fun onChanged(it: RestObservable?) {
            when {
                it!!.status == Status.SUCCESS -> {
                    progressDialog.dismiss()
                    if (it.data is AddAllergy_Response) {
                        val addallergyResponse : AddAllergy_Response = it.data
                        if (addallergyResponse.status!!) {
                            alrgyList = addallergyResponse.data as ArrayList<AddAllergyData>

                            val addAllergyData = AddAllergyData()
                            addAllergyData.title = "0"
                            addAllergyData.title = "Choose Incident"
                            alrgyList.add(0, addAllergyData)

                            setAllergyAdapter()
                        }

                    }
                    else if (it.data is AddIncidentResponse) {
                        val addSurgeryResponse : AddIncidentResponse = it.data
                        if (addSurgeryResponse.status!!) {

                            finish()
                        }

                    }

                }
                it.status == Status.ERROR -> RestProcess.progressDialog?.dismiss()
                it.status == Status.LOADING -> RestProcess.progressDialog?.show()
            }

    }

    override fun onBackPressed(){
        finish()
    }

    fun onSaveClick(){
      viewModel.getAddIncident().observe(this,this)
        viewModel.loadAddIncident(request)
    }

    private fun setSelectedIncident(value : String) {
        viewModel.getSelectedIncident(value).observe(this, Observer {
            binding.selectedIncident = it
            request.title = it!!
//            SharedStorage.setCheckupDetail(this, saveCheckupDetailsRequest)
        })
    }

}
