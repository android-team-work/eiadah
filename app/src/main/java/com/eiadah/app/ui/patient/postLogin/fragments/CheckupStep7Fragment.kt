package com.eiadah.app.ui.patient.postLogin.fragments

import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer

import com.eiadah.app.R
import com.eiadah.app.core.BaseFragment
import com.eiadah.app.databinding.FragmentCheckupStep7Binding
import com.eiadah.app.models.dataModel.GetResultData
import com.eiadah.app.models.requestModels.SaveReportRequest
import com.eiadah.app.models.responseModel.DoctorProfileResponse
import com.eiadah.app.models.responseModel.SaveReportResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.preLogin.activity.DocumentPendingActivity
import com.eiadah.app.ui.patient.postLogin.activity.BookAppointmentStep1Activity
import com.eiadah.app.ui.patient.postLogin.activity.RecommendedDoctorActivity
import com.eiadah.app.ui.patient.postLogin.activity.StartCheckupActivity
import com.eiadah.app.ui.patient.postLogin.adapter.CheckupResultAdapter
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.CheckupStep6ViewModel
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.CheckupStep7ViewModel
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.SaveReportViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.pieboat.shredpreference.SharePreferHelper

class CheckupStep7Fragment : BaseFragment(), Observer<RestObservable> {

    var id:String=""
    var resultList:ArrayList<GetResultData> = ArrayList()
    var user_id:String=""
    lateinit var request: SaveReportRequest

    lateinit var sharePreferHelper: SharePreferHelper

    companion object {
        fun newInstance() = CheckupStep7Fragment()
        val TAG : String = CheckupStep7Fragment::class.java.simpleName
    }

    private val viewModel: SaveReportViewModel
            by lazy {  ViewModelProviders.of(this).get(SaveReportViewModel::class.java) }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding : FragmentCheckupStep7Binding = DataBindingUtil.inflate(inflater, R.layout.fragment_checkup_step7, container, false)

        binding.clickHandler = this

        sharePreferHelper= SharePreferHelper(activity!!)
        request=SaveReportRequest()
        val bundle=arguments
         resultList = bundle?.getSerializable("ARRAYLIST") as ArrayList<GetResultData>

//        Log.e("list", (arguments!!.getSerializable("ARRAYLIST") as ArrayList<GetResultData>).toString())
        val clinicTimeData = GetResultData()
        resultList.add(clinicTimeData)

        val checkupResultAdapter = CheckupResultAdapter(context,resultList)
        binding.resultAdapter = checkupResultAdapter

        request.result111=sharePreferHelper.getString(GlobalVariables.SharePref.RESULT_LIST)!!
        request.symptoms=sharePreferHelper.getString(GlobalVariables.SharePref.SYMPTOMS_NAME)!!
        request.name=sharePreferHelper.getString(GlobalVariables.SharePref.NAME)!!
        request.age=sharePreferHelper.getString(GlobalVariables.SharePref.AGE)!!
        request.weight=sharePreferHelper.getString(GlobalVariables.SharePref.WIGHT)!!
        request.gender=sharePreferHelper.getString(GlobalVariables.SharePref.GENDER)!!

        Log.e("listttt",sharePreferHelper.getString(GlobalVariables.SharePref.RESULT_LIST))
        return binding.root


    }

    fun recommendedDoctor() {
        viewModel.getSaveReport().observe(this,this)
        viewModel.saveReport(request)
//        activitySwitcher(BookAppointmentStep1Activity::class.java, false)
    }
    fun startAgain() {
        activitySwitcher(StartCheckupActivity::class.java, false)
    }

    fun saveReport(){
        viewModel.getSaveReport().observe(this,this)
        viewModel.saveReport(request)
    }

    override fun onChanged(it: RestObservable?) {

        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is SaveReportResponse) {
                    val saveReportResponse : SaveReportResponse = it.data

                    if (saveReportResponse.status!!) {

                        activitySwitcher(BookAppointmentStep1Activity::class.java, false)
                        activity!!.finish()
                    }
                }
            }
            it.status == Status.ERROR -> progressDialog.dismiss()
            it.status == Status.LOADING -> progressDialog.show()
        }
    }

}
