package com.eiadah.app.ui.doctor.postLogin.fragments


import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.eiadah.app.R
import com.eiadah.app.databinding.FragmentRecomendationBinding

/**
 * A simple [Fragment] subclass.
 */
class RecomendationFragment : Fragment() {

    lateinit var binding:FragmentRecomendationBinding
    companion object {
        fun newInstance() = RecomendationFragment()
        val TAG: String = RecomendationFragment::class.java.simpleName
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding=DataBindingUtil.inflate(inflater,R.layout.fragment_recomendation, container, false)
        return binding.root
    }


}
