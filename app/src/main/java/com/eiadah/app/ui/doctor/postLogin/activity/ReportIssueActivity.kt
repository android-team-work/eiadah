package com.eiadah.app.ui.doctor.postLogin.activity

import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityReportIssueBinding

class ReportIssueActivity : BaseActivity() {
    lateinit var binding:ActivityReportIssueBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_report_issue)
        binding.clickHandler=this
    }

    fun backOncllick() {
        finish()
    }

    fun reportOncllick() {
        activitySwitcher(ProfileIssueActivity::class.java, false)
    }
}
