package com.eiadah.app.ui.patient.postLogin.fragments.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.models.requestModels.SaveReportRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ChatListViewModel : ObservableViewModel() {

    private var chatListResponse : MutableLiveData<RestObservable> = MutableLiveData()

    fun getChatList() : LiveData<RestObservable> {
        return chatListResponse
    }

    fun loadChatList() {
        getService().getChatList(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { chatListResponse.value = RestObservable.loading() }
            .subscribe(
                {chatListResponse.value = RestObservable.success(it)},
                {chatListResponse.value = RestObservable.error(it)}
            )
    }


}