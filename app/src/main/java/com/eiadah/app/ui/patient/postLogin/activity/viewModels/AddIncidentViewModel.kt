package com.eiadah.app.ui.patient.postLogin.activity.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddAllergytRequest
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AddIncidentViewModel : ObservableViewModel() {

    private var incidentResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var addincidentResponse : MutableLiveData<RestObservable> = MutableLiveData()
    var selectedIncident : MutableLiveData<String> =  MutableLiveData()


    fun getIncidentList() : LiveData<RestObservable> {
        return incidentResponse
    }
   fun getAddIncident() : LiveData<RestObservable> {
        return addincidentResponse
    }


    fun loadIncidentList(){
        getService().getIncidentlist(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { incidentResponse.value = RestObservable.loading() }
            .subscribe(
                {incidentResponse.value = RestObservable.success(it)},
                {incidentResponse.value = RestObservable.error (it)}
            )
    }
    fun loadAddIncident(request: AddAllergytRequest){
        getService().AddAIncient(getHeader(),request.title)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addincidentResponse.value = RestObservable.loading() }
            .subscribe(
                {addincidentResponse.value = RestObservable.success(it)},
                {addincidentResponse.value = RestObservable.error (it)}
            )
    }

    fun getSelectedIncident(value : String) : LiveData<String> {
        if (selectedIncident.value==null) {
            selectedIncident.value = "$value, "
        } else {
            if (!selectedIncident.value!!.contains(value)) {
                selectedIncident.value = selectedIncident.value + value + ", "
            }
        }

        return selectedIncident
    }

}