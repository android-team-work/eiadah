package com.eiadah.app.ui.doctor.postLogin.viewModals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddPatientRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.models.responseModel.AddPatientResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ReportViewModel : ObservableViewModel() {

    private var reporttResponse : MutableLiveData<RestObservable> = MutableLiveData()





    fun getAddReport() : LiveData<RestObservable> {
        return reporttResponse
    }

    fun AddReport(request: AddPatientRequest) {
        getService().getConsultReport(getHeader(), request.text)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { reporttResponse.value = RestObservable.loading() }
            .subscribe(
                {reporttResponse.value = RestObservable.success(it)},
                {reporttResponse.value = RestObservable.error(it)}
            )
    }
}