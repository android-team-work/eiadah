package com.myapplication.adapter

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.eiadah.app.Listener.OnItemClickListener
import com.eiadah.app.R
import com.eiadah.app.models.dataModel.ReportListData
import com.eiadah.app.ui.patient.postLogin.activity.ReportDetailsActivity
import com.pieboat.shredpreference.SharePreferHelper

class RecordAdapter(private val mContext: Context,var reportList:ArrayList<ReportListData>) : RecyclerView.Adapter<RecordAdapter.ViewHolder>() {
    lateinit var sharePreferHelper: SharePreferHelper
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        sharePreferHelper= SharePreferHelper(mContext)
        p0.itemView.setOnClickListener(View.OnClickListener {
            val intent = Intent(mContext, ReportDetailsActivity::class.java)
            intent.putExtra("id",reportList.get(p1).id.toString())
            mContext?.startActivity(intent)

        })
//        p0.optin_menu.setOnClickListener(View.OnClickListener {
//
//            onItemClickListener.onItemClick(p1)
//        })

        p0.tv_record.setText(reportList.get(p1).name)
        p0.tv_count.setText(reportList.get(p1).date)

        if(reportList.get(p1).name.isNullOrEmpty()){
            p0.liner_lay.visibility=View.GONE
        }
        else{
            p0.liner_lay.visibility=View.VISIBLE

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_record_list_item, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return reportList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var tv_record:TextView
        lateinit var tv_count:TextView
        lateinit var liner_lay:LinearLayout
        init {

            tv_record=itemView.findViewById(R.id.tv_record)
            tv_count=itemView.findViewById(R.id.tv_count)
            liner_lay=itemView.findViewById(R.id.liner_lay)
        }

//        fun onItemClick() {
//            onItemClickListener.onItemClick(adapterPosition)
//        }
    }




}