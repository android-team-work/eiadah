package com.eiadah.app.ui.doctor.postLogin.viewModals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ApptDetailsViewModel : ObservableViewModel() {


    private var apptDetailsResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var cancelAppointmentResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var approveAppointmentResponse : MutableLiveData<RestObservable> = MutableLiveData()


    fun getApptDetails() : LiveData<RestObservable> {
        return apptDetailsResponse
    }
    fun getCancelAppointment() : LiveData<RestObservable> {
        return cancelAppointmentResponse
    }
     fun getApproveAppointment() : LiveData<RestObservable> {
        return approveAppointmentResponse
    }



    fun apptDetail(id : String) {
        getService().getapptDetail(getHeader(), id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { apptDetailsResponse.value = RestObservable.loading() }
            .subscribe(
                {apptDetailsResponse.value = RestObservable.success(it)},
                {apptDetailsResponse.value = RestObservable.error (it)}
            )
    }

    fun CancelAppointment(request: AddAppointmenttRequest) {
        getService().cancelAppt(getHeader(), request.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { cancelAppointmentResponse.value = RestObservable.loading() }
            .subscribe(
                {cancelAppointmentResponse.value = RestObservable.success(it)},
                {cancelAppointmentResponse.value = RestObservable.error(it)}
            )
    }
    fun ApproveAppointment(request: AddAppointmenttRequest) {
        getService().approveAppt(getHeader(), request.id,request.email)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { cancelAppointmentResponse.value = RestObservable.loading() }
            .subscribe(
                {cancelAppointmentResponse.value = RestObservable.success(it)},
                {cancelAppointmentResponse.value = RestObservable.error(it)}
            )
    }

}