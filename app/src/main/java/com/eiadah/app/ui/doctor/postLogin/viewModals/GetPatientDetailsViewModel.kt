package com.eiadah.app.ui.doctor.postLogin.viewModals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GetPatientDetailsViewModel : ObservableViewModel() {


    private var patientDetailsResponse : MutableLiveData<RestObservable> = MutableLiveData()


    fun getPatientDetails() : LiveData<RestObservable> {
        return patientDetailsResponse
    }


    fun patientDetail(id : String) {
        getService().getpatientDetail(getHeader(), id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { patientDetailsResponse.value = RestObservable.loading() }
            .subscribe(
                {patientDetailsResponse.value = RestObservable.success(it)},
                {patientDetailsResponse.value = RestObservable.error (it)}
            )
    }



}