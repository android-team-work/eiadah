package com.eiadah.app.ui.patient.postLogin.view_modals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.SaveClinicRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DocListViewModel : ObservableViewModel() {

    var doctorListResponse : MutableLiveData<RestObservable> = MutableLiveData()

    fun getdocList() : LiveData<RestObservable> {
        return doctorListResponse
    }

    fun doctorList() {
        getService().getDoctorlist(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { doctorListResponse.value = RestObservable.loading() }
            .subscribe(
                {doctorListResponse.value = RestObservable.success(it)},
                {doctorListResponse.value = RestObservable.error (it)}
            )
    }
}