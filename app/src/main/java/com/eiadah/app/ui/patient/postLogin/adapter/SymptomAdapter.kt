package com.eiadah.app.ui.patient.postLogin.adapter

import android.content.Context
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eiadah.app.R
import com.eiadah.app.databinding.LayoutSymptomsListItemBinding
import com.eiadah.app.models.dataModel.SymptomData

class SymptomAdapter(val context : Context, val symptomChecked: SymptomChecked) : RecyclerView.Adapter<SymptomAdapter.SymptomViewHolder>() {

    var list : List<SymptomData> = ArrayList()

    fun updateList(list : List<SymptomData>) {
        this.list = list
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SymptomViewHolder {
        val itemBinding : LayoutSymptomsListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_symptoms_list_item, p0, false)
        return SymptomViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: SymptomViewHolder, p1: Int) {
        p0.itemBinding.data = list[p1]
    }

    inner class SymptomViewHolder(val itemBinding: LayoutSymptomsListItemBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        init {
            itemBinding.clickHandler = this
        }

        fun onCheckedChanged(checkBox: View, checked : Boolean) {
            symptomChecked.onSymptomChecked(adapterPosition, checked)
        }
    }

    interface SymptomChecked {
        fun onSymptomChecked(position : Int, checked : Boolean)
    }
}