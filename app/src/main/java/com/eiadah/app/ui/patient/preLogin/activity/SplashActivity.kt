package com.eiadah.app.ui.patient.preLogin.activity

import androidx.databinding.DataBindingUtil
import android.os.Bundle
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityMainBinding
import com.eiadah.app.ui.doctor.preLogin.activity.*
import com.eiadah.app.ui.patient.postLogin.activity.HomeActivity
import com.eiadah.app.ui.patient.postLogin.dialogs.ChooseRegisterTypeDialog
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage


class SplashActivity : BaseActivity(), ChooseRegisterTypeDialog.OnChooseRegisterLoginDialogListener {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (SharedStorage.isSession(this)) {
            if (SharedStorage.getUserType(this) == GlobalVariables.User.PATIENT)
                activitySwitcher(HomeActivity::class.java, true)
            else {
                val doctorData = SharedStorage.getDoctorData(this)
                if (doctorData != null) {
                    if (doctorData.doctor!!.name!!.isEmpty()) {
                        activitySwitcher(CreateDoctorProfileStep1Activity::class.java, true)
                    } else if (doctorData.clinic!!.clinicName ==null) {
                        activitySwitcher(RegisterClinicActivity::class.java, true)
                    } else if (doctorData.doctor!!.verify .equals("0")) {
                        activitySwitcher(LoginDoctorActivity::class.java, true)
                    }
//                    else if (doctorData.doctor!!.registrationImage!!.isEmpty() || doctorData.doctor!!.idProof!!.isEmpty()) {
//                        activitySwitcher(DocumentPendingActivity::class.java, true)
//                    }
                    else {
                        activitySwitcher(com.eiadah.app.ui.doctor.postLogin.activity.HomeActivity::class.java, true)
                    }
                } else {
                    activitySwitcher(CreateDoctorProfileStep1Activity::class.java, true)
                }

            }

        } else {
            val binding : ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
            binding.clickEvent = this
        }
    }

    fun onDoctorClick() {
        val chooseRegisterTypeDialog = ChooseRegisterTypeDialog(this, this, GlobalVariables.User.DOCTOR)
        chooseRegisterTypeDialog.show()
    }

    fun onPatientClick() {
        val chooseRegisterTypeDialog = ChooseRegisterTypeDialog(this, this, GlobalVariables.User.PATIENT)
        chooseRegisterTypeDialog.show()
    }

    override fun onLogin(type : Int) {
        if (type == GlobalVariables.User.PATIENT) {
            activitySwitcher(LoginActivity::class.java, true)
        } else {
            activitySwitcher(LoginDoctorActivity::class.java, true)
        }
    }

    override fun onRegister(type : Int) {
        if (type == GlobalVariables.User.PATIENT) {
            activitySwitcher(RegisterActivity::class.java, true)
        } else {
            activitySwitcher(RegisterDoctorActivity::class.java, true)
        }
    }
}
