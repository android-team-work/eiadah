package com.eiadah.app.ui.doctor.postLogin.activity

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityAppointmentDetailBinding
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.models.responseModel.ApproveApptResponse
import com.eiadah.app.models.responseModel.ApptCancelResponse
import com.eiadah.app.models.responseModel.ApptDetailResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.viewModals.ApptDetailsViewModel
import com.eiadah.app.utils.GlobalVariables
import com.pieboat.shredpreference.SharePreferHelper
import kotlinx.android.synthetic.main.edit_appointment_dialog.*

class AppointmentDetailActivity : BaseActivity(), Observer<RestObservable> {

    lateinit var binding:ActivityAppointmentDetailBinding
     var id: String=""
     var status: String=""
     var phone: String=""
     var email: String=""
    lateinit var sharePreferHelper: SharePreferHelper
    lateinit var request: AddAppointmenttRequest
    private val viewModel: ApptDetailsViewModel
            by lazy {  ViewModelProviders.of(this).get(ApptDetailsViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_appointment_detail)
        binding.clickHandler=this

        id=intent.getStringExtra("id")
        viewModel.apptDetail(id)
        viewModel.getApptDetails().observe(this,this)

        request=AddAppointmenttRequest();
        sharePreferHelper= SharePreferHelper(this)
        binding.tvDocName.setText(sharePreferHelper.getString(GlobalVariables.SharePref.DOCTOR_NAME))

        request.id=id
    }

    fun optionOncllick() {
        dialog()
    }
    fun backOncllick() {
        finish()
    }

     fun dialog() {

        val dialog = Dialog(this,R.style.Theme_Dialog)
        dialog .requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog .setContentView(R.layout.edit_appointment_dialog)
        dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        val window = dialog.window
        window!!.setGravity(Gravity.BOTTOM)
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

         var tv_app_cancel:TextView
         var edt_appt:TextView
         var tv_call:TextView
         var btn_approve:Button
         tv_app_cancel=dialog.findViewById(R.id.tv_app_cancel)
         edt_appt=dialog.findViewById(R.id.edt_appt)
         btn_approve=dialog.findViewById(R.id.btn_approve)
         tv_call=dialog.findViewById(R.id.tv_call)
         tv_app_cancel.setOnClickListener(View.OnClickListener {
             dialog.dismiss()
             viewModel.CancelAppointment(request)
             viewModel.getCancelAppointment().observe(this,this)

         })
         edt_appt.setOnClickListener(View.OnClickListener {
             dialog.dismiss()
             val intent = Intent(this, EditApptActivity::class.java)
             intent.putExtra("id",id)
              startActivity(intent)
              finish()

         })
         tv_call.setOnClickListener(View.OnClickListener {
             dialog.dismiss()
             var intent = Intent(Intent.ACTION_DIAL)
             intent.data = Uri.parse("tel:"+phone);
             startActivity(intent)

         })

         btn_approve.setOnClickListener(View.OnClickListener {
             dialog.dismiss()
             approve()
         })

//         if (status.equals("pending")){
//             btn_approve.visibility=View.VISIBLE
//         }
//
//         else{
//             btn_approve.visibility=View.GONE
//         }
         dialog .show()

    }

    fun approve(){
        request.id=id
        request.email=email
        viewModel.getApproveAppointment().observe(this,this)
        viewModel.ApproveAppointment(request)
    }
    override fun onChanged(it: RestObservable?) {

        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is ApptDetailResponse) {
                    val appointmentResponse : ApptDetailResponse = it.data

                    if (appointmentResponse.status!!){

                        binding.tvName.setText(appointmentResponse.getData()?.get(0)?.getName())
                        binding.tvTime.setText(appointmentResponse.getData()?.get(0)?.getTime())
                        binding.tvCancel.setText(appointmentResponse.getData()?.get(0)?.getStatus())

                        email=appointmentResponse.getData()?.get(0)?.getEmail()!!
                        phone=appointmentResponse.getData()?.get(0)?.getPhone()!!

                        if (appointmentResponse.getData()?.get(0)?.getStatus().equals("cancel")){
                            binding.apptOption.visibility=View.GONE
                        }
                        else{
                            binding.apptOption.visibility=View.VISIBLE
                        }
                    }

                }

                else if (it.data is ApptCancelResponse){
                    val cancelResponse : ApptCancelResponse = it.data

                    if (cancelResponse.status!!){
                        binding.tvCancel.setText(cancelResponse.getUser()?.getStatus())
                    }

                }
                else if (it.data is ApproveApptResponse){
                    val approveResponse : ApproveApptResponse = it.data

                    if (approveResponse.status!!){
                        binding.tvCancel.setText(approveResponse.getUser()?.status)
                    }

                }

            }
            it.status == Status.ERROR -> {
                it.error!!.fillInStackTrace()
                progressDialog.dismiss()
            }
            it.status == Status.LOADING -> progressDialog.show()
        }
    }
}
