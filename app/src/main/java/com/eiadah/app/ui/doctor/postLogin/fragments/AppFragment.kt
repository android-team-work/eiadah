package com.eiadah.app.ui.doctor.postLogin.fragments


import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.eiadah.app.R
import com.eiadah.app.core.BaseFragment
import com.eiadah.app.databinding.FragmentAppBinding
import com.eiadah.app.ui.doctor.postLogin.activity.*

/**
 * A simple [Fragment] subclass.
 *
 */
class AppFragment : BaseFragment() {

    lateinit var binding : FragmentAppBinding

    companion object {
        fun newInstance() = AppFragment()
        val TAG: String = AppFragment::class.java.simpleName
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_app, container, false)
        binding.clickHandler = this
        return binding.root
    }


    fun onProfileClick() {
        activitySwitcher(DoctorProfileActivity::class.java, false)
    }
    fun onCalenderClick() {
        activitySwitcher(CalenderActivity::class.java, false)
    }
    fun onPatientClick() {
        activitySwitcher(PatientActivity::class.java, false)
    }
    fun onReachClick() {
        activitySwitcher(ReachActivity::class.java, false)
    }
    fun onConsultClick() {
        activitySwitcher(ConsultActivity::class.java, false)
    }
    fun onFeeedbackClick() {
        activitySwitcher(ChatListActivity::class.java, false)
    }

}
