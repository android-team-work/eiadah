package com.eiadah.app.ui.doctor.preLogin.activity

import android.annotation.SuppressLint
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityLoginDoctorBinding
import com.eiadah.app.models.responseModel.DoctorProfileResponse
import com.eiadah.app.models.responseModel.ProfileResponse
import com.eiadah.app.models.responseModel.RegisterResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.preLogin.viewModels.DoctorLoginViewModel
import com.eiadah.app.ui.doctor.preLogin.viewModels.DoctorRegisterViewModel
import com.eiadah.app.ui.patient.postLogin.activity.HomeActivity
import com.eiadah.app.ui.patient.preLogin.activity.SplashActivity
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.pieboat.shredpreference.SharePreferHelper
import com.google.firebase.iid.InstanceIdResult
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.util.Log
import com.eiadah.app.models.responseModel.DeviceTokenResponse
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging


class LoginDoctorActivity : BaseActivity(), Observer<RestObservable> {

    private val TAG = "MyFirebaseToken"
    lateinit var binding : ActivityLoginDoctorBinding
    lateinit var sharePreferHelper: SharePreferHelper
    var token:String?=null
    private val viewModel: DoctorLoginViewModel
            by lazy {  ViewModelProviders.of(this).get(DoctorLoginViewModel::class.java) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login_doctor)
        binding.clickHandler = this

        sharePreferHelper= SharePreferHelper(this)

        FirebaseApp.initializeApp(this);
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                token = task.result?.token

                // Log and toast
//                val msg = getString(R.string.msg_token_fmt, token)
                Log.e("device_token", token)
//                Toast.makeText(baseContext, token, Toast.LENGTH_SHORT).show()
            })
    }

    override fun onBackPressed() {
        activitySwitcher(SplashActivity::class.java, true)
    }

    fun onLoginClick() {
        val email = binding.edtRegisterEmail.text.toString()
        val password = binding.edtRegisterPassword.text.toString()

        if (isValid(email, password)) {
            viewModel.loginApi(email, password)
            viewModel.getLoginResponse().observe(this, this)
        }
    }

    fun onForgotClick() {

    }

    private fun isValid(email : String, password : String) : Boolean {
        var check = false

        if (email.isEmpty()  && password.isEmpty()) {
            showToast(R.string.please_fill_all_fields)
        } else if (email.isEmpty()) {
            showToast(R.string.empty_email)
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            showToast(R.string.invalid_email)
        } else if (password.isEmpty()) {
            showToast(R.string.empty_password)
        } else {
            check = true
        }

        return check
    }

    private fun getProfileApi() {
        viewModel.profileApi()
        viewModel.getProfileResponse().observe(this, this)
    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()

                if (it.data is RegisterResponse) {

                    val registerResponse : RegisterResponse = it.data
                    if (registerResponse.token.isNotEmpty()) {
                        SharedStorage.setToken(this, registerResponse.token)
                        getProfileApi()
                        getDeviceToken()
                    }

                } else if (it.data is DeviceTokenResponse) {
                    val deviceTokenResponse : DeviceTokenResponse = it.data

                }
                else if (it.data is DoctorProfileResponse) {

                    val profileResponse : DoctorProfileResponse = it.data
                    if (profileResponse.status!!) {

                        SharedStorage.setSession(this, true)
                        SharedStorage.setUserType(this, GlobalVariables.User.DOCTOR)
                        SharedStorage.setDoctorData(this, profileResponse.data!!)
                        val doctorData = SharedStorage.getDoctorData(this)
                        sharePreferHelper.saveString(GlobalVariables.SharePref.DOCTOR_NAME, profileResponse.data.doctor?.name!!)
                        sharePreferHelper.saveString(GlobalVariables.SharePref.DOCTOR_ID, profileResponse.data.doctor?.userId.toString()!!)
                        sharePreferHelper.saveString(GlobalVariables.SharePref.USER_ID, profileResponse.data.doctor?.userId.toString()!!)
                        sharePreferHelper.saveString(GlobalVariables.SharePref.USER_Image, profileResponse.data.doctor?.docterImage.toString()!!)
                        if (doctorData != null) {
                            if (doctorData.doctor!!.name!!.isEmpty()) {
                                activitySwitcher(CreateDoctorProfileStep1Activity::class.java, true)
                            } else if (doctorData.clinic!!.clinicName == null) {
                                activitySwitcher(RegisterClinicActivity::class.java, true)
                            }
//                            else if (doctorData.doctor!!.registrationImage!!.isEmpty() || doctorData.doctor!!.idProof!!.isEmpty()) {
//                                activitySwitcher(DocumentPendingActivity::class.java, true)
//                            }
                            else if (doctorData.doctor!!.verify!!.equals("0")) {

                                Toast.makeText(this,"Please Wait for verification call",Toast.LENGTH_LONG).show()
                                SharedStorage.clear(this)
                                activitySwitcher(SplashActivity::class.java, true)
                                finish()
                            } else {
                                activitySwitcher(com.eiadah.app.ui.doctor.postLogin.activity.HomeActivity::class.java, true)
                            }
                        } else {
                            activitySwitcher(CreateDoctorProfileStep1Activity::class.java, true)
                        }
                    } else {
                        activitySwitcher(CreateDoctorProfileStep1Activity::class.java, true)
                    }
                }
            }
            it.status == Status.ERROR -> progressDialog.dismiss()
            it.status == Status.LOADING -> progressDialog.show()
        }
    }


    private fun getDeviceToken() {
        viewModel.getSaveToken().observe(this,this)
        viewModel.saveToekn(token!!,"android")
    }

}
