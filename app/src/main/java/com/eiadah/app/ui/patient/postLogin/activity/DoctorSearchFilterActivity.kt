package com.eiadah.app.ui.patient.postLogin.activity

import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.eiadah.app.R
import com.eiadah.app.databinding.ActivityDoctorSearchFilterBinding

class DoctorSearchFilterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityDoctorSearchFilterBinding = DataBindingUtil.setContentView(this, R.layout.activity_doctor_search_filter)
        binding.clickHandler = this
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun onApplyClick() {
        finish()
    }
}
