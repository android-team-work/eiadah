package com.eiadah.app.ui.patient.postLogin.dialogs

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.Window
import android.widget.EditText
import com.eiadah.app.Listener.UpdateProfileData
import com.eiadah.app.R
import com.eiadah.app.core.BaseDialog
import com.eiadah.app.databinding.DialogAddContactNumberBinding
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.ProfileViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage

class ContactNumberDialog(activity: FragmentActivity, private val updateData: UpdateProfileData) : BaseDialog(activity), LifecycleOwner {
    override fun getLifecycle(): Lifecycle {
        return lifecycleRegistry
    }

    private val profileViewModel : ProfileViewModel
            by lazy {
                ViewModelProviders.of(activity).get(ProfileViewModel::class.java)
            }

    private lateinit var edtPhone : EditText
    lateinit var binding : DialogAddContactNumberBinding
    private lateinit var lifecycleRegistry: LifecycleRegistry

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_contact_number, null, false)

        setContentView(binding.root)

        var window  = window
        window?.setGravity(Gravity.CENTER)
        //window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        window?.attributes?.windowAnimations = R.style.DialogAnimation

        lifecycleRegistry = LifecycleRegistry(this)
        binding.clickHandler = this
        profileViewModel.setUserData(SharedStorage.getUserData(context))
        binding.data = profileViewModel
        binding.lifecycleOwner = this
        edtPhone = binding.edtPhone
        Log.e("hint", edtPhone.hint.toString())
    }

    fun onSubmit() {
        if (binding.edtPhone.text.toString().isEmpty()) {
            showToast(R.string.empty_contact_number)
        } else {
            updateContactNumber(binding.edtPhone.text.toString())
        }

    }

    fun onCancel() {
        dismiss()
    }

    private fun updateContactNumber(phone : String) {
        var params : HashMap<String, String> = HashMap()
        params[GlobalVariables.PARAMS.PROFILE.PHONE] = phone
        updateData.updateProfileData(params)
        dismiss()
    }

}