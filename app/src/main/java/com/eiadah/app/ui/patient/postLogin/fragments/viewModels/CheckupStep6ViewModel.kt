package com.eiadah.app.ui.patient.postLogin.fragments.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.dataModel.DiseaseData
import com.eiadah.app.models.dataModel.QuestionData
import com.eiadah.app.models.responseModel.GetQuestionResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CheckupStep6ViewModel : ObservableViewModel() {
    // TODO: Implement the ViewModel

    private var getQuestionResponse : MutableLiveData<GetQuestionResponse> = MutableLiveData()
    private var questionData : MutableLiveData<QuestionData> = MutableLiveData()
    private var end : MutableLiveData<Boolean> = MutableLiveData()
    private var getResultResponse : MutableLiveData<RestObservable> = MutableLiveData()

    fun getQuestionList() : LiveData<GetQuestionResponse> {
        return getQuestionResponse
    }

    fun loadQuesuionList(id : String) {
        getCheckupService().getQuestionList(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::setQuestionListResponse, this::onError)
    }

    private fun setQuestionListResponse(getQuestionResponse: GetQuestionResponse) {
        this.getQuestionResponse.value = getQuestionResponse
    }

    private fun onError(throwable: Throwable) {
        throwable.fillInStackTrace()
    }

    fun getQuestionData(check: Boolean) : LiveData<QuestionData> {
        val diseaseList : ArrayList<DiseaseData> = getQuestionResponse.value?.diseaseList!!
        if (check) {

            removeQuestion(diseaseList)
        } else {
            diseaseList.removeAt(0)
            if (diseaseList.isNotEmpty()) {
                if (diseaseList.size>1) {
                    if (diseaseList[0].questions?.size!=0) {
                        questionData.value = diseaseList[0].questions?.get(0)
                        end.value = false
                    } else {
                        diseaseList.removeAt(0)
                        questionData.value = diseaseList[0].questions?.get(0)
                        end.value = false
                    }
                } else {
                    if (diseaseList.size!=0)
                        questionData.value = diseaseList[0].questions?.get(0)
                }
            } else {
                end.value = true
            }

            //removeQuestion(diseaseList)
        }

        return questionData
    }

    private fun removeQuestion(diseaseList : ArrayList<DiseaseData>) {
        if (diseaseList.size>1) {
            if (diseaseList[0].questions?.size!! >1) {
                diseaseList[0].questions?.removeAt(0)
                questionData.value = diseaseList[0].questions?.get(0)
                end.value = false
            } else {
                diseaseList.removeAt(0)
                questionData.value = diseaseList[0].questions?.get(0)
                end.value = false
            }
        } else {
            if (diseaseList[0].questions?.size!! >1) {
                diseaseList[0].questions?.removeAt(0)
                questionData.value = diseaseList[0].questions?.get(0)
                end.value = false
            } else {
                diseaseList.removeAt(0)
                end.value = true
            }
        }

        getQuestionResponse.value?.diseaseList = diseaseList
    }

    fun getEnd() : MutableLiveData<Boolean> {
        return end
    }

    fun setEnd(end : Boolean) {
        this.end.value = end
    }

    fun getResultResponse() : LiveData<RestObservable> {
        return getResultResponse
    }

    fun loadResult(checkupId : String, symptomId : String, listCount : String, ansList : String) {
        getCheckupService().getCheckupResult(checkupId, symptomId, listCount, ansList)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { getResultResponse.value = RestObservable.loading() }
            .subscribe({ getResultResponse.value = RestObservable.success(it) },
                { getResultResponse.value = RestObservable.error(it)})
    }

}
