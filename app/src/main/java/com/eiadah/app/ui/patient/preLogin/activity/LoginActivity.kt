package com.eiadah.app.ui.patient.preLogin.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.AsyncTask
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityLoginBinding
import com.eiadah.app.models.requestModels.SaveTokentRequest
import com.eiadah.app.models.responseModel.DeviceTokenResponse
import com.eiadah.app.models.responseModel.ProfileResponse
import com.eiadah.app.models.responseModel.RegisterResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.activity.HomeActivity
import com.eiadah.app.ui.patient.preLogin.viewModels.LoginViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.facebook.*
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.GoogleAuthException
import com.google.android.gms.auth.GoogleAuthUtil
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.pieboat.shredpreference.SharePreferHelper
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*

class LoginActivity : BaseActivity(), Observer<RestObservable> {

    private val TAG = "MyFirebaseToken"
    lateinit var binding : ActivityLoginBinding
    lateinit var mGoogleApiClient: GoogleApiClient
    lateinit var googleSignInOptions: GoogleSignInOptions
    private val RC_SIGN_IN = 0
    lateinit var Facebook: LoginButton
    lateinit var callbackManager: CallbackManager
    private val viewModel: LoginViewModel
            by lazy {  ViewModelProviders.of(this).get(LoginViewModel::class.java) }

    lateinit var request: SaveTokentRequest

    lateinit var sharePreferHelper: SharePreferHelper
    var token:String?=null
    @SuppressLint("StringFormatInvalid")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FacebookSdk.sdkInitialize(this.applicationContext)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.clickEvent = this

        sharePreferHelper= SharePreferHelper(this)
        callbackManager = CallbackManager.Factory.create()
        Facebook = findViewById(R.id.Facebook) as LoginButton
        Facebook.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
        Facebook.compoundDrawablePadding = 0
        Facebook.setPadding(0, 0, 0, 0)


        request=SaveTokentRequest()
        googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

        //Initializing google api client
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
            .build()

        facebookLogin()
        FirebaseApp.initializeApp(this);
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                token = task.result?.token

                // Log and toast
                val msg  = getString(R.string.msg_token_fmt, token)
                Log.e("device_token", token)
//                Toast.makeText(baseContext, token, Toast.LENGTH_SHORT).show()
            })
    }

    fun onGoogleClick() {
//            //Creating an intent
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN)
        mGoogleApiClient.connect();
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {

            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            //Calling a new function to handle signin
            handleSignInResult(result)
        } else {

//            CommonMethods.showProgress(this@SigninActivity)
//            mPresenter.onSocialLogin()
        }
    }

    private fun handleSignInResult(result: GoogleSignInResult) {
//        CommonMethods.showProgress(this)
        //If the login succeed
        if (result.isSuccess) {

            val acct = result.signInAccount

            var scope = "oauth2:" + Scopes.EMAIL + " " + Scopes.PROFILE
//            var accessToken = GoogleAuthUtil.getToken(applicationContext, acct.getAccount(), scope, Bundle())

            var runnable: Runnable = Runnable {
                try {
                    val scope = "oauth2:" + Scopes.EMAIL + " " + Scopes.PROFILE
                    val accessToken = GoogleAuthUtil.getToken(applicationContext, acct?.getAccount(), scope, Bundle())
                    Log.e("accessToken", accessToken) //accessToken:ya29.Gl...

//                    CommonMethods.showProgress(this@SigninActivity)
//                    aPresenter.onSocialLogin("google",accessToken)

                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: GoogleAuthException) {
                    e.printStackTrace()
                }
            }
            AsyncTask.execute(runnable)

        } else {
            //If login fails
            Log.e("resultttt",result.status.toString())
            Toast.makeText(this, result.status.toString(), Toast.LENGTH_LONG).show()
        }
//        CommonMethods.hideProgress()
    }


    override fun onBackPressed() {
        activitySwitcher(SplashActivity::class.java, true)
    }

    fun onLoginClick() {
        hideKeyboard()
        if (isValid())
            loginApi()
    }

    fun onForgotPasswordClick() {
        val forgotPasswordDialog = ForgotPasswordDialog(this)
        forgotPasswordDialog.show()
    }


    fun onFacebookClick() {

    }

    private fun isValid() : Boolean {
        var check  = false

        val strUsername : String = binding.edtLoginUsername.text.toString()
        val strPassword : String = binding.edtLoginPassword.text.toString()

        if (strUsername.isEmpty() && strPassword.isEmpty()) {
            showToast(R.string.please_fill_all_fields)
        } else if (strUsername.isEmpty()) {
            showToast(R.string.empty_username)
        } else if (strPassword.isEmpty()) {
            showToast(R.string.empty_password)
        } else {
            check = true
        }

        return check
    }

    private fun loginApi() {
        viewModel.loginApi(binding.edtLoginUsername.text.toString(), binding.edtLoginPassword.text.toString())
        viewModel.getLoginResponse().observe(this, this)
    }

    private fun getProfileApi() {
        viewModel.profileApi()
        viewModel.getProfileResponse().observe(this, this)
    }
  private fun getDeviceToken() {
      request.registration_id=token!!
      request.type="android"
        viewModel.getSaveToken().observe(this,this)
        viewModel.saveToekn(request)
    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()

                if (it.data is RegisterResponse) {

                    val registerResponse : RegisterResponse = it.data
                    if (registerResponse.token.isNotEmpty()) {
                        SharedStorage.setToken(this, registerResponse.token)
                        getProfileApi()

                    }

                } else if (it.data is ProfileResponse) {

                    val profileResponse : ProfileResponse = it.data
                    if (profileResponse.status!!) {
                        sharePreferHelper.saveString(GlobalVariables.SharePref.USER_ID,profileResponse.user?.userId!!)
                        sharePreferHelper.saveString(GlobalVariables.SharePref.USER_Image, profileResponse.user?.image!!)

                        SharedStorage.setSession(this, true)
                        SharedStorage.setUserType(this, GlobalVariables.User.PATIENT)
                        SharedStorage.setUserData(this, profileResponse.user!!)
                        activitySwitcher(HomeActivity::class.java, true)

                        Log.e("user_idddddd",profileResponse.user?.userId)

                        getDeviceToken()
                    }

                }

                else if (it.data is DeviceTokenResponse) {
                    val deviceTokenResponse : DeviceTokenResponse = it.data
                }

            }
            it.status == Status.ERROR -> progressDialog.dismiss()
            it.status == Status.LOADING -> progressDialog.show()
        }
    }



    fun facebookLogin() {
        Facebook.setReadPermissions(
            Arrays.asList("public_profile", "email", "user_birthday",
                "user_friends", "user_photos"))
        Facebook.registerCallback(callbackManager, object: FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, response ->

                }
                val parameters = Bundle()
                parameters.putString("fields", "id,name,email,gender,birthday")
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() {
            }

            override fun onError(error: FacebookException) {
                Toast.makeText(this@LoginActivity, "Something went wrong in Facebook login", Toast.LENGTH_SHORT).show()
            }

        })
    }
}
