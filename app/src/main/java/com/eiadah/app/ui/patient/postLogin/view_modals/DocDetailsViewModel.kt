package com.eiadah.app.ui.patient.postLogin.view_modals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.SaveClinicRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DocDetailsViewModel : ObservableViewModel() {

    var doctorDetailsResponse : MutableLiveData<RestObservable> = MutableLiveData()

    fun getdocdetails() : LiveData<RestObservable> {
        return doctorDetailsResponse
    }

    fun doctorDetails(id : String) {
        getService().getDocDetails(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { doctorDetailsResponse.value = RestObservable.loading() }
            .subscribe(
                {doctorDetailsResponse.value = RestObservable.success(it)},
                {doctorDetailsResponse.value = RestObservable.error (it)}
            )
    }
}