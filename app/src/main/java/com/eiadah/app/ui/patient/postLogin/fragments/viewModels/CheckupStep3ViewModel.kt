package com.eiadah.app.ui.patient.postLogin.fragments.viewModels

import androidx.databinding.Bindable
import android.util.Log
import com.eiadah.app.viewModels.ObservableViewModel
import java.lang.Float.parseFloat

class CheckupStep3ViewModel : ObservableViewModel() {

    private var bmi : String = ""
    private var result : String = ""

    @Bindable
    fun getBmi() : String {
        return bmi
    }

    fun setBmi(weight: Float, height: Float) {
        this.bmi = String.format("%.2f", calculateBMI(weight, height))
        Log.e("BMI", "//$bmi")
        setResult(bmi)
    }

    @Bindable
    fun getResult() : String {

        return result
    }

    private fun setResult(bmi : String) {
        result = interpretBMI(parseFloat(bmi))
        Log.e("Result", "//$result")
    }

    //Calculate BMI
    private fun calculateBMI(weight: Float, height: Float): Float {
        return weight / (height * height)
    }

    // Interpret what BMI means
    private fun interpretBMI(bmiValue : Float) : String {

        return when {
            bmiValue < 18.5 -> "You are Underweight"
            bmiValue < 25 -> "You are Normal"
            bmiValue < 30 -> "You are Overweight"
            else -> "You are Obese"
        }
    }

    //Convert Feet-Inches to Inches
    fun getInches(feet : Float, inches : Float) : Float {
        return (feet*12) + inches
    }

    //Convert Inches to cm
    fun getCm(inches : Float) : Float {
        return (inches*2.54F)
    }
}