package com.eiadah.app.ui.doctor.postLogin.activity

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityAddAppointmentBinding
import com.eiadah.app.models.dataModel.CityData
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.models.responseModel.AddAppointmentResponse
import com.eiadah.app.models.responseModel.AddPatientResponse
import com.eiadah.app.models.responseModel.CitiesResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.viewModals.AddAppointmentViewModel
import com.eiadah.app.ui.doctor.postLogin.viewModals.AddPatientViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import kotlinx.android.synthetic.main.activity_add_appointment.*
import kotlinx.android.synthetic.main.dialog_add_open_close_time_picker.*
import java.text.SimpleDateFormat
import java.util.*
import javax.xml.datatype.DatatypeConstants.MONTHS

class AddAppointmentActivity : BaseActivity(), Observer<RestObservable> {


    lateinit var binding:ActivityAddAppointmentBinding

    lateinit var datePickerDialog: DatePickerDialog
    lateinit var request:AddAppointmenttRequest
    var id:String=""
    private val viewModel: AddAppointmentViewModel
            by lazy {  ViewModelProviders.of(this).get(AddAppointmentViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_add_appointment)
        binding.clickHandler=this

        request=AddAppointmenttRequest()
    }

    fun backOncllick() {
        finish()
    }

    fun onSaveClick() {
        val name = binding.edtName.text.toString()
        val number = binding.edtMobileNumber.text.toString()
        val date = binding.tvAppDate.text.toString()
        val time = binding.tvAppTime.text.toString()
        val category = binding.edtCategory.text.toString()
        val email = binding.edtEmail.text.toString()
        val notes = binding.edtNotes.text.toString()

        if (isValid(name, date, time,email,category,notes)) {

            request.name = name
            request.mobile=number
            request.email=email
            request.date=date
            request.time=time
            request.category=category
            request.note=notes

            if (CalenderActivity.is_from!!){

                request.user_id="0"
                CalenderActivity.is_from=false
            }
            else{

                id=intent.getStringExtra("id")
                request.user_id=id
            }
            viewModel.AddAppointment(request)
            viewModel.getAddAppointment().observe(this, this)

           finish()
        }
    }

    fun ontimeclick(){

        val cal = Calendar.getInstance()

        val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)

            tv_app_time.text = SimpleDateFormat("HH:mm").format(cal.time)
        }

        tv_app_time.setOnClickListener {
            TimePickerDialog(this, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show()
        }

    }
    private fun isValid(name : String, date : String, time : String, email : String, category : String, notes : String) : Boolean {
        var check = false

        if (name.isEmpty() && date.isEmpty() && time.isEmpty()&& email.isEmpty()&& category.isEmpty()&& notes.isEmpty()) {
            showToast(R.string.please_fill_all_fields)
        } else if (name.isEmpty()) {
            showToast("Please Enter Your Name")
        }  else if (date.isEmpty()) {
            showToast("Please Select Appointment Date")
        } else if (time.isEmpty()) {
            showToast("Please Select Appointment Time")
        }  else if (email.isEmpty()) {
            showToast("Please Enter your email id")
        }else if (category.isEmpty()) {
            showToast("Please Enter Category")
        }else if (notes.isEmpty()) {
            showToast("Please Enter Notes")
        } else {
            check = true
        }

        return check
    }


    fun openCalender(){
           val c = Calendar.getInstance()
           val mYear = c.get(Calendar.YEAR) // current year
           val mMonth = c.get(Calendar.MONTH) // current month
           val mDay = c.get(Calendar.DAY_OF_MONTH) // current day
           // date picker dialog   MM/dd/yyyy
           datePickerDialog = DatePickerDialog(this,
               DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                   // set day of month , month and year value in the edit text
                   tv_app_date.setText(year.toString() + "-" + (monthOfYear + 1).toString() + "-" + dayOfMonth) }, mYear, mMonth, mDay)
//                   tv_app_date.setText((monthOfYear + 1).toString() + "/" + dayOfMonth + "/" + year) }, mYear, mMonth, mDay)
           datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000
           datePickerDialog.show()
    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is AddAppointmentResponse) {
                    val appointmentResponse : AddAppointmentResponse = it.data

                    if (appointmentResponse.status!!) {
                        activitySwitcher(HomeActivity::class.java, true)
                    }
                }
            }
            it.status == Status.ERROR -> {
                it.error!!.fillInStackTrace()
                progressDialog.dismiss()
            }
            it.status == Status.LOADING -> progressDialog.show()
        }
    }

}
