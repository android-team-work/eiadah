package com.eiadah.app.ui.doctor.preLogin.activity

import android.app.Activity
import android.content.Intent
import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityAddRegistrationDetailBinding
import com.eiadah.app.utils.GlobalVariables

class AddRegistrationDetailActivity : BaseActivity() {

    lateinit var binding : ActivityAddRegistrationDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_registration_detail)
        binding.clickHandler = this
    }

    fun onSaveClick() {
        val number = binding.edtRegistrationNumber.text.toString()
        val council = binding.edtRegistrationCouncil.text.toString()
        val year = binding.edtRegistrationYear.text.toString()

        if (isValid(number, council, year)) {
            val returnIntent = Intent()
            returnIntent.putExtra(GlobalVariables.PARAMS.DoctorProfile.REG_NUMBER, number)
            returnIntent.putExtra(GlobalVariables.PARAMS.DoctorProfile.REG_COUNCIL, council)
            returnIntent.putExtra(GlobalVariables.PARAMS.DoctorProfile.REG_YEAR, year)
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
        }
    }

    private fun isValid(number : String, council : String, year : String) : Boolean {
        var check = false

        if (number.isEmpty() && council.isEmpty() && year.isEmpty()) {
            showToast(R.string.please_fill_all_fields)
        } else if (number.isEmpty()) {
            showToast(R.string.empty_reg_number)
        } else if (council.isEmpty()) {
            showToast(R.string.empty_reg_council)
        } else if (year.isEmpty()) {
            showToast(R.string.empty_reg_year)
        } else {
            check = true
        }

        return check
    }


}
