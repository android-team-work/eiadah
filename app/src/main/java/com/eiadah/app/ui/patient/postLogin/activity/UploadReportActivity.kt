package com.eiadah.app.ui.patient.postLogin.activity

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityUploadReportBinding
import com.eiadah.app.databinding.ActivityUploadedReportBinding
import com.eiadah.app.models.responseModel.DoctorProfileResponse
import com.eiadah.app.models.responseModel.UploadReportResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.activity.HomeActivity
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.AddAllrgyViewModel
import com.eiadah.app.ui.patient.postLogin.view_modals.UploadReportViewModel
import com.eiadah.app.ui.patient.preLogin.activity.SplashActivity
import com.eiadah.app.utils.AppUtils.showToast
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.esafirm.imagepicker.model.Image
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class UploadReportActivity : BaseActivity(), Observer<RestObservable> {

    lateinit var binding: ActivityUploadReportBinding
    private val viewModel: UploadReportViewModel
            by lazy {  ViewModelProviders.of(this).get(UploadReportViewModel::class.java) }
    var idPath = ""
    var str_report = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_upload_report)
        binding.clickHandler=this

        str_report=binding.edtReportName.text.toString()
    }

    override fun onBackPressed(){
        finish()
    }


    fun fileclick(){
        openImagePicker()
    }
    private fun openImagePicker() {
        ImagePicker.create(this)
            .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
            .folderMode(true) // folder mode (false by default)
            .toolbarFolderTitle("Folder") // folder selection title
            .toolbarImageTitle("Tap to select") // image selection title
            .toolbarArrowColor(Color.BLACK) // Toolbar 'up' arrow color
            .single() // single mode
            .showCamera(true) // show camera or not (true by default)
            .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
            .enableLog(false) // disabling log
            .start() // start image picker activity with request code
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            val images : List<Image> = ImagePicker.getImages(data)
            // or get a single image only
            val image : Image = ImagePicker.getFirstImageOrNull(data)

            Glide.with(this).load(image.path).into(binding.ivReport)
//            if (isIdImage) {
                idPath = image.path
//                binding.photoIdPath.text = idPath
//            } else {
//                docPath = image.path
//                binding.registrationDocPath.text = docPath
//            }

            //binding.data?.image?.value = image.path
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun updateImage(title: String, doc: String) {
//        val file = File(idProof)
//        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
//        val proof1 = MultipartBody.Part.createFormData("id_proof", file.name, requestFile)

        val file2 = File(doc)
        val requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file2)
        val proof2 = MultipartBody.Part.createFormData("file", file2.name, requestFile2)

        val smileBody = RequestBody.create(MediaType.parse("text/plain"), title)
        viewModel.saveUploadReport(smileBody, proof2)
        viewModel.getUploadReport().observe(this, this)

    }

    private fun isValid(): Boolean {
        var check = false
        if (idPath.isEmpty() && str_report.isEmpty()) {
            Toast.makeText(this,"Please fill all fields",Toast.LENGTH_SHORT).show()
        } else if (idPath.isEmpty()) {
            Toast.makeText(this,"Please choose Report image",Toast.LENGTH_SHORT).show()
        } else if (binding.edtReportName.text.toString().isEmpty()) {
            Toast.makeText(this,"Please enter Report name",Toast.LENGTH_SHORT).show()

        } else {
            check = true
        }

        return check
    }

    fun onSubmitClick() {

        if (isValid()) {
            updateImage( (binding.edtReportName.text.toString()),idPath)
        }

    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is UploadReportResponse) {
                    val uploadReportResponse : UploadReportResponse = it.data

                    if (uploadReportResponse.status!!) {
                       finish()

                    }
                }
            }
            it.status == Status.ERROR -> progressDialog?.dismiss()
            it.status == Status.LOADING -> progressDialog?.show()
        }
    }

}
