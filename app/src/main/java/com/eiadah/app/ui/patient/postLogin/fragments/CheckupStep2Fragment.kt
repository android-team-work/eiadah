package com.eiadah.app.ui.patient.postLogin.fragments


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.RadioGroup
import com.eiadah.app.Listener.CheckupChangeFragmentListener

import com.eiadah.app.R
import com.eiadah.app.databinding.FragmentCheckupStep2Binding
import android.widget.ArrayAdapter
import com.eiadah.app.core.BaseFragment
import com.eiadah.app.models.dataModel.CityData
import com.eiadah.app.models.dataModel.CountryData
import com.eiadah.app.models.dataModel.StateData
import com.eiadah.app.models.requestModels.SaveCheckupDetailsRequest
import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.CheckupStep2ViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.pieboat.shredpreference.SharePreferHelper
import kotlinx.android.synthetic.main.fragment_checkup_step2.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException


/**
 * A simple [Fragment] subclass.
 *
 */
class CheckupStep2Fragment : BaseFragment() {



    lateinit var mListener : CheckupChangeFragmentListener
    lateinit var binding : FragmentCheckupStep2Binding
//    arrays of state object
//    var stateObject: MutableList<State> = ArrayList()
    // arrays of city object
//    var cityObject: MutableList<City> = ArrayList()

    var countryID: Int = 0
    var stateID:Int = 0
    lateinit var sharePreferHelper: SharePreferHelper
    companion object {
        fun newInstance() = CheckupStep2Fragment()

        val TAG : String = CheckupStep2Fragment::class.java.simpleName
    }

    private val viewModel: CheckupStep2ViewModel
            by lazy {  ViewModelProviders.of(this).get(CheckupStep2ViewModel::class.java) }

    private var countryList : ArrayList<CountryData> = ArrayList()
    var stateList : ArrayList<StateData> = ArrayList()
    var cityList : ArrayList<CityData> = ArrayList()
    var saveCheckupDetailsRequest = SaveCheckupDetailsRequest()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_checkup_step2, container, false)
        binding.clickHandler = this

        sharePreferHelper= SharePreferHelper(activity!!)
        loadData()


        try {
            getStateJson()
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        // get City from assets JSON
        try {
            getCityJson()
        } catch (e: JSONException) {
            e.printStackTrace()
        }


        // initialize country picker
//        countryPicker = CountryPicker.Builder().with(activity!!).listener(this).build()

        // initialize listeners
        return binding.root


    }

    private fun loadData() {

        progressDialog.show()
        viewModel.loadCountryList()
        viewModel.getCountryList().observe(this, Observer {
            progressDialog.dismiss()
            if (it?.status!!) {
                countryList = (it.countryList as ArrayList<CountryData>?)!!
                var countryData = CountryData()
                countryData.id = 0
                countryData.name = "Choose country"
                countryList.add(0, countryData)
                setCountryAdapter()
            }
        })

        viewModel.getStateList().observe(this, Observer {
            progressDialog.dismiss()
            if (it?.status!!) {
                stateList = (it.stateList as ArrayList<StateData>?)!!
                var stateData = StateData()
                stateData.id = 0
                stateData.countryId =0
                stateData.name = "Choose state"
                stateList.add(0, stateData)
                setStateAdapter()
            }
        })

        viewModel.getCityList().observe(this, Observer {
            progressDialog.dismiss()
            if (it?.status!!) {
                cityList = (it.cityList as ArrayList<CityData>?)!!
                var cityData = CityData()
                cityData.id = "0"
                cityData.name = "Choose city"
                cityList.add(0, cityData)
                setCityAdapter()
            }
        })
    }

    private fun setCountryAdapter() {
        val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, countryList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.countryAdapter = adapter
    }


    private fun setStateAdapter() {
        val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, stateList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.stateAdapter = adapter
    }

    private fun setCityAdapter() {
        val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, cityList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.cityAdapter = adapter
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is CheckupChangeFragmentListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement CheckupChangeFragmentListener")
        }
    }

    fun onSaveClick() {
        if (isValid()) {
            SharedStorage.setCheckupDetail(context!!, saveCheckupDetailsRequest)
            sharePreferHelper.saveString(GlobalVariables.SharePref.AGE,binding.edtCheckupStep1Age.text.toString())
            sharePreferHelper.saveString(GlobalVariables.SharePref.NAME,binding.edtCheckupStep1Name.text.toString())
            mListener.changeFragment(
                CheckupStep3Fragment.newInstance(),
                CheckupStep3Fragment.TAG

            )
        }

    }

    fun onCheckedChanged(radioGroup: RadioGroup, id : Int) {
        when(id) {
            R.id.radioButtonMale -> {
                viewModel.setGender(GlobalVariables.Gender.MALE)
                binding.radioButtonMale.setTextColor(context?.let { ContextCompat.getColorStateList(it, R.color.white) })
                binding.radioButtonFemale.setTextColor(context?.let { ContextCompat.getColorStateList(it, R.color.black) })
                sharePreferHelper.saveString(GlobalVariables.SharePref.GENDER,"Male")
            }

            R.id.radioButtonFemale -> {
                viewModel.setGender(GlobalVariables.Gender.FEMALE)
                binding.radioButtonMale.setTextColor(context?.let { ContextCompat.getColorStateList(it, R.color.black) })
                binding.radioButtonFemale.setTextColor(context?.let { ContextCompat.getColorStateList(it, R.color.white) })
                sharePreferHelper.saveString(GlobalVariables.SharePref.GENDER,"Female")
            }
        }
    }

    fun onItemSelectedCountry(adapterView: AdapterView<*> , view : View , i : Int , l : Long) {
        if (i!=0) {
            viewModel.setCountryName(countryList[i].name!!)
            progressDialog.show()
            viewModel.loadStateList(countryList[i].id.toString())
        } else {
            viewModel.setCountryName("")
        }

    }

    fun onItemSelectedState(adapterView: AdapterView<*> , view : View , i : Int , l : Long) {
        if (i!==0) {
            viewModel.setStateName(stateList[i].name!!)
            progressDialog.show()
            viewModel.loadCityList(stateList[i].id.toString())
        } else {
            viewModel.setStateName("")
        }

    }

    fun onItemSelectedCity(adapterView: AdapterView<*> , view : View , i : Int , l : Long) {
        if (i!=0) {
            viewModel.setCityName(cityList[i].name!!)
        } else {
            viewModel.setCityName("")
        }
        //viewModel.loadCityList(stateList[i].id.toString())
    }

    fun isValid()  : Boolean {
        var check = false

        var strName = binding.edtCheckupStep1Name.text.toString()
        var strAge = binding.edtCheckupStep1Age.text.toString()

        if (strName.isEmpty() && strAge.isEmpty()
            && viewModel.getGender().isEmpty()
            && viewModel.getCountryName().isEmpty()
            && viewModel.getStateName().isEmpty()
            && viewModel.getCityName().isEmpty()) {
            showToast(R.string.please_fill_all_fields)
        } else if (strName.isEmpty()) {
            showToast(R.string.empty_name)
        } else if (strAge.isEmpty()) {
            showToast(R.string.empty_age)
        } else if (viewModel.getGender().isEmpty()) {
            showToast(R.string.please_choose_gender)
        } else if (viewModel.getCountryName().isEmpty()) {
            showToast(R.string.please_choose_country)
        } else if (viewModel.getStateName().isEmpty()) {
            showToast(R.string.please_choose_state)
        } else if (viewModel.getCityName().isEmpty()) {
            showToast(R.string.please_choose_city)
        } else {
            saveCheckupDetailsRequest.first_name = strName
            saveCheckupDetailsRequest.age = strAge
            saveCheckupDetailsRequest.gender = viewModel.getGender()
//            saveCheckupDetailsRequest.country = binding.tvContry.text.toString()
//            saveCheckupDetailsRequest.state = binding.tvState.text.toString()
//            saveCheckupDetailsRequest.city =binding.tvCity.text.toString()
            saveCheckupDetailsRequest.country = viewModel.getCountryName()
            saveCheckupDetailsRequest.state = viewModel.getStateName()
            saveCheckupDetailsRequest.city = viewModel.getCityName()
            check = true
        }


        return check
    }

    fun onBackClick(){
        activity?.finish()
    }

//    private fun setListener() {
//        binding.tvState.setOnClickListener(View.OnClickListener {
//            statePicker?.showDialog(activity!!.getSupportFragmentManager())
//        })
//    }
//
//    //SET COUNTRY LISTENER
//    private fun setCountryListener() {
//        binding.tvContry.setOnClickListener(View.OnClickListener {
//            countryPicker?.showDialog(activity!!.getSupportFragmentManager()
//            )
//        })
//    }
//
//    //SET CITY LISTENER
//    private fun setCityListener() {
//        binding.tvCity.setOnClickListener(View.OnClickListener {
//            cityPicker?.showDialog(activity!!.getSupportFragmentManager()
//            )
//        })
//    }
//
//    override fun onSelectState(state: State?) {
//
//        binding.tvCity.setText("City")
//        cityPicker?.allCities?.clear()
//
//        binding.tvState.setText(state?.getStateName())
//        stateID = state!!.getStateId()
//
//
//
//        for (i in cityObject.indices) {
//            cityPicker = CityPicker.Builder().with(activity!!).listener(this).build()
//            val cityData = City()
//            if (cityObject[i].stateId == stateID) {
//                cityData.cityId = cityObject[i].cityId
//                cityData.cityName = cityObject[i].cityName
//                cityData.stateId = cityObject[i].stateId
//                cityPicker?.allCities?.add(cityData)
//            }
//        }
//    }
//
//    override fun onSelectCountry(country: Country?) {
//        // get country name and country ID
//        binding.tvContry.setText(country?.getName())
//        countryID = country!!.getCountryId()
//        statePicker?.allStates?.clear()
//        cityPicker?.allCities?.clear()
//
//
//        // GET STATES OF SELECTED COUNTRY
//        for (i in stateObject.indices) {
//            // init state picker
//            statePicker = StatePicker.Builder().with(activity!!).listener(this).build()
//            val stateData = State()
//            if (stateObject[i].countryId == countryID) {
//
//                stateData.stateId = stateObject[i].stateId
//                stateData.stateName = stateObject[i].stateName
//                stateData.countryId = stateObject[i].countryId
//                stateData.flag = country.getFlag()
//                statePicker?.allStates?.add(stateData)
//            }
//        }
//    }
//
//    override fun onSelectCity(city: City?) {
//        binding.tvCity.setText(city?.getCityName())
//    }


    // GET STATE FROM ASSETS JSON
    @Throws(JSONException::class)
    fun getStateJson() {
        var json: String? = null
        try {
            val inputStream = activity?.getAssets()?.open("states.json")
            val size = inputStream?.available()
            val buffer = ByteArray(size!!)
            inputStream.read(buffer)
            inputStream.close()
            json = String(buffer)

        } catch (e: IOException) {
            e.printStackTrace()
        }


//        val jsonObject = JSONObject(json)
//        val events = jsonObject.getJSONArray("states")
//        for (j in 0 until events.length()) {
//            val cit = events.getJSONObject(j)
//            val stateData = State()
//
//            stateData.stateId = Integer.parseInt(cit.getString("id"))
//            stateData.stateName = cit.getString("name")
//            stateData.countryId = Integer.parseInt(cit.getString("country_id"))
//            stateObject.add(stateData)
//        }
    }

    // GET CITY FROM ASSETS JSON
    @Throws(JSONException::class)
    fun getCityJson() {
        var json: String? = null
        try {
            val inputStream = activity?.getAssets()?.open("cities.json")
            val size = inputStream?.available()
            val buffer = ByteArray(size!!)
            inputStream.read(buffer)
            inputStream.close()
            json = String(buffer)

        } catch (e: IOException) {
            e.printStackTrace()
        }
//
//
//        val jsonObject = JSONObject(json)
//        val events = jsonObject.getJSONArray("cities")
//        for (j in 0 until events.length()) {
//            val cit = events.getJSONObject(j)
//            val cityData = City()
//
//            cityData.cityId = Integer.parseInt(cit.getString("id"))
//            cityData.cityName = cit.getString("name")
//            cityData.stateId = Integer.parseInt(cit.getString("state_id"))
//            cityObject.add(cityData)
//        }
    }
}
