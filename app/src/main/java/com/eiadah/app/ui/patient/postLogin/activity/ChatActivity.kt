package com.eiadah.app.ui.patient.postLogin.activity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityChatBinding
import com.eiadah.app.models.dataModel.NotificationListData
import com.eiadah.app.models.dataModel.SendMsgData
import com.eiadah.app.models.responseModel.GetMessageResponse
import com.eiadah.app.models.responseModel.SendMsgResponse
import com.eiadah.app.models.responseModel.UploadReportResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.SendMsgViewModel
import com.eiadah.app.utils.GlobalVariables
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.esafirm.imagepicker.model.Image
import com.google.firebase.FirebaseApp
import com.myapplication.adapter.ChatAdapter
import com.myapplication.adapter.NotificationAdapter
import com.pieboat.shredpreference.SharePreferHelper
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class ChatActivity : BaseActivity(), Observer<RestObservable> {


    lateinit var binding:ActivityChatBinding
    var idPath = ""
    var id:String=""
    var name:String=""
    private val viewModel: SendMsgViewModel
            by lazy {  ViewModelProviders.of(this).get(SendMsgViewModel::class.java) }

    lateinit var sharePreferHelper: SharePreferHelper
    var chatingList: ArrayList<SendMsgData>?=ArrayList()
    lateinit var adapter: ChatAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_chat)

        binding.clickHandler=this

        FirebaseApp.initializeApp(this);

        sharePreferHelper= SharePreferHelper(this)
        id=intent.getStringExtra("id")
        name=intent.getStringExtra("name")

        binding.tvUserName.setText(name)
        viewModel.getMsg().observe(this,this)
        viewModel.loadMsg(sharePreferHelper.getString(GlobalVariables.SharePref.USER_ID)!!,id)

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
            IntentFilter(GlobalVariables.SharePref.NOTIFICATION_MESSAGE)
        )
        sharePreferHelper.isChatScreen(true)
    }

    fun backOncllick(){
        finish()
    }

    fun GetMessage(){
        viewModel.getMsg().observe(this,this)
        viewModel.loadMsg(sharePreferHelper.getString(GlobalVariables.SharePref.USER_ID)!!,id)

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            val images : List<Image> = ImagePicker.getImages(data)
            // or get a single image only
            val image : Image = ImagePicker.getFirstImageOrNull(data)

            idPath = image.path

        }
        super.onActivityResult(requestCode, resultCode, data)
    }


    private fun isValid(): Boolean {
        var check = false
        if (binding.edtMsg.text.toString().isEmpty()) {
            Toast.makeText(this,"Please  enter text", Toast.LENGTH_SHORT).show()
        }else {
            check = true
        }

        return check
    }

    fun onSubmitClick() {

        if (isValid()) {

            viewModel.loadSendMsg(sharePreferHelper.getString(GlobalVariables.SharePref.USER_ID)!!,id,binding.edtMsg.text.toString(),idPath)
            viewModel.getSendMsg().observe(this, this)

        }

    }
    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is SendMsgResponse) {
                    val sendMsgResponse : SendMsgResponse = it.data

                    if (sendMsgResponse.status!!) {

                        binding.edtMsg.setText(" ")
                        viewModel.getMsg().observe(this,this)
                        viewModel.loadMsg(sharePreferHelper.getString(GlobalVariables.SharePref.USER_ID)!!,id)

                        if (chatingList?.size !!> 1) {
                            adapter.notifyDataSetChanged()
                            binding.chatRecyclerview.scrollToPosition(adapter.getItemCount() - 1)
                        } else {
                            adapter = ChatAdapter(this, chatingList!!)
                            binding.chatRecyclerview.setLayoutManager(LinearLayoutManager(this))
                            binding.chatRecyclerview.scrollToPosition(adapter.getItemCount() - 1)
                            binding.chatRecyclerview.setAdapter(adapter)
                        }
                    }
                }
                else if (it.data is GetMessageResponse) {
                    val getMessageResponse : GetMessageResponse = it.data

                    if (getMessageResponse.status!!) {

                        chatingList = getMessageResponse.data as ArrayList<SendMsgData>
                        val chatData=SendMsgData()
                        chatingList?.add(chatData)

                        adapter = ChatAdapter(this, chatingList!!)
                        binding.chatRecyclerview.setLayoutManager(LinearLayoutManager(this))
                        binding.chatRecyclerview.scrollToPosition(adapter.getItemCount() - 1)
                        binding.chatRecyclerview.setAdapter(adapter)

                    }
                }
            }
            it.status == Status.ERROR -> progressDialog?.dismiss()
            it.status == Status.LOADING -> progressDialog?.show()
        }
    }

    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val bundle = intent.extras
            try {
                GetMessage()

            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }


    }
  override  fun onDestroy() {
        super.onDestroy()
        sharePreferHelper.isChatScreen(false)
        Log.e("Check", "false")
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver)
    }

    override  fun onPause() {
        super.onPause()
        sharePreferHelper.isChatScreen(false)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver)
    }

}
