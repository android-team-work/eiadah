package com.eiadah.app.ui.doctor.postLogin.viewModals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.AddPrescriptionRequest
import com.eiadah.app.models.requestModels.SaveClinicRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.models.requestModels.WithdrawRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CreditDataViewModel : ObservableViewModel() {

    var creditDataResponse : MutableLiveData<RestObservable> = MutableLiveData()
    var bankListResponse : MutableLiveData<RestObservable> = MutableLiveData()
    var deleteBankResponse : MutableLiveData<RestObservable> = MutableLiveData()
    var withdrawRequestResponse : MutableLiveData<RestObservable> = MutableLiveData()



    fun getMyCredit() : LiveData<RestObservable> {
        return creditDataResponse
    }

    fun getBankList() : LiveData<RestObservable> {
        return bankListResponse
    }
    fun getDeleteBank() : LiveData<RestObservable> {
        return deleteBankResponse
    }
    fun getwithdrawRequest() : LiveData<RestObservable> {
        return withdrawRequestResponse
    }

    fun loaadMyCredit() {
        getService().getCreditData(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { creditDataResponse.value = RestObservable.loading() }
            .subscribe(
                {creditDataResponse.value = RestObservable.success(it)},
                {creditDataResponse.value = RestObservable.error (it)}
            )
    }

    fun loadBankList() {
        getService().getBankList(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { bankListResponse.value = RestObservable.loading() }
            .subscribe(
                {bankListResponse.value = RestObservable.success(it)},
                {bankListResponse.value = RestObservable.error(it)}
            )
    }
    fun loadDeleteBank(id:String) {
        getService().DeleteBank(getHeader(),id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { bankListResponse.value = RestObservable.loading() }
            .subscribe(
                {bankListResponse.value = RestObservable.success(it)},
                {bankListResponse.value = RestObservable.error(it)}
            )
    }
    fun loadWithdrawRequest(request: WithdrawRequest) {
        getService().withdrawRequest(getHeader(),request.id,request.amount)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { withdrawRequestResponse.value = RestObservable.loading() }
            .subscribe(
                {withdrawRequestResponse.value = RestObservable.success(it)},
                {withdrawRequestResponse.value = RestObservable.error(it)}
            )
    }
}