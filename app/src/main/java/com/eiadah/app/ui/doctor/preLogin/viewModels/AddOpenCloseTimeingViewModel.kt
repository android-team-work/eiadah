package com.eiadah.app.ui.doctor.preLogin.viewModels

import androidx.databinding.Bindable
import com.eiadah.app.BR
import com.eiadah.app.R
import com.eiadah.app.core.MyApplication.Companion.getAppContext
import com.eiadah.app.models.dataModel.ClinicTimeData
import com.eiadah.app.viewModels.ObservableViewModel

class AddOpenCloseTimeingViewModel : ObservableViewModel() {

    private var monTime : String = ""
    private var tueTime : String = ""
    private var wedTime : String = ""
    private var thuTime : String = ""
    private var friTime : String = ""
    private var satTime : String = ""
    private var sunTime : String = ""
    private var monFriTime : String = ""

    @Bindable
    fun getMonTime() : String {
        return monTime
    }

    @Bindable
    fun getTueTime() : String {
        return tueTime
    }

    @Bindable
    fun getWedTime() : String {
        return wedTime
    }

    @Bindable
    fun getThuTime() : String {
        return thuTime
    }

    @Bindable
    fun getFriTime() : String {
        return friTime
    }

    @Bindable
    fun getSatTime() : String {
        return satTime
    }

    @Bindable
    fun getSunTime() : String {
        return sunTime
    }

    @Bindable
    fun getMonFriTime() : String {
        return monFriTime
    }

    fun setMonTime(data : ClinicTimeData) {
        monTime = if (data.close == "1") {
            getAppContext()!!.resources.getString(R.string.closed)
        } else {
            data.openHours + " - " +data.closeHours
        }
        notifyPropertyChanged(BR.monTime)
    }

    fun setTueTime(data : ClinicTimeData) {
        tueTime = if (data.close == "1") {
            getAppContext()!!.resources.getString(R.string.closed)
        } else {
            data.openHours + " - " +data.closeHours
        }
        notifyPropertyChanged(BR.tueTime)
    }

    fun setWedTime(data : ClinicTimeData) {
        wedTime = if (data.close == "1") {
            getAppContext()!!.resources.getString(R.string.closed)
        } else {
            data.openHours + " - " +data.closeHours
        }
        notifyPropertyChanged(BR.wedTime)
    }

    fun setThuTime(data : ClinicTimeData) {
        thuTime = if (data.close == "1") {
            getAppContext()!!.resources.getString(R.string.closed)
        } else {
            data.openHours + " - " +data.closeHours
        }
        notifyPropertyChanged(BR.thuTime)
    }

    fun setFriTime(data : ClinicTimeData) {
        friTime = if (data.close == "1") {
            getAppContext()!!.resources.getString(R.string.closed)
        } else {
            data.openHours + " - " +data.closeHours
        }
        notifyPropertyChanged(BR.friTime)
    }

    fun setSatTime(data : ClinicTimeData) {
        satTime = if (data.close == "1") {
            getAppContext()!!.resources.getString(R.string.closed)
        } else {
            data.openHours + " - " +data.closeHours
        }
        notifyPropertyChanged(BR.satTime)
    }

    fun setSunTime(data : ClinicTimeData) {
        sunTime = if (data.close == "1") {
            getAppContext()!!.resources.getString(R.string.closed)
        } else {
            data.openHours + " - " +data.closeHours
        }
        notifyPropertyChanged(BR.sunTime)
    }

    fun setMonFriTime(data : ClinicTimeData) {
        monFriTime = if (data.close == "1") {
            getAppContext()!!.resources.getString(R.string.closed)
        } else {
            data.openHours + " - " +data.closeHours
        }
        notifyPropertyChanged(BR.monFriTime)
    }
}