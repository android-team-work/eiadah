package com.eiadah.app.ui.doctor.postLogin.activity

import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.eiadah.app.R
import com.eiadah.app.databinding.ActivityCreateProfile2Binding

class CreateProfile2Activity : AppCompatActivity() {

    lateinit var binding:ActivityCreateProfile2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_create_profile2)
    }
}
