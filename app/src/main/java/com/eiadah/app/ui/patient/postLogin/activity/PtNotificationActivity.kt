package com.eiadah.app.ui.patient.postLogin.activity

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityPtNotificationBinding
import com.eiadah.app.models.dataModel.AddAllergyData
import com.eiadah.app.models.dataModel.NotificationListData
import com.eiadah.app.models.responseModel.AddAllergy_Response
import com.eiadah.app.models.responseModel.AddAlrgyResponse
import com.eiadah.app.models.responseModel.NotiicationListResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.NotificationListViewModel
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.ReportDetailsViewModel
import com.eiadah.app.utils.GlobalVariables
import com.myapplication.adapter.NotificationAdapter
import kotlinx.android.synthetic.main.activity_notification_list.*

class PtNotificationActivity : BaseActivity(), Observer<RestObservable> {


    lateinit var binding:ActivityPtNotificationBinding
    lateinit var adapter: NotificationAdapter

    private var notificationList : ArrayList<NotificationListData> = ArrayList()

    private val viewModel: NotificationListViewModel
            by lazy {  ViewModelProviders.of(this).get(NotificationListViewModel::class.java) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_pt_notification)
        binding.clickHandler=this


        viewModel.getNotificationlist().observe(this,this)
        viewModel.loadNotificationList()
    }

    override fun onBackPressed(){
        finish()
    }
    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is NotiicationListResponse) {
                    val notiicationListResponse : NotiicationListResponse = it.data
                    if (notiicationListResponse.status!!) {
                        notificationList = notiicationListResponse.data as ArrayList<NotificationListData>
                        val notificationData=NotificationListData()

                        notificationList.add(notificationData)
                        adapter=NotificationAdapter(this,notificationList)
                        binding.ptRecycle.setLayoutManager(LinearLayoutManager(this))
                        binding.ptRecycle.setAdapter(adapter)


                    }

                }

            }
            it.status == Status.ERROR -> progressDialog?.dismiss()
            it.status == Status.LOADING -> progressDialog?.show()
        }
    }

}
