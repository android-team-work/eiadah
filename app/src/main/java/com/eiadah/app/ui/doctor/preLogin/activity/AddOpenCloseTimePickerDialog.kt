package com.eiadah.app.ui.doctor.preLogin.activity

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import android.content.Context
import android.content.res.ColorStateList
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.core.content.ContextCompat
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.TimePicker
import com.eiadah.app.R
import com.eiadah.app.core.BaseDialog
import com.eiadah.app.databinding.DialogAddOpenCloseTimePickerBinding

class AddOpenCloseTimePickerDialog(context: Context, private val day : String, private val onTimeChanged: TimeChanged) : BaseDialog(context), LifecycleOwner{

    lateinit var binding : DialogAddOpenCloseTimePickerBinding
    lateinit var lifecycleRegistry: LifecycleRegistry

    var isStartTime = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_open_close_time_picker, null, false)

        setContentView(binding.root)

        var window = window
        window?.setGravity(Gravity.CENTER)
        //window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        window?.attributes?.windowAnimations = R.style.DialogAnimation

        lifecycleRegistry = LifecycleRegistry(this)
        binding.clickHandler = this
    }

    override fun getLifecycle(): Lifecycle {
        return lifecycleRegistry
    }
    
    fun onCancelClick() {
        dismiss()
    }
    
    fun onSubmitClick() {
        if (binding.startTime.text.isEmpty() && binding.endTime.text.isEmpty()) {
            showToast(R.string.empty_start_end_time)
        } else if (binding.startTime.text.isEmpty()) {
            showToast(R.string.empty_start_time)
        } else if (binding.endTime.text.isEmpty()) {
            showToast(R.string.empty_end_time)
        } else {
            onTimeChanged.onTimeChanged(day, binding.startTime.text.toString(), binding.endTime.text.toString())
            dismiss()
        }
    }

    fun onStartTimeClick() {
        isStartTime = true
        binding.txtStart.setTextColor(ContextCompat.getColorStateList(context, R.color.colorAccent))
        binding.txtEnd.setTextColor(ContextCompat.getColorStateList(context, R.color.light_grey))
    }

    fun onEndTimeClick() {
        isStartTime = false
        binding.txtStart.setTextColor(ContextCompat.getColorStateList(context, R.color.light_grey))
        binding.txtEnd.setTextColor(ContextCompat.getColorStateList(context, R.color.colorAccent))
    }

    fun onTimeChanged(p1: Int, p2: Int) {
        Log.e("TimePicker", "$p1:$p2")
        
        if (isStartTime) {
            binding.startTime.text = "$p1:$p2"
        } else {
            binding.endTime.text = "$p1:$p2"
        }
    }

    interface TimeChanged {
        fun onTimeChanged(day : String, startTime : String, endTime : String)
    }
}