package com.eiadah.app.ui.patient.postLogin.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityReportDetailsBinding
import com.eiadah.app.models.dataModel.AddAllergyData
import com.eiadah.app.models.responseModel.AddAllergy_Response
import com.eiadah.app.models.responseModel.AddAlrgyResponse
import com.eiadah.app.models.responseModel.ReportDetailResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.AddAllrgyViewModel
import com.eiadah.app.ui.patient.postLogin.activity.viewModels.ReportDetailsViewModel
import com.eiadah.app.utils.GlobalVariables

class ReportDetailsActivity : BaseActivity(), Observer<RestObservable> {

    lateinit var binding:ActivityReportDetailsBinding
    var id:String=""
    private val viewModel: ReportDetailsViewModel
            by lazy {  ViewModelProviders.of(this).get(ReportDetailsViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_report_details)
        binding.clickHandler=this

        id=intent.getStringExtra("id")
        viewModel.getreportDetails().observe(this,this)
        viewModel.reportDetails(id)
    }
    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                 if (it.data is ReportDetailResponse) {
                    val reportDetailResponse : ReportDetailResponse = it.data
                    if (reportDetailResponse.status!!) {

                        binding.tvAge.setText("Patient Age : "+" "+reportDetailResponse.data?.get(0)?.age)
                        binding.tvName.setText("Patient Name : "+" "+reportDetailResponse.data?.get(0)?.name)
                        binding.tvProblm.setText("Patient Problem : "+" "+reportDetailResponse.data?.get(0)?.symptoms)
                        binding.tvWeight.setText("Patient Weight : "+" "+reportDetailResponse.data?.get(0)?.weight+" Kg")
                        binding.tvGender.setText("Patient Gender : "+" "+reportDetailResponse.data?.get(0)?.gender)
                    }
                }

            }
            it.status == Status.ERROR -> progressDialog?.dismiss()
            it.status == Status.LOADING -> progressDialog?.show()
        }
    }

    override fun onBackPressed(){
        finish()
    }
}
