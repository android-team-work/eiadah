package com.eiadah.app.ui.patient.postLogin.activity

import android.os.Bundle
import androidx.drawerlayout.widget.DrawerLayout
import com.eiadah.app.models.NavMenu
import com.eiadah.app.ui.patient.postLogin.fragments.NavigationDrawerFragment
import android.content.Context
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.core.view.GravityCompat
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.models.responseModel.DeviceTokenResponse
import com.eiadah.app.models.responseModel.ProfileResponse
import com.eiadah.app.models.responseModel.RegisterResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.patient.postLogin.fragments.HomeFragment
import com.eiadah.app.ui.patient.postLogin.fragments.MainFragment
import com.eiadah.app.ui.patient.postLogin.view_modals.ProfileViewModel
import com.eiadah.app.ui.patient.preLogin.activity.SplashActivity
import com.eiadah.app.ui.patient.preLogin.viewModels.LoginViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.google.firebase.FirebaseApp


class HomeActivity : BaseActivity(), NavigationDrawerFragment.NavDrawerFgmtListener, HomeFragment.HomeFragmentListener,
    Observer<RestObservable> {

    private lateinit var drawerLayout : DrawerLayout

    private lateinit var mNavigationDrawerFragment: NavigationDrawerFragment
    private val context : Context = this
    private val viewModel: ProfileViewModel
            by lazy {  ViewModelProviders.of(this).get(ProfileViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        FirebaseApp.initializeApp(this);
        drawerLayout = findViewById(R.id.drawer_layout)


        mNavigationDrawerFragment = supportFragmentManager.findFragmentById(R.id.navigation_drawer) as NavigationDrawerFragment

        if (mNavigationDrawerFragment != null)
            mNavigationDrawerFragment.setupDrawer(R.id.navigation_drawer, drawerLayout)

        viewModel.getProfileResponse().observe(this,this)
        viewModel.profileApi()
    }

    override fun onBackPressed() {
        //super.onBackPressed()

        if (supportFragmentManager.backStackEntryCount == 1){
            if (mNavigationDrawerFragment != null){
                mNavigationDrawerFragment.setNavMenuItems(NavMenu.START_CHECKUP)
                super.onBackPressed()
            }

        }else{
            showExitDialog()
        }
    }

    override fun onResume() {
        mNavigationDrawerFragment.setNavMenuItems(NavMenu.START_CHECKUP)
        super.onResume()
    }

    private fun showExitDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setIcon(R.mipmap.ic_launcher)
        builder.setTitle(getString(R.string.app_name))
        builder.setMessage(getString(R.string.exit_app_confirm))
        builder.setPositiveButton(R.string.yes
        ) { _, _ -> super.onBackPressed() }
        builder.setNegativeButton(R.string.no) { dialog, _ ->
            //Reset to previous seletion menu in navigation
            dialog.dismiss()
        }
        builder.setCancelable(false)
        val dialog = builder.create()
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                .setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                .setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
        }
        dialog.show()
    }

    private fun switchContent(fragment: Fragment?, tag: String) {
        val fragmentManager = supportFragmentManager
        fragmentManager.popBackStack()
        if (fragment != null) {
            val transaction = fragmentManager.beginTransaction()
            transaction.replace(R.id.container, fragment, tag)
            if (fragment !is MainFragment)
                transaction.addToBackStack(tag)
            transaction.commit()
        }
    }

    private fun showLogoutDialog() {
        if (!isFinishing) {
            val builder = AlertDialog.Builder(this)
            builder.setIcon(R.mipmap.ic_launcher)
            builder.setTitle(getString(R.string.app_name))
            builder.setMessage(getString(R.string.exit_confirm))

            builder.setPositiveButton(R.string.yes) { _, _ ->
                SharedStorage.clear(this)
                activitySwitcher(SplashActivity::class.java, true)
            }

            builder.setNegativeButton(R.string.no) { dialog, _ ->
                //Reset to previous seletion menu in navigation
                dialog.dismiss()
                drawerLayout!!.openDrawer(GravityCompat.START)
            }
            builder.setCancelable(false)
            val dialog = builder.create()
            //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            dialog.setOnShowListener {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                    .setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                    .setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            }
            dialog.show()
        }
    }

    override fun handleDrawer() {
        Log.e("test", "2222")
        if (mNavigationDrawerFragment!!.isDrawerOpen()) {
            mNavigationDrawerFragment!!.closeDrawer()
        } else {
            mNavigationDrawerFragment!!.openDrawer()
        }
    }
//    activitySwitcher(RecommendedDoctorActivity::class.java, false)

    override fun menuClicked(navMenu: NavMenu) {
        val fragmentManager = supportFragmentManager
        var fragment: Fragment? = null
        val currentFgmt = fragmentManager.findFragmentById(R.id.container)
        var tag: String? = ""
        when (navMenu) {
            NavMenu.START_CHECKUP ->
                //===============
                if (fragment == null) {
                    fragment = MainFragment.newInstance()
                } else {
                    //Log.e("", "home fragment, no need to add")
                }

            NavMenu.LOGOUT -> showLogoutDialog()
//            NavMenu.HELP ->  activitySwitcher(HelpActivity::class.java, false)
            NavMenu.NOTIFICATIONS ->  activitySwitcher(PtNotificationActivity::class.java, false)
            NavMenu.MY_ACCOUNT ->  activitySwitcher(ProfileActivity::class.java, false)
            NavMenu.PREVIOUS_REPORTS ->  activitySwitcher(RecordsActivity::class.java, false)
            NavMenu.MY_APPOINTMENT ->  activitySwitcher(MyAppointmentActivity::class.java, false)
        }
        if (tag != null) {
            val existingFragment = supportFragmentManager.findFragmentById(R.id.container)
            if (existingFragment != null) {
                if (existingFragment.tag != tag) {
                    switchContent(fragment, tag)
                }
            } else {
                switchContent(fragment, tag)
            }
        }
    }

    override fun headerClicked() {
        activitySwitcher(ProfileActivity::class.java, false)
    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is ProfileResponse) {

                    val profileResponse : ProfileResponse = it.data
                    if (profileResponse.status!!) {
                        SharedStorage.setSession(this, true)
                    }
                }

                else if (it.data is DeviceTokenResponse) {
                    val deviceTokenResponse : DeviceTokenResponse = it.data
                }

            }
            it.status == Status.ERROR -> progressDialog.dismiss()
            it.status == Status.LOADING -> progressDialog.show()
        }
    }

}
