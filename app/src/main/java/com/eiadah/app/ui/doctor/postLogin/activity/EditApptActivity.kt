
package com.eiadah.app.ui.doctor.postLogin.activity

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.databinding.ActivityEditApptBinding
import com.eiadah.app.models.requestModels.AddAppointmenttRequest
import com.eiadah.app.models.responseModel.AddAppointmentResponse
import com.eiadah.app.models.responseModel.ApptCancelResponse
import com.eiadah.app.models.responseModel.EditApptResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.RestProcess.Companion.progressDialog
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.viewModals.AddAppointmentViewModel
import com.eiadah.app.ui.doctor.postLogin.viewModals.EditAppointmentViewModel
import com.eiadah.app.utils.AppUtils.showToast
import kotlinx.android.synthetic.main.activity_add_appointment.*
import java.text.SimpleDateFormat
import java.util.*

class EditApptActivity : AppCompatActivity(), Observer<RestObservable> {

    lateinit var binding:ActivityEditApptBinding
     var  name:String=""
     var  number:String=""
     var  date:String=""
     var  time:String=""
     var  category:String=""
     var  email:String=""
     var  notes:String=""
     var  id:String=""


    lateinit var datePickerDialog: DatePickerDialog
    lateinit var request: AddAppointmenttRequest

    private val viewModel: EditAppointmentViewModel
            by lazy {  ViewModelProviders.of(this).get(EditAppointmentViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil.setContentView(this,R.layout.activity_edit_appt)
        binding.clickHandler=this

        request= AddAppointmenttRequest()
        id=intent.getStringExtra("id")
    }

    fun backOncllick() {
        finish()
    }

    fun onSaveClick() {
         name = binding.edtName.text.toString()
         number = binding.edtMobileNumber.text.toString()
         date = binding.tvAppDate.text.toString()
         time = binding.tvAppTime.text.toString()
         category = binding.edtCategory.text.toString()
         email = binding.edtEmail.text.toString()
         notes = binding.edtNotes.text.toString()

        if (isValid(name,number, date, time,email,category,notes)) {

            request.name = name
            request.mobile=number
            request.email=email
            request.date=date
            request.time=time
            request.category=category
            request.note=notes
            request.id=id

            viewModel.EditAppointment(request)
            viewModel.getEditAppointment().observe(this, this)

            finish()
        }
    }

    fun ontimeclick(){

        val cal = Calendar.getInstance()

        val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)

            tv_app_time.text = SimpleDateFormat("HH:mm").format(cal.time)
        }

        tv_app_time.setOnClickListener {
            TimePickerDialog(this, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show()
        }

    }
    private fun isValid(name : String,number : String, date : String, time : String, email : String, category : String, notes : String) : Boolean {
        var check = false

        if (name.isEmpty() &&number.isEmpty() && date.isEmpty() && time.isEmpty()&& email.isEmpty()&& category.isEmpty()&& notes.isEmpty()) {
            showToast(this,R.string.please_fill_all_fields)
        } else if (name.isEmpty()) {
            showToast(this,"Please Enter Your Name")
        }  else if (number.isEmpty()) {
            showToast(this,R.string.empty_reg_number)
        } else if (date.isEmpty()) {
            showToast(this,"Please Select Appointment Date")
        } else if (time.isEmpty()) {
            showToast(this,"Please Select Appointment Time")
        }  else if (email.isEmpty()) {
            showToast(this,"Please Enter your email id")
        }else if (category.isEmpty()) {
            showToast(this,"Please Enter Category")
        }else if (notes.isEmpty()) {
            showToast(this,"Please Enter Notes")
        } else {
            check = true
        }

        return check
    }


    fun openCalender(){
        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR) // current year
        val mMonth = c.get(Calendar.MONTH) // current month
        val mDay = c.get(Calendar.DAY_OF_MONTH) // current day
        // date picker dialog   MM/dd/yyyy
        datePickerDialog = DatePickerDialog(this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // set day of month , month and year value in the edit text
                tv_app_date.setText(
                    (monthOfYear + 1).toString() + "/" + dayOfMonth + "/" + year) }, mYear, mMonth, mDay)
        datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000
        datePickerDialog.show()
    }

    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog?.dismiss()
                if (it.data is EditApptResponse) {
                    val editlappointmentResponse : EditApptResponse = it.data

                    if (editlappointmentResponse.status!!) {
                        finish()
                    }
                }
            }
            it.status == Status.ERROR -> {
                it.error!!.fillInStackTrace()
                progressDialog?.dismiss()
            }
            it.status == Status.LOADING -> progressDialog?.show()
        }
    }


}
