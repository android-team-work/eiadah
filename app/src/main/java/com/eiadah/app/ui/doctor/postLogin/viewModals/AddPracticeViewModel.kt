package com.eiadah.app.ui.doctor.postLogin.viewModals

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.viewModels.ObservableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AddPracticeViewModel : ObservableViewModel() {


    private var cityResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var specialistResponse : MutableLiveData<RestObservable> = MutableLiveData()
    private var saveDoctorProfile : MutableLiveData<RestObservable> = MutableLiveData()


    fun getCityList() : LiveData<RestObservable> {
        return cityResponse
    }

    fun getSpecialist() : LiveData<RestObservable> {
        return specialistResponse
    }

    fun getDoctorPracticeProfileData() : LiveData<RestObservable> {
        return saveDoctorProfile
    }

    fun loadCityList(stateId : String) {
        getService().getCityList(getHeader(), stateId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { cityResponse.value = RestObservable.loading() }
            .subscribe(
                {cityResponse.value = RestObservable.success(it)},
                {cityResponse.value = RestObservable.error (it)}
            )
    }

    fun loadSpecialist() {
        getService().getSpecialist(getHeader())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { specialistResponse.value = RestObservable.loading() }
            .subscribe(
                {specialistResponse.value = RestObservable.success(it)},
                {specialistResponse.value = RestObservable.error(it)}
            )
    }

    fun saveDoctorPracticeProfile(request: SaveDoctorProfileRequest) {
        getService().saveDoctorPracticeProfile(getHeader(), request.name,
           request.city, request.specilaity, request.own_practice,request.mobile,request.email)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { saveDoctorProfile.value = RestObservable.loading() }
            .subscribe(
                {saveDoctorProfile.value = RestObservable.success(it)},
                {saveDoctorProfile.value = RestObservable.error(it)}
            )
    }
}