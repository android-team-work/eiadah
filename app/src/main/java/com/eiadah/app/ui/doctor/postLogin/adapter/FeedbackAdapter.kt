package com.eiadah.app.ui.doctor.postLogin.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.eiadah.app.ui.doctor.postLogin.fragments.ExperienceFeedbackFragment
import com.eiadah.app.ui.doctor.postLogin.fragments.RecomendationFragment

class FeedbackAdapter(context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val mContext: Context = context

    override fun getItem(position: Int): Fragment? {
        when (TABS[position]) {
            EXPERIENCE -> return ExperienceFeedbackFragment.newInstance()
            RECOMMENDATION -> return RecomendationFragment.newInstance()
        }
        return null
    }

    override fun getCount(): Int {
        return TABS.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (TABS[position]) {
            EXPERIENCE -> return "APPS"
            RECOMMENDATION -> return "SUMMARY"
        }
        return null
    }

    companion object {
        private const val EXPERIENCE = 0
        private const val RECOMMENDATION = 1

        private val TABS = intArrayOf(EXPERIENCE, RECOMMENDATION)
    }
}