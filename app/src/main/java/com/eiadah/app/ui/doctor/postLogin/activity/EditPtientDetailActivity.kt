package com.eiadah.app.ui.doctor.postLogin.activity

import android.app.DatePickerDialog
import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eiadah.app.R
import com.eiadah.app.core.BaseActivity
import com.eiadah.app.databinding.ActivityEditPtientDetailBinding
import com.eiadah.app.models.dataModel.CityData
import com.eiadah.app.models.requestModels.AddPatientRequest
import com.eiadah.app.models.responseModel.AddPatientResponse
import com.eiadah.app.models.responseModel.CitiesResponse
import com.eiadah.app.models.responseModel.PatientProfileResponse
import com.eiadah.app.restAPI.RestObservable
import com.eiadah.app.restAPI.Status
import com.eiadah.app.ui.doctor.postLogin.viewModals.AddPatientViewModel
import com.eiadah.app.ui.doctor.postLogin.viewModals.EditPatientViewModel
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.SharedStorage
import com.mukesh.countrypicker.Country
import com.mukesh.countrypicker.CountryPicker
import com.mukesh.countrypicker.listeners.OnCountryPickerListener
import com.pieboat.shredpreference.SharePreferHelper
import java.util.*
import kotlin.collections.ArrayList

class EditPtientDetailActivity : BaseActivity(), Observer<RestObservable> {


    lateinit var binding:ActivityEditPtientDetailBinding
    private var strCity = ""
    private var strName = ""
    private var strMobile = ""
    private var strDob = ""
    private var strAge = ""
    private var strAddress = ""
    private var strlocatily = ""
    private var strEmail = ""
    private var strPincode = ""
    private var strDialcode = ""
    private var id = ""
    private var cityList : ArrayList<CityData> = ArrayList()
    lateinit var request: AddPatientRequest
    private var strGender = GlobalVariables.Gender.MALE
    lateinit var sharePreferHelper: SharePreferHelper
    lateinit var datePickerDialog: DatePickerDialog

    private val viewModel: EditPatientViewModel
            by lazy {  ViewModelProviders.of(this).get(EditPatientViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_edit_ptient_detail)
        binding.clickHandler=this
        request = AddPatientRequest()

        id=intent.getStringExtra("id")
        sharePreferHelper= SharePreferHelper(this)

        request.gender = strGender
        request.id=id

        viewModel.patientDetail(id)
        viewModel.getPatientDetails().observe(this,this)


    }
    fun onSaveClick(){

        strAddress=binding.edtStreetAddress.text.toString()
        strCity=binding.edtStreetAddress.text.toString()
        strName=binding.edtName.text.toString()
        strMobile=binding.edtPhNo.text.toString()
        strDob=binding.edtDob.text.toString()
        strAge=binding.edtAge.text.toString()
        strlocatily=binding.edtLocality.text.toString()
        strPincode=binding.edtPincode.text.toString()
        strEmail=binding.edtEmail.text.toString()
        strDialcode=binding.edtDoctorMobileCode.text.toString()


            request.name = strName
            request.mobile=strDialcode+strMobile
            request.email=strEmail
            request.locality=strlocatily
            request.address=strAddress
            request.pincode=strPincode
            request.age=strAge
            request.dob=strDob

            viewModel.EditPatient(request)
            viewModel.getEditPatient().observe(this, this)
//            activitySwitcher(HomeActivity::class.java, true)


    }
    fun backOncllick() {
        finish()
    }


    private fun loadCityData(stateId : String) {
        viewModel.loadCityList(stateId)
        viewModel.getCityList().observe(this, this)
    }

    private fun setCityAdapter() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, cityList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.cityAdapter = adapter
    }
    fun onItemSelectedCity(adapterView: AdapterView<*>, view : View, i : Int, l : Long) {
        strCity = if (i!=0) {
            cityList[i].name!!
        } else {
            ""
        }

        request.city = strCity
    }
    override fun onChanged(it: RestObservable?) {
        when {
            it!!.status == Status.SUCCESS -> {
                progressDialog.dismiss()
                if (it.data is CitiesResponse) {
                    val citiesResponse : CitiesResponse = it.data
                    if (citiesResponse.status!!) {
                        cityList = (citiesResponse.cityList as ArrayList<CityData>?)!!
                        val cityData = CityData()
                        cityData.id = "0"
                        cityData.name = "Choose city"
                        cityList.add(0, cityData)
                        setCityAdapter()
                    }

                }
                else if (it.data is AddPatientResponse) {
                    val addPatientResponse : AddPatientResponse = it.data

                    if (addPatientResponse.status!!) {
                        SharedStorage.setPatientData(this, addPatientResponse.getUser()!!)
                        activitySwitcher(HomeActivity::class.java, true)
                    }
                }

               else if (it.data is PatientProfileResponse) {
                    val patientDetailsViewModel : PatientProfileResponse = it.data

                    if (patientDetailsViewModel.status!!) {

                        binding.edtName.setText(patientDetailsViewModel.data?.name)
                        binding.edtPhNo.setText(patientDetailsViewModel.data?.phone)
                        binding.edtEmail.setText(patientDetailsViewModel.data?.email)
                        binding.edtAge.setText(patientDetailsViewModel.data?.age+" Years")
                        binding.edtDob.setText(patientDetailsViewModel.data?.dob)
                        binding.edtStreetAddress.setText(patientDetailsViewModel.data?.streetAddress)
                        binding.edtLocality.setText(patientDetailsViewModel.data?.locality)
                        binding.edtPincode.setText(patientDetailsViewModel.data?.pincode)

                    }
                }

            }
            it.status == Status.ERROR -> {
                it.error!!.fillInStackTrace()
                progressDialog.dismiss()
            }
            it.status == Status.LOADING -> progressDialog.show()
        }
    }
    fun openCalender(){
        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR) // current year
        val mMonth = c.get(Calendar.MONTH) // current month
        val mDay = c.get(Calendar.DAY_OF_MONTH) // current day
        // date picker dialog   MM/dd/yyyy
        datePickerDialog = DatePickerDialog(this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // set day of month , month and year value in the edit text
                binding.edtDob.setText(year.toString() + "-" + (monthOfYear + 1).toString() + "-" + dayOfMonth) }, mYear, mMonth, mDay)
//                   tv_app_date.setText((monthOfYear + 1).toString() + "/" + dayOfMonth + "/" + year) }, mYear, mMonth, mDay)
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis() - 1000
        datePickerDialog.show()
    }
    fun showcountrypickerdialog() {

        var builder = CountryPicker.Builder().with(this).listener(object :
            OnCountryPickerListener {

            override fun onSelectCountry(country: Country) {
                binding.edtDoctorMobileCode.setText(country.dialCode)
            }
        })
        var picker = builder.build()
        picker.showDialog(this);

    }
}
