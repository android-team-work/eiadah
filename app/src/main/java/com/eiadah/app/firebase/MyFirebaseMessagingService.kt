package com.eiadah.app.firebase

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.eiadah.app.R
import com.eiadah.app.ui.doctor.postLogin.activity.HomeActivity
import com.eiadah.app.ui.patient.postLogin.activity.ChatActivity
import com.eiadah.app.utils.GlobalVariables
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.pieboat.shredpreference.SharePreferHelper
import org.json.JSONObject


class MyFirebaseMessagingService : FirebaseMessagingService() {

    private val TAG = "MyFirebaseToken"
    var notificationManager: NotificationManager?=null
    internal var CHANNEL_ID = ""// The id of the channel.
    internal var CHANNEL_ONE_NAME = "Channel One"
    lateinit var notificationChannel: NotificationChannel
    lateinit var notification: Notification
    lateinit var context: Context
    lateinit var savePref: SharePreferHelper
    internal var message: String? = ""
    internal var type:String? = null
    internal var image:String? = null
    internal var notification_code:String? = null
    internal var id:String? = null
    internal var username:String? = null
    val CHANNEL_DESC = "Android Push Notification Tutorial"

    override fun onNewToken(s: String) {
        FirebaseApp.initializeApp(this);
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(object : OnCompleteListener<InstanceIdResult> {
                override fun onComplete(@NonNull task: Task<InstanceIdResult>) {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "getInstanceId failed", task.getException())
                        return
                    }

                    // Get new Instance ID token
                    val token = task.getResult()?.getToken()
                    Log.e("My Token", token)
                }
            })

        sendRegistrationToServer(s)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        val obj: JSONObject? = null
        savePref = SharePreferHelper(applicationContext)
        context = applicationContext
        CHANNEL_ID = applicationContext.packageName
        Log.e(TAG, "Notification Message Body: " + remoteMessage.data)
        message = remoteMessage.data["message"]
        notification_code = remoteMessage.data["notification_code"]

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(CHANNEL_ID, CHANNEL_ONE_NAME, NotificationManager.IMPORTANCE_DEFAULT)
            channel.description = CHANNEL_DESC
            val manager = getSystemService(NotificationManager::class.java)
            manager.createNotificationChannel(channel)
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = NotificationChannel(
                CHANNEL_ID,
                CHANNEL_ONE_NAME,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationChannel.enableLights(true)
            notificationChannel.setLightColor(Color.RED)
            notificationChannel.setShowBadge(true)
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC)
        }

        if (notification_code == "1") {
            if (savePref.getisChatScreen()!!) {
                publishResultsMessage()
            } else {
                displayNotification()
            }
        } else {
            displayNotification()
        }
        displayNotification()

    }

    fun displayNotification() {

        val intent = Intent(context, HomeActivity::class.java)

        val pendingIntent = PendingIntent.getActivity(
            context,
            100,
            intent,
            PendingIntent.FLAG_CANCEL_CURRENT
        )

        val mBuilder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.drawable.eiadah_app_logo)
            .setContentTitle("Eiadah")
            .setContentText(message)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_HIGH)

        val mNotificationMgr = NotificationManagerCompat.from(context)
        mNotificationMgr.notify(1, mBuilder.build())

    }

    private fun publishResultsMessage() {

        val intent = Intent(GlobalVariables.SharePref.NOTIFICATION_MESSAGE)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    private fun sendRegistrationToServer(token: String?) {
        // TODO: Implement this method to send token to your app server.
        Log.d(TAG, "sendRegistrationTokenToServer($token)")
        savePref=SharePreferHelper(applicationContext)
        savePref.setDeviceToken(applicationContext, "token", token!!)

    }
}