package com.eiadah.app.utils

class GlobalVariables {

    class CONSTANTS {
        companion object {
            const val SHARED_NAME : String = "EIADAH"

            const val SESSION : String = "session"
            const val TOKEN : String = "token"
            const val USER_DATA : String = "user_data"
            const val CHECKUP_DETAIL = "checkup_detail"
            const val SELECTED_SYMPTOMS_IDS = "selected_symptoms_ids"
            const val CHECKUP_ID = "checkup_id"
            const val USER_TYPE = "user_type"
            const val DOCTOR_DATA = "doctor_data"
            const val PATIENT_DATA = "patient_data"
        }
    }

    class URL {
        companion object {
//            const val BASE_URL : String = "http://ec2-3-16-181-38.us-east-2.compute.amazonaws.com/"
            const val BASE_URL : String = "https://www.eiadahapp.com/"
            const val CHECKUP_BASE_URL = "https://www.aivaid.com/"
            const val IMAGE_BASE_URL = BASE_URL + ""
            const val JSON_FORMAT : String = "/?format=json"

            const val REGISTER : String = "rest-auth/registration$JSON_FORMAT"
            const val LOGIN : String = "rest-auth/login$JSON_FORMAT"
            const val FORGOT_PASSWORD : String = "rest-auth/password/reset/$JSON_FORMAT"
            const val PROFILE : String = "api/v1/get_profile_api$JSON_FORMAT"
            const val UPDATE_PROFILE = "api/v1/profile_save$JSON_FORMAT"
            const val CHANGE_PASSWORD = "PasswordChangeView$JSON_FORMAT"
            const val COUNTRY_LIST = "api/v1/Country_list$JSON_FORMAT"
            const val STATES_LIST = "api/v1/State_list"
            const val CITIES_LIST = "api/v1/City_list"
            const val SPECIALIST_LIST = "api/v1/Specilaity_list$JSON_FORMAT"
            const val NOTIFICTION_LIST = "api/v1/Notification_db_list$JSON_FORMAT"
            const val SAVE_TOKEN = "chat/devices"

//            const val SPECIALIST_LIST = "api/v1/list-Specilaity$JSON_FORMAT"
            const val SUBSPECIALIST_LIST = "api/v1/subspecilaitylist"
            const val SAVE_DOCOTR_PROFILE = "api/v1/Docter_Profile_Save$JSON_FORMAT"
            const val DOCTOR_PROFILE = "api/v1/Get_Docter_Profile_Api$JSON_FORMAT"
            const val SAVE_CLINIC = "api/v1/Clinic_Profile_Save$JSON_FORMAT"
            const val PRACTICE_PROFILE = "api/v1/Doctor_Practice_serializers_db_Save$JSON_FORMAT"
            const val SERVICE_LIST = "api/v1/Services_data_list$JSON_FORMAT"
            const val SYMPTOMSLIST = "api/v1/Symptoms_list$JSON_FORMAT"

            //Checkup url
            const val SYMPTOMS_LIST = "api/v1/Symptoms_list$JSON_FORMAT"
            const val SAVE_CHECKUP = "api/v1/Check_up_save$JSON_FORMAT"
            const val QUESTION_LIST = "api/v1/Disease_Filter_by_Symptoms_id"
            const val GET_RESULT = "api/v1/result_api$JSON_FORMAT"
            const val DOCTOR_LIST = "api/v1/Docter_Profile_list$JSON_FORMAT"
            const val DOCTOR_DETAIL = "api/v1/Get_Docter_Profile_api_front"
            const val RECOMMEND_DOCTOR = "api/v1/Docter_Profile_api_specilaity"


            //Doctor Api
            const val DOCTOR_REGISTER = "api/v1/Doctor_Singup$JSON_FORMAT"
            const val DOCTOR_LOGIN = "api/v1/LoginView/$JSON_FORMAT"
            const val PATIENT_LIST = "api/v1/Doctor_Patient_Profile_list/$JSON_FORMAT"
            const val ADD_PATIENT = "api/v1/Doctor_Patient_Profile_Save/$JSON_FORMAT"
            const val PATIENT_DETAIL = "api/v1/Doctor_Patient_Profile_view"
            const val EDIT_PATIENT_DETAIL = "api/v1/Doctor_Patient_Profile_edit/$JSON_FORMAT"
            const val CONSULT_REPORT = "api/v1/Consult_Report_serializers_Save/$JSON_FORMAT"
            const val DELETE_PATIENT = "api/v1/Doctor_Patient_Profile_db_Delete"
            const val ADD_APPOINTMENT = "api/v1/Doctor_Appointment_serializers_db_Save/$JSON_FORMAT"
            const val APPOINTMENT_LIST = "api/v1/Doctor_Appointment_serializers_db_list/$JSON_FORMAT"
            const val APPOINTMENT_DETAIL = "api/v1/Doctor_Appointment_serializers_db_Detail"
            const val APPOINTMENT_CANCEL = "api/v1/Doctor_Appointment_serializers_db_cancel/$JSON_FORMAT"
            const val APPOINTMENT_APPROVE = "api/v1/Doctor_Appointment_serializers_db_approve/$JSON_FORMAT"
            const val EDIT_APPOINTMENT = "api/v1/Doctor_Appointment_serializers_db_Edit/$JSON_FORMAT"
            const val APP_LIST_ACC_DATE = "api/v1/Doctor_Appointment_date"
            const val ADD_PRESCRIPTION= "api/v1/Prescription_Save/$JSON_FORMAT"
            const val ADD_BANK_DETAIL= "api/v1/Doctor_bank_details_Save/$JSON_FORMAT"
            const val BANK_LIST= "api/v1/Doctor_bank_details_list/$JSON_FORMAT"
            const val MY_CREDIT= "api/v1/Doctor_vault_data/$JSON_FORMAT"
            const val DELETE_BANK= "api/v1/Doctor_bank_details_delete/"
            const val WITHDRAW_REQUEST= "api/v1/Doctor_withdraw_data_request/"
            const val ADD_CITIES= "api/v1/Doctor_City_db_Save/$JSON_FORMAT"
            const val TIMESLOT= "api/v1/time_slots/$JSON_FORMAT"


            //Patient Api
            const val ADD_PT_APPOINTMENT = "api/v1/Appointment_serializers_db_Save/$JSON_FORMAT"
            const val TIME_SLOT = "api/v1/get_time_slot"
            const val SAVE_REPORT = "api/v1/user_report_save/$JSON_FORMAT"
            const val REPORT_LIST = "api/v1/user_report_list"
            const val ALLERGY_LIST = "api/v1/list-aller"
            const val INCIDENT_LIST = "api/v1/list-ingu"
            const val SURGERY_LIST = "api/v1/list-surg"
            const val MEDIC_LIST = "api/v1/list-medic"
            const val ADD_SURGERY = "api/v1/surgeries/$JSON_FORMAT"
            const val ADD_ALLERGY = "api/v1/allergies/$JSON_FORMAT"
            const val ADD_INCIDENT = "api/v1/allinguries/$JSON_FORMAT"
            const val ADD_MEDIC = "api/v1/medications/$JSON_FORMAT"
            const val UPLOAD_REPORT = "api/v1/health_Record_save/$JSON_FORMAT"
            const val UPLOAD_REPORT_LIST = "api/v1/health_Record_list/$JSON_FORMAT"
            const val PAYMENT = "api/v1/Payemt_user_db_save/$JSON_FORMAT"
            const val REPORT_DETAILS = "api/v1/user_report_single"
            const val MY_APPOINTMENT_LIST = "api/v1/Patient_Appointment_list/$JSON_FORMAT"
            const val CHAT_LIST = "chat/list/$JSON_FORMAT"
            const val SEND_MSG = "chat/send/$JSON_FORMAT"
            const val GET_MSG = "chat/messages/"
        }
    }

    class SERVICE_MODE {
        companion object {
            const val PROFILE : Int = 3
            const val FORGOT_PASSWORD : Int = 4
            const val CHANGE_PASSWORD = 6
        }
    }

    class PARAMS {

        class Register {
            companion object {
                const val USERNAME  = "username"
                const val EMAIL  = "email"
                const val PASSWORD = "password1"
                const val CONFIRM_PASSWORD  = "password2"
                const val NAME  = "first_name"
                const val MOBILE = "mobile"
            }
        }

        class Login {
            companion object {
                const val USERNAME : String = "username"
                const val PASSWORD : String = "password"
                const val EMAIL = "email"
                const val REGISTRATION_ID = "registration_id"
                const val TYPE = "type"
            }
        }

        class ForgotPassword {
            companion object {
                const val EMAIL : String = "email"
            }
        }

        class PROFILE {
            companion object {
                const val NAME : String = "first_name"
                const val PHONE : String = "phone"
                const val GENDER : String = "gender"
                const val DOB : String = "dob"
                const val BLOOD_GROUP : String = "bloodgroup"
                const val EMAIL : String = "email"
                const val WEIGHT : String = "weight"
                const val HEIGHT : String = "height"
                const val LOCATION : String = "location"
                const val ADD_ALLERGY : String = "allergy"
            }
        }

        class ChangePassword {
            companion object {
                const val OLD_PASSWORD = "old_password"
                const val NEW_PASSWORD = "new_password"
                const val CONFIRM_PASSWORD = "confirm_new_password"
            }
        }

        class DoctorProfile {
            companion object {
                const val NAME = "name"
                const val COUNTRY = "country"
                const val STATE = "state"
                const val CITY = "city"
                const val SPECIALITY = "specilaity"
                const val GENDER = "gender"

                const val DEGREE = "degree"
                const val COLLEGE = "college"
                const val YEAR = "year"

                const val REG_NUMBER = "reg_number"
                const val REG_COUNCIL = "reg_council"
                const val REG_YEAR = "reg_year"
                const val SUB_SPECIALITY = "sub_specilaity"
                const val SERVICES = "services"
                const val SYMPTOMS = "symptoms"
                const val FEE = "fee"
                const val EXPERIENCE ="experience"
                const val REG_IMAGE = "registration_image"
                const val ID_PROOF = "id_proof"
                const val MOBILE = "mobile"
                const val CURRENCY = "currancy_code"
                const val Allergy_NAME = "allergy_name"
            }
        }
        class DoctorPracticeProfile {
            companion object {
                const val NAME = "name"
                const val CITY = "city"
                const val SPECIALITY = "speciality"
                const val OWN_PRACTICE = "own_practice"
                const val MOBILE = "mobilenumber"
                const val EMAIL = "email"
            }
        }

        class AddPatient {
            companion object {
                const val NAME = "name"
                const val ID = "id"
                const val CITY = "city"
                const val GENDER = "gender"

                const val DOB = "dob"
                const val AGE = "age"
                const val STREET_ADDRESS = "street_address"

                const val LOCALITY = "locality"
                const val PINCODE = "pincode"
                const val MOBILE = "phone"
                const val EMAIL = "email"
            }
        }
        class AddAppointment {
            companion object {
                const val NAME = "name"
                const val ID = "user_id"
                const val DATE = "date"
                const val TIME = "time"

                const val CATEGORY = "category"
                const val NOTE = "note"

                const val MOBILE = "phone"
                const val EMAIL = "email"
                const val Id = "id"
                const val DOCTOR_ID = "doctor_id"

                const val TITLE = "title"
                const val RESULT = "result111"
                const val SYMPTOMS = "symptoms"
                const val WEIGHT = "weight"
                const val AGE = "age"
                const val USERID = "userid"
                const val GENDER = "gender"
            }
        }
        class AddPrescription {
            companion object {
                const val NAME = "name"
                const val ID = "user_id"
            }
        }
        class SaveClinic {
            companion object {
                const val CLINIC_NAME = "clinic_name"
                const val CLINIC_LOCATION = "clinic_location"
                const val TIMING = "timing"
                const val TIME = "time"
            }
        }
        class Paymnet {
            companion object {
                const val APPOINTMENT_ID = "appointment_id"
                const val TRANSACTION_ID = "transaction_id"
                const val DOCTOR_ID = "doctor_id"
            }
        }
        class withdrawRequest {
            companion object {
                const val ID = "id"
                const val AMOUNT = "amount"
            }
        }
        class AddBank {
            companion object {
                const val NAME = "name"
                const val BANK_NAME = "bank_name"
                const val ACCOUNT_NUMBER = "account_number"
                const val IFSC_CODE = "ifsc_code"
            }
        }
        class SendMsg {
            companion object {
                const val RECEIVER = "receiver"
                const val SENDER = "sender"
                const val MESSAGE = "message"
                const val IMAGE = "image"

            }
        }
        class Consult_Report {
            companion object {
                const val TEXT = "text"

            }
        }


        class AddCities {
            companion object {
                const val CITY_NAME = "city_name"


            }
        }

    }

    class Gender {
        companion object {
            const val MALE : String = "Male"
            const val FEMALE : String = "Female"
            const val YES : String = "yes"
            const val NO : String = "No"
            const val TIME_1 : String = "15"
            const val TIME_2 : String = "30"
            const val TIME_3 : String = "1"

        }

    }
    class AddSurgery {
        companion object {
            const val TITLE : String = "title"


        }

    }

    class Header {
        companion object{
            const val TOKEN : String = "Authorization"
        }

    }

    class User {
        companion object {
            const val PATIENT : Int = 0
            const val DOCTOR : Int = 1
        }
    }

    class SharePref {
        companion object {
            val NOTIFICATION_MESSAGE = "com.android.Message_Handle_Brodcast_Reciever.SEND_MESSAGE"

            const val APP_PREFS : String="Pref"
            const val State_ID : String="state_id"
            const val OWN_PRACTICE : String="own_practice"
            const val PATIENT_ID : String="id"
            const val RESULT_LIST : String="rsult_list"
            const val DOCTOR_NAME : String="doc_name"
            const val DOCTOR_ID : String="doc_id"
            const val SYMPTOMS_NAME : String="symptoms_name"
            const val CLINIC_TIME : String="clinic_time"
            const val AGE : String="age"
            const val WIGHT : String="weight"
            const val NAME : String="name"
            const val PATIENTID : String="patient_id"
            const val USER_ID : String="user_id"
            const val USER_Image : String="user_image"
            const val GENDER : String="gender"

        }
    }

}