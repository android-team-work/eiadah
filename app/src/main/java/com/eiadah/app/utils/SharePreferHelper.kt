package com.pieboat.shredpreference

import android.content.Context
import android.content.SharedPreferences
import com.eiadah.app.utils.GlobalVariables


class SharePreferHelper(internal var mContext: Context) {
    private val langPref: SharedPreferences? = null
    private var mEditor: SharedPreferences.Editor? = null
    private val mSharedPreferences: SharedPreferences
    val PREF_TOKEN = "BindingApp"
    val PREF_TOKEN_DATA = "BindingAppToken"


    init {
        mSharedPreferences = mContext.getSharedPreferences(GlobalVariables.SharePref.APP_PREFS, Context.MODE_PRIVATE)
        mEditor = mSharedPreferences.edit()
    }

    fun saveString(key: String, value: String): Boolean {

        mEditor = mSharedPreferences.edit()
        mEditor!!.putString(key, value)
        mEditor!!.apply()
        mEditor!!.commit()
        return false
    }

    fun intValue(key: String, value: Int): Boolean {
        mEditor = mSharedPreferences.edit()
        mEditor!!.putInt(key, value)
        mEditor!!.apply()
        mEditor!!.commit()
        return false

    }

    // to get values
    fun getString(key: String): String? {
        return mSharedPreferences.getString(key, null)
    }

    fun getInt(key: String): Int {
        return mSharedPreferences.getInt(key, 0)
    }

    fun removeKeyValuePosition(Key: String, position: Int) {
        val editor = mSharedPreferences.edit()
        editor.remove(position.toString())
        editor.commit()
    }

    fun getString(key: String, defaultValue: String): String? {
        return mSharedPreferences.getString(key, defaultValue)
    }

    fun saveBoolean(key: String, value: Boolean) {

        mEditor = mSharedPreferences.edit()
        mEditor!!.putBoolean(key, value)
        mEditor!!.commit()
    }

    protected fun <T> getPref(key: String, defValue: T): T {
        val returnValue = mSharedPreferences.all[key] as T?
        return returnValue ?: defValue
    }

    fun getBoolean(key: String, defaultValue: Boolean?): Boolean {
        return mSharedPreferences.getBoolean(key, defaultValue!!)
    }

    fun savePref(key: String, value: Any?) {
        delete(key)

        if (value is Boolean) {
            mEditor!!.putBoolean(key, (value as Boolean?)!!)
        } else if (value is Int) {
            mEditor!!.putInt(key, (value as Int?)!!)
        } else if (value is Float) {
            mEditor!!.putFloat(key, (value as Float?)!!)
        } else if (value is Long) {
            mEditor!!.putLong(key, (value as Long?)!!)
        } else if (value is String) {
            mEditor!!.putString(key, value as String?)
        } else if (value is Enum<*>) {
            mEditor!!.putString(key, value.toString())
        } else if (value != null) {
        }
        mEditor!!.commit()
    }

    private fun delete(key: String) {
        if (mSharedPreferences.contains(key)) {
            mEditor!!.remove(key).commit()
        }
    }

    fun removeKey(key: String) {
        val editor = mSharedPreferences.edit()
        editor.remove(key)
        editor.commit()
        editor.apply()
    }

//    fun saveString(screenshots: String, imageModal: Image_Modal): Boolean {
//        mEditor = mSharedPreferences.edit()
//        mEditor!!.putString(screenshots, null)
//        mEditor!!.putString(imageModal.toString(), null)
//        mEditor!!.commit()
//        return false
//    }
    fun clearPreferences() {
        mSharedPreferences.edit().clear().commit()
    }
    /*companion object {
        private val TAG = SharePreferHelper::class.java.simpleName
        private val instance: SharePreferHelper? = null
        val IMAGE_PATH = "image_path"
        fun getInstance(): SharePreferHelper? {
            if (instance == null) {

            }
            return instance
        }
    }*/


    fun isChatScreen(chat_id: Boolean?) {
        mEditor?.putBoolean("chat_id", chat_id!!)
        mEditor?.commit()
    }


    fun getisChatScreen(): Boolean? {
        return mSharedPreferences.getBoolean("chat_id", false)
    }

    fun setDeviceToken(mContext: Context, key: String, value: String) {
        val sharedpreferences = mContext.getSharedPreferences(PREF_TOKEN_DATA, Context.MODE_PRIVATE)
        val editor = sharedpreferences.edit()
        editor.putString(key, value)
        editor.commit()
    }

    fun getDeviceToken(mContext: Context, key: String): String? {
        val preferences = mContext.getSharedPreferences(PREF_TOKEN_DATA, Context.MODE_PRIVATE)
        return preferences.getString(key, "")
    }
}
