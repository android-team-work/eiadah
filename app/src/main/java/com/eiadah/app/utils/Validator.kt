package com.eiadah.app.utils

import android.content.Context
import android.view.View
import android.widget.Toast
import com.eiadah.app.R
import java.util.regex.Matcher
import java.util.regex.Pattern

class Validator {
    companion object {
        fun isValidPassword(password: String): Boolean {

            val pattern: Pattern
            val matcher: Matcher

            val PASSWORD_PATTERN =
                "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$"

            pattern = Pattern.compile(PASSWORD_PATTERN)
            matcher = pattern.matcher(password)

            return matcher.matches()

        }

        fun registerValidator(
            name: String, email: String, passord: String,
            confirm_passord: String, context: Context
        ): Boolean {
            if (name.isEmpty()) {
                Toast.makeText(context, R.string.error_name, Toast.LENGTH_SHORT).show()
                return false
            } else if (email.isEmpty()) {
                Toast.makeText(context, R.string.error_email, Toast.LENGTH_SHORT).show()
                return false
            } else if (passord.isEmpty()) {
                Toast.makeText(context, R.string.error_password, Toast.LENGTH_SHORT).show()
                return false
            } else if (confirm_passord.isEmpty()) {
                Toast.makeText(context, R.string.error_password, Toast.LENGTH_SHORT).show()
                return false
            } else if (passord != confirm_passord) {
                Toast.makeText(context, R.string.password_mismatch, Toast.LENGTH_SHORT).show()
                return false
            } else {
                return true
            }
        }
    }

    fun loginValidator(username: String, password: String, context: Context): Boolean {
        if (username.isEmpty()) {
            Toast.makeText(context, R.string.error_email, Toast.LENGTH_SHORT).show()
            return false
        } else if (password.isEmpty()) {
            Toast.makeText(context, R.string.error_password, Toast.LENGTH_SHORT).show()

            return false
        } else {
            return true
        }
    }


    fun resetPasswordValidator(oldpass: String, pass: String, confirm_passord: String, context: Context): Boolean {
        if (oldpass.isEmpty()) {
            Toast.makeText(context, R.string.error_password, Toast.LENGTH_SHORT).show()
            return false

        } else if (pass.isEmpty()) {
            Toast.makeText(context, R.string.error_password, Toast.LENGTH_SHORT).show()
            return false

        } else if (confirm_passord.isEmpty()) {
            Toast.makeText(context, R.string.error_password, Toast.LENGTH_SHORT).show()
            return false

        } else if (pass != confirm_passord) {
            Toast.makeText(context, R.string.password_mismatch, Toast.LENGTH_SHORT).show()
            return false
        } else {
            return true
        }
    }


    fun isValidPassword(password: String): Boolean {

        val pattern: Pattern
        val matcher: Matcher

        val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$"

        pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)

        return matcher.matches()

    }


}


