package com.eiadah.app.utils

import android.content.Context
import android.content.SharedPreferences
import com.eiadah.app.models.dataModel.AddPatientData
import com.eiadah.app.models.dataModel.DoctorProfileData
import com.eiadah.app.models.dataModel.UserData
import com.eiadah.app.models.requestModels.SaveCheckupDetailsRequest
import com.google.gson.Gson

class SharedStorage {


    companion object {

        private fun getInstance(context: Context) : SharedPreferences {
            return context.getSharedPreferences(GlobalVariables.CONSTANTS.SHARED_NAME, Context.MODE_PRIVATE)
        }

        fun setSession(context: Context, session : Boolean) {
            getInstance(context).edit().putBoolean(GlobalVariables.CONSTANTS.SESSION, session).commit()
        }

        fun isSession(context: Context) : Boolean {
            return getInstance(context).getBoolean(GlobalVariables.CONSTANTS.SESSION, false)
        }

        fun setToken(context: Context, token : String) {
            getInstance(context).edit().putString(GlobalVariables.CONSTANTS.TOKEN, token).commit()
        }

        fun getToken(context: Context) : String {
            return getInstance(context).getString(GlobalVariables.CONSTANTS.TOKEN, "")
        }

        fun setUserType(context: Context, type : Int) {
            getInstance(context).edit().putInt(GlobalVariables.CONSTANTS.USER_TYPE, type).commit()
        }

        fun getUserType(context: Context) : Int {
            return getInstance(context).getInt(GlobalVariables.CONSTANTS.USER_TYPE , GlobalVariables.User.PATIENT)
        }

        fun setUserData(context: Context, userData: UserData) {
            getInstance(context).edit().putString(GlobalVariables.CONSTANTS.USER_DATA, Gson().toJson(userData)).commit()
        }

        fun getUserData(context: Context) : UserData {
            return Gson().fromJson(getInstance(context).getString(GlobalVariables.CONSTANTS.USER_DATA, ""), UserData::class.java)
        }

        fun setCheckupDetail(context: Context, saveCheckupDetailsRequest: SaveCheckupDetailsRequest) {
            getInstance(context).edit().putString(GlobalVariables.CONSTANTS.CHECKUP_DETAIL, Gson().toJson(saveCheckupDetailsRequest)).commit()
        }

        fun getCheckupDetail(context: Context) : SaveCheckupDetailsRequest {
            return Gson().fromJson(getInstance(context).getString(GlobalVariables.CONSTANTS.CHECKUP_DETAIL, ""), SaveCheckupDetailsRequest::class.java)
        }

        fun setSelectedSymptomsIds(context: Context, ids : String) {
            getInstance(context).edit().putString(GlobalVariables.CONSTANTS.SELECTED_SYMPTOMS_IDS, ids).commit()
        }

        fun getSelectedSymptomsIds(context: Context) : String {
            return getInstance(context).getString(GlobalVariables.CONSTANTS.SELECTED_SYMPTOMS_IDS, "")
        }

        fun getCheckupId(context: Context) : String {
            return getInstance(context).getString(GlobalVariables.CONSTANTS.CHECKUP_ID, "")
        }

        fun setCheckupId(context: Context, id : String) {
            getInstance(context).edit().putString(GlobalVariables.CONSTANTS.CHECKUP_ID, id).commit()
        }

        fun getDoctorData(context: Context) : DoctorProfileData {
            return Gson().fromJson(getInstance(context).getString(GlobalVariables.CONSTANTS.DOCTOR_DATA, ""), DoctorProfileData::class.java)
        }

        fun setDoctorData(context: Context, doctorProfileData: DoctorProfileData) {
            getInstance(context).edit().putString(GlobalVariables.CONSTANTS.DOCTOR_DATA, Gson().toJson(doctorProfileData)).commit()
        }

        fun getPatientData(context: Context) : AddPatientData {
            return Gson().fromJson(getInstance(context).getString(GlobalVariables.CONSTANTS.PATIENT_DATA, ""), AddPatientData::class.java)
        }

        fun setPatientData(context: Context, patientData: AddPatientData) {
            getInstance(context).edit().putString(GlobalVariables.CONSTANTS.PATIENT_DATA, Gson().toJson(patientData)).commit()
        }



        fun clear(context: Context) {
            getInstance(context).edit().clear().commit()
        }
    }
}