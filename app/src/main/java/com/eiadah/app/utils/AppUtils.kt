package com.eiadah.app.utils

import android.content.Context
import android.net.ConnectivityManager
import android.os.Environment
import android.util.Log
import android.widget.Toast

import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

import okhttp3.ResponseBody

/**
 * Created by varinder on 30/8/18.
 */

object AppUtils {

    @JvmStatic
    fun showToast(context: Context?, msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    fun showToast(context: Context, msg: Int) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    @JvmStatic
    fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        return cm.activeNetworkInfo != null
    }

    fun writeResponseBodyToDisk(body: ResponseBody, path: String, ext: String, name: String): String {
        Log.e("Expension  ", ext)
        try {
            // todo change the file location/name according to your needs
            val direct = File(Environment.getExternalStorageDirectory().toString() + path)
            if (!direct.exists()) {
                //File wallpaperDirectory = new File(dir);
                direct.mkdirs()
            }
            val file = File(direct, "$name.$ext")
            if (file.exists()) {
                file.delete()
            }
            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null
            try {
                val fileReader = ByteArray(4096)
                val fileSize = body.contentLength()
                var fileSizeDownloaded: Long = 0
                inputStream = body.byteStream()
                outputStream = FileOutputStream(file)

                while (true) {
                    val read = inputStream!!.read(fileReader)
                    if (read == -1) {
                        break
                    }
                    outputStream.write(fileReader, 0, read)
                    fileSizeDownloaded += read.toLong()
                    Log.e("Data Saved", "file download: $fileSizeDownloaded of $fileSize")
                }
                outputStream.flush()
                return file.absolutePath
            } catch (e: IOException) {
                return ""
            } finally {
                inputStream?.close()
                outputStream?.close()
            }
        } catch (e: IOException) {
            return ""
        }

    }

    fun getExt(file: String): String {
        val sb = StringBuilder()
        for (i in file.length - 1 downTo 0) {
            if (file[i] != '.') {
                sb.append(file[i])
            } else {
                break
            }
        }
        val ext = sb.reverse().toString()
        Log.e("ext", ext)
        return ext
    }

    fun isFileExists(path: String): Boolean {
        val file = File(path)
        return file.exists()
    }

    fun deleteFolder(path: String) {
        val dir = File(Environment.getExternalStorageDirectory(), path)
        if (dir.isDirectory) {
            val children = dir.list()
            for (i in children.indices) {
                File(dir, children[i]).delete()
            }
        }
    }
}
