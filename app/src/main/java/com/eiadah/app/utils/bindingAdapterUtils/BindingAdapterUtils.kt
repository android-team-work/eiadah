package com.eiadah.app.utils.bindingAdapterUtils

import android.annotation.SuppressLint
import androidx.databinding.BindingAdapter
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.ViewPager
import android.util.Log
import android.widget.ImageView
import android.widget.RadioGroup
import android.widget.TextView
import com.bumptech.glide.Glide
import com.eiadah.app.R
import com.eiadah.app.utils.GlobalVariables
import com.bumptech.glide.request.RequestOptions
import com.eiadah.app.models.dataModel.ClinicTimeData
import com.eiadah.app.ui.doctor.postLogin.activity.HomeActivity
import com.eiadah.app.ui.doctor.postLogin.adapter.DoctorHomeAdapter


object BindingAdapterUtils {

    @BindingAdapter("checked")
    @JvmStatic
    fun isChecked(radioButton: RadioGroup, gender : String) {
        if (gender == GlobalVariables.Gender.FEMALE)
            radioButton.check(R.id.radioButtonFemale)
        else
            radioButton.check(R.id.radioButtonMale)
    }

    @BindingAdapter("src")
    @JvmStatic
    fun setImage(imageView: ImageView, image : String?) {
        if (image!=null) {
            Glide.with(imageView.context).load(image).apply(RequestOptions().centerCrop()).into(imageView)
        }
    }

    @BindingAdapter("adapter")
    @JvmStatic
    fun bindViewPagerAdapter(view: ViewPager, activity : HomeActivity) {
        Log.e("viewPager", "adapter")
        val adapter = DoctorHomeAdapter(view.context, activity.supportFragmentManager)
        view.adapter = adapter
    }

    @BindingAdapter("pager")
    @JvmStatic
    fun bindViewPagerTabs(view: TabLayout, pagerView: ViewPager) {
        Log.e("setTab", "true")
        view.setupWithViewPager(pagerView, true)
    }
    
    @SuppressLint("SetTextI18n")
    @BindingAdapter("clinicTimeText")
    @JvmStatic
    fun clinicTimeText(textView: TextView, data : ClinicTimeData) {
        if (data.close == "1") {
            textView.text = textView.context.getString(R.string.closed)
        } else {
            textView.text = data.openHours +" - " + data.closeHours
        }
    }
}