package com.eiadah.app.Listener;


import androidx.fragment.app.Fragment;

public interface CheckupChangeFragmentListener {
    void changeFragment(Fragment fragment, String TAG);
}
