package com.eiadah.app.Listener

import com.eiadah.app.ui.patient.postLogin.fragments.viewModels.ProfileViewModel

interface UpdateProfileData {
    fun updateProfileData(data: ProfileViewModel)

    fun updateProfileData(params : HashMap<String, String>)

}