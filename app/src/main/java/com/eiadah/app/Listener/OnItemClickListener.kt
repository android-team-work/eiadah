package com.eiadah.app.Listener

interface OnItemClickListener {

    fun onItemClick(position : Int)
}