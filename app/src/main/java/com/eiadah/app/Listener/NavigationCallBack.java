package com.eiadah.app.Listener;

/**
 * Created by varinder on 14-06-2019.
 */
public interface NavigationCallBack {
    public void handleNavigationDrawer();

    public void enableDisableNavigationDrawer(boolean isEnable);
}
