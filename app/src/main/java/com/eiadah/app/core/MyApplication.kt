package com.eiadah.app.core

import android.app.Application
import android.content.Context

class MyApplication : Application() {

    companion object {
        private var context: Context? = null
        fun getAppContext(): Context? {
            return context
        }
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
    }


}