package com.eiadah.app.core

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.Toast
import com.eiadah.app.restAPI.RestApiInterface
import com.eiadah.app.restAPI.ServiceGenerator
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.ProgressHUD
import com.eiadah.app.utils.SharedStorage

open class BaseDialog(context: Context) : Dialog(context) {

    lateinit var progressDialog: ProgressHUD

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        progressDialog = ProgressHUD.create(context,
            "Please wait..",
            true,
            cancelable = false,
            show = false,
            cancelListener = null
        )
    }


    fun showToast(message : String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun showToast(message : Int) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun getService() : RestApiInterface {
        return ServiceGenerator.createService(RestApiInterface::class.java)
    }

    fun getHeader() : HashMap<String, String> {
        var header : HashMap<String, String> = HashMap()
        header[GlobalVariables.Header.TOKEN] = "Token " + SharedStorage.getToken(context)
        return header

    }
 }