package com.eiadah.app.core

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.eiadah.app.R
import android.app.Activity
import android.content.Context
import android.os.PersistableBundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.eiadah.app.restAPI.RestApiInterface
import com.eiadah.app.restAPI.ServiceGenerator
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.ProgressHUD
import com.eiadah.app.utils.SharedStorage


open class BaseActivity : AppCompatActivity() {

    val progressDialog: ProgressHUD by lazy {
        ProgressHUD.create(this,
            "Please wait..",
            true,
            cancelable = false,
            show = false,
            cancelListener = null
        )
    }

    fun activitySwitcher(className: Class<*>, bundle: Bundle?, isFinish: Boolean) {
        val intent = Intent(this, className)
        intent.putExtras(bundle!!)
        startActivity(intent)
        overridePendingTransition(R.anim.enter, R.anim.exit)
        if (isFinish)
            finish()
    }

    fun activitySwitcher(className: Class<*>, isFinish: Boolean) {
        val intent = Intent(this, className)
        startActivity(intent)
        overridePendingTransition(R.anim.enter, R.anim.exit)
        if (isFinish)
            finish()
    }

    fun activitySwitcher(className: Class<*>, resultCode: Int) {
        val intent = Intent(this, className)
        startActivityForResult(intent, resultCode)
        overridePendingTransition(R.anim.enter, R.anim.exit)
    }

    fun hideKeyboard() {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = currentFocus
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showToast(message : String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun showToast(message : Int) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun getService() : RestApiInterface {
        return ServiceGenerator.createService(RestApiInterface::class.java)
    }

    fun getHeader() : HashMap<String, String> {
        var header : HashMap<String, String> = HashMap()
        header[GlobalVariables.Header.TOKEN] = "Token " + SharedStorage.getToken(this)
        return header

    }
}