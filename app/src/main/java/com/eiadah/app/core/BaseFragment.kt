package com.eiadah.app.core

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.eiadah.app.R
import com.eiadah.app.restAPI.*
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.ProgressHUD
import com.eiadah.app.utils.SharedStorage

open class BaseFragment : Fragment() {

    lateinit var progressDialog: ProgressHUD

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressDialog = ProgressHUD.create(context!!,
            "Please wait..",
            true,
            cancelable = false,
            show = false,
            cancelListener = null
        )
    }

    fun activitySwitcher(className: Class<*>, bundle: Bundle?, isFinish: Boolean) {
        val intent = Intent(activity, className)
        intent.putExtras(bundle!!)
        startActivity(intent)
        activity?.overridePendingTransition(R.anim.enter, R.anim.exit)
        if (isFinish)
            activity?.finish()
    }

    fun activitySwitcher(className: Class<*>, isFinish: Boolean) {
        val intent = Intent(activity, className)
        startActivity(intent)
        activity?.overridePendingTransition(R.anim.enter, R.anim.exit)
        if (isFinish)
            activity?.finish()
    }

    fun activitySwitcher(className: Class<*>, resultCode: Int) {
        val intent = Intent(activity, className)
        startActivityForResult(intent, resultCode)
        activity?.overridePendingTransition(R.anim.enter, R.anim.exit)
    }
    fun hideKeyboard() {
        val imm = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity?.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showToast(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun showToast(message : Int) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun getService() : RestApiInterface {
        return ServiceGenerator.createService(RestApiInterface::class.java)
    }

    fun getHeader() : HashMap<String, String> {
        var header : HashMap<String, String> = HashMap()
        header[GlobalVariables.Header.TOKEN] = "Token " + context?.let { SharedStorage.getToken(it) }
        return header

    }

    fun getCheckupService() : CheckupRestApiInterface {
        return CheckupServiceGenertor.createService(CheckupRestApiInterface::class.java)
    }


}