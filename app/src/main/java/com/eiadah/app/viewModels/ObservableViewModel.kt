package com.eiadah.app.viewModels

import androidx.lifecycle.ViewModel
import android.content.Context
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import com.eiadah.app.core.MyApplication
import com.eiadah.app.restAPI.CheckupRestApiInterface
import com.eiadah.app.restAPI.CheckupServiceGenertor
import com.eiadah.app.restAPI.RestApiInterface
import com.eiadah.app.restAPI.ServiceGenerator
import com.eiadah.app.utils.GlobalVariables
import com.eiadah.app.utils.ProgressHUD
import com.eiadah.app.utils.SharedStorage
import io.reactivex.disposables.CompositeDisposable



open class ObservableViewModel : ViewModel() ,  Observable {

    private val callbacks: PropertyChangeRegistry by lazy { PropertyChangeRegistry() }
    private val disposables = CompositeDisposable()

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.remove(callback)
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.add(callback)
    }

    /**
     * Notifies listeners that all properties of this instance have changed.
     */
    @Suppress("unused")
    fun notifyChange() {
        callbacks.notifyCallbacks(this, 0, null)
    }

    /**
     * Notifies listeners that a specific property has changed. The getter for the property
     * that changes should be marked with [Bindable] to generate a field in
     * `BR` to be used as `fieldId`.
     *
     * @param fieldId The generated BR id for the Bindable field.
     */
    fun notifyPropertyChanged(fieldId: Int) {
        callbacks.notifyCallbacks(this, fieldId, null)
    }

    fun getCheckupService() : CheckupRestApiInterface {
        return CheckupServiceGenertor.createService(CheckupRestApiInterface::class.java)
    }

    fun getService() : RestApiInterface {
        return ServiceGenerator.createService(RestApiInterface::class.java)
    }

    fun getHeader() : HashMap<String, String> {
        var header : HashMap<String, String> = HashMap()
        header[GlobalVariables.Header.TOKEN] = "Token " + SharedStorage.getToken(MyApplication.getAppContext()!!)
        return header

    }
}