package com.eiadah.app.restAPI

import retrofit2.Response

interface ObservableCallback {

    fun onSuccess(response: Response<*>, serviceMode: Int)

    fun onError(t: Throwable, serviceMode: Int)

}