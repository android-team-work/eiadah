package com.eiadah.app.restAPI

import com.eiadah.app.models.requestModels.SaveCheckupDetailsRequest
import com.eiadah.app.models.responseModel.*
import com.eiadah.app.utils.GlobalVariables
import io.reactivex.Observable
import retrofit2.http.*

interface CheckupRestApiInterface {

    @GET(GlobalVariables.URL.SYMPTOMS_LIST)
    fun getSymptomsListResponse(): Observable<GetSymptomsResponse>

    @GET(GlobalVariables.URL.QUESTION_LIST + "/{symptoms_id}${GlobalVariables.URL.JSON_FORMAT}")
    fun getQuestionList(@Path("symptoms_id") symptomsId: String): Observable<GetQuestionResponse>

    @POST(GlobalVariables.URL.SAVE_CHECKUP)
    fun saveCheckupResponse(@Body saveCheckupDetailsRequest: SaveCheckupDetailsRequest): Observable<SaveCheckupResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.GET_RESULT)
    fun getCheckupResult(
        @Field("user_id") checkupId: String,
        @Field("symptoms_id") symptomsId: String,
        @Field("list_count") listCount: String,
        @Field("ans_list") ansList: String
    ): Observable<GetResultResponse>
}