package com.eiadah.app.restAPI

import com.eiadah.app.models.requestModels.SaveClinicRequest
import com.eiadah.app.models.requestModels.SaveDoctorProfileRequest
import com.eiadah.app.models.requestModels.SaveReportRequest
import com.eiadah.app.models.responseModel.*
import com.eiadah.app.utils.GlobalVariables
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*


interface RestApiInterface {

    @FormUrlEncoded
    @POST(GlobalVariables.URL.REGISTER)
    fun registerUser(
        @Field(GlobalVariables.PARAMS.Register.USERNAME) username: String,
        @Field(GlobalVariables.PARAMS.Register.NAME) name: String,
        @Field(GlobalVariables.PARAMS.Register.EMAIL) email: String,
        @Field(GlobalVariables.PARAMS.Register.PASSWORD) password: String,
        @Field(GlobalVariables.PARAMS.Register.CONFIRM_PASSWORD) confirmPassword: String): Observable<RegisterResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.LOGIN)
    fun loginApi(@Field(GlobalVariables.PARAMS.Login.EMAIL) username: String,
                 @Field(GlobalVariables.PARAMS.Login.PASSWORD) password: String): Observable<RegisterResponse>



    @GET(GlobalVariables.URL.PROFILE)
    fun getProfile(@HeaderMap headers: HashMap<String, String>): Observable<ProfileResponse>


    @FormUrlEncoded
    @POST(GlobalVariables.URL.FORGOT_PASSWORD)
    fun forgotPassword(@Field(GlobalVariables.PARAMS.ForgotPassword.EMAIL) email: String): Call<BasicResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.UPDATE_PROFILE)
    fun updateProfile(@HeaderMap headers: HashMap<String, String>,
                      @FieldMap params: HashMap<String, String>): Observable<ProfileResponse>

    @Multipart
    @POST(GlobalVariables.URL.UPDATE_PROFILE)
    fun updateProfileImage(@HeaderMap headers: HashMap<String, String>,
                           @Part body: MultipartBody.Part): Observable<ProfileResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.CHANGE_PASSWORD)
    fun changePassword(
        @HeaderMap headers: HashMap<String, String>,
        @Field(GlobalVariables.PARAMS.ChangePassword.OLD_PASSWORD) oldPassword: String,
        @Field(GlobalVariables.PARAMS.ChangePassword.NEW_PASSWORD) newPassword: String,
        @Field(GlobalVariables.PARAMS.ChangePassword.CONFIRM_PASSWORD) confirmPassword: String): Call<BasicResponse>

    @GET(GlobalVariables.URL.COUNTRY_LIST)
    fun getCountryList(@HeaderMap headers: HashMap<String, String>) : Observable<CountryResponse>

    @GET(GlobalVariables.URL.STATES_LIST + "/{country_id}${GlobalVariables.URL.JSON_FORMAT}")
    fun getStatesList( @HeaderMap headers: HashMap<String, String>,
                       @Path("country_id") countryId : String) : Observable<StatesResponse>

    @GET(GlobalVariables.URL.CITIES_LIST + "/{state_id}${GlobalVariables.URL.JSON_FORMAT}")
    fun getCityList(@HeaderMap headers: HashMap<String, String>,
                     @Path("state_id") stateId : String) : Observable<CitiesResponse>



    //Doctor App Api
    @FormUrlEncoded
    @POST(GlobalVariables.URL.DOCTOR_REGISTER)
    fun doctorRegister(@Field(GlobalVariables.PARAMS.Register.EMAIL) email: String,
                       @Field(GlobalVariables.PARAMS.Register.MOBILE) mobile : String,
                       @Field(GlobalVariables.PARAMS.Register.PASSWORD) password: String,
                       @Field(GlobalVariables.PARAMS.Register.CONFIRM_PASSWORD) confirmPassword: String) : Observable<RegisterResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.DOCTOR_LOGIN)
    fun doctorLogin(@Field(GlobalVariables.PARAMS.Login.EMAIL) email: String,
                    @Field(GlobalVariables.PARAMS.Login.PASSWORD) password: String) :Observable<RegisterResponse>

    @GET(GlobalVariables.URL.SPECIALIST_LIST)
    fun getSpecialist(@HeaderMap headers: HashMap<String, String>) : Observable<GetSpecialistResponse>

    @GET(GlobalVariables.URL.SERVICE_LIST)
    fun getServiceList(@HeaderMap headers: HashMap<String, String>) : Observable<ServicDataResponse>

    @GET(GlobalVariables.URL.SYMPTOMSLIST)
    fun getSymptomsList(@HeaderMap headers: HashMap<String, String>) : Observable<SymptomsListResponse>

    @GET(GlobalVariables.URL.NOTIFICTION_LIST)
    fun getNotificationList(@HeaderMap headers: HashMap<String, String>) : Observable<NotiicationListResponse>


    @GET(GlobalVariables.URL.SUBSPECIALIST_LIST + "/{id}${GlobalVariables.URL.JSON_FORMAT}")
    fun getSubSpecialist( @HeaderMap headers: HashMap<String, String>,
                       @Path("id") id : String) : Observable<SubSpecilityResponse>

    @GET(GlobalVariables.URL.DOCTOR_LIST)
    fun getDoctorlist(@HeaderMap headers: HashMap<String, String>) : Observable<DocListResponse>

    @GET(GlobalVariables.URL.RECOMMEND_DOCTOR + "/{speciality}${GlobalVariables.URL.JSON_FORMAT}")
    fun getRecomendDocList(@HeaderMap headers: HashMap<String, String>,
                    @Path("speciality") speciality : String) : Observable<DocListResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.SAVE_DOCOTR_PROFILE)
    fun saveDoctorProfile(@HeaderMap headers: HashMap<String, String>,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.NAME) name: String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.COUNTRY) country: String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.STATE) state : String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.CITY) city : String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.SPECIALITY) speciality : String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.GENDER) gender : String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.DEGREE) degree : String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.COLLEGE) college : String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.YEAR) year : String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.REG_NUMBER) regNumber : String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.REG_COUNCIL) regCouncil : String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.REG_YEAR) regYear : String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.EXPERIENCE) experience : String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.SUB_SPECIALITY) sub_specilaity : String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.SERVICES) services : String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.FEE) fee : String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.CURRENCY) currancy_code : String,
                          @Field(GlobalVariables.PARAMS.DoctorProfile.SYMPTOMS) symptoms : String) : Observable<DoctorProfileResponse>

    @POST(GlobalVariables.URL.SAVE_DOCOTR_PROFILE)
    fun saveDoctorProfile(@HeaderMap headers:  HashMap<String, String>,
                          @Body saveDoctorProfileRequest: SaveDoctorProfileRequest) : Observable<DoctorProfileResponse>

    @GET(GlobalVariables.URL.DOCTOR_PROFILE)
    fun getDoctorProfile(@HeaderMap headers: HashMap<String, String>) : Observable<DoctorProfileResponse>

//
//    @POST(GlobalVariables.URL.SAVE_CLINIC)
//    fun saveClinic(@HeaderMap headers: HashMap<String, String>,
//                   @Body request: SaveClinicRequest) : Observable<DoctorProfileResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.SAVE_CLINIC)
    fun saveClinic(@HeaderMap headers: HashMap<String, String>,
                        @Field(GlobalVariables.PARAMS.SaveClinic.CLINIC_NAME) clinic_name: String,
                        @Field(GlobalVariables.PARAMS.SaveClinic.CLINIC_LOCATION) clinic_location: String,
                        @Field(GlobalVariables.PARAMS.SaveClinic.TIMING) timing: String,
                        @Field(GlobalVariables.PARAMS.SaveClinic.TIME) time: String) : Observable<DoctorProfileResponse>


    @Multipart
    @POST(GlobalVariables.URL.SAVE_DOCOTR_PROFILE)
    fun saveDocument(@HeaderMap headers: HashMap<String, String>,
                     @Part idProof: MultipartBody.Part,
                     @Part doc: MultipartBody.Part) : Observable<RestObservable>


    @Multipart
    @POST(GlobalVariables.URL.UPLOAD_REPORT)
    fun uploadReport(@HeaderMap headers: HashMap<String, String>,
                     @Part(GlobalVariables.PARAMS.AddAppointment.TITLE) title: RequestBody,
                     @Part file:MultipartBody.Part) : Observable<UploadReportResponse>


    @FormUrlEncoded
    @POST(GlobalVariables.URL.PRACTICE_PROFILE)
    fun saveDoctorPracticeProfile(@HeaderMap headers: HashMap<String, String>,
                          @Field(GlobalVariables.PARAMS.DoctorPracticeProfile.NAME) name: String,
                          @Field(GlobalVariables.PARAMS.DoctorPracticeProfile.CITY) city : String,
                          @Field(GlobalVariables.PARAMS.DoctorPracticeProfile.SPECIALITY) speciality : String,
                          @Field(GlobalVariables.PARAMS.DoctorPracticeProfile.OWN_PRACTICE) own_practice : String,
                          @Field(GlobalVariables.PARAMS.DoctorPracticeProfile.MOBILE) mobile : String,
                          @Field(GlobalVariables.PARAMS.DoctorPracticeProfile.EMAIL) email : String) : Observable<AddPracticeResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.ADD_PATIENT)
    fun AddPatient(@HeaderMap headers: HashMap<String, String>,
                          @Field(GlobalVariables.PARAMS.AddPatient.NAME) name: String,
                          @Field(GlobalVariables.PARAMS.AddPatient.CITY) city : String,
                          @Field(GlobalVariables.PARAMS.AddPatient.GENDER) gender : String,
                          @Field(GlobalVariables.PARAMS.AddPatient.DOB) dob : String,
                          @Field(GlobalVariables.PARAMS.AddPatient.AGE) age : String,
                          @Field(GlobalVariables.PARAMS.AddPatient.STREET_ADDRESS) street_address : String,
                          @Field(GlobalVariables.PARAMS.AddPatient.LOCALITY) locality : String,
                          @Field(GlobalVariables.PARAMS.AddPatient.PINCODE) pincode : String,
                          @Field(GlobalVariables.PARAMS.AddPatient.MOBILE) mobile : String,
                          @Field(GlobalVariables.PARAMS.AddPatient.EMAIL) email : String) : Observable<AddPatientResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.EDIT_PATIENT_DETAIL)
    fun EditPatientDetails(@HeaderMap headers: HashMap<String, String>,
                          @Field(GlobalVariables.PARAMS.AddPatient.ID) id: String,
                          @Field(GlobalVariables.PARAMS.AddPatient.NAME) name: String,
                          @Field(GlobalVariables.PARAMS.AddPatient.CITY) city : String,
                          @Field(GlobalVariables.PARAMS.AddPatient.GENDER) gender : String,
                          @Field(GlobalVariables.PARAMS.AddPatient.DOB) dob : String,
                          @Field(GlobalVariables.PARAMS.AddPatient.AGE) age : String,
                          @Field(GlobalVariables.PARAMS.AddPatient.STREET_ADDRESS) street_address : String,
                          @Field(GlobalVariables.PARAMS.AddPatient.LOCALITY) locality : String,
                          @Field(GlobalVariables.PARAMS.AddPatient.PINCODE) pincode : String,
                          @Field(GlobalVariables.PARAMS.AddPatient.MOBILE) mobile : String,
                          @Field(GlobalVariables.PARAMS.AddPatient.EMAIL) email : String) : Observable<AddPatientResponse>

    @GET(GlobalVariables.URL.PATIENT_LIST)
    fun getpatientList(@HeaderMap headers: HashMap<String, String>) : Observable<PatientListResponse>

    @GET(GlobalVariables.URL.PATIENT_DETAIL+ "/{id}${GlobalVariables.URL.JSON_FORMAT}")
    fun getpatientDetail(@HeaderMap headers: HashMap<String, String>,
                         @Path("id") id : String) : Observable<PatientProfileResponse>

    @GET(GlobalVariables.URL.DELETE_PATIENT+ "/{id}${GlobalVariables.URL.JSON_FORMAT}")
    fun getDeletePatient(@HeaderMap headers: HashMap<String, String>,
                         @Path("id") id : String) : Observable<DeletePatientResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.CONSULT_REPORT)
    fun getConsultReport(@HeaderMap headers: HashMap<String, String>,
                         @Field(GlobalVariables.PARAMS.Consult_Report.TEXT) text : String) : Observable<ReportResponse>



    @FormUrlEncoded
    @POST(GlobalVariables.URL.ADD_APPOINTMENT)
    fun AddAppointment(@HeaderMap headers: HashMap<String, String>,
                   @Field(GlobalVariables.PARAMS.AddAppointment.USERID) user_id: String,
                   @Field(GlobalVariables.PARAMS.AddAppointment.NAME) name: String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.MOBILE) mobile : String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.EMAIL) email : String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.DATE) date : String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.TIME) time : String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.CATEGORY) category : String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.NOTE) note : String) : Observable<AddAppointmentResponse>



    @GET(GlobalVariables.URL.APPOINTMENT_LIST)
    fun getappointmentList(@HeaderMap headers: HashMap<String, String>) : Observable<AddAppointmentListResponse>



    @GET(GlobalVariables.URL.APPOINTMENT_DETAIL+ "/{id}${GlobalVariables.URL.JSON_FORMAT}")
    fun getapptDetail(@HeaderMap headers: HashMap<String, String>,
                         @Path("id") id : String) : Observable<ApptDetailResponse>

    @GET(GlobalVariables.URL.APP_LIST_ACC_DATE+ "/{date}${GlobalVariables.URL.JSON_FORMAT}")
    fun getapptlistdate(@HeaderMap headers: HashMap<String, String>,
                         @Path("date") date : String) : Observable<AddAppointmentListResponse>


    @FormUrlEncoded
    @POST(GlobalVariables.URL.APPOINTMENT_CANCEL)
    fun cancelAppt(@HeaderMap headers: HashMap<String, String>,
                       @Field(GlobalVariables.PARAMS.AddPatient.ID) id: String) : Observable<ApptCancelResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.APPOINTMENT_APPROVE)
    fun approveAppt(@HeaderMap headers: HashMap<String, String>,
                       @Field(GlobalVariables.PARAMS.AddPatient.ID) id: String,
                       @Field(GlobalVariables.PARAMS.AddPatient.EMAIL) email: String) : Observable<ApproveApptResponse>



    @FormUrlEncoded
    @POST(GlobalVariables.URL.EDIT_APPOINTMENT)
    fun EditAppointment(@HeaderMap headers: HashMap<String, String>,
                       @Field(GlobalVariables.PARAMS.AddAppointment.Id) id: String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.NAME) name: String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.MOBILE) mobile : String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.EMAIL) email : String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.DATE) date : String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.TIME) time : String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.CATEGORY) category : String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.NOTE) note : String) : Observable<EditApptResponse>



    @GET(GlobalVariables.URL.DOCTOR_DETAIL+ "/{id}${GlobalVariables.URL.JSON_FORMAT}")
    fun getDocDetails(@Path("id") id : String) : Observable<DoctorProfileResponse>



    @FormUrlEncoded
    @POST(GlobalVariables.URL.ADD_PT_APPOINTMENT)
    fun AddPtAppointment(@HeaderMap headers: HashMap<String, String>,
                       @Field(GlobalVariables.PARAMS.AddAppointment.DOCTOR_ID) user_id: String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.NAME) name: String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.MOBILE) mobile : String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.EMAIL) email : String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.DATE) date : String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.TIME) time : String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.CATEGORY) category : String,
                       @Field(GlobalVariables.PARAMS.AddAppointment.NOTE) note : String) : Observable<PtAppointmentResponse>



    @GET(GlobalVariables.URL.TIME_SLOT+ "/{id}/{date}${GlobalVariables.URL.JSON_FORMAT}")
    fun getTimeSlot(@HeaderMap headers: HashMap<String, String>,
                    @Path("id") id : String, @Path("date") date: String) : Observable<TimeSlotResponse>


    @FormUrlEncoded
    @POST(GlobalVariables.URL.SAVE_REPORT)
    fun saveReport(@HeaderMap headers: HashMap<String, String>,
                   @Field(GlobalVariables.PARAMS.AddAppointment.RESULT) result111: String,
                   @Field(GlobalVariables.PARAMS.AddAppointment.SYMPTOMS) symptoms: String,
                   @Field(GlobalVariables.PARAMS.AddAppointment.NAME) name: String,
                   @Field(GlobalVariables.PARAMS.AddAppointment.AGE) age: String,
                   @Field(GlobalVariables.PARAMS.AddAppointment.GENDER) gender: String,
                   @Field(GlobalVariables.PARAMS.AddAppointment.WEIGHT) weight: String) : Observable<SaveReportResponse>


    @GET(GlobalVariables.URL.REPORT_LIST)
    fun getaReportList(@HeaderMap headers: HashMap<String, String>) : Observable<ReportListResponse>


    @GET(GlobalVariables.URL.ALLERGY_LIST)
    fun getAllergylist(@HeaderMap headers: HashMap<String, String>) : Observable<AddAllergy_Response>

    @GET(GlobalVariables.URL.INCIDENT_LIST)
    fun getIncidentlist(@HeaderMap headers: HashMap<String, String>) : Observable<AddAllergy_Response>

    @GET(GlobalVariables.URL.SURGERY_LIST)
    fun getSurgerylist(@HeaderMap headers: HashMap<String, String>) : Observable<AddAllergy_Response>

    @GET(GlobalVariables.URL.MEDIC_LIST)
    fun getMediclist(@HeaderMap headers: HashMap<String, String>) : Observable<AddAllergy_Response>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.ADD_SURGERY)
    fun AddSurgery(@HeaderMap headers: HashMap<String, String>,
                         @Field(GlobalVariables.PARAMS.AddAppointment.TITLE) title: String) : Observable<AddSurgeryResponse>
    @FormUrlEncoded
    @POST(GlobalVariables.URL.ADD_ALLERGY)
    fun AddAllergy(@HeaderMap headers: HashMap<String, String>,
                         @Field(GlobalVariables.PARAMS.AddAppointment.TITLE) title: String) : Observable<AddAlrgyResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.ADD_INCIDENT)
    fun AddAIncient(@HeaderMap headers: HashMap<String, String>,
                    @Field(GlobalVariables.PARAMS.AddAppointment.TITLE) title: String) : Observable<AddIncidentResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.ADD_MEDIC)
    fun AddMedic(@HeaderMap headers: HashMap<String, String>,
                    @Field(GlobalVariables.PARAMS.AddAppointment.TITLE) title: String) : Observable<AddMedicResponse>


    @GET(GlobalVariables.URL.UPLOAD_REPORT_LIST)
    fun getUploadReportList(@HeaderMap headers: HashMap<String, String>) : Observable<UploadReportListResponse>


    @FormUrlEncoded
    @POST(GlobalVariables.URL.ADD_PRESCRIPTION)
    fun AddPrescription(@HeaderMap headers: HashMap<String, String>,
                   @Field(GlobalVariables.PARAMS.AddPrescription.ID) user_id: String,
                   @Field(GlobalVariables.PARAMS.AddPrescription.NAME) name: String) : Observable<AddPrescriptionResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.PAYMENT)
    fun paymentResponse(@HeaderMap headers: HashMap<String, String>,
                   @Field(GlobalVariables.PARAMS.Paymnet.APPOINTMENT_ID) appointment_id: String,
                   @Field(GlobalVariables.PARAMS.Paymnet.TRANSACTION_ID) transaction_id: String,
                   @Field(GlobalVariables.PARAMS.Paymnet.DOCTOR_ID) doctor_id: String) : Observable<PaymentResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.ADD_BANK_DETAIL)
    fun addBank(@HeaderMap headers: HashMap<String, String>,
                   @Field(GlobalVariables.PARAMS.AddBank.NAME) appointment_id: String,
                   @Field(GlobalVariables.PARAMS.AddBank.BANK_NAME) bank_name: String,
                   @Field(GlobalVariables.PARAMS.AddBank.ACCOUNT_NUMBER) account_number: String,
                   @Field(GlobalVariables.PARAMS.AddBank.IFSC_CODE) ifsc_code: String) : Observable<AddBankDetailRespons>


    @GET(GlobalVariables.URL.MY_CREDIT)
    fun getCreditData(@HeaderMap headers: HashMap<String, String>) : Observable<MyCreditResponse>

    @GET(GlobalVariables.URL.BANK_LIST)
    fun getBankList(@HeaderMap headers: HashMap<String, String>) : Observable<BankListResponse>


    @GET(GlobalVariables.URL.DELETE_BANK+ "/{id}${GlobalVariables.URL.JSON_FORMAT}")
    fun DeleteBank(@HeaderMap headers: HashMap<String, String>,
                         @Path("id") id : String) : Observable<DeleteBankResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.WITHDRAW_REQUEST)
    fun withdrawRequest(@HeaderMap headers: HashMap<String, String>,
                        @Field(GlobalVariables.PARAMS.withdrawRequest.ID) id: String,
                        @Field(GlobalVariables.PARAMS.withdrawRequest.AMOUNT) amount: String) : Observable<WithdrawRequestResponse>


    @GET(GlobalVariables.URL.REPORT_DETAILS+ "/{id}${GlobalVariables.URL.JSON_FORMAT}")
    fun reportDetail(@HeaderMap headers: HashMap<String, String>,
                   @Path("id") id : String) : Observable<ReportDetailResponse>

    @GET(GlobalVariables.URL.MY_APPOINTMENT_LIST)
    fun getMyAppontmentList(@HeaderMap headers: HashMap<String, String>) : Observable<MyAppointmentResponse>

   @GET(GlobalVariables.URL.CHAT_LIST)
    fun getChatList(@HeaderMap headers: HashMap<String, String>) : Observable<ChatListResponse>

    @GET(GlobalVariables.URL.TIMESLOT)
    fun getTimeSlot(@HeaderMap headers: HashMap<String, String>) : Observable<TimeslotResponse>


    @FormUrlEncoded
    @POST(GlobalVariables.URL.SEND_MSG)
    fun SendMsg(@HeaderMap headers: HashMap<String, String>,
                        @Field(GlobalVariables.PARAMS.SendMsg.SENDER) sender: String,
                        @Field(GlobalVariables.PARAMS.SendMsg.RECEIVER) receiver: String,
                        @Field(GlobalVariables.PARAMS.SendMsg.MESSAGE) message: String,
                        @Field(GlobalVariables.PARAMS.SendMsg.IMAGE) image: String) : Observable<SendMsgResponse>

    @FormUrlEncoded
    @POST(GlobalVariables.URL.SAVE_TOKEN)
    fun SaveToken(@HeaderMap headers: HashMap<String, String>,
                  @Field(GlobalVariables.PARAMS.Login.REGISTRATION_ID) registration_id: String,
                  @Field(GlobalVariables.PARAMS.Login.TYPE) type: String): Observable<DeviceTokenResponse>


    @GET(GlobalVariables.URL.GET_MSG+ "/{sender}/{receiver}${GlobalVariables.URL.JSON_FORMAT}")
    fun getMsg(@HeaderMap headers: HashMap<String, String>,
                    @Path("sender") sender : String, @Path("receiver") receiver: String) : Observable<GetMessageResponse>



    @FormUrlEncoded
    @POST(GlobalVariables.URL.ADD_CITIES)
    fun AddCitiez(@HeaderMap headers: HashMap<String, String>,
                  @Field(GlobalVariables.PARAMS.AddCities.CITY_NAME) city_name: String): Observable<AddCitiesNameResponse>


}