package com.eiadah.app.restAPI

enum class Status {

    LOADING,
    SUCCESS,
    ERROR,
    COMPLETED

}