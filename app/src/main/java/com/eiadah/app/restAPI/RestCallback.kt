package com.eiadah.app.restAPI

import retrofit2.Call
import retrofit2.Response


interface RestCallback {

    fun onResponse(call: Call<*>, response: Response<*>, serviceMode: Int)

    fun onFailure(call: Call<*>, t: Throwable, serviceMode: Int)

}
