package com.eiadah.app.restAPI

import android.content.Context

import java.io.IOException

import com.eiadah.app.utils.AppUtils
import com.eiadah.app.utils.ProgressHUD
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RestProcess<T>(
    internal var context: Context?,
    private val restCallback: RestCallback,
    private val serviceMode: Int,
    isDialogShow: Boolean
) : Callback<T> {

    init {
        if (isDialogShow) {
            progressDialog = ProgressHUD.create(context!!,"Please wait..", true, false, true, null)
            progressDialog!!.show()
        }
    }

    override fun onResponse(call: Call<T>, response: Response<T>) {

        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }

        if (response.code() != 200 && response.code() !=201) {
            AppUtils.showToast(context, "Server Error.")
            return
        }

        restCallback.onResponse(call, response, serviceMode)
    }

    override fun onFailure(call: Call<T>, t: Throwable) {

        t.printStackTrace()

        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
        if (t is IOException) {
            AppUtils.showToast(context, "Unable to connect to server.")
        }
        restCallback.onFailure(call, t, serviceMode)
    }

    companion object {
        var progressDialog: ProgressHUD? = null
    }
}
