package com.eiadah.app.restAPI

import android.util.Log
import com.google.gson.JsonElement
import io.reactivex.annotations.NonNull



class RestObservable(val status: Status, val data: Any?, val error: Throwable?)  {

    companion object {
        fun loading(): RestObservable {
            Log.e("REST", "Loading")
            return RestObservable(Status.LOADING, null, null)
        }

        fun success(@NonNull data: Any): RestObservable {
            Log.e("REST", "Success")
            return RestObservable(Status.SUCCESS, data, null)
        }

        fun error(@NonNull error: Throwable): RestObservable {
            Log.e("REST", "Error")
            return RestObservable(Status.ERROR, null, error)
        }
    }

}