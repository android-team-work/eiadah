package com.eiadah.app.restAPI

import com.eiadah.app.utils.GlobalVariables
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

object CheckupServiceGenertor {

    private val httpClient = OkHttpClient.Builder()
        .readTimeout((3 * 60).toLong(), TimeUnit.SECONDS)
        .connectTimeout((3 * 60).toLong(), TimeUnit.SECONDS)
        .writeTimeout((3 * 60).toLong(), TimeUnit.SECONDS)
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .connectionSpecs(Arrays.asList(ConnectionSpec.MODERN_TLS, ConnectionSpec.CLEARTEXT)).build()

    private val gson = GsonBuilder()
        .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        .create()

    private val builder = Retrofit.Builder()
        .baseUrl(GlobalVariables.URL.CHECKUP_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

    @JvmStatic
    fun <S> createService(serviceClass: Class<S>): S {
        val retrofit = builder.client(httpClient)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setPrettyPrinting().create()))
            .build()
        return retrofit.create(serviceClass)
    }
}