package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class PatientAppData {
    @SerializedName("id")
    @Expose
    private val id: Int? = null
    @SerializedName("user_id")
    @Expose
    private val userId: String? = null
    @SerializedName("name")
    @Expose
    private val name: String? = null
    @SerializedName("phone")
    @Expose
    private val phone: String? = null
    @SerializedName("email")
    @Expose
    private val email: String? = null
    @SerializedName("date")
    @Expose
    private val date: String? = null
    @SerializedName("time")
    @Expose
    private val time: String? = null
    @SerializedName("category")
    @Expose
    private val category: String? = null
    @SerializedName("note")
    @Expose
    private val note: String? = null
    @SerializedName("status")
    @Expose
    private val status: String? = null
}