package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class QuestionData {

    @SerializedName("questions")
    @Expose
    var questions: String? = null
    @SerializedName("count")
    @Expose
    var count: Int? = null
}