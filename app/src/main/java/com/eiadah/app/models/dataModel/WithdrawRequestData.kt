package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class WithdrawRequestData {
    @SerializedName("id")
    @Expose
    private val id: Int? = null
    @SerializedName("amount")
    @Expose
    private val amount: String? = null
    @SerializedName("date")
    @Expose
    private val date: String? = null
    @SerializedName("user_id")
    @Expose
    private val userId: Any? = null

}