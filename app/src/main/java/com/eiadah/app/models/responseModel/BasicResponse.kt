package com.eiadah.app.models.responseModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BasicResponse {

    @SerializedName("status")
    @Expose
    var status : Boolean = false

    @SerializedName("message")
    @Expose
    lateinit var message : String
}