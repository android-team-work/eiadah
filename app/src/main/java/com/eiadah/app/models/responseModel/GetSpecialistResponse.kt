package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.SpecialistData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class GetSpecialistResponse {

    @SerializedName("status")
    @Expose
    var status: Boolean? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: List<SpecialistData>? = null
}