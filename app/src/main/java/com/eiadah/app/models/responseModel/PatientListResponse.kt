package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.PatientListData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class PatientListResponse {
    @SerializedName("status")
    @Expose
    private var status: Boolean? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    @Expose
    private var data: List<PatientListData>? = null

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData(): List<PatientListData>? {
        return data
    }

    fun setData(data: List<PatientListData>) {
        this.data = data
    }


}