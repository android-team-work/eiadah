package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class RegistrationData {

    @SerializedName("reg_number")
    @Expose
    var regNumber: String? = null
    @SerializedName("reg_council")
    @Expose
    var regCouncil: String? = null
    @SerializedName("reg_year")
    @Expose
    var regYear: String? = null
}