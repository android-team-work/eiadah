package com.eiadah.app.models.responseModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class AddSurgeryResponse {
    @SerializedName("status")
    @Expose
     internal var status: Boolean? = null
    @SerializedName("message")
    @Expose
    internal var message: String? = null

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

}