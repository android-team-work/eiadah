package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class MyAppointmentData {
    @SerializedName("name")
    @Expose
    internal val name: String? = null
    @SerializedName("phone")
    @Expose
    internal val phone: String? = null
    @SerializedName("email")
    @Expose
    internal val email: String? = null
    @SerializedName("date")
    @Expose
    internal val date: String? = null
    @SerializedName("time")
    @Expose
    internal val time: String? = null
    @SerializedName("category")
    @Expose
    internal val category: String? = null
    @SerializedName("note")
    @Expose
    internal val note: String? = null
    @SerializedName("status")
    @Expose
    internal val status: String? = null
    @SerializedName("dr_mobile")
    @Expose
    internal val drMobile: String? = null
    @SerializedName("dr_name")
    @Expose
    internal val drName: String? = null

    @SerializedName("doctor_id")
    @Expose
    internal val doctor_id: String? = null

}