package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ServiceListData {
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("description")
    @Expose
     val description: String? = null
    @SerializedName("date")
    @Expose
     val date: String? = null

    override fun toString(): String {
        return this.title!!
    }
}