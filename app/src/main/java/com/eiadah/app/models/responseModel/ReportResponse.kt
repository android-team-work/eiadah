package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.ReportData
import com.eiadah.app.utils.GlobalVariables.User
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ReportResponse {
    @SerializedName("status")
    @Expose
     var status: Boolean? = null
    @SerializedName("user")
    @Expose
     var user: ReportData? = null
    @SerializedName("message")
    @Expose
     var message: String? = null


}