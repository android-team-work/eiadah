package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class NotificationListData {
    @SerializedName("id")
    @Expose
    internal val id: Int? = null
    @SerializedName("name")
    @Expose
    internal val name: String? = null
    @SerializedName("meesage")
    @Expose
    internal val meesage: String? = null
    @SerializedName("date")
    @Expose
    internal val date: String? = null
    @SerializedName("time")
    @Expose
    internal val time: String? = null
    @SerializedName("user_id")
    @Expose
    internal val userId: Int? = null

}