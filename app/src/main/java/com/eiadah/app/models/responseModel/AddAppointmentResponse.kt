package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.AddAppointmentData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.eiadah.app.utils.GlobalVariables.User



class AddAppointmentResponse {
    @SerializedName("status")
    @Expose
    internal val status: Boolean? = null
    @SerializedName("user")
    @Expose
    private val user: AddAppointmentData? = null
    @SerializedName("message")
    @Expose
    private val message: String? = null
}