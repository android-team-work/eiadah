package com.eiadah.app.models.requestModels

class RegisterRequest {

    lateinit var username : String
    lateinit var email : String
    lateinit var password1 : String
    lateinit var password2 : String
}