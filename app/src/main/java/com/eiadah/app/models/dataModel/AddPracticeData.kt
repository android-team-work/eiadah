package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class AddPracticeData {
    @SerializedName("id")
    @Expose
    private var id: Int? = null
    @SerializedName("name")
    @Expose
    private var name: String? = null
    @SerializedName("city")
    @Expose
    private var city: String? = null
    @SerializedName("speciality")
    @Expose
    private var speciality: String? = null
    @SerializedName("mobilenumber")
    @Expose
    private var mobilenumber: String? = null
    @SerializedName("own_practice")
    @Expose
    private var ownPractice: String? = null
    @SerializedName("email")
    @Expose
    private var email: String? = null
    @SerializedName("date")
    @Expose
    private var date: String? = null
    @SerializedName("doctor_id")
    @Expose
    private var doctorId: Int? = null

    fun getId(): Int? {
        return id
    }

    fun setId(id: Int?) {
        this.id = id
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getCity(): String? {
        return city
    }

    fun setCity(city: String) {
        this.city = city
    }

    fun getSpeciality(): String? {
        return speciality
    }

    fun setSpeciality(speciality: String) {
        this.speciality = speciality
    }

    fun getMobilenumber(): String? {
        return mobilenumber
    }

    fun setMobilenumber(mobilenumber: String) {
        this.mobilenumber = mobilenumber
    }

    fun getOwnPractice(): String? {
        return ownPractice
    }

    fun setOwnPractice(ownPractice: String) {
        this.ownPractice = ownPractice
    }

    fun getEmail(): String? {
        return email
    }

    fun setEmail(email: String) {
        this.email = email
    }

    fun getDate(): String? {
        return date
    }

    fun setDate(date: String) {
        this.date = date
    }

    fun getDoctorId(): Int? {
        return doctorId
    }

    fun setDoctorId(doctorId: Int?) {
        this.doctorId = doctorId
    }

}