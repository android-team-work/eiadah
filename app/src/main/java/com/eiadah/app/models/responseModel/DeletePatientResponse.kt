package com.eiadah.app.models.responseModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class DeletePatientResponse {
    @SerializedName("status")
    @Expose
    var status: Boolean? = null
    @SerializedName("message")
    @Expose
     var message: String? = null


}