package com.eiadah.app.models.requestModels

class SaveDoctorProfileRequest {

    var name = ""
    var mobile = ""
    var country = ""
    var state = ""
    var city = ""
    var specilaity = ""
    var gender = ""
    var experience = ""
    var reg_number = ""
    var reg_council = ""
    var reg_year = ""
    var degree = ""
    var college = ""
    var year = ""
    var own_practice=""
    var email=""
    var specilaity_id=""
    var sub_specilaity=""
    var services=""
    var symptoms=""
    var fee=""
    var currancy_code=""

}