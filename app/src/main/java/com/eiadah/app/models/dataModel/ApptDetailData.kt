package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ApptDetailData {

    @SerializedName("id")
    @Expose
    private var id: Int? = null
    @SerializedName("name")
    @Expose
    private var name: String? = null
    @SerializedName("phone")
    @Expose
    private var phone: String? = null
    @SerializedName("email")
    @Expose
    private var email: String? = null
    @SerializedName("date")
    @Expose
    private var date: String? = null
    @SerializedName("time")
    @Expose
    private var time: String? = null
    @SerializedName("category")
    @Expose
    private var category: String? = null
    @SerializedName("note")
    @Expose
    private var note: String? = null
    @SerializedName("status")
    @Expose
    private var status: String? = null
    @SerializedName("doctor_id")
    @Expose
    private var doctorId: Int? = null
    @SerializedName("user_id")
    @Expose
    private var userId: Int? = null

    fun getId(): Int? {
        return id
    }

    fun setId(id: Int?) {
        this.id = id
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getPhone(): String? {
        return phone
    }

    fun setPhone(phone: String) {
        this.phone = phone
    }

    fun getEmail(): String? {
        return email
    }

    fun setEmail(email: String) {
        this.email = email
    }

    fun getDate(): String? {
        return date
    }

    fun setDate(date: String) {
        this.date = date
    }

    fun getTime(): String? {
        return time
    }

    fun setTime(time: String) {
        this.time = time
    }

    fun getCategory(): String? {
        return category
    }

    fun setCategory(category: String) {
        this.category = category
    }

    fun getNote(): String? {
        return note
    }

    fun setNote(note: String) {
        this.note = note
    }

    fun getStatus(): String? {
        return status
    }

    fun setStatus(status: String) {
        this.status = status
    }

    fun getDoctorId(): Int? {
        return doctorId
    }

    fun setDoctorId(doctorId: Int?) {
        this.doctorId = doctorId
    }

    fun getUserId(): Int? {
        return userId
    }

    fun setUserId(userId: Int?) {
        this.userId = userId
    }

}