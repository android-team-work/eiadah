package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.DocListData
import com.eiadah.app.models.dataModel.PatientAppData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class DocListResponse {
    @SerializedName("status")
    @Expose
    private var status: Boolean? = null
    @SerializedName("user")
    @Expose
    private var user: User? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getUser(): User? {
        return user
    }

    fun setUser(user: User) {
        this.user = user
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    inner class User{
        @SerializedName("doctor")
        @Expose
        private var doctor: List<DocListData>? = null

        fun getDoctor(): List<DocListData>? {
            return doctor
        }

        fun setDoctor(doctor: List<DocListData>) {
            this.doctor = doctor
        }
    }


}