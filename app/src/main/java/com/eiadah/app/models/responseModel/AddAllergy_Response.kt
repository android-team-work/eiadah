package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.AddAllergyData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class AddAllergy_Response {
    @SerializedName("status")
    @Expose
    internal var status: Boolean? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    @Expose
    internal var data: List<AddAllergyData>? = null


}