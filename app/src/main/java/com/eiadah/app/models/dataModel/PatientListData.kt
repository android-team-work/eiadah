package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PatientListData {
    @SerializedName("id")
    @Expose
    var id:Integer ?=null
    @SerializedName("name")
    @Expose
    var name:String?=null ;
    @SerializedName("mobile_number")
    @Expose
    var mobileNumber:String?=null ;
    @SerializedName("secondry_mobile_number")
    @Expose
    var secondryMobileNumber:String?=null ;
    @SerializedName("city")
    @Expose
    var city:String?=null ;
    @SerializedName("gender")
    @Expose
    var gender:String?=null ;
    @SerializedName("dob")
    @Expose
    var  dob:String?=null ;
    @SerializedName("age")
    @Expose
    var age:String?=null ;
    @SerializedName("blood_group")
    @Expose
    var bloodGroup:String?=null ;
    @SerializedName("street_address")
    @Expose
    var  streetAddress:String?=null ;
    @SerializedName("locality")
    @Expose
    var locality:String?=null ;
    @SerializedName("pincode")
    @Expose
    var pincode:String?=null ;
    @SerializedName("email")
    @Expose
    var email:String?=null ;
    @SerializedName("date")
    @Expose
    var date:String?=null ;
    @SerializedName("doctor_id")
    @Expose
    var doctorId:Integer?=null ;


    override fun toString(): String {
        return this.name!!
    }



}