package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class AppontmentListData {

    @SerializedName("id")
    @Expose
     val id: Int? = null
    @SerializedName("name")
    @Expose
     val name: String? = null
    @SerializedName("phone")
    @Expose
     val phone: String? = null
    @SerializedName("email")
    @Expose
     val email: String? = null
    @SerializedName("date")
    @Expose
     val date: String? = null
    @SerializedName("time")
    @Expose
     val time: String? = null
    @SerializedName("category")
    @Expose
     val category: String? = null
    @SerializedName("note")
    @Expose
     val note: String? = null
    @SerializedName("status")
    @Expose
     val status: String? = null
    @SerializedName("doctor_id")
    @Expose
     val doctorId: Int? = null
    @SerializedName("user_id")
    @Expose
     val userId: Int? = null

}