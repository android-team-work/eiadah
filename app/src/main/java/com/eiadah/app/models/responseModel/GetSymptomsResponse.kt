package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.BodyPartData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class GetSymptomsResponse {

    @SerializedName("status")
    @Expose
    var status: Boolean? = null
    @SerializedName("id_list")
    @Expose
    var bodyPartList: List<BodyPartData>? = null
}