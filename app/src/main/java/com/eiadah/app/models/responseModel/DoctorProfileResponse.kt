package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.DoctorProfileData
import com.eiadah.app.models.dataModel.EducationData
import com.eiadah.app.models.dataModel.RegistrationData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DoctorProfileResponse {


    @SerializedName("status")
    @Expose
    val status: Boolean? = null
    @SerializedName("user")
    @Expose
    val data: DoctorProfileData? = null
    @SerializedName("message")
    @Expose
   val message: String? = null

}