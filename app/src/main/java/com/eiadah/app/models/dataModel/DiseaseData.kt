package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class DiseaseData {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("questions")
    @Expose
    var questions: ArrayList<QuestionData>? = null
}