package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ChatListData {
    @SerializedName("receiver")
    @Expose
    internal val receiver: Int? = null
    @SerializedName("name")
    @Expose
    internal val name: String? = null
    @SerializedName("image")
    @Expose
    internal val image: String? = null

}