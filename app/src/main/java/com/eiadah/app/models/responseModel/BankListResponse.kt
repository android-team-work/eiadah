package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.BankListData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class BankListResponse {
    @SerializedName("status")
    @Expose
    private var status: Boolean? = null
    @SerializedName("user")
    @Expose
    private var user: List<BankListData>? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getUser(): List<BankListData>? {
        return user
    }

    fun setUser(user: List<BankListData>) {
        this.user = user
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

}