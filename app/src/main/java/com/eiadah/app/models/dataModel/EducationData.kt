package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class EducationData {


    @SerializedName("degree")
    @Expose
    var degree: String? = null
    @SerializedName("college")
    @Expose
    var college: String? = null
    @SerializedName("year")
    @Expose
    var year: String? = null
}