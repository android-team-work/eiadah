package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.ApproveApptData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ApproveApptResponse {
    @SerializedName("status")
    @Expose
    internal var status: Boolean? = null
    @SerializedName("user")
    @Expose
    private var user: ApproveApptData? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getUser(): ApproveApptData? {
        return user
    }

    fun setUser(user: ApproveApptData) {
        this.user = user
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

}