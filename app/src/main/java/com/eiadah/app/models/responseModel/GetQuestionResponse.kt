package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.DiseaseData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class GetQuestionResponse {

    @SerializedName("status")
    @Expose
    var status: Boolean? = null
    @SerializedName("id_list")
    @Expose
    var diseaseList: ArrayList<DiseaseData>? = null
    @SerializedName("list_count")
    @Expose
    var listCount: Int? = null
}