package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ReportData {

    @SerializedName("id")
    @Expose
    private var id: Int? = null
    @SerializedName("text")
    @Expose
    private var text: String? = null
    @SerializedName("date")
    @Expose
    private var date: String? = null
    @SerializedName("doctor_id")
    @Expose
    private var doctorId: Int? = null

    fun getId(): Int? {
        return id
    }

    fun setId(id: Int?) {
        this.id = id
    }

    fun getText(): String? {
        return text
    }

    fun setText(text: String) {
        this.text = text
    }

    fun getDate(): String? {
        return date
    }

    fun setDate(date: String) {
        this.date = date
    }

    fun getDoctorId(): Int? {
        return doctorId
    }

    fun setDoctorId(doctorId: Int?) {
        this.doctorId = doctorId
    }


}