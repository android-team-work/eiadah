package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.StateData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class StatesResponse {

    @SerializedName("status")
    @Expose
    var status: Boolean? = null
    @SerializedName("state_list")
    @Expose
    var stateList: List<StateData>? = null
    @SerializedName("message")
    @Expose
    var message: String? = null

}