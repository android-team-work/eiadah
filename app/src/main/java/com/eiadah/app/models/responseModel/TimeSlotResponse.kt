package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.TimeSlotData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class TimeSlotResponse {

    @SerializedName("status")
    @Expose
    internal var status: Boolean? = null
    @SerializedName("data")
    @Expose
    var list: List<TimeSlotData>? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

}