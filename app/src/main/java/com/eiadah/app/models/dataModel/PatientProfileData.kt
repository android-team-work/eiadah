package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class PatientProfileData {

    @SerializedName("name")
    @Expose
     val name: String? = null
    @SerializedName("phone")
    @Expose
     val phone: String? = null
    @SerializedName("secondry_mobile_number")
    @Expose
     val secondryMobileNumber: String? = null
    @SerializedName("city")
    @Expose
     val city: String? = null
    @SerializedName("gender")
    @Expose
     val gender: String? = null
    @SerializedName("dob")
    @Expose
     val dob: String? = null
    @SerializedName("age")
    @Expose
     val age: String? = null
    @SerializedName("blood_group")
    @Expose
     val bloodGroup: String? = null
    @SerializedName("street_address")
    @Expose
     val streetAddress: String? = null
    @SerializedName("locality")
    @Expose
     val locality: String? = null
    @SerializedName("pincode")
    @Expose
     val pincode: String? = null
    @SerializedName("email")
    @Expose
     val email: String? = null
    @SerializedName("Appointment_data")
    @Expose
     val appointmentData: List<PatientAppData>? = null


}