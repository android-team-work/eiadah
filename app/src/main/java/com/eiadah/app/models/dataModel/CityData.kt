package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class CityData {

    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("state_id")
    @Expose
    var stateId: Int? = null
    @SerializedName("country_id")
    @Expose
    var countryId: Int? = null

    override fun toString(): String {
        return name!!
    }
}