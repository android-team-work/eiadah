package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class BankListData {

    @SerializedName("id")
    @Expose
    internal val id: Int? = null
    @SerializedName("name")
    @Expose
    internal val name: String? = null
    @SerializedName("account_number")
    @Expose
    internal val accountNumber: String? = null
    @SerializedName("bank_name")
    @Expose
    internal val bankName: String? = null
    @SerializedName("ifsc_code")
    @Expose
    internal val ifscCode: String? = null
    @SerializedName("date")
    @Expose
    internal val date: String? = null
    @SerializedName("user_id")
    @Expose
    internal val userId: Int? = null

}