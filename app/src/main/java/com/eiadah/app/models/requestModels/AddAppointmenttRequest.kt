package com.eiadah.app.models.requestModels

class AddAppointmenttRequest {
    var name = ""
    var mobile = ""
    var category = ""
    var note = ""
    var time = ""
    var date = ""
    var user_id = ""
    var email=""
    var id=""
    var doctor_id=""
    var appointment_id=""
    var transaction_id=""

}