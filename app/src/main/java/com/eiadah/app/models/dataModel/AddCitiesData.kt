package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class AddCitiesData {
    @SerializedName("city_name")
    @Expose
    internal var cityName: String? = null

    fun getCityName(): String? {
        return cityName
    }

    fun setCityName(cityName: String) {
        this.cityName = cityName
    }

}