package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class BodyPartData {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("symptoms_type_name")
    @Expose
    var symptomsTypeName: String? = null
    @SerializedName("symptoms")
    @Expose
    var symptoms: List<SymptomData>? = null
}