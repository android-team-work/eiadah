package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ReportDetailData {
    @SerializedName("id")
    @Expose
    internal val id: Int? = null
    @SerializedName("name")
    @Expose
    internal val name: String? = null
    @SerializedName("symptoms")
    @Expose
    internal val symptoms: String? = null
    @SerializedName("age")
    @Expose
    internal val age: String? = null
    @SerializedName("weight")
    @Expose
    internal val weight: String? = null
    @SerializedName("result")
    @Expose
    internal val result: String? = null
    @SerializedName("date")
    @Expose
    internal val date: String? = null
    @SerializedName("user_id")
    @Expose
    internal val userId: Int? = null
    @SerializedName("gender")
    @Expose
    internal val gender: String? = null

}