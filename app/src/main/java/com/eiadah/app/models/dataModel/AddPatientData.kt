package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class AddPatientData {

    @SerializedName("id")
    @Expose
    private var id: Int? = null
    @SerializedName("name")
    @Expose
    private var name: String? = null
    @SerializedName("mobile_number")
    @Expose
    private var mobileNumber: String? = null
    @SerializedName("secondry_mobile_number")
    @Expose
    private var secondryMobileNumber: String? = null
    @SerializedName("city")
    @Expose
    private var city: String? = null
    @SerializedName("gender")
    @Expose
    private var gender: String? = null
    @SerializedName("dob")
    @Expose
    private var dob: String? = null
    @SerializedName("age")
    @Expose
    private var age: String? = null
    @SerializedName("blood_group")
    @Expose
    private var bloodGroup: String? = null
    @SerializedName("street_address")
    @Expose
    private var streetAddress: String? = null
    @SerializedName("locality")
    @Expose
    private var locality: String? = null
    @SerializedName("pincode")
    @Expose
    private var pincode: String? = null
    @SerializedName("email")
    @Expose
    private var email: String? = null
    @SerializedName("date")
    @Expose
    private var date: String? = null
    @SerializedName("doctor_id")
    @Expose
    private var doctorId: Int? = null

    fun getId(): Int? {
        return id
    }

    fun setId(id: Int?) {
        this.id = id
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getMobileNumber(): String? {
        return mobileNumber
    }

    fun setMobileNumber(mobileNumber: String) {
        this.mobileNumber = mobileNumber
    }

    fun getSecondryMobileNumber(): String? {
        return secondryMobileNumber
    }

    fun setSecondryMobileNumber(secondryMobileNumber: String) {
        this.secondryMobileNumber = secondryMobileNumber
    }

    fun getCity(): String? {
        return city
    }

    fun setCity(city: String) {
        this.city = city
    }

    fun getGender(): String? {
        return gender
    }

    fun setGender(gender: String) {
        this.gender = gender
    }

    fun getDob(): String? {
        return dob
    }

    fun setDob(dob: String) {
        this.dob = dob
    }

    fun getAge(): String? {
        return age
    }

    fun setAge(age: String) {
        this.age = age
    }

    fun getBloodGroup(): String? {
        return bloodGroup
    }

    fun setBloodGroup(bloodGroup: String) {
        this.bloodGroup = bloodGroup
    }

    fun getStreetAddress(): String? {
        return streetAddress
    }

    fun setStreetAddress(streetAddress: String) {
        this.streetAddress = streetAddress
    }

    fun getLocality(): String? {
        return locality
    }

    fun setLocality(locality: String) {
        this.locality = locality
    }

    fun getPincode(): String? {
        return pincode
    }

    fun setPincode(pincode: String) {
        this.pincode = pincode
    }

    fun getEmail(): String? {
        return email
    }

    fun setEmail(email: String) {
        this.email = email
    }

    fun getDate(): String? {
        return date
    }

    fun setDate(date: String) {
        this.date = date
    }

    fun getDoctorId(): Int? {
        return doctorId
    }

    fun setDoctorId(doctorId: Int?) {
        this.doctorId = doctorId
    }

}