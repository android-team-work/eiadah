package com.eiadah.app.models.responseModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RegisterResponse {

    @SerializedName("key")
    @Expose
    lateinit var token : String
}