package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.AddPatientData
import com.eiadah.app.utils.GlobalVariables.User
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class AddPatientResponse {
    @SerializedName("status")
    @Expose
    internal var status: Boolean? = null
    @SerializedName("user")
    @Expose
    private var user: AddPatientData? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getUser(): AddPatientData? {
        return user
    }

    fun setUser(user: AddPatientData) {
        this.user = user
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

}