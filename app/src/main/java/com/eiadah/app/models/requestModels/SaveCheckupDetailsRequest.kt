package com.eiadah.app.models.requestModels

class SaveCheckupDetailsRequest {

    var first_name : String = ""
    var age : String = ""
    var gender : String = ""
    var country : String = ""
    var state : String = ""
    var city : String = ""
    var weight : String = ""
    var height_feet : String = ""
    var body_mass_index : String = ""
    var results : String = ""
    var your_symptoms : String = ""
    var Recently_Visited_Locations : String = ""


}