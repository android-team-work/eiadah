package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class DoctorProfileData:Serializable {


    @SerializedName("doctor")
    @Expose
    var doctor: Doctor? = null
    @SerializedName("clinic")
    @Expose
    var clinic: Clinic? = null

    inner class Doctor:Serializable{

        @SerializedName("user_id")
        @Expose
        var userId: Int? = null
        @SerializedName("username")
        @Expose
        var username: String? = null
        @SerializedName("emailId")
        @Expose
        var emailId: String? = null
        @SerializedName("mobile")
        @Expose
        var mobile: String? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("country")
        @Expose
        var country: String? = null
        @SerializedName("state")
        @Expose
        var state: String? = null
        @SerializedName("city")
        @Expose
        var city: String? = null
        @SerializedName("specilaity")
        @Expose
        var specilaity: String? = null
        @SerializedName("gender")
        @Expose
        var gender: String? = null
        @SerializedName("experience")
        @Expose
        var experience: String? = null
        @SerializedName("registration_image")
        @Expose
        var registrationImage: String? = null
        @SerializedName("id_proof")
        @Expose
        var idProof: String? = null
        @SerializedName("docter_image")
        @Expose
        var docterImage: String? = null
        @SerializedName("degree")
        @Expose
        var degree: String? = null
        @SerializedName("college")
        @Expose
        var college: String? = null
        @SerializedName("year")
        @Expose
        var year: String? = null
        @SerializedName("reg_number")
        @Expose
        var regNumber: String? = null
        @SerializedName("reg_council")
        @Expose
        var regCouncil: String? = null
        @SerializedName("reg_year")
        @Expose
        var regYear: String? = null
        @SerializedName("verify")
        @Expose
        var verify: String? = null
        @SerializedName("specilaity_id")
        @Expose
        var specilaity_id: String? = null
        @SerializedName("fee")
        @Expose
        var fee: String? = null
        @SerializedName("sub_specilaity")
        @Expose
        var sub_specilaity: String? = null
        @SerializedName("services")
        @Expose
        var services: String? = null
        @SerializedName("symptoms")
        @Expose
        var symptoms: String? = null
        @SerializedName("currancy_code")
        @Expose
        var currancy_code: String? = null

    }

    inner class Clinic :Serializable{

        @SerializedName("clinic_name")
        @Expose
        var clinicName: String? = null
        @SerializedName("clinic_location")
        @Expose
        var clinicLocation: String? = null
        @SerializedName("clinic_image")
        @Expose
        var clinicImage: String? = null
        @SerializedName("timing")
        @Expose
        var timing: ArrayList<ClinicTiming>? = null

    }
}