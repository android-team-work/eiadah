package com.eiadah.app.models.responseModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class UploadReportResponse {
    @SerializedName("status")
    @Expose
    internal var status: Boolean? = null
    @SerializedName("message")
    @Expose
    internal var message: String? = null


}