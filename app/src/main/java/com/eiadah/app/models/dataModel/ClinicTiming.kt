package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ClinicTiming:Serializable {

    @SerializedName("day")
    @Expose
    var day: String? = null
    @SerializedName("open_hours")
    @Expose
    var openHours: String? = null
    @SerializedName("close_hours")
    @Expose
    var closeHours: String? = null
    @SerializedName("closed")
    @Expose
    var close: String? = null
}