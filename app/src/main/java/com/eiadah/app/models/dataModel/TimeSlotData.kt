package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class TimeSlotData {
    @SerializedName("id")
    @Expose
     val id: Int? = null
    @SerializedName("day")
    @Expose
     val day: String? = null
    @SerializedName("date")
    @Expose
     val date: String? = null
    @SerializedName("time_slot")
    @Expose
     val timeSlot: String? = null
    @SerializedName("status")
    @Expose
     val status: String? = null
    @SerializedName("user_id")
    @Expose
     val userId: Int? = null
    @SerializedName("clinic_id")
    @Expose
    private val clinicId: Int? = null
}