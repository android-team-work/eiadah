package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.SendMsgData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class SendMsgResponse {
    @SerializedName("data")
    @Expose
    private var data: SendMsgData? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("status")
    @Expose
    internal var status: Boolean? = null

    fun getData(): SendMsgData? {
        return data
    }

    fun setData(data: SendMsgData) {
        this.data = data
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }
}