package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.SendMsgData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class GetMessageResponse {
    @SerializedName("status")
    @Expose
    internal var status: Boolean? = null
    @SerializedName("message")
    @Expose
    internal var message: String? = null
    @SerializedName("data")
    @Expose
    internal var data: List<SendMsgData>? = null

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData(): List<SendMsgData>? {
        return data
    }

    fun setData(data: List<SendMsgData>) {
        this.data = data
    }

}