package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class GetResultData:Serializable {
    @SerializedName("name")
    @Expose
     val name: String? = null
    @SerializedName("value")
    @Expose
     val value: String? = null


}