package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.Appointment
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class PatientProfileResponse {
    @SerializedName("status")
    @Expose
    internal var status: Boolean? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    @Expose
    internal var data: Data? = null

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData(): Data? {
        return data
    }

    fun setData(data: Data) {
        this.data = data
    }
    inner class Data{
        @SerializedName("name")
        @Expose
         val name: String? = null
        @SerializedName("phone")
        @Expose
         val phone: String? = null
        @SerializedName("secondry_mobile_number")
        @Expose
         val secondryMobileNumber: String? = null
        @SerializedName("city")
        @Expose
         val city: String? = null
        @SerializedName("gender")
        @Expose
         val gender: String? = null
        @SerializedName("dob")
        @Expose
         val dob: String? = null
        @SerializedName("age")
        @Expose
         val age: String? = null
        @SerializedName("blood_group")
        @Expose
         val bloodGroup: String? = null
        @SerializedName("street_address")
        @Expose
         val streetAddress: String? = null
        @SerializedName("locality")
        @Expose
         val locality: String? = null
        @SerializedName("pincode")
        @Expose
         val pincode: String? = null
        @SerializedName("email")
        @Expose
         val email: String? = null
        @SerializedName("Appointment_data")
        @Expose
         val appointmentData: AppointmentData? = null
    }

    inner class AppointmentData{
        @SerializedName("Appointment")
        @Expose
        private var appointment: List<Appointment>? = null

        fun getAppointment(): List<Appointment>? {
            return appointment
        }

        fun setAppointment(appointment: List<Appointment>) {
            this.appointment = appointment
        }

    }
}