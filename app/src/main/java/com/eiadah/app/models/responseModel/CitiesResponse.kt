package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.CityData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class CitiesResponse {
    @SerializedName("status")
    @Expose
    var status: Boolean? = null
    @SerializedName("city_list")
    @Expose
    var cityList: List<CityData>? = null
    @SerializedName("message")
    @Expose
    var message: String? = null


}