package com.eiadah.app.models.dataModel

import com.eiadah.app.models.dataModel.DoctorProfileData.Doctor
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class DocListData {

        @SerializedName("user_id")
        @Expose
         val userId: Int? = null
        @SerializedName("mobile")
        @Expose
         val mobile: String? = null
        @SerializedName("name")
        @Expose
         val name: String? = null
        @SerializedName("country")
        @Expose
         val country: String? = null
        @SerializedName("state")
        @Expose
         val state: String? = null
        @SerializedName("city")
        @Expose
         val city: String? = null
        @SerializedName("specilaity")
        @Expose
         val specilaity: String? = null
        @SerializedName("gender")
        @Expose
         val gender: String? = null
        @SerializedName("experience")
        @Expose
         val experience: String? = null
        @SerializedName("registration_image")
        @Expose
         val registrationImage: String? = null
        @SerializedName("id_proof")
        @Expose
         val idProof: String? = null
        @SerializedName("docter_image")
        @Expose
         val docterImage: String? = null
        @SerializedName("clinic_name")
        @Expose
         val clinicName: String? = null
        @SerializedName("clinic_location")
        @Expose
         val clinicLocation: String? = null
        @SerializedName("specilaity_id")
        @Expose
        var specilaity_id: String? = null
        @SerializedName("fee")
        @Expose
        var fee: String? = null
        @SerializedName("sub_specilaity")
        @Expose
        var sub_specilaity: String? = null
        @SerializedName("services")
        @Expose
        var services: String? = null
        @SerializedName("symptoms")
        @Expose
        var symptoms: String? = null
        @SerializedName("currancy_code")
        @Expose
        var currancy_code: String? = null
    }
