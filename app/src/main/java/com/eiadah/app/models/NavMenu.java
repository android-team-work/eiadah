package com.eiadah.app.models;

import com.eiadah.app.R;

public enum NavMenu {
    START_CHECKUP(R.string.start_checkup),
    PREVIOUS_REPORTS(R.string.previous_reports),
    MY_ACCOUNT(R.string.my_account),
    NOTIFICATIONS(R.string.notifications),
    MY_APPOINTMENT(R.string.my_appointment),
//    HELP(R.string.help),
    LOGOUT(R.string.sign_out);

    private int stringId;

    NavMenu(int stringId) {
        this.stringId = stringId;
    }

    public int getStringId() {
        return stringId;
    }

}
