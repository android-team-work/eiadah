package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class SympotomsListData {
    @SerializedName("id")
    @Expose
     val id: Int? = null
    @SerializedName("name")
    @Expose
     val name: String? = null
    @SerializedName("date")
    @Expose
     val date: String? = null

}