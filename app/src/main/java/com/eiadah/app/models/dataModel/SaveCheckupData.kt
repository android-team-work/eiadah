package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class SaveCheckupData {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("user_id")
    @Expose
    var userId: Any? = null
    @SerializedName("first_name")
    @Expose
    var firstName: String? = null
    @SerializedName("last_name")
    @Expose
    var lastName: Any? = null
    @SerializedName("phone")
    @Expose
    var phone: Any? = null
    @SerializedName("address")
    @Expose
    var address: Any? = null
    @SerializedName("occupation")
    @Expose
    var occupation: Any? = null
    @SerializedName("myfile")
    @Expose
    var myfile: Any? = null
    @SerializedName("blood_group")
    @Expose
    var bloodGroup: Any? = null
    @SerializedName("state")
    @Expose
    var state: String? = null
    @SerializedName("city")
    @Expose
    var city: String? = null
    @SerializedName("weight")
    @Expose
    var weight: String? = null
    @SerializedName("height_cm")
    @Expose
    var heightCm: Any? = null
    @SerializedName("height_feet")
    @Expose
    var heightFeet: String? = null
    @SerializedName("age")
    @Expose
    var age: String? = null
    @SerializedName("gender")
    @Expose
    var gender: String? = null
    @SerializedName("body_mass_index")
    @Expose
    var bodyMassIndex: String? = null
    @SerializedName("results")
    @Expose
    var results: String? = null
    @SerializedName("your_symptoms")
    @Expose
    var yourSymptoms: String? = null
    @SerializedName("Recently_Visited_Locations")
    @Expose
    var recentlyVisitedLocations: String? = null
    @SerializedName("auth_user_id")
    @Expose
    var authUserId: Any? = null
}