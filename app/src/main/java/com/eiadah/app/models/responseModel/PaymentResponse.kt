package com.eiadah.app.models.responseModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class PaymentResponse {

    @SerializedName("status")
    @Expose
    internal val status: Boolean? = null
    @SerializedName("message")
    @Expose
    private val message: String? = null
}