package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ReportListData {

    @SerializedName("id")
    @Expose
    internal val id: Int? = null
    @SerializedName("name")
    @Expose
    internal val name: String? = null
    @SerializedName("value")
    @Expose
    val value: String? = null
    @SerializedName("date")
    @Expose
    val date: String? = null
    @SerializedName("user_id")
    @Expose
    private val userId: Int? = null

}