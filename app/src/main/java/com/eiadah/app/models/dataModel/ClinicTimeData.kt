package com.eiadah.app.models.dataModel

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose
import java.io.Serializable


class ClinicTimeData:Serializable{

    @SerializedName("day")
    @Expose
    var day: String? = null
    @SerializedName("open_hours")
    @Expose
    var openHours: String? = null
    @SerializedName("close_hours")
    @Expose
    var closeHours: String? = null
    @SerializedName("breaks_hours")
    @Expose
    var breaksHours: BreakHours = BreakHours()
    @SerializedName("closed")
    @Expose
    var close: String? = null

//    var isFill = false


    constructor(day : String, openHours : String, closedHours : String, closed : String) {
        this.day = day
        this.openHours = openHours
        this.closeHours = closedHours
        this.close = closed
    }

    constructor(day : String, openHours : String, closedHours : String) {
        this.day = day
        this.openHours = openHours
        this.closeHours = closedHours
//        this.isFill = true
    }

    inner class BreakHours {

        @SerializedName("start_hours")
        @Expose
        var startHours: String = "13:00"
        @SerializedName("end_hour")
        @Expose
        var endHour: String = "15:00"
    }
}