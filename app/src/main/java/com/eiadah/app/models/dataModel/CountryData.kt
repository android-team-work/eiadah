package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class CountryData {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("iso3")
    @Expose
    var iso3: String? = null
    @SerializedName("iso2")
    @Expose
    var iso2: String? = null
    @SerializedName("phone_code")
    @Expose
    var phoneCode: String? = null
    @SerializedName("capital")
    @Expose
    var capital: String? = null
    @SerializedName("currency")
    @Expose
    var currency: String? = null

    override fun toString(): String {
        return name.toString()
    }
}