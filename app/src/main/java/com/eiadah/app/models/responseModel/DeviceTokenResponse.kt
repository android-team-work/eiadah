package com.eiadah.app.models.responseModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class DeviceTokenResponse {
    @SerializedName("id")
    @Expose
    internal var id: Int? = null
    @SerializedName("name")
    @Expose
    internal var name: Any? = null
    @SerializedName("registration_id")
    @Expose
    internal var registrationId: String? = null
    @SerializedName("device_id")
    @Expose
    internal var deviceId: Any? = null
    @SerializedName("active")
    @Expose
    internal var active: Boolean? = null
    @SerializedName("date_created")
    @Expose
    internal var dateCreated: String? = null
    @SerializedName("type")
    @Expose
    internal var type: String? = null

    fun getId(): Int? {
        return id
    }

    fun setId(id: Int?) {
        this.id = id
    }

    fun getName(): Any? {
        return name
    }

    fun setName(name: Any) {
        this.name = name
    }

    fun getRegistrationId(): String? {
        return registrationId
    }

    fun setRegistrationId(registrationId: String) {
        this.registrationId = registrationId
    }

    fun getDeviceId(): Any? {
        return deviceId
    }

    fun setDeviceId(deviceId: Any) {
        this.deviceId = deviceId
    }

    fun getActive(): Boolean? {
        return active
    }

    fun setActive(active: Boolean?) {
        this.active = active
    }

    fun getDateCreated(): String? {
        return dateCreated
    }

    fun setDateCreated(dateCreated: String) {
        this.dateCreated = dateCreated
    }

    fun getType(): String? {
        return type
    }

    fun setType(type: String) {
        this.type = type
    }

}