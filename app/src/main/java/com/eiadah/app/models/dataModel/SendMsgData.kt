package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class SendMsgData {

    @SerializedName("sender")
    @Expose
    internal val sender: Int? = null
    @SerializedName("receiver")
    @Expose
    private val receiver: Int? = null
    @SerializedName("message")
    @Expose
    internal val message: String? = null
    @SerializedName("image")
    @Expose
    private val image: Any? = null
    @SerializedName("timestamp")
    @Expose
    private val timestamp: String? = null
    private var user_type: String? = null

    fun getUser_type(): String {
        return user_type!!
    }

    fun setUser_type(user_type: String) {
        this.user_type = user_type
    }
}