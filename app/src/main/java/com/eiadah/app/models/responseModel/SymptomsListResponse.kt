package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.SymptomsListData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class SymptomsListResponse {
    @SerializedName("status")
    @Expose
    internal var status: Boolean? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    @Expose
    internal var data: List<SymptomsListData>? = null

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData(): List<SymptomsListData>? {
        return data
    }

    fun setData(data: List<SymptomsListData>) {
        this.data = data
    }
}