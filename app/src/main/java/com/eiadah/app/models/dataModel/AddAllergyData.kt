package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class AddAllergyData {
    @SerializedName("title")
    @Expose
     var title: String? = null
    @SerializedName("description")
    @Expose
     var description: String? = null


    override fun toString(): String {
        return title.toString()
    }
}