package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.WithdrawRequestData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class WithdrawRequestResponse {
    @SerializedName("status")
    @Expose
    internal var status: Boolean? = null
    @SerializedName("user")
    @Expose
    internal var user: WithdrawRequestData? = null
    @SerializedName("message")
    @Expose
    internal var message: String? = null

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getUser(): WithdrawRequestData? {
        return user
    }

    fun setUser(user: WithdrawRequestData) {
        this.user = user
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }
}