package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.CountryData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class CountryResponse {

    @SerializedName("status")
    @Expose
    var status: Boolean? = null
    @SerializedName("country_list")
    @Expose
    var countryList: List<CountryData>? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
}