package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class AddPrescriptionData {
    @SerializedName("user_id")
    @Expose
    private val userId: String? = null
    @SerializedName("name")
    @Expose
    private val name: String? = null

}