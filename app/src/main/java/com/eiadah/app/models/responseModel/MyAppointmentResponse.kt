package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.MyAppointmentData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class MyAppointmentResponse {
    @SerializedName("status")
    @Expose
    internal var status: Boolean? = null
    @SerializedName("user")
    @Expose
    internal var user: List<MyAppointmentData>? = null
    @SerializedName("message")
    @Expose
    internal var message: String? = null

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getUser(): List<MyAppointmentData>? {
        return user
    }

    fun setUser(user: List<MyAppointmentData>) {
        this.user = user
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

}