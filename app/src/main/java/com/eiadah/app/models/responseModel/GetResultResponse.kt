package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.GetResultData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class GetResultResponse {
    @SerializedName("status")
    @Expose
    internal var status: Boolean? = null
    @SerializedName("result111")
    @Expose
    internal var result111: List<GetResultData>? = null

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getResult111(): List<GetResultData>? {
        return result111
    }

    fun setResult111(result111: List<GetResultData>) {
        this.result111 = result111
    }
}