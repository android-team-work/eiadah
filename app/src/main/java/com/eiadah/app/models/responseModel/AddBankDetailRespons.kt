package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.AddBankDetailData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class AddBankDetailRespons {
    @SerializedName("status")
    @Expose
    internal var status: Boolean? = null
    @SerializedName("user")
    @Expose
    private var user: AddBankDetailData? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getUser(): AddBankDetailData? {
        return user
    }

    fun setUser(user: AddBankDetailData) {
        this.user = user
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

}