package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.SaveCheckupData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class SaveCheckupResponse {

    @SerializedName("status")
    @Expose
    var status: Boolean? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("user")
    @Expose
    var data: SaveCheckupData? = null
}