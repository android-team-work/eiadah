package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.SubSpecialityData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class SubSpecilityResponse {
    @SerializedName("status")
    @Expose
    internal var status: Boolean? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    @Expose
    internal var data: List<SubSpecialityData>? = null

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData(): List<SubSpecialityData>? {
        return data
    }

    fun setData(data: List<SubSpecialityData>) {
        this.data = data
    }

}