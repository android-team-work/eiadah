package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserData {

    @SerializedName("first_name")
    @Expose
    var name: String? = null
    @SerializedName("phone")
    @Expose
    var phone: String? = null
    @SerializedName("gender")
    @Expose
    var gender: String? = null
    @SerializedName("dob")
    @Expose
    var dob: String? = null
    @SerializedName("location")
    @Expose
    var location: String? = null
    @SerializedName("image")
    @Expose
    var image: String? = null
    @SerializedName("bloodgroup")
    @Expose
    var bloodgroup: String? = null
    @SerializedName("height")
    @Expose
    var height: String? = null
    @SerializedName("weight")
    @Expose
    var weight: String? = null
    @SerializedName("usertype")
    @Expose
    var usertype: String? = null
    @SerializedName("socialtype")
    @Expose
    var socialtype: String? = null
    @SerializedName("email")
    @Expose
    var emailId: String? = null
    @SerializedName("user_id")
    @Expose
    var userId: String? = null
    @SerializedName("username")
    @Expose
    var username: String? = null
    @SerializedName("medications")
    @Expose
    var medications: String? = null
    @SerializedName("allergies")
    @Expose
    var allergies: String? = null
    @SerializedName("surgeries")
    @Expose
    var surgeries: String? = null
    @SerializedName("inguries")
    @Expose
    var inguries: String? = null

}
