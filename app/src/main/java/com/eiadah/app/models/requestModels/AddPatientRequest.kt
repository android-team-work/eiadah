package com.eiadah.app.models.requestModels

class AddPatientRequest {
    var name = ""
    var mobile = ""
    var city = ""
    var specilaity = ""
    var gender = ""
    var locality = ""
    var address = ""
    var pincode = ""
    var dob=""
    var email=""
    var age=""
    var id=""
    var text=""
}