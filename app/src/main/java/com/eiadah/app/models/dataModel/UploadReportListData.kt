package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class UploadReportListData {
    @SerializedName("title")
    @Expose
     val title: String? = null
    @SerializedName("file")
    @Expose
     val file: String? = null
    @SerializedName("id")
    @Expose
     val id: String? = null
}