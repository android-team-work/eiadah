package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class MyCreditData {
    @SerializedName("id")
    @Expose
     val id: Int? = null
    @SerializedName("amount")
    @Expose
     val amount: String? = null
    @SerializedName("date")
    @Expose
     val date: String? = null
    @SerializedName("user_id")
    @Expose
     val userId: Int? = null
}