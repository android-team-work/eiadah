package com.eiadah.app.models.responseModel

import com.eiadah.app.models.dataModel.AddPracticeData
import com.eiadah.app.utils.GlobalVariables.User
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class AddPracticeResponse {

    @SerializedName("status")
    @Expose
    internal var status: Boolean? = null
    @SerializedName("user")
    @Expose
    private var user: AddPracticeData? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null

    fun getStatus(): Boolean? {
        return status
    }

    fun setStatus(status: Boolean?) {
        this.status = status
    }

    fun getUser(): AddPracticeData? {
        return user
    }

    fun setUser(user: AddPracticeData) {
        this.user = user
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }


}