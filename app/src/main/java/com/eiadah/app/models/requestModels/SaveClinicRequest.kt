package com.eiadah.app.models.requestModels

import com.eiadah.app.models.dataModel.ClinicTimeData

class SaveClinicRequest {

    var clinic_name = ""
    var clinic_location = ""
//    var timing : ArrayList<ClinicTimeData> = ArrayList()
    var timing =""
    var time =""
}