package com.eiadah.app.models.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class AddBankDetailData {
    @SerializedName("id")
    @Expose
     val id: Int? = null
    @SerializedName("name")
    @Expose
     val name: String? = null
    @SerializedName("account_number")
    @Expose
     val accountNumber: String? = null
    @SerializedName("bank_name")
    @Expose
     val bankName: String? = null
    @SerializedName("ifsc_code")
    @Expose
     val ifscCode: String? = null
    @SerializedName("date")
    @Expose
     val date: String? = null
    @SerializedName("user_id")
    @Expose
     val userId: Int? = null

}